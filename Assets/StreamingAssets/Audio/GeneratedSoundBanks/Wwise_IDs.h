/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PALY_PLAYGROUND_SCOOP_DROP = 2134005970U;
        static const AkUniqueID PLAY_AMB_BACKSTREET = 3063999513U;
        static const AkUniqueID PLAY_AMB_BACKYARD = 2606489532U;
        static const AkUniqueID PLAY_AMB_BASEMENT = 896596010U;
        static const AkUniqueID PLAY_AMB_CAT = 1467608791U;
        static const AkUniqueID PLAY_AMB_CHILDHALL = 638319360U;
        static const AkUniqueID PLAY_AMB_CORRIDOR = 2021608263U;
        static const AkUniqueID PLAY_AMB_CRY_BABY = 3063154086U;
        static const AkUniqueID PLAY_AMB_CRY_BABY_SFX = 1014281318U;
        static const AkUniqueID PLAY_AMB_DOG_BARK = 1765546338U;
        static const AkUniqueID PLAY_AMB_FOOTSTEP_WOMAN = 1564781196U;
        static const AkUniqueID PLAY_AMB_GAZ452_PULLAWAY = 1174105686U;
        static const AkUniqueID PLAY_AMB_GAZ3307_PULLAWAY = 3215255436U;
        static const AkUniqueID PLAY_AMB_GRANNY = 1207494464U;
        static const AkUniqueID PLAY_AMB_MAIN_MENU = 2080667080U;
        static const AkUniqueID PLAY_AMB_POGONYA_OT_MONSTRA = 376359323U;
        static const AkUniqueID PLAY_AMB_POGONYA_OT_MONSTRA_CENTER = 2537011221U;
        static const AkUniqueID PLAY_AMB_REFRIGERATOR = 3343618607U;
        static const AkUniqueID PLAY_AMB_SECURITY = 1543321495U;
        static const AkUniqueID PLAY_AMB_STREETLAMP = 3038560738U;
        static const AkUniqueID PLAY_AMB_SWING = 1508043625U;
        static const AkUniqueID PLAY_AMBIENT_STREET = 3630988464U;
        static const AkUniqueID PLAY_BACKSTREET_BOTTLE = 536345861U;
        static const AkUniqueID PLAY_BACKSTREET_ESCAPE = 4134789148U;
        static const AkUniqueID PLAY_BACKSTREET_FENCE = 275585676U;
        static const AkUniqueID PLAY_BACKSTREET_TRUCK = 2326450162U;
        static const AkUniqueID PLAY_BASEMENT_BAG_DROP = 3994142006U;
        static const AkUniqueID PLAY_BASEMENT_CAT = 2081496252U;
        static const AkUniqueID PLAY_BASEMENT_CAT_SCENE = 1967204101U;
        static const AkUniqueID PLAY_BASEMENT_EXIT_REACTION = 827064006U;
        static const AkUniqueID PLAY_BASEMENT_FALL = 1021754995U;
        static const AkUniqueID PLAY_BASEMENT_HANDS_GRAB = 1229033643U;
        static const AkUniqueID PLAY_BASEMENT_HANDS_LOOP = 307237219U;
        static const AkUniqueID PLAY_BASEMENT_HANDS_OUT = 2811270943U;
        static const AkUniqueID PLAY_BASEMENT_POINTER_DROP = 2968475845U;
        static const AkUniqueID PLAY_BASEMENT_POINTER_ON = 1629625153U;
        static const AkUniqueID PLAY_BASEMENT_POINTER_TAKE = 3746763371U;
        static const AkUniqueID PLAY_BASEMENT_STAND_UP = 1692661704U;
        static const AkUniqueID PLAY_BEDROOM_KNOCK_GLASS = 2225847978U;
        static const AkUniqueID PLAY_BULB = 1406278311U;
        static const AkUniqueID PLAY_CAR_DOOR_CLOSE = 1738890490U;
        static const AkUniqueID PLAY_CAR_DOOR_OPEN = 454444414U;
        static const AkUniqueID PLAY_CHAIR_HIT_V2 = 1935103010U;
        static const AkUniqueID PLAY_CHECKPOINT_DOOR_OPEN = 3906424190U;
        static const AkUniqueID PLAY_CHECKPOINT_SECURITY_CHAIR = 924628379U;
        static const AkUniqueID PLAY_CHECKPOINT_SECURITY_IDLE_COUGH = 3617582813U;
        static const AkUniqueID PLAY_CHECKPOINT_SECURITY_IDLE_SMOKES = 3941961359U;
        static const AkUniqueID PLAY_CHECKPOINT_SECURITY_STANDUP_VO = 1164273195U;
        static const AkUniqueID PLAY_CHECKPOINT_STOP_VO = 1152693463U;
        static const AkUniqueID PLAY_CHOPPING_BONE_ACTION = 2279672308U;
        static const AkUniqueID PLAY_CHOPPING_BONE_CERAMIC = 1894549582U;
        static const AkUniqueID PLAY_CHOPPING_BONE_GET = 2973902182U;
        static const AkUniqueID PLAY_CHOPPING_BONE_GET_GUTS = 788289090U;
        static const AkUniqueID PLAY_CHOPPING_BONE_GUTS = 1900539761U;
        static const AkUniqueID PLAY_CHOPPING_BONE_METAL = 3351133223U;
        static const AkUniqueID PLAY_CHOPPING_BONE_PLASTIC = 4182005566U;
        static const AkUniqueID PLAY_CHOPPING_BONE_PUT_CERAMIC = 2791529440U;
        static const AkUniqueID PLAY_CHOPPING_BONE_PUT_GUTS = 2907293331U;
        static const AkUniqueID PLAY_CHOPPING_BONE_PUT_METAL = 679635133U;
        static const AkUniqueID PLAY_CHOPPING_BOX = 1382816424U;
        static const AkUniqueID PLAY_CHOPPING_BOX_HIT = 3619930702U;
        static const AkUniqueID PLAY_CHOPPING_GRID_BREAK = 3269452153U;
        static const AkUniqueID PLAY_CHOPPING_GRINDER_WORK = 3714514396U;
        static const AkUniqueID PLAY_CHOPPING_PIG = 1419225843U;
        static const AkUniqueID PLAY_CHOPPING_STAIRS_IN = 4041908803U;
        static const AkUniqueID PLAY_CHOPPING_STAIRS_OUT = 2639170622U;
        static const AkUniqueID PLAY_CHOPPING_STAIRS_STEPS = 265866999U;
        static const AkUniqueID PLAY_CLICK = 311910498U;
        static const AkUniqueID PLAY_CORRIDOR_HEARTBEAT = 299121437U;
        static const AkUniqueID PLAY_DOOR_CLOSE_GUARD_ROOM = 2022437721U;
        static const AkUniqueID PLAY_FEMALE_STEPS_HEEL = 3108062259U;
        static const AkUniqueID PLAY_FOLEY_SEC_CHAIR_DOWN = 2229926534U;
        static const AkUniqueID PLAY_FOLEY_SEC_CHAIR_MOVE = 2310188845U;
        static const AkUniqueID PLAY_FOLEY_SEC_CHAIR_UP = 3685972909U;
        static const AkUniqueID PLAY_FOLEY_SECURITY_STEPS = 1452344984U;
        static const AkUniqueID PLAY_GAME_ROOM_CLOCK = 1413142245U;
        static const AkUniqueID PLAY_GAME_ROOM_DUCK = 3281634778U;
        static const AkUniqueID PLAY_GAME_ROOM_HORSE = 2893112652U;
        static const AkUniqueID PLAY_GAME_ROOM_NANNY_SITDOWN = 2613909726U;
        static const AkUniqueID PLAY_GAME_ROOM_NANNY_STANDUP_GRUMBLE_SFX = 4057751920U;
        static const AkUniqueID PLAY_GAME_ROOM_NANNY_STANDUP_GRUMBLE_VO = 3356813298U;
        static const AkUniqueID PLAY_GAME_ROOM_NANNY_STANDUP_SFX = 2250502253U;
        static const AkUniqueID PLAY_GAME_ROOM_NANNY_STANDUP_VO = 1817441765U;
        static const AkUniqueID PLAY_GAME_ROOM_NANNY_STEPS = 3524390135U;
        static const AkUniqueID PLAY_GAME_ROOM_NANNY_STEPS_VO = 3005305911U;
        static const AkUniqueID PLAY_GAME_ROOM_NANNY_STOP_VO = 3620745040U;
        static const AkUniqueID PLAY_GAME_ROOM_NOISE = 3679631501U;
        static const AkUniqueID PLAY_GAME_ROOM_PYRAMID = 4076029807U;
        static const AkUniqueID PLAY_GAME_ROOM_TOY = 1834137327U;
        static const AkUniqueID PLAY_GAME_ROOM_TV_OFF = 2735855485U;
        static const AkUniqueID PLAY_GAME_ROOM_TV_ON = 610560337U;
        static const AkUniqueID PLAY_GAME_ROOM_TV_ON_WITHOUT_REMOTE = 2203697449U;
        static const AkUniqueID PLAY_GAME_ROOM_TV_WORK = 1382448533U;
        static const AkUniqueID PLAY_GAME_ROOM_WOOD_TRAIN = 2959133501U;
        static const AkUniqueID PLAY_GAME_ROOM_YULE = 1763375706U;
        static const AkUniqueID PLAY_GASTRONOME_BANKS = 3786183353U;
        static const AkUniqueID PLAY_GASTRONOME_CONCRETE_SLIDE = 2508151631U;
        static const AkUniqueID PLAY_GASTRONOME_CUT_SCENE = 1254241357U;
        static const AkUniqueID PLAY_GASTRONOME_GRAB = 2289805842U;
        static const AkUniqueID PLAY_GASTRONOME_MONSTER_AXE = 240777763U;
        static const AkUniqueID PLAY_GASTRONOME_MONSTER_BOTTLES = 3722774672U;
        static const AkUniqueID PLAY_GASTRONOME_MONSTER_BOXES = 103280332U;
        static const AkUniqueID PLAY_GASTRONOME_MONSTER_MEAT = 2808755628U;
        static const AkUniqueID PLAY_GASTRONOME_MONSTER_VO = 2191327076U;
        static const AkUniqueID PLAY_GASTRONOME_MONSTER_WEIGHER = 4061320362U;
        static const AkUniqueID PLAY_GASTRONOME_MONSTER_WEIGHER_HARD = 4021634322U;
        static const AkUniqueID PLAY_GASTRONOME_PROPS = 3371022414U;
        static const AkUniqueID PLAY_GASTRONOME_ROPE = 2597875840U;
        static const AkUniqueID PLAY_GASTRONOME_STEPS_VEG_FALL = 3445045622U;
        static const AkUniqueID PLAY_GASTRONOME_WINDOW = 310511272U;
        static const AkUniqueID PLAY_HANDS_LOOP_REFRIGERATOR = 3233704614U;
        static const AkUniqueID PLAY_HOMING = 3989298510U;
        static const AkUniqueID PLAY_PANTRY_BOX = 1728716780U;
        static const AkUniqueID PLAY_PANTRY_DOLL = 3192791034U;
        static const AkUniqueID PLAY_PANTRY_NAILS_DROP = 1243961392U;
        static const AkUniqueID PLAY_PANTRY_NAILS_LAND = 3756995976U;
        static const AkUniqueID PLAY_PLAYER_CLIMB = 2317657839U;
        static const AkUniqueID PLAY_PLAYER_FOOTSTEP = 1724675634U;
        static const AkUniqueID PLAY_PLAYER_LAND = 4249207015U;
        static const AkUniqueID PLAY_PLAYER_STEP_METAL_GRID_HIT = 1866715211U;
        static const AkUniqueID PLAY_PLAYGROUND_BALL_HIT = 1052983729U;
        static const AkUniqueID PLAY_PLAYGROUND_SCALES_MOVE = 2544215095U;
        static const AkUniqueID PLAY_PLAYGROUND_SCOOP_DIG = 373925961U;
        static const AkUniqueID PLAY_PLAYGROUND_SCOOP_GET = 4065046125U;
        static const AkUniqueID PLAY_PLAYGROUND_SWING_IDLES = 2352086942U;
        static const AkUniqueID PLAY_REFRIGERATOR_BODY = 491906535U;
        static const AkUniqueID PLAY_REFRIGERATOR_BUTTON = 2056727611U;
        static const AkUniqueID PLAY_REFRIGERATOR_COCKROACH = 948084738U;
        static const AkUniqueID PLAY_REFRIGERATOR_CONVEYOR_FAIL = 1007176363U;
        static const AkUniqueID PLAY_REFRIGERATOR_CONVEYOR_START = 101098985U;
        static const AkUniqueID PLAY_REFRIGERATOR_CONVEYOR_WORK = 2589873164U;
        static const AkUniqueID PLAY_REFRIGERATOR_DOOR = 2253696419U;
        static const AkUniqueID PLAY_REFRIGERATOR_HOOK_DROP = 2764810914U;
        static const AkUniqueID PLAY_REFRIGERATOR_HOOK_HIT = 2409485792U;
        static const AkUniqueID PLAY_REFRIGERATOR_HOOK_LAND = 1560647398U;
        static const AkUniqueID PLAY_REFRIGERATOR_HOOK_TAKE = 4084116100U;
        static const AkUniqueID PLAY_REFRIGERATOR_LAMP = 4017185737U;
        static const AkUniqueID PLAY_SCOOP_METAL_HIT = 3015138564U;
        static const AkUniqueID PLAY_SECURITY_HEAD = 1770202369U;
        static const AkUniqueID PLAY_SFX_CART = 3982920712U;
        static const AkUniqueID PLAY_SFX_CLOCK = 1334253732U;
        static const AkUniqueID PLAY_SFX_CURTAINS = 3662092463U;
        static const AkUniqueID PLAY_SFX_CURTAINS_BEDROOM = 635809494U;
        static const AkUniqueID PLAY_SFX_DOOR_METAL1_CLOSE = 2443797598U;
        static const AkUniqueID PLAY_SFX_DOOR_METAL1_OPEN = 2239885562U;
        static const AkUniqueID PLAY_SFX_DOOR_METAL2_CLOSE = 1750416541U;
        static const AkUniqueID PLAY_SFX_DOOR_METAL2_OPEN = 1947009975U;
        static const AkUniqueID PLAY_SFX_DOOR_METAL3_CLOSE = 1964631308U;
        static const AkUniqueID PLAY_SFX_DOOR_METAL3_OPEN = 2246830232U;
        static const AkUniqueID PLAY_SFX_DOOR_WOOD_CLOSE = 1365536687U;
        static const AkUniqueID PLAY_SFX_DOOR_WOOD_OPEN = 3244725865U;
        static const AkUniqueID PLAY_SFX_GLASS_HIT = 2208779140U;
        static const AkUniqueID PLAY_SFX_MONSTER_PIGMALE = 1827226138U;
        static const AkUniqueID PLAY_SFX_RAIN = 3286075488U;
        static const AkUniqueID PLAY_SFX_RATS = 2967300694U;
        static const AkUniqueID PLAY_SFX_THUNDER = 1542278216U;
        static const AkUniqueID PLAY_SFX_TV_ONLINE = 4158767618U;
        static const AkUniqueID PLAY_SFX_TV_SWITCH = 1840366487U;
        static const AkUniqueID PLAY_SFX_WATER_BOX = 3289425441U;
        static const AkUniqueID PLAY_SFX_WATER_STREAM = 3985813554U;
        static const AkUniqueID PLAY_SFX_WIND = 3330427648U;
        static const AkUniqueID PLAY_SHADOWS_MONSTER = 965041074U;
        static const AkUniqueID PLAY_SHOP_BACKYARD_SCENE = 1521676417U;
        static const AkUniqueID PLAY_SWEEP_NAILS = 545575048U;
        static const AkUniqueID PLAY_TRUCK_CLIMB = 4042356369U;
        static const AkUniqueID PLAY_TRUCK_MOVE = 3393473509U;
        static const AkUniqueID PLAY_UAZ_AWAY = 545598759U;
        static const AkUniqueID PLAY_YARD_BUSH = 3564287711U;
        static const AkUniqueID PLAY_YARD_FIREFLIES = 2363710316U;
        static const AkUniqueID PLAY_YARD_TRUCK_WORK = 1569104630U;
        static const AkUniqueID STOP_AMB_BACKSTREET = 3777605411U;
        static const AkUniqueID STOP_AMB_BACKYARD = 788754522U;
        static const AkUniqueID STOP_AMB_BASEMENT = 3538453964U;
        static const AkUniqueID STOP_AMB_CHILDHALL = 2605934070U;
        static const AkUniqueID STOP_AMB_CORRIDOR = 140710341U;
        static const AkUniqueID STOP_AMB_CRY_BABY = 2794682120U;
        static const AkUniqueID STOP_AMB_CRY_BABY_SFX = 61618196U;
        static const AkUniqueID STOP_AMB_DOG_BARK = 1847142832U;
        static const AkUniqueID STOP_AMB_GRANNY = 3019802430U;
        static const AkUniqueID STOP_AMB_MAIN_MENU = 1167353446U;
        static const AkUniqueID STOP_AMB_POGONYA_OT_MONSTRA = 3440484669U;
        static const AkUniqueID STOP_AMB_REFRIGERATOR = 2172350513U;
        static const AkUniqueID STOP_AMB_SECURITY = 1220162913U;
        static const AkUniqueID STOP_AMB_STREETLAMP = 805908352U;
        static const AkUniqueID STOP_AMB_SWING = 3848103403U;
        static const AkUniqueID STOP_AMBIENT_STREET = 2583237610U;
        static const AkUniqueID STOP_BASEMENT_CAT = 4016521158U;
        static const AkUniqueID STOP_BASEMENT_HANDS_LOOP = 2677056417U;
        static const AkUniqueID STOP_CHECKPOINT_SECURITY_IDLE_SMOKES = 3174799661U;
        static const AkUniqueID STOP_CHOPPING_BOX = 2198155890U;
        static const AkUniqueID STOP_CHOPPING_GRINDER_WORK = 777187526U;
        static const AkUniqueID STOP_CLICK = 3995118272U;
        static const AkUniqueID STOP_CORRIDOR_HEARTBEAT = 3362331119U;
        static const AkUniqueID STOP_GAME_ROOM_CLOCK = 3765196655U;
        static const AkUniqueID STOP_GAME_ROOM_NOISE = 846599083U;
        static const AkUniqueID STOP_GAME_ROOM_TV_ON_WITHOUT_REMOTE = 409383287U;
        static const AkUniqueID STOP_GAME_ROOM_TV_WORK = 3094117967U;
        static const AkUniqueID STOP_HANDS_LOOP_REFRIGERATOR = 2811722100U;
        static const AkUniqueID STOP_HOMING = 2370142280U;
        static const AkUniqueID STOP_PLAYGROUND_SCALES_MOVE = 2965629745U;
        static const AkUniqueID STOP_PLAYGROUND_SCOOP_DIG = 1492649371U;
        static const AkUniqueID STOP_PLAYGROUND_SWING_IDLES = 256058784U;
        static const AkUniqueID STOP_REFRIGERATOR_COCKROACH = 3609163084U;
        static const AkUniqueID STOP_REFRIGERATOR_CONVEYOR_WORK = 498663398U;
        static const AkUniqueID STOP_REFRIGERATOR_LAMP = 2377583867U;
        static const AkUniqueID STOP_SFX_CART = 3805075634U;
        static const AkUniqueID STOP_SFX_CLOCK = 1565900202U;
        static const AkUniqueID STOP_SFX_CURTAINS = 2616006433U;
        static const AkUniqueID STOP_SFX_CURTAINS_BEDROOM = 1930028380U;
        static const AkUniqueID STOP_SFX_RAIN = 1656208626U;
        static const AkUniqueID STOP_SFX_TV_ONLINE = 1595367636U;
        static const AkUniqueID STOP_SFX_WATER_STREAM = 3461046120U;
        static const AkUniqueID STOP_SFX_WIND = 1623107714U;
        static const AkUniqueID STOP_SHADOWS_MONSTER = 1250516584U;
        static const AkUniqueID STOP_YARD_BUSH = 2551213633U;
        static const AkUniqueID STOP_YARD_FIREFLIES = 1918662866U;
        static const AkUniqueID STOP_YARD_TRUCK_WORK = 2201060256U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace PLAYER_FOOTSTEP_MATERIAL_TYPE
        {
            static const AkUniqueID GROUP = 310668400U;

            namespace SWITCH
            {
                static const AkUniqueID ASPHALT = 4169408098U;
                static const AkUniqueID BAG = 513390133U;
                static const AkUniqueID BED_MATRASS = 402416088U;
                static const AkUniqueID BED_SPRING = 139925190U;
                static const AkUniqueID BODY = 1845389165U;
                static const AkUniqueID CARPET = 2412606308U;
                static const AkUniqueID CONCRETE = 841620460U;
                static const AkUniqueID GARBAGE = 2092055118U;
                static const AkUniqueID GLASS = 2449969375U;
                static const AkUniqueID GROUND = 2528658256U;
                static const AkUniqueID GUTS = 856732378U;
                static const AkUniqueID ICE = 344481046U;
                static const AkUniqueID LEAVES = 582824249U;
                static const AkUniqueID METAL = 2473969246U;
                static const AkUniqueID PAPER = 3126687001U;
                static const AkUniqueID PLASTIC_BOX = 3867877853U;
                static const AkUniqueID SAND = 803837735U;
                static const AkUniqueID TILE = 2637588553U;
                static const AkUniqueID TIRE = 3006696107U;
                static const AkUniqueID VEGITATION = 502864313U;
                static const AkUniqueID VENTILATION = 3172605362U;
                static const AkUniqueID WOOD_BOX = 515397756U;
                static const AkUniqueID WOOD_FLOOR = 434567717U;
                static const AkUniqueID WOOD_STAIRS = 1994379643U;
            } // namespace SWITCH
        } // namespace PLAYER_FOOTSTEP_MATERIAL_TYPE

        namespace PLAYER_LANDS
        {
            static const AkUniqueID GROUP = 566112541U;

            namespace SWITCH
            {
                static const AkUniqueID BED_MATRASS = 402416088U;
                static const AkUniqueID BED_SPRING = 139925190U;
                static const AkUniqueID CARPET = 2412606308U;
                static const AkUniqueID GLASS = 2449969375U;
                static const AkUniqueID PAPER = 3126687001U;
                static const AkUniqueID SAND = 803837735U;
                static const AkUniqueID TIRE = 3006696107U;
                static const AkUniqueID VENTILATION = 3172605362U;
                static const AkUniqueID WOOD_BOX = 515397756U;
                static const AkUniqueID WOOD_FLOOR = 434567717U;
            } // namespace SWITCH
        } // namespace PLAYER_LANDS

        namespace PLAYER_LOCOMOTION_TYPE
        {
            static const AkUniqueID GROUP = 2023837685U;

            namespace SWITCH
            {
                static const AkUniqueID RUNNING = 3863236874U;
                static const AkUniqueID SNEAKING = 3418139957U;
                static const AkUniqueID SPRINTING = 3691594375U;
            } // namespace SWITCH
        } // namespace PLAYER_LOCOMOTION_TYPE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID AMBIENTVOLUME = 3546521921U;
        static const AkUniqueID MASTERVOLUME = 2918011349U;
        static const AkUniqueID SFXVOLUME = 988953028U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID AMBIENT = 77978275U;
        static const AkUniqueID PLAYER_SFX = 817096458U;
        static const AkUniqueID SFX = 393239870U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENT = 77978275U;
        static const AkUniqueID ENVIRONMENTAL = 1973600711U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
