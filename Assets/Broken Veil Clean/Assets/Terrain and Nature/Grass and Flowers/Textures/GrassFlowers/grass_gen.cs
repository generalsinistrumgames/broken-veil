﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grass_gen : MonoBehaviour
{
    [Range(1,500)]
    public int step = 10;
    public int max_count = 1000;
    public float progress = 0f;
    public GameObject[] prefabs;
    public GameObject parent_object;

    public int current_step = 0;
    BoxCollider collider;

    //spawn data
    float spawn_x1, spawn_dx;
    float spawn_z1, spawn_dz;
    float spawn_y1, spawn_dy;

    public float density = 0f;

    // Start is called before the first frame update
    void Start()
    {
        collider = this.GetComponent<BoxCollider>();
        calc_spawn_data();
        parent_object.transform.position = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        progress = 100f * (float)current_step / (float)max_count;
        if (current_step>=max_count)
            return;
        calc_spawn_data();
        for (int i=0;i<step;i++)
        {
            if (current_step >= max_count)
                return;
            create_instance();
        }
    }
    void calc_spawn_data()
    {
        
        Vector3 center = collider.center + transform.position;

        float d = collider.size.x;
        if (d < 0)
            d *= -1f;
        spawn_x1 = center.x - d/2f;
        spawn_dx = d;

        d = collider.size.z;
        if (d < 0)
            d *= -1f;
        spawn_z1 = center.z - d/2f;
        spawn_dz = d;

        d = collider.size.y;
        if (d < 0)
            d *= -1f;
        spawn_y1 = center.y - d/2f;
        spawn_dy = d;

        density = max_count / (spawn_dx * spawn_dy);
    }

    void create_instance()
    {
        RaycastHit hit = new RaycastHit();
        const int layermask = 2147475451;
        Vector3 direction = Vector3.down;
        Vector3 origin = new Vector3
            (spawn_x1+Random.value*spawn_dx,
            spawn_y1+spawn_dy,
            spawn_z1 + Random.value * spawn_dz);
        Vector3 offset = new Vector3
            (0f,
            0f,
            0f);
        if (Physics.Raycast(new Ray(origin, direction), out hit, spawn_dy, layermask))
        {
            Debug.DrawRay(origin, direction * hit.distance, Color.blue);
            GameObject.Instantiate(prefabs[Random.Range(0,prefabs.Length)], hit.point+offset , Quaternion.Euler(0f, 0f, 0f)*Quaternion.Euler(0f, 360f * Random.value, 0), parent_object.transform);
            current_step++;
        }
        else
        {
            Debug.DrawRay(origin, direction * spawn_dy, Color.red);
        }
    }
}
