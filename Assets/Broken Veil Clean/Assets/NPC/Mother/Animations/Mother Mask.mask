%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Mother Mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Geometry
    m_Weight: 1
  - m_Path: Geometry/Dress
    m_Weight: 1
  - m_Path: Geometry/Hair
    m_Weight: 1
  - m_Path: Geometry/M_Mama_Body
    m_Weight: 1
  - m_Path: Geometry/shoes
    m_Weight: 1
  - m_Path: Main
    m_Weight: 1
  - m_Path: Main/DeformationSystem
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Bk1_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Bk1_M/Dr_Bk2_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Bk1_M/Dr_Bk2_M/Dr_Bk3_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Bk1_M/Dr_Bk2_M/Dr_Bk3_M/Dr_Bk4_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Bk1_M/Dr_Bk2_M/Dr_Bk3_M/Dr_Bk4_M/Dr_Bk5_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Fr01_1_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Fr01_1_M/Dr_Fr01_2_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Fr01_1_M/Dr_Fr01_2_M/Dr_Fr01_3_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Fr01_1_M/Dr_Fr01_2_M/Dr_Fr01_3_M/Dr_Fr01_4_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Fr01_1_M/Dr_Fr01_2_M/Dr_Fr01_3_M/Dr_Fr01_4_M/Dr_Fr01_5_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Leg1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Leg1_L/Dr_Leg2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Leg1_L/Dr_Leg2_L/Dr_Leg3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Leg1_L/Dr_Leg2_L/Dr_Leg3_L/Dr_Leg4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Leg1_L/Dr_Leg2_L/Dr_Leg3_L/Dr_Leg4_L/Dr_Leg5_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Leg1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Leg1_R/Dr_Leg2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Leg1_R/Dr_Leg2_R/Dr_Leg3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Leg1_R/Dr_Leg2_R/Dr_Leg3_R/Dr_Leg4_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_Leg1_R/Dr_Leg2_R/Dr_Leg3_R/Dr_Leg4_R/Dr_Leg5_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_leg_bk1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_leg_bk1_L/Dr_leg_bk2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_leg_bk1_L/Dr_leg_bk2_L/Dr_leg_bk3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_leg_bk1_L/Dr_leg_bk2_L/Dr_leg_bk3_L/Dr_leg_bk4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_leg_bk1_L/Dr_leg_bk2_L/Dr_leg_bk3_L/Dr_leg_bk4_L/Dr_leg_bk5_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_leg_bk1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_leg_bk1_R/Dr_leg_bk2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_leg_bk1_R/Dr_leg_bk2_R/Dr_leg_bk3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_leg_bk1_R/Dr_leg_bk2_R/Dr_leg_bk3_R/Dr_leg_bk4_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_leg_bk1_R/Dr_leg_bk2_R/Dr_leg_bk3_R/Dr_leg_bk4_R/Dr_leg_bk5_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_side1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_side1_L/Dr_side2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_side1_L/Dr_side2_L/Dr_side3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_side1_L/Dr_side2_L/Dr_side3_L/Dr_side4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_side1_L/Dr_side2_L/Dr_side3_L/Dr_side4_L/Dr_side5_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_side1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_side1_R/Dr_side2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_side1_R/Dr_side2_R/Dr_side3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_side1_R/Dr_side2_R/Dr_side3_R/Dr_side4_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Dress_base_M/Dr_side1_R/Dr_side2_R/Dr_side3_R/Dr_side4_R/Dr_side5_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L/Ankle_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L/Ankle_L/Toes_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L/Ankle_L/Toes_L/ToesEnd_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R/Ankle_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R/Ankle_R/Toes_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R/Ankle_R/Toes_R/ToesEnd_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Bearst_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Bearst_L/Bearst1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Bearst_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Bearst_R/Bearst1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Eye_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Eye_L/EyeEnd_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Eye_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Eye_R/EyeEnd_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair1_L/Hair2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair1_L/Hair2_L/Hair3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair1_L/Hair2_L/Hair3_L/Hair4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair1_R/Hair2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair1_R/Hair2_R/Hair3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair1_R/Hair2_R/Hair3_R/Hair4_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair5_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair5_L/Hair22_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair5_L/Hair22_L/Hair32_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair5_L/Hair22_L/Hair32_L/Hair42_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair5_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair5_R/Hair22_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair5_R/Hair22_R/Hair32_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair5_R/Hair22_R/Hair32_R/Hair42_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair6_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair6_L/Hair21_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair6_L/Hair21_L/Hair31_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair6_L/Hair21_L/Hair31_L/Hair41_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair6_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair6_R/Hair21_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair6_R/Hair21_R/Hair31_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Hair_base/Hair6_R/Hair21_R/Hair31_R/Hair41_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Jaw_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Jaw_M/JawEnd_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L/IndexFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L/IndexFinger2_L/IndexFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L/IndexFinger2_L/IndexFinger3_L/IndexFinger4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L/MiddleFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L/MiddleFinger2_L/MiddleFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L/MiddleFinger2_L/MiddleFinger3_L/MiddleFinger4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L/PinkyFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L/PinkyFinger2_L/PinkyFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L/PinkyFinger2_L/PinkyFinger3_L/PinkyFinger4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/RingFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/RingFinger1_L/RingFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/RingFinger1_L/RingFinger2_L/RingFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/RingFinger1_L/RingFinger2_L/RingFinger3_L/RingFinger4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L/ThumbFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L/ThumbFinger2_L/ThumbFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L/ThumbFinger2_L/ThumbFinger3_L/ThumbFinger4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R/IndexFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R/IndexFinger2_R/IndexFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R/IndexFinger2_R/IndexFinger3_R/IndexFinger4_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R/MiddleFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R/MiddleFinger2_R/MiddleFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R/MiddleFinger2_R/MiddleFinger3_R/MiddleFinger4_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R/PinkyFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R/PinkyFinger2_R/PinkyFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R/PinkyFinger2_R/PinkyFinger3_R/PinkyFinger4_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/RingFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/RingFinger1_R/RingFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/RingFinger1_R/RingFinger2_R/RingFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/RingFinger1_R/RingFinger2_R/RingFinger3_R/RingFinger4_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R/ThumbFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R/ThumbFinger2_R/ThumbFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R/ThumbFinger2_R/ThumbFinger3_R/ThumbFinger4_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Zad_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Zad_L/Zad1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Zad_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Zad_R/Zad1_R
    m_Weight: 1
