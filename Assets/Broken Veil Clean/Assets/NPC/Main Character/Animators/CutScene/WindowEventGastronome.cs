using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using GlobalScripts;

public class WindowEventGastronome : MonoBehaviour
{
    public RuntimeAnimatorController animator;
    public RuntimeAnimatorController oldAnimator;
    Coroutine aim;
    [SerializeField] float aimTime;

    [SerializeField] PlayableDirector director;

    [SerializeField] GameObject root;
    [SerializeField] DeliTriggerWeight deliTrigger;
    [SerializeField] DeliWeight weight;
    IEnumerator Aim(PlayerController playerController,float time , Rigidbody rb, Collider other)
    {
        weight.StopCoroutine(weight.bubbleDelay);
        playerController.GetComponent<Player>().enabled = false;
        rb.isKinematic = true;
        other.enabled = false;
        playerController.enabled = false;
        
        var startPos = playerController.transform.position;
        var startRot = playerController.transform.rotation;
        float timer = 0;
        while (timer <= time)
        {
            playerController.transform.position = Vector3.SlerpUnclamped(startPos, root.transform.position + new Vector3(0, -0.65f, 0) , timer / time);
            playerController.transform.rotation = Quaternion.Lerp(startRot, Quaternion.LookRotation(Vector3.right), timer / time);
            timer += Time.deltaTime;
            yield return null;
        }

        director.gameObject.SetActive(true);
        playerController.transform.parent = root.transform;
        playerController.transform.localPosition = new Vector3(0.44f, -0.004f, 0);
        playerController.transform.localRotation = Quaternion.Euler(-89.473f, 90, 0);
        
        oldAnimator = other.GetComponent<Animator>().runtimeAnimatorController;
        other.GetComponent<Animator>().runtimeAnimatorController = animator;
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            player.GetComponent<PlayerHatControl>().HatPhysicsActive(false);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetMouseButton(0))
        {
            var playerControler = other.GetComponent<PlayerController>();
            var rb = other.GetComponent<Rigidbody>();
            if (rb.velocity.y < 0 && aim == null)
            {
                deliTrigger.stay = true;
                aim = StartCoroutine(Aim(playerControler, aimTime, rb, other));
            }
        }
    }
}
