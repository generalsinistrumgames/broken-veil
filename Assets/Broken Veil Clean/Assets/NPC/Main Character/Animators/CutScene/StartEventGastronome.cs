using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Animations.Rigging;
using GlobalScripts;

public class StartEventGastronome : MonoBehaviour
{
    public RuntimeAnimatorController animator;

    public FindPlayer findPlayer;

    [SerializeField] PlayableDirector director;

    [SerializeField] GameObject root;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            //if(slowMo == null)
            //    slowMo = StartCoroutine(SlowMotion());

            player.GetComponent<PlayerHatControl>().HatPhysicsActive(false);

            findPlayer.player = player.gameObject;
            foreach (var item in player.ears)
            {
                item.transform.parent = player.transform;
            } 
            player.GetComponent<PlayerController>().enabled = false;
            player.GetComponent<Player>().enabled = false;

            player.GetComponent<RigBuilder>().layers[0].active = false;

            player.GetComponent<Animator>().runtimeAnimatorController = animator;
            player.enabled = false;
            player.GetComponent<Rigidbody>().isKinematic = true;
            director.gameObject.SetActive(true);
            player.transform.parent = root.transform;
            player.transform.localRotation = Quaternion.Euler(-90, 90, 0);
            player.transform.localPosition = new Vector3(0.439f, 0.004f, 0.005f);
        }
    }
}
