using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;
using UnityEngine.Animations.Rigging;

public class GiveControl : MonoBehaviour
{
    GameObject boy;
    [SerializeField] FindPlayer findPlayer;

    private void Awake()
    {
        boy = findPlayer.player;
    }

    void Start()
    {
        boy.GetComponent<Player>().enabled = true;
        boy.GetComponent<PlayerController>().enabled = true;
        boy.GetComponent<Animator>().runtimeAnimatorController = boy.GetComponent<PlayerController>().animators[0].controller;
        boy.GetComponent<CapsuleCollider>().enabled = true;
        boy.GetComponent<Rigidbody>().isKinematic = false;
        boy.transform.parent = null;
        foreach (var item in boy.GetComponent<Player>().ears)
        {
            item.transform.parent = null;
        }
        boy.GetComponent<RigBuilder>().layers[0].active = true;
    }
}
