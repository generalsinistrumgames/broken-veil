using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Deli
{
    public class DeliTimeScaleChange : MonoBehaviour
    {
        [SerializeField] float time = 1;
        [SerializeField] List<ChangeMaterial> materials;
        [SerializeField] List<ChangeMaterial> standartMaterials;
        [SerializeField] float emissive = 0;


        private void Start()
        {
            foreach (var item in materials)
            {
                item.material.SetColor("Color_07a007baf4fb44ebaa215c339daef41e", item.startColor);
                item.material.SetFloat("Vector1_c42e44337aac47afa677b7e0740bf3c2", item.minEmissive);
            }

            foreach (var item in standartMaterials)
            {
                item.material.SetFloat("_EmissiveIntensity", item.minEmissive);
            }
        }

        void Update()
        {
            foreach (var item in materials)
            {
                item.material.SetColor("Color_07a007baf4fb44ebaa215c339daef41e", Color.Lerp(item.startColor, Color.white, emissive));
                item.material.SetFloat("Vector1_c42e44337aac47afa677b7e0740bf3c2", Mathf.Lerp(item.minEmissive, item.maxEmissive, emissive));
            }

            foreach (var item in standartMaterials)
            {
                item.material.SetFloat("_EmissiveIntensity", Mathf.Lerp(item.minEmissive, item.maxEmissive, emissive));
            }

            Time.timeScale = time;
        }
    }

    [Serializable]
    public struct ChangeMaterial
    {
        public Material material;
        public Color startColor;
        public float maxEmissive;
        public float minEmissive;
    }
}
