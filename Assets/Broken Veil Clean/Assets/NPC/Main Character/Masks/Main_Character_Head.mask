%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Main_Character_Head
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Geometry
    m_Weight: 0
  - m_Path: Geometry/Body2
    m_Weight: 0
  - m_Path: Geometry/Hair1
    m_Weight: 0
  - m_Path: Geometry/shirt
    m_Weight: 0
  - m_Path: Main
    m_Weight: 1
  - m_Path: Main/DeformationSystem
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L/Ankle_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L/Ankle_L/Toes_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L/Ankle_L/Toes_L/ToesEnd_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R/Ankle_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R/Ankle_R/Toes_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R/Ankle_R/Toes_R/ToesEnd_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Eye_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Eye_L/EyeEnd_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Eye_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Eye_R/EyeEnd_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint27
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint27/joint70
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint27/joint70/joint71
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint27/joint70/joint71/joint72
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint27/joint70/joint71/joint72/joint30
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint32
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint32/joint94
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint32/joint94/joint95
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint32/joint94/joint95/joint96
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint32/joint94/joint95/joint96/joint97
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint32/joint94/joint95/joint96/joint97/joint98
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint32/joint94/joint95/joint96/joint97/joint98/joint36
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint39
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint39/joint89
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint39/joint89/joint90
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint39/joint89/joint90/joint91
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint39/joint89/joint90/joint91/joint92
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint39/joint89/joint90/joint91/joint92/joint93
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint39/joint89/joint90/joint91/joint92/joint93/joint43
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint44
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint44/joint99
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint44/joint99/joint100
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint44/joint99/joint100/joint101
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint44/joint99/joint100/joint101/joint47
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint48
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint48/joint73
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint48/joint73/joint74
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint48/joint73/joint74/joint75
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint48/joint73/joint74/joint75/joint51
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint52
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint52/joint85
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint52/joint85/joint86
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint52/joint85/joint86/joint87
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint57
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint57/joint102
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint57/joint102/joint103
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint57/joint102/joint103/joint104
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint57/joint102/joint103/joint104/joint60
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint61
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint61/joint65
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint61/joint65/joint62
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint61/joint65/joint62/joint63
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint61/joint65/joint62/joint63/joint64
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint66
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint66/joint76
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint66/joint76/joint77
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint66/joint76/joint77/joint78
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint66/joint76/joint77/joint78/joint69
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint79
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint79/joint105
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint79/joint105/joint106
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint79/joint105/joint106/joint107
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/joint79/joint105/joint106/joint107/joint82
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/nurbsCylinder1_collision_capsule
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/nurbsCylinder1_collision_capsule/nurbsSphere1_collision_capsule
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/nurbsCylinder1_collision_capsule/nurbsSphere1_collision_capsule_locator_collision_capsule
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/nurbsCylinder1_collision_capsule/nurbsSphere2_collision_capsule
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M/transform2/Base/nurbsCylinder1_collision_capsule/nurbsSphere2_collision_capsule_locator_collision_capsule
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Jaw_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Jaw_M/JawEnd_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L/IndexFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L/IndexFinger2_L/IndexFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L/IndexFinger2_L/IndexFinger3_L/IndexFinger4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L/MiddleFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L/MiddleFinger2_L/MiddleFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L/MiddleFinger2_L/MiddleFinger3_L/MiddleFinger4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L/PinkyFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L/PinkyFinger2_L/PinkyFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L/PinkyFinger2_L/PinkyFinger3_L/PinkyFinger4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/RingFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/RingFinger1_L/RingFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/RingFinger1_L/RingFinger2_L/RingFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/RingFinger1_L/RingFinger2_L/RingFinger3_L/RingFinger4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L/ThumbFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L/ThumbFinger2_L/ThumbFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L/ThumbFinger2_L/ThumbFinger3_L/ThumbFinger4_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R/IndexFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R/IndexFinger2_R/IndexFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R/IndexFinger2_R/IndexFinger3_R/IndexFinger4_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R/MiddleFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R/MiddleFinger2_R/MiddleFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R/MiddleFinger2_R/MiddleFinger3_R/MiddleFinger4_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R/PinkyFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R/PinkyFinger2_R/PinkyFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R/PinkyFinger2_R/PinkyFinger3_R/PinkyFinger4_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/RingFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/RingFinger1_R/RingFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/RingFinger1_R/RingFinger2_R/RingFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/RingFinger1_R/RingFinger2_R/RingFinger3_R/RingFinger4_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R/ThumbFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R/ThumbFinger2_R/ThumbFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R/ThumbFinger2_R/ThumbFinger3_R/ThumbFinger4_R
    m_Weight: 1
