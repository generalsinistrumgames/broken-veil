%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Granny Mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Geometry
    m_Weight: 0
  - m_Path: Geometry/body
    m_Weight: 0
  - m_Path: Geometry/Dress
    m_Weight: 0
  - m_Path: Geometry/Hair
    m_Weight: 0
  - m_Path: Geometry/shoes
    m_Weight: 0
  - m_Path: Main
    m_Weight: 0
  - m_Path: Main/DeformationSystem
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Bk_L_dr
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Bk_L_dr/Bk_L_dr1
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Bk_L_dr/Bk_L_dr1/Bk_L_dr2
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Bk_L_dr/Bk_L_dr1/Bk_L_dr2/Bk_L_dr_end1
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Bk_R_dr
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Bk_R_dr/Bk_R_dr1
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Bk_R_dr/Bk_R_dr1/Bk_R_dr2
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Bk_R_dr/Bk_R_dr1/Bk_R_dr2/Bk_R_dr_end1
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/FL_dr
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/FL_dr/FL_dr1
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/FL_dr/FL_dr1/FL_dr2
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/FL_dr/FL_dr1/FL_dr2/FL_dr_end1
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Fr_dr
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Fr_dr/Fr_dr1
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Fr_dr/Fr_dr1/Fr_dr2
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Fr_dr/Fr_dr1/Fr_dr2/Fr_dr_end1
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L/Ankle_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L/Ankle_L/Toes_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L/Ankle_L/Toes_L/ToesEnd_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R/Ankle_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R/Ankle_R/Toes_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R/Ankle_R/Toes_R/ToesEnd_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/L_dr
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/L_dr/L_dr1
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/L_dr/L_dr1/L_dr2
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/L_dr/L_dr1/L_dr2/L_dr_end1
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/R_dr
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/R_dr/R_dr1
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/R_dr/R_dr1/R_dr2
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/R_dr/R_dr1/R_dr2/R_dr_end1
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Eye_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Eye_L/EyeEnd_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Eye_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Eye_R/EyeEnd_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/HeadEnd_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Jaw_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Jaw_M/JawEnd_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L/IndexFinger2_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L/IndexFinger2_L/IndexFinger3_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L/IndexFinger2_L/IndexFinger3_L/IndexFinger4_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L/MiddleFinger2_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L/MiddleFinger2_L/MiddleFinger3_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L/MiddleFinger2_L/MiddleFinger3_L/MiddleFinger4_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L/PinkyFinger2_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L/PinkyFinger2_L/PinkyFinger3_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L/PinkyFinger2_L/PinkyFinger3_L/PinkyFinger4_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/RingFinger1_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/RingFinger1_L/RingFinger2_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/RingFinger1_L/RingFinger2_L/RingFinger3_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/RingFinger1_L/RingFinger2_L/RingFinger3_L/RingFinger4_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L/ThumbFinger2_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L/ThumbFinger2_L/ThumbFinger3_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L/ThumbFinger2_L/ThumbFinger3_L/ThumbFinger4_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R/IndexFinger2_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R/IndexFinger2_R/IndexFinger3_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R/IndexFinger2_R/IndexFinger3_R/IndexFinger4_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R/MiddleFinger2_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R/MiddleFinger2_R/MiddleFinger3_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R/MiddleFinger2_R/MiddleFinger3_R/MiddleFinger4_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R/PinkyFinger2_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R/PinkyFinger2_R/PinkyFinger3_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R/PinkyFinger2_R/PinkyFinger3_R/PinkyFinger4_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/RingFinger1_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/RingFinger1_R/RingFinger2_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/RingFinger1_R/RingFinger2_R/RingFinger3_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/RingFinger1_R/RingFinger2_R/RingFinger3_R/RingFinger4_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R/ThumbFinger2_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R/ThumbFinger2_R/ThumbFinger3_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R/ThumbFinger2_R/ThumbFinger3_R/ThumbFinger4_R
    m_Weight: 0
