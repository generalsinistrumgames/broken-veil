%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: PigManTorso
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Geometry
    m_Weight: 0
  - m_Path: Geometry/Cloth
    m_Weight: 0
  - m_Path: Geometry/Torso
    m_Weight: 0
  - m_Path: Hook_grp
    m_Weight: 0
  - m_Path: Hook_grp/Geometry 1
    m_Weight: 0
  - m_Path: Hook_grp/Geometry 1/Hook2
    m_Weight: 0
  - m_Path: Hook_grp/Main 1
    m_Weight: 0
  - m_Path: Hook_grp/Main 1/DeformationSystem 1
    m_Weight: 0
  - m_Path: Hook_grp/Main 1/DeformationSystem 1/Root_M 1
    m_Weight: 0
  - m_Path: Main
    m_Weight: 0
  - m_Path: Main/DeformationSystem
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress25_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress25_M/dress26_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress25_M/dress26_M/dress27_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress25_M/dress26_M/dress27_M/dress28_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress29_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress29_L/dress30_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress29_L/dress30_L/dress31_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress29_L/dress30_L/dress31_L/dress32_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress29_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress29_R/dress30_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress29_R/dress30_R/dress31_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress29_R/dress30_R/dress31_R/dress32_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress33_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress33_M/dress34_M
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress35_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress35_L/dress36_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress35_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress35_R/dress36_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress37_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress37_L/dress38_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress37_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress37_R/dress38_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress39_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress39_L/dress40_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress39_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress39_R/dress40_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress41_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress41_L/dress42_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress41_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress41_R/dress42_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress45_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress45_L/dress46_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress45_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/dress_M/dress45_R/dress46_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_bk_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_bk_L/Knee_bk_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_bk_L/Knee_bk_L/Ankle_bk_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_bk_L/Knee_bk_L/Ankle_bk_L/Toes_bk_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_bk_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_bk_R/Knee_bk_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_bk_R/Knee_bk_R/Ankle_bk_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_bk_R/Knee_bk_R/Ankle_bk_R/Toes_bk_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_fr_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_fr_L/Knee_fr_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_fr_L/Knee_fr_L/Ankle_fr_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_fr_L/Knee_fr_L/Ankle_fr_L/Toes_fr_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_fr_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_fr_R/Knee_fr_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_fr_R/Knee_fr_R/Ankle_fr_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_fr_R/Knee_fr_R/Ankle_fr_R/Toes_fr_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L/Ankle_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_L/Knee_L/Ankle_L/Toes_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R/Ankle_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Hip_R/Knee_R/Ankle_R/Toes_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/joint7_L
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/joint7_R
    m_Weight: 0
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/joint3_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/joint1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/joint1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Brow1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Brow1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Brow2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Brow2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Brow_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Brow_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Check_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Check_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Jaw_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Jaw_M/Jaw1_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Jaw_M/Jaw1_M/Jaw2_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Jaw_M/Jaw1_M/Jaw2_M/Jaw3_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Lip_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Lip_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Neck_M/Head_M/Nose_M
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L/IndexFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/IndexFinger1_L/IndexFinger2_L/IndexFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L/MiddleFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/MiddleFinger1_L/MiddleFinger2_L/MiddleFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L/PinkyFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/PinkyFinger1_L/PinkyFinger2_L/PinkyFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L/ThumbFinger2_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_L/Shoulder_L/ShoulderPart1_L/ShoulderPart2_L/Elbow_L/Wrist_L/ThumbFinger1_L/ThumbFinger2_L/ThumbFinger3_L
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R/IndexFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/IndexFinger1_R/IndexFinger2_R/IndexFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R/MiddleFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/MiddleFinger1_R/MiddleFinger2_R/MiddleFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R/PinkyFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/PinkyFinger1_R/PinkyFinger2_R/PinkyFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R/ThumbFinger2_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/Chest_M/Scapula_R/Shoulder_R/ShoulderPart1_R/ShoulderPart2_R/Elbow_R/Wrist_R/ThumbFinger1_R/ThumbFinger2_R/ThumbFinger3_R
    m_Weight: 1
  - m_Path: Main/DeformationSystem/Root_M/Spine1_M/Spine2_M/joint5_M
    m_Weight: 1
