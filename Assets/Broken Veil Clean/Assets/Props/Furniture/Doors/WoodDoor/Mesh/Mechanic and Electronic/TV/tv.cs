﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tv : MonoBehaviour
{
    public Light[] lights;
    public Material[] mass;
    public Material mat_off;
    public Color[] lamp_col;
    public Color base_col;
    public float base_col_weight = 0.6f;
    public float flicker_time_mod = 3f;
    public float flicker_weight = 0.2f;
    public float time1=0;
    public float change_speed = 1.3f;
    public float rand_max_change = 1.2f;
    public float rand = 0f;
    public int index = 0;
    public Color col_now;
    public tw_state state= tw_state.on;
    public tw_state last_state = tw_state.on;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (state == tw_state.on)
        {
            if (last_state != state)
            {
                foreach (Light l in lights)
                    l.enabled = true;
                time1 += change_speed + rand;
            }
            time1 += Time.deltaTime;
            if (time1 > change_speed + rand)
            {
                time1 -= change_speed + rand;
                rand = Random.value * rand_max_change;
                index = (index + 1) % mass.Length;
                gameObject.GetComponent<MeshRenderer>().material = mass[index];
                col_now = base_col * base_col_weight + lamp_col[index] * (1 - base_col_weight);
                //mass[1].SetFloat
                for (int i = 0; i < lights.Length; i++)
                    lights[i].color = col_now * (Mathf.PingPong(Time.time * flicker_time_mod, 1f) * flicker_weight + (1f - flicker_weight));
            }
            else
                for (int i = 0; i < lights.Length; i++)
                    if(lights[i]!=null)
                        lights[i].color = col_now * (Mathf.PingPong(Time.time * flicker_time_mod, 1f) * flicker_weight + (1f - flicker_weight));
        }
        else
        {
            if (last_state != state)
            {
                foreach (Light l in lights)
                    l.enabled = false;
                gameObject.GetComponent<MeshRenderer>().material = mat_off;
            }
        }
        last_state = state;
    }

    public enum tw_state
    {
        on,
        off
    }
}
