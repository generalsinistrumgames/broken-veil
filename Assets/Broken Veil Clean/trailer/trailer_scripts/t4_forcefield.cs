﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class t4_forcefield : MonoBehaviour
{
    public SphereCollider debug_collider;
    public GameObject velocity_change;
    public Vector3 debug_velocity_change;
    public GameObject target;
    public Rigidbody debug_target_rb;
    public float debug_force;
    public float strength = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        debug_collider = GetComponent<SphereCollider>();
        GetComponent<MeshRenderer>().enabled = false;
        velocity_change.GetComponent<MeshRenderer>().enabled = false;
        debug_collider.enabled = false;
        debug_velocity_change = velocity_change.transform.position - transform.position;
        debug_velocity_change /= debug_velocity_change.magnitude;
        debug_target_rb = target.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float dist = Vector3.Distance(transform.position, target.transform.position);
        if(dist<debug_collider.radius)
        {
            float force = 1f - (dist / debug_collider.radius);
            force *= force;
            debug_force = force;
            target.GetComponent<Rigidbody>().velocity = target.GetComponent<Rigidbody>().velocity + debug_velocity_change * force;
        }
    }
}
