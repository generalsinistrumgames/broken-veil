﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hide_render_in_playmode : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        MeshRenderer render;
        if (TryGetComponent<MeshRenderer>(out render))
            render.enabled = false;
    }
}
