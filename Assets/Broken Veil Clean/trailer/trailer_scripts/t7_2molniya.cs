﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class t7_2molniya : MonoBehaviour
{
    bool debug_del = true;

    public GameObject lamp;
    Light temp_light;
    public float lamp_intencity
    {
        get { return temp_light.intensity; }
    }

    public Vector2 remap_in = new Vector2();
    public Vector2 remap_out = new Vector2();
    // Start is called before the first frame update
    void Start()
    {
        temp_light = lamp.GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        upd_mat();
    }

    public float remap_last_in = 0f;
    public float remap(float val)
    {
        if (remap_in.x > remap_in.y)
        {
            float temp = remap_in.y;
            remap_in.y = remap_in.x;
            remap_in.x = temp;
        }
        if (remap_out.x > remap_out.y)
        {
            float temp = remap_in.y;
            remap_in.y = remap_in.x;
            remap_in.x = temp;
        }

        remap_last_in = 0f;
        if (remap_in.x != remap_in.y)
            remap_last_in = Mathf.Clamp((val - remap_in.x) / (remap_in.y - remap_in.x), 0, 1);
        float _out = remap_last_in * (remap_out.y - remap_out.x) + remap_out.x;
        return _out;
    }

    public void upd_mat()
    {
        MeshRenderer mesh_renderer;
        if (!TryGetComponent<MeshRenderer>(out mesh_renderer))
            return;
        float remap_val = remap(lamp_intencity);
        Color c = mesh_renderer.materials[0].color;

        Renderer nRend = GetComponent<Renderer>();
        Material _mat = nRend.material;
        _mat.SetColor("_BaseColor", new Color(c.r, c.g, c.b, remap_last_in));
        _mat.SetFloat("_EmissionIntensity", remap_val);
    }
}
