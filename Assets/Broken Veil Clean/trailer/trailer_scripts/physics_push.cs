﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class physics_push : MonoBehaviour
{
    public Vector3 force = new Vector3(0, 0, 0);
    public bool b = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (b)
        {
            Rigidbody rb = GetComponent<Rigidbody>();
            rb.velocity = force;
            b = false;
        }
    }
}
