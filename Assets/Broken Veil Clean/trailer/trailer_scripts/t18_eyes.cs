﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class t18_eyes : MonoBehaviour
{
    public GameObject eye1, eye2,center,look_target;
    public Vector3 offset;
    public bool hide_cube_in_playmode = true;
    // Start is called before the first frame update
    void Start()
    {
        if(hide_cube_in_playmode)
        {
            MeshRenderer temp;
            if(TryGetComponent<MeshRenderer>(out temp))
                temp.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Quaternion temp = center.transform.rotation;
        center.transform.LookAt(look_target.transform.position);
        Quaternion temp2 = center.transform.rotation;
        center.transform.rotation = temp;

        temp2 *= Quaternion.Euler(offset);
        eye1.transform.rotation = temp2;
        eye2.transform.rotation = temp2;
    }
}
