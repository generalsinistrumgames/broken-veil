﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class t18_lock : MonoBehaviour
{
    public GameObject sveta;
    public GameObject pram;

    public Vector3 debug_offset;
    // Start is called before the first frame update
    void Start()
    {
        debug_offset =  pram.transform.position - sveta.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        sveta.transform.position = pram.transform.position + debug_offset;
    }
}
