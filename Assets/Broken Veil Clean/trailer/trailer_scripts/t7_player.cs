﻿using GlobalScripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class t7_player : MonoBehaviour
{
    [SerializeField] public Animator animator;
    [SerializeField] PlayerController playerController;

    public Vector3 move = new Vector3(0,0,0);
    public bool sneak = true;


    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        playerController = GetComponent<PlayerController>();
    }

    private void FixedUpdate()
    {
        playerController.Move(move);
        playerController.movementInput =1f;
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetFloat("Movement", playerController.Movement);

        //animator.SetFloat("Grab Value", playerController.HandsHeightAnimation);

        animator.SetBool("OnGround", playerController.rays.commonCollisionDown);
        animator.SetFloat("SpeedFall", playerController.speedFall);
        animator.SetBool("HangOnHook", playerController.hangOnHook);
        animator.SetBool("Drag", playerController.drag);
        animator.SetFloat("MovementDiretionX", playerController.movementDiretion.x);
        animator.SetFloat("MovementDiretionZ", playerController.movementDiretion.z);
        animator.SetBool("Jump", playerController.jump);

        if (sneak)
        {
            animator.SetBool("Sneak", true);
            playerController.Sneak = true;
            playerController.SpeedFactor = 0.5f;
        }
        else
        {
            animator.SetBool("Sneak", false);
            playerController.Sneak = false;
            playerController.SpeedFactor = 1f;
        }


    }
}
