﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class t_railroads : MonoBehaviour
{
    // Start is called before the first frame update
    public float t = 0;
    public float time_lengh = 3f;
    public AnimationCurve curve_over_time;
    public CinemachineDollyCart cart;
    public bool negative;
    void Start()
    {
        cart = GetComponent<CinemachineDollyCart>();
    }

    // Update is called once per frame
    void Update()
    {
        t += Time.deltaTime / time_lengh;
        if (t > 1)
            t = 1;
        if (t < 0)
            t = 0;
        if(negative)
            cart.m_Position = curve_over_time.Evaluate(Mathf.Clamp01(1f - t));
        else
            cart.m_Position = curve_over_time.Evaluate(Mathf.Clamp01(t));
    }
}
