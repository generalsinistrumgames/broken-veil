﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class follower : MonoBehaviour
{
    public GameObject folowed_object;
    public Vector3 offset = new Vector3(0,0,0);

    // Update is called once per frame
    void Update()
    {
        if (folowed_object == null)
            return;
        this.transform.position = folowed_object.transform.position+offset;
    }
}
