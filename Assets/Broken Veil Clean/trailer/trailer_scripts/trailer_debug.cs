﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trailer_debug : MonoBehaviour
{
    public KeyCode key = KeyCode.Space;
    public float timeScale = 1f;
    public List<Rigidbody> glasses;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(key))
            Time.timeScale = timeScale;
        if (Input.GetKeyDown(KeyCode.G))
            foreach (var item in glasses)
            {
                item.useGravity = true;
            }
    }
}
