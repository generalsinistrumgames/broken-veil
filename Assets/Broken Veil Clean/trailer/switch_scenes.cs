﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class switch_scenes : MonoBehaviour
{

    public KeyCode right = KeyCode.RightArrow;
    public KeyCode left = KeyCode.LeftArrow;
    public KeyCode unpause = KeyCode.Space;

    [SerializeField]
    public string[] scene_names;

    public int debug_scene_index = 0;
    public int _scene_index = -1;

    public int Scene_counts { get => scene_names.GetLength(0); }
    public int Scene_index
    {
        get { return _scene_index; }
        set
        {
            if (_scene_index == value)
                return;
            if (Scene_counts != 0 && _scene_index >= 0 && _scene_index <= Scene_counts - 1)
                unload_scene();

            _scene_index = value;
            if (_scene_index < 0)
                _scene_index = Scene_counts - 1;
            if (_scene_index > Scene_counts - 1)
                _scene_index = 0;

            debug_scene_index = value;

            if (Scene_counts != 0)
                load_scene();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Scene_index = 0;
    }

    // Update is called once per frame
    void Update()
    {

        if (Scene_counts == 0)
            return;
        int temp_scene_index = Scene_index;

        if (Input.GetKeyDown(right))
            temp_scene_index++;

        if (Input.GetKeyDown(left))
            temp_scene_index--;

        if (Input.GetKeyDown(unpause))
            Time.timeScale = 1f;

        Scene_index = temp_scene_index;
    }


    void load_scene()
    {
        if (!CheckIfSceneWasLoaded(scene_names[Scene_index]))
        {
            SceneManager.LoadScene(scene_names[Scene_index], LoadSceneMode.Additive);
            Time.timeScale = 0f;
        }
    }

    void unload_scene()
    {
        if (CheckIfSceneWasLoaded(scene_names[Scene_index]))
            SceneManager.UnloadScene(scene_names[Scene_index]);
    }

    bool CheckIfSceneWasLoaded(string name)
    {
        return SceneManager.GetSceneByName(name).isLoaded;
    }
}
