﻿Shader "Unlit/SiShaderDropWaterGlass"
{
    Properties
    {
        _MainTex ("Albedo Texture", 2D) = "white" {}
        _Size("Size", range(0,100)) = 1
        _T("Time", range(0,100)) = 1
        _Distortion("Distortion", range(-10,10)) = 1
        _Blur("Blur", range(0,10)) = 1
        _Metallic("Metallic",range(1,10)) = 1
        _TintColor("TintColor",Color) = (1,1,1,1)
    }

    SubShader
    {
        Tags {  "RenderType" = "Transparent" 
                "Queue" = "Transparent"}
        LOD 100
          
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        //GrabPass{"_GrabTexture"}   //

        Pass
        {
            CGPROGRAM 
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog
            #define S(a,b,t) smoothstep(a,b,t)   
            #include "UnityCG.cginc"
          
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {                
                float2 uv : TEXCOORD0; 
                float4 grabUv : TEXCOORD1;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex, _GrabTexture;
            float4 _MainTex_ST;
            float _Size, _T, _Distortion, _Blur;
            float4 _TintColor;

            v2f vert (appdata v) 
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                //o.grabUv = UNITY_PROJ_COORD(ComputeGrabScreenPos(o.vertex));
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            float N21(float2 p) 
            {
                p = frac(p * float2(123.34, 345.45));
                p += dot(p, p + 34.345);
                return frac(p.x*p.y);
            }
            
            float3 Layer(float2 UV, float t) 
            {
                float2 aspect = float2(2, 1);
                float2 uv = UV * _Size * aspect;
                uv.y += t * 0.25;
                float2 gv = frac(uv) - 0.5;
                float2 id = floor(uv);

                float n = N21(id);
                t += n * 6.2831;

                float w = UV.y * 10;
                float x = 0.8 * (n - 0.5);
                x += (0.4 - abs(x)) * sin(3 * w) * pow(sin(w), 6) * 0.45;    //TrailForm_AddPhysics

                float y = -sin(t + sin(t + sin(t) * 0.5)) * 0.45;       //DropForm_AddPhysics
                y -= (gv.x - x) * (gv.x - x);                         

                float2 dropPos = (gv - float2(x, y)) / aspect;          //Drops
                float drop = S(0.05, 0.03, length(dropPos));

                float2 trailPos = (gv - float2(x, t * 0.25)) / aspect;  //Trail
                trailPos.y = (frac(trailPos.y * 8) - 0.5) / 6;
          
                float trail = S(0.03, 0.01, length(trailPos));
                float fogTrail = S(-0.5, 0.05, dropPos.y);
                fogTrail *= S(0.5, y, gv.y);
                trail *= fogTrail;
                fogTrail *= S(0.05, 0.035, abs(dropPos.x));
                               
                float2 offs = drop * dropPos + trail * trailPos;
                return float3(offs, fogTrail);
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float t = fmod(_Time.y+_T,7200);
                float4 col = 0;

                float3 drops = Layer(i.uv, t);                  //DropsSize     
                drops += Layer(i.uv * 6.05 + 1.34, t*0.1);      //MinDropSize
                drops += Layer(i.uv * 6.73 + 3.24, t*0.3);
                drops += Layer(i.uv * 6.25 + 6.74, t*0.5);
                drops += Layer(i.uv * 6.47 - 7.09, t*0.6);
                drops += Layer(i.uv * 6.84 - 5.45, t*0.7);
                drops += Layer(i.uv * 6.99 + 1.54, t*1.1);
                                
                drops += Layer(i.uv * 1.15 + 2.44, t*0.6);      //MedDropSize
                drops += Layer(i.uv * 1.23 + 7.54, t*0.4);
                drops += Layer(i.uv * 1.35 + 7.54, t*0.3);
                drops += Layer(i.uv * 1.57 - 7.03, t*0.1);
                drops += Layer(i.uv * 1.64 - 5.01, t*0.9);
                drops += Layer(i.uv * 1.75 + 3.64, t*0.8);

                drops += Layer(i.uv * 2.05 + 1.44, t*0.4);      //MaxDropSize
                drops += Layer(i.uv * 2.73 + 3.54, t*0.3);
                drops += Layer(i.uv * 2.25 + 6.84, t*0.3);
                drops += Layer(i.uv * 2.47 - 7.13, t*0.2);
                drops += Layer(i.uv * 2.84 - 5.54, t*0.1);
                drops += Layer(i.uv * 2.99 + 3.74, t*0.7);
               
                float blur = _Blur * 7 * (1 - drops.z);         //Add_PhysicsDistort+Anisotropy+Reflect+Refract
                col = tex2Dlod(_MainTex, float4(i.uv + drops.xy*_Distortion,0,blur))+_TintColor;
                //col = tex2Dproj(_GrabTexture, i.grabUv);
                
                return col;
            } 
            ENDCG
        }
    }
}
 