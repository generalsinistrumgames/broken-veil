using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class traska_test : MonoBehaviour
{
    public GameObject player;
    public Vector3 offset = new Vector3();
    public GameObject cam_pos;
    public GameObject cam_target;
    [Space]
    [Header("traska_param")]
    public float intencity = 0f;
    public KeyCode key = KeyCode.U;
    [Range(0f,1f)]
    public float str_per_distance_pos = 0.2f;
    [Range(0f, 0.25f)]
    public float intencity_change = 0.06f;
    [Range(0f, 1f)]
    public float str_per_distance_target = 0.1f;
    public Vector3 cam_pos_new_rnd = new Vector3();
    public Vector3 cam_pos_old_rnd = new Vector3();
    public Vector3 cam_target_new_rnd = new Vector3();
    public Vector3 cam_target_old_rnd = new Vector3();
    public bool cam_pos_shake = true;
    public bool cam_target_shake = true;
    public bool synchronize = false;
    public float time = 0f;
    [Range(0f,1f)]
    public float time_interval = 12f / 60f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        cam_target.transform.position = player.transform.position;
        cam_pos.transform.position = cam_target.transform.position + offset;
        if (Input.GetKeyDown(key))
            intencity = 1f;
        else
            intencity = Mathf.Lerp(intencity, 0f, intencity_change);
        float distance = Vector3.Distance(cam_target.transform.position, cam_pos.transform.position);
        time -= Time.deltaTime;
        if (time < 0)
        {
            time = time_interval;
            cam_pos_old_rnd = cam_pos_new_rnd;
            cam_target_old_rnd = cam_target_new_rnd;
            generate_new_pos();
        }
        Vector3 fin_pos =new Vector3();
        Vector3 fin_target = new Vector3();
        float lerp_param= time/ time_interval;
        if (synchronize && cam_pos_shake && cam_target_shake)
        {
            fin_pos = distance * intencity * str_per_distance_pos * Vector3.Lerp(cam_pos_new_rnd,cam_pos_old_rnd,  lerp_param);
            fin_target = distance * intencity * str_per_distance_target * Vector3.Lerp(cam_pos_new_rnd,cam_pos_old_rnd,  lerp_param);
        }
        else
        {
            if(cam_pos_shake)
                fin_pos = distance * intencity * str_per_distance_pos * Vector3.Lerp(cam_pos_new_rnd,cam_pos_old_rnd,  lerp_param);
            if(cam_target_shake)
                fin_target = distance * intencity * str_per_distance_target * Vector3.Lerp(cam_target_new_rnd,cam_target_old_rnd,  lerp_param);
        }
        Quaternion q = Quaternion.FromToRotation(cam_pos.transform.position,cam_target.transform.position);
        cam_target.transform.position += q*fin_target;
        cam_pos.transform.position += q*fin_pos;


    }
    void generate_new_pos()
    {
        cam_pos_new_rnd = new Vector3(Random.value-0.5f, Random.value - 0.5f, 0f);
        cam_target_new_rnd = new Vector3(Random.value - 0.5f, Random.value - 0.5f, 0f);
    }
}
