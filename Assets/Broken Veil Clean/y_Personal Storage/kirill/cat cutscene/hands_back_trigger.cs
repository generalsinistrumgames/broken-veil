using GlobalScripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hands_back_trigger : MonoBehaviour
{
    public BoxCollider B_collider;
    public bool _in=false;
    public bool _out=false;
    public GameObject player;

    public float time_from_trigger = 0f;

    public GameObject dust;
    public float dust_time_start_hide = 0f;
    public float dust_time_end_hide = 3f;
    public GameObject dust_start_pos;
    public GameObject dust_end_pos;


    public GameObject hands;
    public float hands_time_start_hide = 0f;
    public float hands_time_end_hide = 3f;
    public GameObject hands_start_pos;
    public GameObject hands_end_pos;

    public float light_off_time = 0.4f;
    public Light light;
    // Start is called before the first frame update
    void Start()
    {
        B_collider = gameObject.GetComponent<BoxCollider>();
        B_collider.enabled = false;
        player = GameObject.FindObjectOfType<Player>().gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null || !player.activeSelf)
        {
            PlayerController[] pc = GameObject.FindObjectsOfType<PlayerController>();
            if (pc.Length != 0)
                player = pc[0].gameObject;
            return;
        }



        if (_out && _in)
        {
            float dust_param = time_parameter_calc(time_from_trigger, dust_time_start_hide, dust_time_end_hide);
            float hands_param = time_parameter_calc(time_from_trigger, hands_time_start_hide, hands_time_end_hide);

            hands.transform.position = Vector3.Lerp(hands_start_pos.transform.position, hands_end_pos.transform.position, hands_param);
            dust.transform.position = Vector3.Lerp(dust_start_pos.transform.position, dust_end_pos.transform.position, hands_param);

            time_from_trigger += Time.deltaTime;

            if (time_from_trigger > light_off_time)
                light.enabled = false;
        }
        else
        {
            bool player_in_collider = is_point_in_cube(B_collider, player.transform.position);
            if (!_in)
                if (player_in_collider)
                    _in = true;
            if(_in)
            {
                if (!player_in_collider)
                {
                    if (player.transform.position.x > transform.position.x)
                        _out = true;
                    else
                        _in = false;
                }

            }
        }
    }


    public float time_parameter_calc(float time, float min_time, float max_time)
    {
        if (time < min_time)
            return 0f;
        if (time > max_time)
            return 1f;
        float delta = (max_time - min_time);
        if (delta < 0.001f)
            return 1f;
        return (time - min_time) / delta;
    }

    bool is_point_in_cube(BoxCollider collider, Vector3 pos)
    {
        Vector3 center = collider.center + collider.transform.position;
        Vector3 size = collider_size_normalized(collider);
        Vector3 difference_between_center_and_pos = center - pos;
        if (Mathf.Abs(difference_between_center_and_pos.x) > size.x / 2f)
            return false;
        if (Mathf.Abs(difference_between_center_and_pos.y) > size.y / 2f)
            return false;
        if (Mathf.Abs(difference_between_center_and_pos.z) > size.z / 2f)
            return false;

        return true;
    }

    Vector3 collider_size_normalized(BoxCollider collider)
    {
        Vector3 size = collider.size;
        if (size.x < 0)
        {
            collider.size = new Vector3(size.x * -1f, size.y, size.z);
            size = collider.size;
        }
        if (size.y < 0)
        {
            collider.size = new Vector3(size.x, size.y * -1f, size.z);
            size = collider.size;
        }
        if (size.z < 0)
        {
            collider.size = new Vector3(size.x, size.y, size.z * -1f);
            size = collider.size;
        }
        return size;
    }
}
