using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scene_switch_basement : MonoBehaviour
{
    public hands_catch hands_catch;
    public GameObject hands_catch_main;
    public hands_spawner hands;
    public void pre_ini()
    {
        hands.gameObject.SetActive(true);
    }


    public void final_ini()
    {
        //hands
        hands.ini = true;
        hands_catch.gameObject.SetActive(true);
        hands_catch.transform.parent.gameObject.SetActive(true);

        laser_pick[] lp = GameObject.FindObjectsOfType<laser_pick>();
        for (int i = 0; i < lp.Length; i++)
            lp[i].ini = true;
    }

}