using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scene_switch_lane : MonoBehaviour
{
    scene_switch_basement basement_switch;

    private void Start()
    {
        pre_ini();
    }

    public void pre_ini()
    {
        scene_switch_basement[] bs = GameObject.FindObjectsOfType<scene_switch_basement>();
        if(bs.Length==0)
        {
            Debug.LogError("didnt find scene_switch_basement");
            return;
        }
        basement_switch = bs[0];
        basement_switch.pre_ini();
    }

    public void final_ini()
    {
        scene_switch_basement[] bs = GameObject.FindObjectsOfType<scene_switch_basement>();
        if (bs.Length == 0)
        {
            Debug.LogError("didnt find scene_switch_basement");
            return;
        }
        basement_switch = bs[0];
        basement_switch.final_ini();
    }
}
