using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cat_trigger : MonoBehaviour
{
    public BoxCollider B_collider;
    public bool is_cat_in_cube = false;
    public GameObject cat_part;

    public float time_from_cat_enter_trigger = 0f;

    public GameObject dust;
    public Vector3 dust_ini_pos;
    public Vector3 dust_end_offset;
    public float dust_time_start_hide = 0f;
    public float dust_time_end_hide = 3f;

    public hands_spawner hands_spawner_go;
    public hands_catch hands_catch;
    public float hands_time_start_hide = 0f;
    public float hands_time_end_hide = 3f;
    public Vector3 hands_end_offset;

    public GameObject light_on;
    public Light switch_light;
    public Color switch_light_on_color;
    public GameObject switch_light2;

    public GameObject lamp_go;
    public Material lamp_on_mat;
    // Start is called before the first frame update
    void Start()
    {
        B_collider = gameObject.GetComponent<BoxCollider>();
        B_collider.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(is_cat_in_cube)
        {
            
            
            if(time_from_cat_enter_trigger<0.001f)
            {
                light_on.SetActive(true);
                dust_ini_pos=dust.transform.position;
                switch_light.color = switch_light_on_color;
                hands_catch.gameObject.SetActive(false);
                if(lamp_go!=null && lamp_on_mat!=null)
                {
                    Renderer temp = lamp_go.GetComponent<Renderer>();
                    temp.material = lamp_on_mat;
                }
                if (switch_light2 != null)
                    switch_light2.SetActive(false);
            }
            else
            {
                float dust_end_parameter = time_parameter_calc(time_from_cat_enter_trigger, dust_time_start_hide, dust_time_end_hide);
                float hands_end_parameter = time_parameter_calc(time_from_cat_enter_trigger, hands_time_start_hide, hands_time_end_hide);

                Vector3 dust_new_pos=Vector3.Lerp(dust_ini_pos, dust_ini_pos+ dust_end_offset, dust_end_parameter);
                Vector3 hands_new_offset=Vector3.Lerp(Vector3.zero, hands_end_offset, hands_end_parameter);

                dust.transform.position = dust_new_pos;
                set_hands_offset(hands_new_offset);
            }

            time_from_cat_enter_trigger += Time.deltaTime;
        }
        else
        {
            light_on.SetActive(false);
        }
        is_cat_in_cube = is_point_in_cube(B_collider, cat_part.transform.position) || is_cat_in_cube;
    }
    public void set_hands_offset(Vector3 new_offset)
    {
        for(int i=0;i<hands_spawner_go.hands.Length;i++)
            hands_spawner_go.hands[i].GetComponent<hands_controller>().offset_to_hide=new_offset;
    }
    public float time_parameter_calc(float time,float min_time,float max_time)
    {
        if(time<min_time)            
            return 0f;
        if (time > max_time)
            return 1f;
        float delta = (max_time - min_time);
        if (delta < 0.001f)
            return 1f;
        return (time - min_time) / delta;
    }

    bool is_point_in_cube(BoxCollider collider, Vector3 pos)
    {
        Vector3 center = collider.center + collider.transform.position;
        Vector3 size = collider_size_normalized(collider);
        Vector3 difference_between_center_and_pos = center - pos;
        if (Mathf.Abs(difference_between_center_and_pos.x) > size.x / 2f)
            return false;
        if (Mathf.Abs(difference_between_center_and_pos.y) > size.y / 2f)
            return false;
        if (Mathf.Abs(difference_between_center_and_pos.z) > size.z / 2f)
            return false;

        return true;
    }
    Vector3 collider_size_normalized(BoxCollider collider)
    {
        Vector3 size = collider.size;
        if (size.x < 0)
        {
            collider.size = new Vector3(size.x * -1f, size.y, size.z);
            size = collider.size;
        }
        if (size.y < 0)
        {
            collider.size = new Vector3(size.x, size.y * -1f, size.z);
            size = collider.size;
        }
        if (size.z < 0)
        {
            collider.size = new Vector3(size.x, size.y, size.z * -1f);
            size = collider.size;
        }
        return size;
    }
}
