using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cat;
using UnityEngine.AI;
using UnityEngine.Animations.Rigging;

namespace Basement
{
    public class laser_trigger : MonoBehaviour
    {
        public laser_work laser;
        public BoxCollider B_collider;
        public bool is_laser_in_cube = false;
        public GameObject cat;
        public float seconds_turn_off_cat = 3.6f;
        public Animator cat_jump;
        public Animator props;

        public RuntimeAnimatorController cat_jump_animation;

        public GameObject popUpHintShower;

        Coroutine corut;

        [Header("Play Sound Events")]
        public string catSceneSoundEvent = "Play_basement_cat_scene";
        [Space]
        public string handsOutSoundEvent = "Play_basement_hands_out";

        [Header("Play Sound Events")]
        public string catLoopStopSoundEvent = "Stop_basement_cat";
        [Space]
        public string handsLoopStopSoundEvent = "Stop_basement_hands_loop";
        [Space]
        public GameObject catSoundAmbient;
        public BoxCollider catTrigger;
        bool cat_on_Pos = false;

        // Start is called before the first frame update
        void Start()
        {
            laser = GameObject.FindObjectOfType<laser_work>();
            B_collider = gameObject.GetComponent<BoxCollider>();
            B_collider.enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            bool error = false;
            laser_work[] lw = GameObject.FindObjectsOfType<laser_work>();
            if (lw.Length < 1)
                error = true;
            else
                laser = lw[0];

            if (is_laser_in_cube)
            {
                cat_on_Pos = is_point_in_cube(catTrigger, cat.transform.position);
                if (corut == null && cat_on_Pos)
                {
                    popUpHintShower.SetActive(false);

                    catSoundAmbient.SetActive(false);

                    AkSoundEngine.PostEvent(catLoopStopSoundEvent, gameObject);

                    AkSoundEngine.PostEvent(catSceneSoundEvent, gameObject);

                    corut = StartCoroutine(turn_off_cat(seconds_turn_off_cat));
                    //Goto cat
                    cat.transform.rotation = Quaternion.LookRotation(Vector3.right);
                    cat_jump.runtimeAnimatorController = cat_jump_animation;
                    var catBrain = cat.GetComponent<CatBrain>();
                    catBrain.StopAllCoroutines();
                    catBrain.enabled = false;
                    cat.GetComponent<NavMeshAgent>().enabled = false;
                    cat.GetComponent<RigBuilder>().enabled = false;
                    props.SetBool("jump", true);
                }
            }
            if (!error)
                is_laser_in_cube = is_point_in_cube(B_collider, laser.end_point) || is_laser_in_cube;
            else
                is_laser_in_cube = false;
        }

        IEnumerator turn_off_cat(float seconds)
        {
            AkSoundEngine.PostEvent(handsOutSoundEvent, gameObject);

            // delay before sound event
            yield return new WaitForSeconds(0.54f);
            AkSoundEngine.PostEvent(handsLoopStopSoundEvent, gameObject);

            yield return new WaitForSeconds(seconds);
            cat.SetActive(false);
        }

        bool is_point_in_cube(BoxCollider collider, Vector3 pos)
        {
            Vector3 center = collider.center + collider.transform.position;
            Vector3 size = collider_size_normalized(collider);
            Vector3 difference_between_center_and_pos = center - pos;
            if (Mathf.Abs(difference_between_center_and_pos.x) > size.x / 2f)
                return false;
            if (Mathf.Abs(difference_between_center_and_pos.y) > size.y / 2f)
                return false;
            if (Mathf.Abs(difference_between_center_and_pos.z) > size.z / 2f)
                return false;

            return true;
        }

        Vector3 collider_size_normalized(BoxCollider collider)
        {
            Vector3 size = collider.size;
            if (size.x < 0)
            {
                collider.size = new Vector3(size.x * -1f, size.y, size.z);
                size = collider.size;
            }
            if (size.y < 0)
            {
                collider.size = new Vector3(size.x, size.y * -1f, size.z);
                size = collider.size;
            }
            if (size.z < 0)
            {
                collider.size = new Vector3(size.x, size.y, size.z * -1f);
                size = collider.size;
            }
            return size;
        }
    }
}
