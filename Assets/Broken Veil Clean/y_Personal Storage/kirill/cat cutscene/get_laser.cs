using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class get_laser : MonoBehaviour
{
    public bool work = false;
    public GameObject player;
    public KeyCode laser_pick=KeyCode.K;
    public RuntimeAnimatorController anim;
    public float current_time = 0f;
    public float max_time = 2.1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(work)
        {
            current_time += Time.deltaTime;
            if (current_time > max_time)
            {
                work = false;
                Animator temp_anim = player.GetComponent<Animator>();
                RuntimeAnimatorController temp_ac = temp_anim.runtimeAnimatorController;
                temp_anim.runtimeAnimatorController = anim;
                temp_anim.Update(0.001f);
                anim = temp_ac;
            }
        }
        else
            if (Input.GetKeyDown(laser_pick))
            {
                work = true;
                Animator temp_anim = player.GetComponent<Animator>();
                RuntimeAnimatorController temp_ac = temp_anim.runtimeAnimatorController;
                temp_anim.runtimeAnimatorController = anim;
                anim = temp_ac;
            temp_anim.Update(0.001f);
            current_time = 0f;
            }
        
    }
}
