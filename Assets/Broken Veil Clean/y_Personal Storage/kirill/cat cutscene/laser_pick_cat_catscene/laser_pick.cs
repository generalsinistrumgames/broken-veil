using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;

public class laser_pick : MonoBehaviour
{
    public PlayerController player;
    public GameObject laser_obj;
    public Renderer[] renderers;
    public float range = 0.36f;
    bool work = false;
    public KeyCode activate_key = KeyCode.Tab;
    public RuntimeAnimatorController ac_pick;
    public bool ini = false;
    Coroutine cor;
    public BoolVariable laserControl;
    [Space]
    public PopUpHintShower popUpHintShower;

    [Header("Laser Emissive")]
    public Material laserMaterial;
    [Space]
    public Color emissiveColor = Color.green;
    [Space]
    public float emissiveIntensity = 1000;

    [Header("Take Laser Hint")]
    public GameObject takeLaserHint;

    [Header("Sound Event")]
    public string takeLaserSoundEvent = "Play_basement_pointer_take";

    // Update is called once per frame
    void Update()
    {
        if (!ini)
            return;
        if (work == true)
            return;
        if (cor != null)
            return;
        //Debug.LogError("pick_laser 1");
        if (Input.GetKeyDown(activate_key))
        {         
            //Debug.LogError("pick_laser 2");
            PlayerController[] pc_mass = GameObject.FindObjectsOfType<PlayerController>();
            if (pc_mass.Length == 0)
                return;
            //Debug.LogError("pick_laser 3");
            if (laser_obj == null)
                laser_obj = gameObject;
            Vector3 mypos = laser_obj.transform.position;
            bool check = false;
            for (int i = 0; i < pc_mass.Length; i++)
            {
                if (Vector3.Distance(pc_mass[i].transform.position, mypos) < range)
                    if (pc_mass[i].rays.commonCollisionDown)
                    {
                        check = true;
                        player = pc_mass[i];
                        break;
                    }
            }
            if (check == false)
                return;

            work = true;
            takeLaserHint.SetActive(false);
            cor = StartCoroutine(do_some_shit());
        }
    }

    IEnumerator do_some_shit()
    {
        const float turn_per_second = 150f;
        const float anim_time = 4.2f;
        disable_player();
        yield return new WaitForSeconds(0.3f);
        bool turn = true;

        while (turn)
        {
            Vector3 player_pos = player.transform.position;
            Vector3 laser_pos = laser_obj.transform.position;
            laser_pos.y = player_pos.y;

            float frame_turn_spd = turn_per_second * Time.deltaTime;
            Vector3 newDir = Vector3.RotateTowards(player.transform.forward, (laser_pos - player.transform.position), frame_turn_spd * Mathf.Deg2Rad, 0.0F);
            Vector3 difference = player.transform.rotation.eulerAngles;
            player.transform.rotation = Quaternion.LookRotation(newDir);
            difference -= player.transform.rotation.eulerAngles;
            if (difference.magnitude > 0.005f)
                yield return new WaitForEndOfFrame();
            else
                turn = false;
        }

        Animator animator_p = player.GetComponent<Animator>();
        RuntimeAnimatorController temp_ac = animator_p.runtimeAnimatorController;
        animator_p.runtimeAnimatorController = ac_pick;
        animator_p.Update(0.001f);
        yield return new WaitForSeconds(1f);

        AkSoundEngine.PostEvent(takeLaserSoundEvent, gameObject);
        popUpHintShower.ShowHint();

        for (int i = 0; i < renderers.Length; i++)
            if (renderers[i] != null)
                renderers[i].enabled = false;
        yield return new WaitForSeconds(anim_time - 1f);
        animator_p.runtimeAnimatorController = temp_ac;
        animator_p.Update(0.001f);
        enable_laser();
        enable_player();

        //laserMaterial.SetColor("_EmissiveColor", emissiveColor * emissiveIntensity);

        gameObject.SetActive(false);
    }

    public void enable_laser()
    {
        laserControl.Value = true;
    }
    public void disable_player()
    {
        player.rootMove = true;
    }

    public void enable_player()
    {
        player.rootMove = false;
    }
}
