using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class anim_test_hands : MonoBehaviour
{
    public float parameter=0f;
    public float random=0f;

    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        random = Random.value;
        anim.SetFloat("random", random);
    }

    // Update is called once per frame
    void Update()
    {
        random = Random.value;
        anim.SetFloat("random", random);
        anim.SetFloat("close", parameter);
    }
}
