using GlobalScripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hands_spawner : MonoBehaviour
{
    // Start is called before the first frame update
    [Range(1,64)]
    public int number_of_hands = 16;
    public GameObject hand_sample;
    public GameObject hands_parent;
    public GameObject[] hands;
    [Range(0f,10f)]
    public float distance_from_center=0.9f;
    public bool work = true;
    public GameObject player_go;
    //public GameObject debug_cube;
    public float player_dist_from_center;
    public float player_angle;

    public hands_catch h_catch;
    public float catch_val = 0.05f;
    public Quaternion const_offset;

    public Vector3 angle_offset;

    public bool ini = false;
    public bool last_ini = false;
    void Start()
    {
        hand_sample.SetActive(false);
        PlayerController[] pc = GameObject.FindObjectsOfType<PlayerController>();
        if(pc.Length!=0)
            player_go = pc[0].gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if(last_ini)
            if(player_go==null||!player_go.activeSelf||player_go==this.gameObject)
            {
                PlayerController[] pc = GameObject.FindObjectsOfType<PlayerController>();
                if (pc.Length != 0)
                    player_go = pc[0].gameObject;
                return;
            }
        if (!last_ini)
            player_go = this.gameObject;

        
        for (int i = 0; i < number_of_hands; i++)
        {
            if (hands != null)
                if (i < hands.Length)
                    if (hands[i] != null)
                    {
                        hands[i].GetComponent<hands_controller>().angle_offset = angle_offset;
                        hands[i].GetComponent<hands_controller>().player = player_go;
                    }
        }



        Vector3 pl = player_go.transform.position;
        pl.y = transform.position.y;
        player_dist_from_center = Vector3.Distance(pl, transform.position);

        {
            Vector3 pos_offset = new Vector3();
            pos_offset.x = player_dist_from_center * Mathf.Sin(player_angle);
            pos_offset.z = player_dist_from_center * Mathf.Cos(player_angle);
            //debug_cube.transform.position = transform.position + pos_offset;
        }
        player_angle = Mathf.Deg2Rad*Quaternion.LookRotation(Vector3.up,pl-transform.position).eulerAngles.y-Mathf.PI;


        if (ini != last_ini)
            last_ini = ini;


            if (!work)
            return;
        work = false;

        if(hands.Length>0)
        {
            for (int i = 0; i < hands.Length; i++)
                if (hands[i] != null)
                    Destroy(hands[i]);
            
        }
        hands = new GameObject[number_of_hands];

        for (int i = 0; i < number_of_hands; i++)
        {
            Vector3 pos_offset = new Vector3();
            float step = Mathf.PI * 2f / (float)number_of_hands;
            float angle = Mathf.PI * 2f / (float)number_of_hands * (float)i;
            pos_offset.x = distance_from_center * Mathf.Sin(angle);
            pos_offset.z = distance_from_center * Mathf.Cos(angle);
            Vector3 pos = transform.position+pos_offset;
            Quaternion q = new Quaternion();
            q.eulerAngles = new Vector3(q.eulerAngles.x, q.eulerAngles.y+ (float)i/ (float)number_of_hands*360f, q.eulerAngles.z);

           
            hands[i] = Instantiate(hand_sample, pos, q, hands_parent.transform);

            hands[i].GetComponent<hands_controller>().ini();
            hands[i].GetComponent<hands_controller>().center = transform.position;
            hands[i].GetComponent<hands_controller>().max_angle = angle + (step / 2f);
            hands[i].GetComponent<hands_controller>().current_angle = angle;
            hands[i].GetComponent<hands_controller>().min_angle = angle - (step / 2f);
            hands[i].GetComponent<hands_controller>().min_dist= distance_from_center;
            hands[i].GetComponent<hands_controller>().player = player_go;
            hands[i].SetActive(true);
           
        }
       
    }
}
