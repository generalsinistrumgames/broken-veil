using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hands_entrophy : MonoBehaviour
{
    public Animator anim;
    public float min_val = -1f;
    public float max_val = 1f;

    public Vector2 random_vector = new Vector2();

    public Vector2 prev_vector = new Vector2();

    public Vector2 current_vector = new Vector2();

    public float time = -1f;

    public float time_current = -1f;

    public float max_time = 0.3f;
    public float min_time = 0.8f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (time < 0)
        {
            time_current = Random.Range(min_time, max_time);
            time = time_current;
            prev_vector = random_vector;
            random_vector = new Vector2(Random.Range(min_val,max_val), Random.Range(min_val, max_val));
        }
        else
            time -= Time.deltaTime;
        current_vector = new Vector2(Mathf.Lerp(random_vector.x, prev_vector.x, time / time_current), Mathf.Lerp(random_vector.y, prev_vector.y, time / time_current));
        anim.SetFloat("Random", current_vector.x);
        anim.SetFloat("Random2", current_vector.y);
    }
}
