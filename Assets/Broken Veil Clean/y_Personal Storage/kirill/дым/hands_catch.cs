using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;
using UnityEngine.Events;

public class hands_catch : MonoBehaviour
{
    [Header("Game Over Trigger")]
    [SerializeField] float _gameOverDelay = 1.25f;
    [Space]
    [SerializeField] FloatVariable _gameOverSO;
    [Space]
    [SerializeField] UnityEvent OnGameOver;


    public bool work = true;
    public bool debug_mode = false;
    public GameObject target_cube;
    public GameObject hands_cube;
    public GameObject player;
    public Animator anim;
    public hands_spawner spawner;
    public Vector3 center
    {
        get { return spawner.transform.position; }
    }

    public float p_anim = 0f;
    public float p_angle = 0f;
    public float p_distance = 0f;
    public float p_y = 0f;

    public bool player_catch = false;
    public bool lock_hands_position = false;
    public Vector3 player_catch_position = new Vector3();
    public Quaternion player_catch_rotation = new Quaternion();
    public float player_catch_time = 0f;
    public float player_catch_max_time = 0.45f;
    public Vector3 player_catch_offset = new Vector3();

    public float p_player_angle = 0f;
    public Vector3 p_player_pos;
    public float p_player_distance;

    public float delay = 1f;
    public float cath_player_time = 0.47f;
    public float anim_time = 2.167f;
    public float time_out_circle = 0f;
    public float catch_distance = 1.25f;
    public bool is_player_out_circle = false;

    public bool front_catch = false;

    public bool is_player_death = false;

    public bool is_catched = false;

    public RuntimeAnimatorController rac;

    [Header("Play Sound Event")]
    public string playerCatchSoundEvent = "Play_basement_hands_grab";

    [Header("Play Sound Events")]
    public string handsLoopStopSoundEvent = "Stop_basement_hands_loop";
    [Space]
    public string catLoopStopSoundEvent = "Stop_basement_cat";

    // Start is called before the first frame update
    void Start()
    {
        anim.enabled = false;
        if (player == null)
            player = GameObject.FindObjectOfType<PlayerController>().gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null || !player.activeSelf)
        {
            PlayerController[] pc = GameObject.FindObjectsOfType<PlayerController>();
            if (pc.Length != 0)
                player = pc[0].gameObject;
            return;
        }


        if (!work)
        {
            gameObject.SetActive(false);
            return;
        }

        if (!debug_mode)
        {
            p_player_pos = center - player.transform.position;
            p_player_angle = Quaternion.LookRotation(p_player_pos).eulerAngles.y;
            p_player_distance = p_player_pos.magnitude;


            calc_parameters();
            calc_anim();
        }
        apply_parameters();
    }

    public void calc_anim()
    {
        bool catch_frame = false;
        if (p_anim < cath_player_time)
            catch_frame = true;
        p_anim = (time_out_circle - delay) / anim_time;
        if (catch_frame && p_anim > cath_player_time)
        {
            catch_frame = true;


        }
        else
            catch_frame = false;
        if (p_anim > 1f)
            p_anim = 1f;
        if (catch_frame)
        {

            lock_hands_position = true;

            if (!is_catched)
            {
                is_catched = true;
                Animator player_animator = player.GetComponent<Animator>();
                player_animator.runtimeAnimatorController = rac;

                AkSoundEngine.PostEvent(handsLoopStopSoundEvent, gameObject);

                AkSoundEngine.PostEvent(catLoopStopSoundEvent, gameObject);

                AkSoundEngine.PostEvent(playerCatchSoundEvent, gameObject);

                front_catch = (Vector3.Dot(transform.forward, player.transform.forward) < 0);
                if (front_catch)
                    player_animator.SetBool("lvl2_hands_dead_front", true);
                else
                    player_animator.SetBool("lvl2_hands_dead_back", true);

                player_animator.Update(0.001f);
            }


            player_catch_position = player.transform.position;
            player_catch_rotation = player.transform.rotation;
        }
        if (lock_hands_position)
        {
            //Animator player_animator = player.GetComponent<Animator>();
            //player_animator.SetBool("JumpUp", false);
            //player_animator.SetBool("FreeFallTrigger", false);
            player.GetComponent<PlayerController>().enabled = false;
            //player.transform.position = hands_cube.transform.position + player_catch_offset;
            player.transform.position = Vector3.Lerp(player_catch_position, transform.position + player_catch_offset, player_catch_time / player_catch_max_time);
            if (front_catch)
                player.transform.rotation = Quaternion.Slerp(player_catch_rotation, transform.rotation * Quaternion.Euler(0, 0, 0), player_catch_time / player_catch_max_time);
            else
                player.transform.rotation = Quaternion.Slerp(player_catch_rotation, transform.rotation * Quaternion.Euler(0, -180, 0), player_catch_time / player_catch_max_time);
            player.transform.rotation = Quaternion.Slerp(player_catch_rotation, transform.rotation, player_catch_time / player_catch_max_time);

            player_catch_time += Time.deltaTime;
            if (player_catch_time > player_catch_max_time)
            {
                player_catch_time = player_catch_max_time;

                if (!is_player_death)
                {
                    is_player_death = true;
                    _gameOverSO.Value = _gameOverDelay;
                    OnGameOver?.Invoke();
                }
            }
        }
    }
    public void calc_parameters()
    {
        if (!lock_hands_position)
        {
            p_angle = p_player_angle - 180f;
            p_distance = p_player_distance;
            if (p_distance < catch_distance)
            {
                is_player_out_circle = false;
                p_distance = catch_distance;
            }
            else
                is_player_out_circle = true;
        }

        if (is_player_out_circle)
            time_out_circle += Time.deltaTime;
        else
            time_out_circle = 0f;
    }

    public void normalize_angle(ref float angle)
    {
        if (angle < 0)
            angle += 360f;
        if (angle > 360)
            angle -= 360f;
    }
    public float get_closest_angle(float start_angle, float target_angle)
    {
        float[] temp = new float[3];
        float[] delta = new float[3];
        temp[0] = start_angle;
        delta[0] = Mathf.Abs(target_angle - temp[0]);
        temp[1] = start_angle - 360f;
        delta[1] = Mathf.Abs(target_angle - temp[1]);
        temp[2] = start_angle + 360f;
        delta[2] = Mathf.Abs(target_angle - temp[2]);

        float temp_delta = float.MaxValue;
        for (int i = 0; i < temp.Length; i++)
            if (delta[i] < temp_delta)
                temp_delta = delta[i];

        return temp_delta;
    }

    public void apply_parameters()
    {
        normalize_angle(ref p_angle);

        anim.enabled = true;
        anim.Play("Default", -1, p_anim);
        anim.Update(0);
        anim.enabled = false;

        Vector3 offset = new Vector3(0f, 0f, p_distance);
        Quaternion angle = Quaternion.Euler(0f, p_angle, 0f);
        target_cube.transform.rotation = angle;
        offset = angle * offset;
        offset.y = p_y;
        target_cube.transform.position = center + offset;
    }
}
