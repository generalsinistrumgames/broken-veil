using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hands_controller : MonoBehaviour
{
    public GameObject[] hands_group;
    //public GameObject hands_group2;
    //public GameObject hands_group3;
    [Range(0f, 1f)]
    public float closeness = 0f;
    [Range(0f, 1f)]
    public float angle_difference = 0f;



    [Range(0f, 1f)]
    public float parameter = 0f;

    public float[] dist_change = new float[3];

    public Quaternion[] h_q;
    public Vector3[] h_offfest;

    public float min_z_offset_time = 0.9f;
    public float max_z_offset_time = 1.6f;
    public float max_z_offset = 0.3f;

    public Vector3[] h_prev_z_offset;
    public Vector3[] h_curr_z_offset;
    public float[] h_time_z_offset;
    public float[] h_max_time_z_offset;
    public float[] h_offset;
    public bool debug_mode = false;
    //public Quaternion q2 = new Quaternion();
    //public Quaternion q3 = new Quaternion();

    public float max_speed_per_second = 2.8f;

    public float current_angle = 0f;
    public float max_angle = 0f;
    public float min_angle = 0f;
    public Vector3 center;
    public float current_dist = 0f;
    public float min_dist = 0f;
    [Range(0.001f, 1f)]
    public float angle_change = 0.2f;

    public GameObject player;
    public Vector3 player_offset;

    public Vector3 offset_to_hide = new Vector3();

    [Range(0f, 3f)]
    public float scare_distance = 1.2f;


    public Vector3 angle_offset = new Vector3();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ini()
    {
        h_q = new Quaternion[hands_group.Length];
        h_offfest=new Vector3[hands_group.Length];
        h_prev_z_offset = new Vector3[hands_group.Length];
        h_curr_z_offset = new Vector3[hands_group.Length];
        h_time_z_offset = new float[hands_group.Length];
        h_max_time_z_offset = new float[hands_group.Length];
        h_offset = new float[hands_group.Length];
    }

    // Update is called once per frame
    void Update()
    {

        if (player == null || !player.activeSelf)
            return;

        //position
        //Vector3 target_point = player.transform.position+ player_offset;
        //Vector3 raw_pos = target_point;
        //target_point.y = transform.position.y;
        //float dist= Vector3.Distance(transform.position, target_point);
        //float spd_change = max_speed_per_second * Time.deltaTime;
        //if (dist < spd_change)
        //    spd_change = dist;
        //angle
        float current_player_angle;
        {
            Vector3 p_pos = player.transform.position;
            Vector3 c_pos = center;
            p_pos.y = 0f;
            c_pos.y = 0f;
            //Debug.Log(Quaternion.LookRotation(p_pos, c_pos).eulerAngles);
            current_player_angle = Quaternion.LookRotation(p_pos, c_pos).eulerAngles.y;
        }
        
        //float current_player_distance = dist;
        //if (current_player_distance < min_dist)
        //    current_player_distance = min_dist;
        current_dist = min_dist;


        Vector3 pos_offset = new Vector3();
        pos_offset.x = (offset_to_hide.x+ current_dist) * Mathf.Sin(current_angle);
        pos_offset.z = (offset_to_hide.x + current_dist) * Mathf.Cos(current_angle);
        pos_offset.y = offset_to_hide.y;
        Vector3 pos = transform.position + pos_offset;
        Quaternion q = new Quaternion();
        q.eulerAngles = angle_offset;
        q.eulerAngles = new Vector3(q.eulerAngles.x, q.eulerAngles.y + Mathf.Rad2Deg * current_angle, q.eulerAngles.z);
        transform.position = center + pos_offset;
        transform.rotation = q;

        //change
        //float sine_time = Mathf.Sin((Time.time / time_sin_mod) + time_rnd);
        //if (parameter + znak * change > 1f)
        //    parameter = 1f;
        //else
        //    if (parameter + znak * change < 0f)
        //    parameter = 0f;
        //    else
        //        parameter += znak * change;

        //transform.position = Vector3.Lerp(target_point,transform.position,spd_change/dist);

        #region set_local_val

        Vector3 pl_pos = player.transform.position;
        pl_pos.y = 0f;


        for(int i=0;i<hands_group.Length;i++)
        {
            Vector3 h_pos1 = hands_group[i].transform.position;
            h_pos1.y = 0f;
            float temp_dist = Vector3.Distance(pl_pos, h_pos1);
            float distance_stuck = scare_distance;
            if (temp_dist < distance_stuck)
                h_offset[i] += (distance_stuck - temp_dist) * 0.125f;
            else
                h_offset[i] = Mathf.Lerp(h_offset[i],0f,0.08f);


            hands_group[i].transform.localRotation = h_q[i];
            h_time_z_offset[i] -= Time.deltaTime;

            if(h_time_z_offset[i]<0)
            {
                
                h_time_z_offset[i] = Mathf.Lerp(min_z_offset_time, max_z_offset_time,Random.value);
                h_max_time_z_offset[i] = h_time_z_offset[i];
                h_prev_z_offset[i] = h_curr_z_offset[i];
                Vector3 temp_z = new Vector3();
                temp_z.z = temp_z.z +( max_z_offset*(Random.value*2f-1f));



                h_curr_z_offset[i] = temp_z;
            }
            float t = 1f - (h_time_z_offset[i] / h_max_time_z_offset[i]);

            Vector3 start_point = h_prev_z_offset[i];
            start_point.z+= h_offset[i];
            Vector3 end_point = h_curr_z_offset[i];
            end_point.z += h_offset[i];

            Vector3 current_z_offset = Vector3.Lerp(start_point, end_point, t);
            if (debug_mode && i==0)
            {
                Debug.Log("t="+t);
                Debug.Log("prev="+ h_prev_z_offset[i]);
                Debug.Log("current=" + h_curr_z_offset[i]);
            }
            

            hands_group[i].transform.localPosition = h_offfest[i]+ current_z_offset;
        }

        


        //hands_group1.GetComponent<hands>().anim_parameter = parameter + (1f - parameter) * sine_time * sine_anim_str;
        //hands_group2.GetComponent<hands>().anim_parameter = parameter + (1f - parameter) * sine_time * sine_anim_str;
        //hands_group3.GetComponent<hands>().anim_parameter = parameter + (1f - parameter) * sine_time * sine_anim_str;

        //hands_group1.GetComponent<hands>().hands_close_parameter = parameter + (1f - parameter) * sine_time * sine_move_str;
        //hands_group2.GetComponent<hands>().hands_close_parameter = parameter + (1f - parameter) * sine_time * sine_move_str;
        //hands_group3.GetComponent<hands>().hands_close_parameter = parameter + (1f - parameter) * sine_time * sine_move_str;
        #endregion
    }
}
