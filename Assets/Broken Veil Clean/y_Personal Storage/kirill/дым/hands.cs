using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hands : MonoBehaviour
{
    public float set_parameters
    {
        set 
        {
            anim_parameter = value;
            hands_close_parameter = value;
        }
        get
        {
            return (anim_parameter + hands_close_parameter) / 2f;
        }
    }

    public Animator anim;
    public GameObject L_hand;
    public GameObject R_hand;
    [Range(0f,1f)]
    public float anim_parameter=0f;
    [Range(0f, 1f)]
    public float hands_close_parameter = 0f;
    [Space]
    public Vector3 r_hand_close_offset = new Vector3();
    public Vector3 r_hand_mid_offset = new Vector3();
    public Vector3 r_hand_far_offset = new Vector3();
    [Space]
    public Vector3 l_hand_close_offset = new Vector3();
    public Vector3 l_hand_mid_offset = new Vector3();
    public Vector3 l_hand_far_offset = new Vector3();


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        anim.enabled = true;        
        anim.Play("default",-1, anim_parameter);
        anim.Update(0);
        anim.enabled = false;

        Vector3 fin_l = new Vector3();
        Vector3 fin_r = new Vector3();
        calc_offset(out fin_l, out fin_r);

        R_hand.transform.localPosition = fin_r + R_hand.transform.localPosition;
        L_hand.transform.localPosition = fin_l + L_hand.transform.localPosition;
    }

    public void calc_offset(out Vector3 L,out Vector3 R)
    {
        Vector3 fin_l = new Vector3();
        Vector3 fin_r = new Vector3();
        if (hands_close_parameter<0.5f)
        {
            float mid_p = hands_close_parameter * 2f;
            float far_p = 1f - (hands_close_parameter * 2f);
            fin_r = r_hand_mid_offset * mid_p + r_hand_far_offset * far_p;
            fin_l = l_hand_mid_offset * mid_p + l_hand_far_offset * far_p;
        }
        else
        {
            float close_p = ((hands_close_parameter * 2f) - 1f);
            float mid_p = (2f - (hands_close_parameter * 2f));
            fin_r = r_hand_close_offset * close_p + r_hand_mid_offset * mid_p;
            fin_l = l_hand_close_offset * close_p + l_hand_mid_offset * mid_p;
        }
        L = fin_l;
        R = fin_r;
    }
}
