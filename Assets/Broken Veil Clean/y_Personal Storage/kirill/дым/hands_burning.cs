using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class hands_burning : MonoBehaviour
{
    public GameObject hands_obj;
    public GameObject center;
    public Vector3 rotation_offset = new Vector3();
    public float hands_long_offset = 0f;

    public float max_dist = 0.98f;

    public GameObject debug_end;
    public GameObject debug_start;

    public ParticleSystem ps;
    public float particles_rate_multi = 1f;
    // Start is called before the first frame update
    void Start()
    {
        ps = gameObject.GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = hands_obj.transform.rotation*Quaternion.Euler(rotation_offset);


        Vector3 v3 = hands_obj.transform.position;
        v3.y = center.transform.position.y;
        float dist = Vector3.Distance(v3,center.transform.position);
        set_particles_parameters(dist+ hands_long_offset,max_dist);


    }

    public void set_particles_parameters(float start,float end)
    {
        float length=end-start;
        //Debug.Log("end="+end+" start="+start+" length="+ length);
        if (length < 0)
            length = 0f;
        EmissionModule emis = ps.emission;
        emis.rateOverTime = length / 0.32f* particles_rate_multi;
        ShapeModule shape = ps.shape;
        //shape.position=new Vector3(0f,(start+end)/ 2f,0f);
        shape.scale = new Vector3(shape.scale.x,length, shape.scale.z);


        if (debug_end != null)
            debug_end.transform.localPosition =new Vector3(debug_end.transform.localPosition.x, debug_end.transform.localPosition.y,end);
        if (debug_start != null)
            debug_start.transform.localPosition = new Vector3(debug_start.transform.localPosition.x, debug_start.transform.localPosition.y,start );


    }
}
