using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laser_rotate : MonoBehaviour
{
    public float max_angle = 1f;
    public bool axis_x = true;
    public bool axis_y = true;
    public bool axis_z = true;
    public float time_interval = 0.5f;
    public float time = 0f;
    public Vector3 last_rotate = new Vector3();
    public Vector3 new_rotate = new Vector3();
    public GameObject GO;
    // Start is called before the first frame update
    void Start()
    {
        new_rotate = calc_new_rotation();
    }

    // Update is called once per frame
    void Update()
    {
        clamp_time();
        Vector3 v =Vector3.Lerp(last_rotate,new_rotate, (time/time_interval));
        GO.transform.rotation = Quaternion.Euler(v);
        time += Time.deltaTime;
    }

    void clamp_time()
    {
        if (time > time_interval)
        {
            time -= time_interval;
            clamp_time();
            last_rotate = new_rotate;
            new_rotate = calc_new_rotation();
        }
    }

    Vector3 calc_new_rotation()
    {
        Vector3 v = new Vector3();
        if (axis_x)
            v.x = Mathf.Lerp(max_angle, -max_angle, Random.value);
        if (axis_y)
            v.y = Mathf.Lerp(max_angle, -max_angle, Random.value);
        if (axis_z)
            v.z = Mathf.Lerp(max_angle, -max_angle, Random.value);
        return v;
    }
}
