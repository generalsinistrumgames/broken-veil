using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class terrain_prefabs : MonoBehaviour
{
    public GameObject parent;
    public Terrain terrain;

    //public
    TreeInstance[] ti = new TreeInstance[0];
    public int i=0;
    public bool work = false;
    [Range(1,500)]
    public int obj_per_frame = 30;
    public int debug_max = 0;
    // Start is called before the first frame update
    void Start()
    {
        i = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
        debug_max = ti.Length;
        if (!work)
            return;
        
        if (ti.Length == 0)
            ti = terrain.terrainData.treeInstances;
        if(ti.Length == 0)
        {
            work = false;
            i = 0;
            return;
        }
        int max = Mathf.Min(i+ obj_per_frame, ti.Length);

        Vector3 terrain_size = new Vector3();
        terrain_size = terrain.terrainData.size;

        for (;i< max; i++)
        {
            GameObject prefab = terrain.terrainData.treePrototypes[ti[i].prototypeIndex].prefab;
            Vector3 pos = ti[i].position;
            pos = new Vector3(terrain_size.x*pos.x,terrain_size.y*pos.y,terrain_size.z*pos.z);
            GameObject temp= Instantiate(prefab, pos, Quaternion.Euler(0f, ti[i].rotation,0f), parent.transform);
            temp.transform.localScale=new Vector3(ti[i].widthScale,ti[i].heightScale, ti[i].widthScale);
        }

        if(i==ti.Length)
        {
            parent.transform.position = terrain.transform.position;
            i = 0;
            work = false;
        }

    }
}
