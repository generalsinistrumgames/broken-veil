﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class terrain_to_mesh : MonoBehaviour
{
    public Terrain terrain_target;
    public GameObject corner1;
    public GameObject corner2;
    public Vector2 resolution = new Vector2(128f, 128f);
    public bool work = false;
    public string mesh_name = "new_mesh";

    private Vector3[] newVertices;
    private Vector2[] newUV;
    private int[] newTriangles;

    //private int resolution = 128;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //    if (work)
        //        work = false;
        //    else
        //        return;
        //    Terrain terrain = terrain_target;
        //    //terrain.terrainData.treeInstances
        //    if (terrain)
        //    {
        //        int resolution_x = Mathf.RoundToInt(resolution.x);               // длина в точках
        //        int resolution_z = Mathf.RoundToInt(resolution.y);               // ширина в точках
        //                                                              // Длинна, ширина террейна
        //                                                              //float width = terrain.terrainData.heightmapWidth * terrain.terrainData.heightmapScale.x;
        //                                                              //float length = terrain.terrainData.heightmapWidth * terrain.terrainData.heightmapScale.z;

        //        Vector2 area_1 = new Vector2(corner1.transform.position.x, corner1.transform.position.z);
        //        Vector2 area_2 = new Vector2(corner2.transform.position.x, corner2.transform.position.z);

        //        if (area_2.x < area_1.x)
        //        {
        //            float temp = area_2.x;
        //            area_2.x = area_1.x;
        //            area_1.x = temp;
        //        }

        //        if (area_2.y < area_1.y)
        //        {
        //            float temp = area_2.y;
        //            area_2.y = area_1.y;
        //            area_1.y = temp;
        //        }

        //        float height = 0f;
        //        Vector3 terrainPos = terrain.transform.position;

        //        newVertices = new Vector3[resolution_x * resolution_z];
        //        newTriangles = new int[resolution_x * resolution_z * 2 * 3];        // 2 - два треугольника в каждой ячейке, 3 - три точки для каждого треугольника
        //        int tri = 0;    // счетчик для массива треугольников

        //        newUV = new Vector2[resolution_x * resolution_z];
        //        float uvStepL = 1f / (resolution_x - 1);
        //        float uvStepW = 1f / (resolution_z - 1);

        //        for (int i_x = 0; i_x < resolution_x; i_x++)     // цикл в длину
        //        {
        //            for (int j_z = 0; j_z < resolution_z; j_z++)     // цикл в ширину
        //            {
        //                Vector3 pos = new Vector3(Mathf.Lerp(area_1.x,area_2.x,  (float)(i_x)/ (float)resolution_x), 0, Mathf.Lerp(area_1.y, area_2.y, (float)j_z / (float)resolution_z));
        //                // Получаем высоту в точке
        //                height = terrain.SampleHeight(pos);
        //                pos.y = height;
        //                // Создаем вершины меша
        //                newVertices[i_x * resolution_z + j_z] = pos;
        //                // Задаем текстурные координаты точек

        //                newUV[i_x * resolution_z + j_z] = new Vector2(uvStepL * i_x, uvStepW * j_z);

        //                // С каждой следующей точкой задаем 2 треугольника
        //                // 0   1
        //                // +---+
        //                // |  /|
        //                // | / |
        //                // |/  |
        //                // +---+(i,j)
        //                // 2   3
        //                if (i_x > 0 && j_z > 0)     // кроме крайних начальных точек
        //                {
        //                    int point0 = (i_x - 1) * resolution_z + j_z - 1,
        //                        point1 = (i_x - 1) * resolution_z + j_z,
        //                        point2 = i_x * resolution_z + j_z - 1,
        //                        point3 = i_x * resolution_z + j_z;

        //                    newTriangles[tri + 0] = point0;    // 0
        //                    newTriangles[tri + 1] = point1;              // 1
        //                    newTriangles[tri + 2] = point2;    // 2
        //                    tri += 3;         

        //                    newTriangles[tri + 0] = point1;              // 1
        //                    newTriangles[tri + 1] = point3;              // 3
        //                    newTriangles[tri + 2] = point2;    // 2
        //                    tri += 3;

        //                }
        //            }
        //        }

        //        // Создаем меш
        //        Mesh mesh = new Mesh();
        //        mesh.Clear();
        //        mesh.vertices = newVertices;
        //        mesh.triangles = newTriangles;
        //        mesh.uv = newUV;
        //        mesh.RecalculateNormals();


        //        mesh.RecalculateBounds();
        //        mesh.Optimize();

        //        // Создаем го и присоединяем меш
        //        GameObject goMesh = new GameObject();
        //        goMesh.name = mesh_name;
        //        MeshFilter meshFilter = goMesh.AddComponent<MeshFilter>();
        //        meshFilter.mesh = mesh;
        //        goMesh.AddComponent<MeshRenderer>();
        //        AssetDatabase.CreateAsset(mesh, "Assets\\Broken Veil Clean\\y_Personal Storage\\kirill\\tests\\" + mesh_name + ".asset");
    }
}