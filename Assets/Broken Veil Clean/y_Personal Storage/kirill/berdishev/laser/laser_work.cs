﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laser_work : MonoBehaviour
{
    [Range(2,100)]
    public int max_rays = 10;
    public float max_distance = 100f;
    //public string[] ignored_layers = { "Ignore Raycast", "Laser_Transparent" };
    [SerializeField]
    public LayerMask layermask = int.MaxValue;
    public Material def_mat;
    public LineRenderer line;
    public Light original;
    public Light[] lights;
    public RaycastHit hitRay;

    public Vector3 end_point;
    //public GameObject[] reflection_objects;

    // Start is called before the first frame update
    void Start()
    {
        //layermask = LayerMask.GetMask(ignored_layers);
        //layermask = int.MaxValue ^ layermask;
        //   1 << LayerMask.GetMask("Laser_Transparent");
        lights = new Light[100];
        for(int i=0;i<lights.Length;i++)
        {
            lights[i] = Instantiate<Light>(original,this.transform);
            lights[i].enabled = false;
        }
        original.enabled = false;


        //Material m = Instantiate(def_mat);
        ////Color c = ;
        ////Shader sh = m.shader;
        ////m.EnableKeyword("Color_898E5729");
        ////m.SetColor("Color_898E5729", Color.blue);
        ////m.EnableKeyword("main_color");
        //m.SetFloat("Vector1_E2EB2796",50f);
        //m.SetFloat("start_point", 50f);
        //m.SetFloat("_start_point", 50f);
        ////m.SetColor("start point", Color.blue);
        ////m.SetColor("main_color", Color.blue);
        //go.GetComponent<MeshRenderer>().materials[0] = m;

    }

    // Update is called once per frame
    void Update()
    {
        Calc_rays();
        //Set_game_objs();
        //Set_rotation_to_cam();
    }



    public void Calc_rays()
    {
        RaycastHit hit = new RaycastHit();
        bool work = true;
        int ray_count = 0;
        float dist = 0f;
        Vector3 origin = transform.position;
        Vector3 direction = transform.forward;
        line.positionCount = max_rays+1;
        line.SetPosition(0, origin);
        for (int i = 0; i < lights.Length; i++)
            if (lights[i].enabled)
                lights[i].enabled = false;
            else
                i = lights.Length;
        lights[0].transform.position = origin + direction * 0.05f;
        lights[0].enabled = true;

        while (work)
        {
            ray_count++;
            //Physics.Raycast(new Ray(origin, direction), out hit, max_distance-dist, layermask);
            //if (hit.collider?.gameObject != null)
            if (Physics.Raycast(new Ray(origin, direction), out hit, max_distance - dist, layermask))
            {
                end_point = origin + dist * direction;

                Debug.DrawRay(origin, direction * hit.distance, get_col(ray_count));
                dist += hit.distance;

                direction = Vector3.Reflect(direction, hit.normal);
                origin = hit.point;
                hitRay = hit;
                end_point = origin;

                line.SetPosition(ray_count, origin);

                lights[ray_count].enabled = true;
                lights[ray_count].transform.position = origin + hit.normal * 0.05f;

                bool is_reflect = false;
                laser_reflect temp;
                if(hit.transform.gameObject.TryGetComponent<laser_reflect>(out temp))
                    is_reflect = true;

                //if (reflection_objects.Length != 0)
                //    for (int i = 0; i < reflection_objects.Length; i++)
                //        if (hit.transform == reflection_objects[i].transform)
                //            is_reflect = true;

                if (!is_reflect)
                {
                    work = false;
                    line.positionCount = ray_count + 1;
                }
            }
            else
            {
                Debug.DrawRay(origin, direction * (max_distance - dist), get_col(ray_count));
                dist += max_distance;
                line.positionCount = ray_count + 1;
                end_point = origin + dist * direction;
                line.SetPosition(ray_count, end_point);
            }

            if (max_rays <= ray_count)
            {
                work = false;
                //Debug.Log("ray_count=" + ray_count);
            }
            if (max_distance <= dist)
            {
                work = false;
                //Debug.Log("dist=" + dist);
            }
        }
        //line.SetPosition(ray_count, origin);
        //Debug.Log("i=" + ray_count + " v3=" + origin);
        hit = new RaycastHit();
    }

    public Color get_col(int ray_count)
    {
        if (ray_count % 3 == 0)
            return Color.blue;
        if (ray_count % 3 == 1)
            return Color.yellow;
        return Color.green;
    }
    public void Set_game_objs()
    {

    }
    public void Set_rotation_to_cam()
    {

    }

    //public void Do_some_laser_magic()
    //{
    //    //RaycastHit hit = new RaycastHit();
    //    ////float directionX = Mathf.Sign(velocity.x);
    //    //////float rayLenght = Mathf.Abs(velocity.x) + skinWidth;
    //    ////var direction = new Vector3(velocity.x, 0, velocity.z);// вариант с направлением без оси Y
    //    //for (int i = 0; i < horizontalRayCount; i++)
    //    //{
    //    //    Vector3 rayOrigin = raycastOrigin.top;
    //    //    rayOrigin -= Vector3.up * (horizontalRaySpacing * i);
    //    //    Physics.Raycast(new Ray(rayOrigin, transform.forward), out hit, distanceHorizontalRay);
    //    //    Debug.DrawRay(rayOrigin, transform.forward * distanceHorizontalRay, Color.green);

    //    //    if (hit.collider?.gameObject != null)
    //    //    {
    //    //        rays.commonCollisionForward = true;
    //    //    }
    //    //}
    //}
}
