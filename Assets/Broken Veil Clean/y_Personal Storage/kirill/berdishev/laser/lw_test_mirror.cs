﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lw_test_mirror : MonoBehaviour
{
    public GameObject from;
    public GameObject to;
    public GameObject center;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 local_offset = gameObject.transform.position;
        Vector3 local_normal= (gameObject.transform.rotation * Vector3.forward) * -0.2f;
        Vector3 local_from = local_offset - from.transform.position;
        Vector3 local_to=Vector3.Reflect(local_from,local_normal);
        //to.transform.position = mirror(from.transform.position,gameObject.transform.position);
        to.transform.position = local_to+local_offset;
        center.transform.position = local_offset + local_normal;
        //Vector3.
    }

    public Vector3 mirror(Vector3 target_v,Vector3 norm_mirror_center)
    {
        Vector3 v = target_v - norm_mirror_center;
        v = norm_mirror_center + new Vector3(v.x * -1f, v.y * -1f, v.z);
        return v;
    }
}
