﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hide_in_playmode : MonoBehaviour
{
    public hide_in_playmode core=null;
    public bool hide = false;
    public MeshRenderer render = null;

    bool last_state = false;
    // Start is called before the first frame update
    void Start()
    {
        MeshRenderer temp;
        if (TryGetComponent<MeshRenderer>(out temp))
            render = temp;
        else
            return;
        if (core != null)
            hide = core.hide;
        if (hide == render.enabled)
            render.enabled = !hide;
        last_state = hide;
    }

    // Update is called once per frame
    void Update()
    {  
        if (render == null)
            return;
        if (core != null)
            hide = core.hide;
        if (hide != last_state)
            render.enabled = !hide;
        last_state = hide;
    }
}
