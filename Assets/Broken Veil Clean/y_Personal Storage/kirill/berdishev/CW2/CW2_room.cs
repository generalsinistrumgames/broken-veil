﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW2_room : MonoBehaviour
{   
    const string room_cube_name = "room cube";
    public GameObject _room_cube;
    public BoxCollider room_cube { get => _room_cube.GetComponent<BoxCollider>(); }

    const string cam_cube_name = "cam cube";
    public GameObject _cam_cube;
    public BoxCollider cam_cube { get => _cam_cube.GetComponent<BoxCollider>(); }

    const string smooth_layer_name = "smooth layer";
    public GameObject _smooth_layer;
    public CW2_smooth_layer smooth_layer { get => _smooth_layer.GetComponent<CW2_smooth_layer>(); }

    public List<CW2_transition> transition_list = new List<CW2_transition>();

    public CW2_points points = new CW2_points();

    public void Ini(CW2_error_handler _error_handler)
    {
        #region check_critical_errors

        #region find gameobjects
        Transform _temp;
        //------------find room cube-------------
        if (!CW2_utility.Try_find_child(room_cube_name, out _temp, transform))
            _error_handler.Add(true, "CW2_room: critical error -> cannot find room cube, room name=" + this.name);
        else
            _room_cube = _temp.gameObject;

        //------------find cam cube-------------
        if (!CW2_utility.Try_find_child(cam_cube_name, out _temp, transform))
            _error_handler.Add(true, "CW2_room: critical error -> cannot find cam cube, room name=" + this.name);
        else
            _cam_cube = _temp.gameObject;

        //------------find smooth layer--------
        if (!CW2_utility.Try_find_child(smooth_layer_name, out _temp, transform))
            _error_handler.Add(true, "CW2_room: critical error -> cannot find smooth layer, room name=" + this.name);
        else
            _smooth_layer = _temp.gameObject;

        #endregion

        #region colliders
        if (_room_cube != null)
        {
            BoxCollider bx;
            if (_room_cube.TryGetComponent<BoxCollider>(out bx))
                ;
            else
                _error_handler.Add(true, "CW2_room: critical error -> cannot find box collider in room cube, room name=" + this.name);
        }

        if (_cam_cube != null)
        {
            BoxCollider bx;
            if (_cam_cube.TryGetComponent<BoxCollider>(out bx))
                ;
            else
                _error_handler.Add(true, "CW2_room: critical error -> cannot find box collider in cam cube, room name=" + this.name);
        }

        #endregion

        #endregion

        #region check_and_fix_non_critical_errors

        #region rotation
        if (_room_cube != null)
            if (Mathf.Abs(Quaternion.Angle(_room_cube.transform.rotation, Quaternion.Euler(0, 0, 0))) > 0.01f)
            {
                _error_handler.Add(false, "CW2_room: non-critical error -> wrong rotation on room cube. room name=" + this.name);
                _room_cube.transform.rotation = Quaternion.Euler(0, 0, 0);
            }

        if (_cam_cube != null)
            if (Mathf.Abs(Quaternion.Angle(_cam_cube.transform.rotation, Quaternion.Euler(0, 0, 0))) > 0.01f)
            {
                _error_handler.Add(false, "CW2_room: non-critical error -> wrong rotation on cam cube. room name=" + this.name);
                _cam_cube.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        #endregion

        #region scale
        if (_room_cube != null)
            if (_room_cube.transform.lossyScale!=new Vector3(1f,1f,1f))
                _error_handler.Add(false, "CW2_room: non-critical error -> wrong scale on room cube. room name=" + this.name+". fix this error pls");

        if (_cam_cube != null)
            if (_cam_cube.transform.lossyScale != new Vector3(1f, 1f, 1f))
                _error_handler.Add(false, "CW2_room: non-critical error -> wrong scale on cam cube. room name=" + this.name + ". fix this error pls");
        #endregion

        #region colliders off

        if (_room_cube != null)
        {
            BoxCollider bx;
            if (_room_cube.TryGetComponent<BoxCollider>(out bx))
                bx.enabled = false;
        }

        if (_cam_cube != null)
        {
            BoxCollider bx;
            if (_cam_cube.TryGetComponent<BoxCollider>(out bx))
                bx.enabled = false;
        }

        #endregion

        #endregion

        return;
    }
}
