using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class cutscene_dof : MonoBehaviour
{
    public UnityEngine.Playables.PlayableDirector cutscene_timeline;
    public CinemachineVirtualCamera virt_cam;

    public bool added = false;
    // Start is called before the first frame update
    void Start()
    {
        if(cutscene_timeline==null)
        {
            UnityEngine.Playables.PlayableDirector tl;
            if (gameObject.TryGetComponent<UnityEngine.Playables.PlayableDirector>(out tl))
                cutscene_timeline = tl;

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (cutscene_timeline == null)
            return;
        if (cutscene_timeline.state == UnityEngine.Playables.PlayState.Playing)
        {
            if(!added)
            {
                CW2_Main cw2 = GameObject.FindObjectOfType<CW2_Main>();
                cw2.cutscenes.Add(this);
                added = true;
            }
        }
        else
            added = false;
    }

    public float get_distance()
    {
        return Vector3.Distance(virt_cam.m_LookAt.position, virt_cam.m_Follow.position);
    }
}
