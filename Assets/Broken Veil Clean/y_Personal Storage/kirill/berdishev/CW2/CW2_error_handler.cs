﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW2_error_handler : MonoBehaviour
{
    public int critical_bugs = 0;
    public int non_critical_bugs = 0;

    public List<string> str_critical_bugs = new List<string>();
    public List<string> str_non_critical_bugs = new List<string>();

    public void Add(bool critical,string str)
    {
        if (critical)
        {
            critical_bugs++;
            str_critical_bugs.Add(str);
            Debug.LogError(str);
        }
        else
        {
            non_critical_bugs++;
            str_non_critical_bugs.Add(str);
            Debug.LogWarning(str);
        }
    }

}
