﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW2_cam_shake : MonoBehaviour
{
    public CW2_points out_points=new CW2_points();

    public Vector2 plane_change = new Vector2();
    public CW2_axis axis1 = CW2_axis.x;
    public CW2_axis axis2 = CW2_axis.y;
    //public GameObject target;
    //public GameObject final;

    public float intencity = 0f;

    [Range(0,0.2f)]
    public float intencity_change = 0.05f;

    [Range(0, 0.2f)]
    public float rotation_change_plus=0.12f;
    [Range(0, 0.2f)]
    public float rotation_change_minus=0.075f;

    [Range(0.02f, 1f)]
    public float distance_mod = 0.2f;
    [Range(0.02f, 1f)]
    public float distance_mod_flat = 0.5f;
    [Range(0.5f, 60f)]
    public float rotation_mod = 6f;

    [Range(0, 0.2f)]
    public float distance_change_plus=0.12f;
    [Range(0, 0.2f)]
    public float distance_change_minus = 0.075f;

    public float rotation_speed = 0f;
    public float distance = 0f;
    public float rotarion = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    //void Update()
    //{
    //    CW2_points p = new CW2_points();
    //    p.cam_pos = transform.position;
    //    p.room_pos = target.transform.position;
    //    calc(p);
    //    final.transform.position = out_points.cam_pos;
    //}

    public void calc(CW2_points points)
    {
        set_values(points);
        set_points(points);
    }

    void set_values(CW2_points points)
    {
        if (rotation_speed > intencity)
            rotation_speed = Mathf.Lerp(rotation_speed, intencity, rotation_change_minus);
        else
            rotation_speed = Mathf.Lerp(rotation_speed, intencity, rotation_change_plus);

        if (distance > intencity)
            distance = Mathf.Lerp(distance, intencity, distance_change_minus);
        else
            distance = Mathf.Lerp(distance, intencity, distance_change_plus);

        rotarion += rotation_speed * Time.deltaTime* rotation_mod;

        intencity = Mathf.Lerp(intencity, 0f, intencity_change);

        float distance_current = Vector3.Distance(points.cam_pos,points.room_pos)*distance*distance_mod 
            +
            distance_mod_flat*distance;

        plane_change = new Vector2(
            distance_current * Mathf.Cos(rotarion),
            distance_current * Mathf.Sin(rotarion)
            );
    }
    void set_points(CW2_points points)
    {
        Vector3 offset = Vector3.zero;

        if (axis1 == CW2_axis.x)
            offset.x += plane_change.x;
        if (axis1 == CW2_axis.y)
            offset.y += plane_change.x;
        if (axis1 == CW2_axis.z)
            offset.z += plane_change.x;

        if (axis2 == CW2_axis.x)
            offset.x += plane_change.y;
        if (axis2 == CW2_axis.y)
            offset.y += plane_change.y;
        if (axis2 == CW2_axis.z)
            offset.z += plane_change.y;

        CW2_points result = new CW2_points();
        result.room_pos = points.room_pos;
        result.cam_pos = points.cam_pos + offset;
        out_points = result;
    }
}
