﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kostil : MonoBehaviour
{

    public GameObject player;
    public GameObject point_of_teleport;
    public KeyCode teleport_key = KeyCode.Y;
    public BoxCollider debug_collider; 
    public CW2_Main debug_cam_obj;
    private bool first_frame = true;

    private void Start()
    {
        TryGetComponent<BoxCollider>(out debug_collider);
        debug_cam_obj = GameObject.FindObjectsOfType<CW2_Main>()[0];
        debug_collider.enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        if(first_frame)
        {
            first_frame = false;
            Player[] temp1= GameObject.FindObjectsOfType<Player>();
            if (temp1.Length != 0)
                player = temp1[0].gameObject;
        }

        if(Input.GetKeyDown(teleport_key))
            if(true)
            {
                if(player==null)
                {
                    Player[] temp1 = GameObject.FindObjectsOfType<Player>();
                    if (temp1.Length != 0)
                        player = temp1[0].gameObject;
                }
                float temp_dist = Vector3.Distance(debug_cam_obj.CRT_box_collider_clamp(debug_collider, player.transform.position), player.transform.position);
                if (temp_dist < 0.01f)
                    player.transform.position = point_of_teleport.transform.position;
            }
    }
}
