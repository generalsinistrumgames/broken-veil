﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW2_transition_holder : MonoBehaviour
{
    public List<CW2_transition> transitions = new List<CW2_transition>();


    public void Ini(CW2_error_handler _error_handler)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform tr = transform.GetChild(i);
            CW2_transition trans;
            if (tr.gameObject.TryGetComponent<CW2_transition>(out trans))
                transitions.Add(trans);
        }

        for (int i = 0; i < transitions.Count; i++)
            transitions[i].Ini(_error_handler);

        return;
    }
}
