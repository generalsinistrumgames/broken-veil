using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class DOF_data : MonoBehaviour
{
    public dof_quality_settings quality = dof_quality_settings.off;

    public bool on = false;

    [Range(3,8)]
    public int near_sample_count = 3;
    [Range(0f,8f)]
    public float near_radius = 0f;

    [Range(3, 16)]
    public int far_sample_count = 3;
    [Range(0f, 16f)]
    public float far_radius = 0f;

    public DepthOfFieldResolution resolution = DepthOfFieldResolution.Full;
    public bool high_quality_filtering = false;


    public void set(DepthOfField Dof)
    {
        Dof.active = on;
        if (!on)
            return;

        Dof.resolution = resolution;
        Dof.highQualityFiltering = high_quality_filtering;

        
        Dof.nearSampleCount = near_sample_count;
        Dof.nearMaxBlur = near_radius;
        Dof.farSampleCount = far_sample_count;
        Dof.farMaxBlur = far_radius;
    }

    public enum dof_quality_settings
    {
        off,
        low,
        high,
        ultra
    }
}
