using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using static DOF_data;

public class DOF_controller : MonoBehaviour
{
    public bool is_ini=false;
    public dof_quality_settings current_quality;
    private dof_quality_settings last_quality;
    public DOF_data current_data=null;
    public bool force_reset = false;
    public float distance=0f;
    public List<DOF_data> data = new List<DOF_data>();
    public List<GameObject> dof_sources=new List<GameObject>();
    public List<Volume> volumes = new List<Volume>();

    // Update is called once per frame
    void LateUpdate()
    {
        if (!is_ini)
            ini();
        if (current_quality != last_quality)
            force_reset = true;
        if(force_reset)
        {
            DepthOfField temp_v;
            for (int i = 0; i < volumes.Count; i++)
                if (volumes[i].TryGetComponent<DepthOfField>(out temp_v))
                    current_data.set(temp_v);
        }
    }

    private void ini()
    {
        data = new List<DOF_data>(gameObject.GetComponents<DOF_data>());
        int index = 0;
        for(int i=0;i< data.Count;i++)
        {
            if (data[i].quality == current_quality)
                index = i;
        }
        current_data = data[index];
        last_quality = data[index].quality;
        force_reset = true;
    }

    private void find_dof_sources()
    {

    }
}
