﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class CW2_smooth_layer : MonoBehaviour
{
    public Vector3 out_cam_target_pos;
    public Vector3 out_look_offset_pos;

    public readonly float[] weights= {24f,10f,7f,4f,2f,1.5f,1f };
    public Vector3[] positions_cam;
    public Vector3[] positions_tar;
    public int pos_count;
    public int max_count = 14;

    [Range(0.005f,0.16f)]
    public float settings_smooth_speed = 0.065f;
    //[Range(0f,0.5f)]
    //public float cam_delay = 0.2f;
    public bool settings_use_default_settings = true;



    const float std_timestep = 1f/60f;
    //const int pos_count= 40;
    //List<Vector3> last_pos;
    float last_time;




    public void Reset(Vector3 player_pos)
    {
        last_time = Time.time;
        out_cam_target_pos = player_pos;
        out_look_offset_pos = player_pos;
    }

    public void Upd(Vector3 current_player_pos, Vector3 current_cam_target_pos, Vector3 look_offset,Vector3 look_offset_cam_pos,float time)
    {
        const float std_timestep = 0.08333f;
        const float std_change = 0.24f;
        //if (settings_use_default_settings & default_settings > 0f)
        //    settings_smooth_speed = default_settings;
        float curr_t = time;
        Vector3 n_tar_pos;
        Vector3 n_cam_pos;
        if (time > std_timestep)
        {
            curr_t = std_timestep;
            n_tar_pos = Vector3.Lerp(current_cam_target_pos,current_player_pos, std_change);
            n_cam_pos = Vector3.Lerp(look_offset_cam_pos,look_offset, std_change);
            Upd(current_player_pos, current_cam_target_pos, look_offset, look_offset_cam_pos, time - std_timestep);
        }
        else
        {
            n_tar_pos = Vector3.Lerp(current_cam_target_pos, current_player_pos, (curr_t/std_timestep)* std_change);
            n_cam_pos = Vector3.Lerp(look_offset_cam_pos, look_offset, (curr_t / std_timestep) * std_change);
        }
        //Vector3 n_tar_pos = Vector3.Lerp(player_pos, cam_target_pos, Lerp_calc(delta_time));
        //Vector3 n_cam_pos = Vector3.Lerp(look_offset, look_offset_cam_pos,Lerp_calc(delta_time));

        

        //add_position(n_cam_pos, n_tar_pos);
        //get_weighted_pos(out n_cam_pos,out n_tar_pos);

        out_cam_target_pos = n_cam_pos;
        out_look_offset_pos = n_tar_pos;

        //out_cam_target_pos = Vector3.Lerp(player_pos, cam_target_pos, Lerp_calc(delta_time));
        //out_look_offset_pos = Vector3.Lerp(look_offset, look_offset_cam_pos, Lerp_calc(delta_time));
    }


    float get_weight(int index)
    {
        int inv_index = positions_cam.Length - 1 - index;
        if(inv_index < 0)
            return 0f;
        if (weights.Length <= inv_index)
            return weights[weights.Length - 1];
        return weights[inv_index];
    }

    float get_total_weight()
    {
        float f = 0f;
        for (int i = 0; i < positions_cam.Length; i++)
            f += get_weight(i);
        return f;
    }

    void get_weighted_pos(out Vector3 cam_pos,out Vector3 tar_pos)
    {
        Vector3 final_position_cam = new Vector3();
        Vector3 final_position_tar = new Vector3();
        float total_weight = get_total_weight();
        for (int i = 0; i < positions_cam.Length; i++)
        {
            float w = get_weight(i);
            final_position_cam += positions_cam[i] * w / total_weight;
            final_position_tar += positions_tar[i] * w / total_weight;
        }
        cam_pos = final_position_cam;
        tar_pos = final_position_tar;
    }

    void add_position(Vector3 pos_cam,Vector3 pos_tar)
    {
        List<Vector3> list_cam = new List<Vector3>();
        List<Vector3> list_tar = new List<Vector3>();
        for (int i = 0; i < positions_cam.Length; i++)
        {
            list_cam.Add(positions_cam[i]);
            list_tar.Add(positions_tar[i]);
        }
        list_cam.Add(pos_cam);
        list_tar.Add(pos_tar);
        if (positions_cam.Length > max_count)
        {
            list_cam.RemoveAt(0);
            list_tar.RemoveAt(0);
        }
        positions_cam = list_cam.ToArray();
        positions_tar = list_tar.ToArray();
    }

    //public void change_pos(Vector3 _new_pos)
    //{
    //    last_pos.RemoveAt(0);
    //    last_pos.Add(_new_pos);        
    //}

    private float Lerp_calc(float timestep)
    {
        if (timestep < 0.0001)
            return 0f;
        Debug.Log(Mathf.Pow(1f - settings_smooth_speed, timestep / std_timestep));
        return Mathf.Pow(1f- settings_smooth_speed, timestep/std_timestep);
    }
}
