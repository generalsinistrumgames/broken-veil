using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class activate_by_key : MonoBehaviour
{
    public KeyCode activate_key = KeyCode.K;
    public cw2_cutscene debug_cutscene;
    // Start is called before the first frame update
    void Start()
    {
        debug_cutscene = gameObject.GetComponent<cw2_cutscene>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(activate_key))
            debug_cutscene.debug_timeline.GetComponent<PlayableDirector>().Play();
    }
}
