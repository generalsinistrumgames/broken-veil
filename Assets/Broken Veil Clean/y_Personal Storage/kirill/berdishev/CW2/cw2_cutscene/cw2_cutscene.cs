using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using Cinemachine;

public class cw2_cutscene : MonoBehaviour
{
    public bool work = false;
    private bool last_work = false;

    public GameObject anim_cam_target;
    public GameObject anim_cam_position;
    public CinemachineVirtualCamera cm_vcam;

    [Space]

    public CW2_Main debug_cw2_main;

    public bool debug_is_ini = false;

    public GameObject debug_timeline_camera;
    public GameObject debug_cutscene_camera;
    public GameObject debug_timeline;
    // Start is called before the first frame update
    void Start()
    {
        debug_is_ini = false;
        if(debug_cutscene_camera!=null)
            debug_cutscene_camera.SetActive(false);
        if (debug_timeline_camera != null)
            debug_timeline_camera.SetActive(false);
        Camera temp;
        if (debug_cutscene_camera != null)
            if (debug_cutscene_camera.TryGetComponent<Camera>(out temp))
                temp.enabled = false;

        cm_vcam.m_LookAt = anim_cam_target.transform;
        cm_vcam.m_Follow = anim_cam_position.transform;

    }

    // Update is called once per frame
    void Update()
    {
        if (!debug_is_ini)
            ini();
        PlayableDirector temp = debug_timeline.GetComponent<PlayableDirector>();


        if (temp.state==PlayState.Playing)
        {
            work = true;           
        }
        else
        {
            work = false;
        }

        if (last_work != work)
        {
            if (work == true)
                Enable();
            else
                Disable();
        }
        last_work = work;
    }
    private void Disable()
    {
       // debug_cutscene_camera.SetActive(false);
        cm_vcam.gameObject.SetActive(false);
       // this.cw2_main_camera.enabled = true;
    }
    public void Enable()
    {
        //CinemachineBrain[] cb = GameObject.FindObjectsOfType<CinemachineBrain>();
        //for (int i = 0; i < cb.Length; i++)
        //    cb[i].m_DefaultBlend.m_Time = 0f;

        //cam_target.transform.position = cw2_cam_target.transform.position;
        //cam_position.transform.position = cw2_cam_position.transform.position;
        //debug_cutscene_camera.SetActive(true);
        cm_vcam.gameObject.SetActive(true);
        //this.cw2_main_camera.enabled = false; 
    }


    private void ini()
    {
        debug_is_ini = true;
    }
}
