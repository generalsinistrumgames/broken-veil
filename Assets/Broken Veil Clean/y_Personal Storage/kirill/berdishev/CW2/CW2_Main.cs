﻿using Cinemachine;
using GlobalScripts;
using System;
using System.Collections.Generic;
//using System.Diagnostics;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UIElements;

[Serializable]
public class CW2_points
{
    public Vector3 cam_pos;
    public Vector3 room_pos;

    public CW2_points()
    {

    }
    public CW2_points(Vector3 cam_pos_set, Vector3 room_pos_set)
    {
        cam_pos = cam_pos_set;
        room_pos = room_pos_set;
    }
}

public class cw2_data
{
    public Vector3 raw_player_pos;
    public Vector3 player_pos_modified;
    public CW2_points points_before_mod;
    public Vector2 look_offset;
    public CW2_points points_after_look_offset;
    public CW2_points points_after_shake;
    public CW2_points points_after_extrapolation;

}

public class CW2_Main : MonoBehaviour
{
    public static CW2_Main main;
    public bool work = true;
    bool ini = false;

    [Range(-1f,2f)]
    public float player_y_offset = 0.83f;
    public GameObject player_neck;
    public GameObject player;
    [SerializeField]
    private GameObject hidden_current;
    public GameObject current
    {
        get
        {
            return hidden_current;
        }
        set
        {
            CW2_room temp_room;
            CW2_transition temp_trans;
            if (hidden_current != null)
            {
                if (hidden_current.TryGetComponent<CW2_room>(out temp_room))
                    CRT_room_current_end(temp_room, player_neck.transform.position);
                if (hidden_current.TryGetComponent<CW2_transition>(out temp_trans))
                    CRT_transition_current_end(temp_trans, player_neck.transform.position);
            }

            hidden_current = value;

            if (hidden_current.TryGetComponent<CW2_room>(out temp_room))
                CRT_room_current_start(temp_room, player_neck.transform.position);
            if (hidden_current.TryGetComponent<CW2_transition>(out temp_trans))
                CRT_transition_current_start(temp_trans, player_neck.transform.position);
        }
    }
    public List<GameObject> actives = new List<GameObject>();
    public string[] way_to_Neck = new string[] {"Main", "DeformationSystem", "Root_M" , "Spine1_M", "Spine2_M", "Chest_M", "Neck_M" };

    [Space]


    const string room_holder_name = "room holder";
    [Header("debug values")]
    [SerializeField]
    public CW2_room_holder _room_holder;

    const string transition_holder_name = "transition holder";
    public CW2_transition_holder _transition_holder;

    const string cam_holder_name = "camera holder";
    public CW2_camera_holder _cam_holder;

    public CW2_smooth_layer _default_smooth_layer_settings;
    public CW2_error_handler _error_handler; 
    public CW2_roll _roll;
    public CW2_look_offset _look_offset;
    public CW2_cam_shake _shake;
    public CW2_depth_of_field _dof;
    public cw2_data data;

    public Vector3 last_cam_pos;
    public Vector3 last_cam_target;
    public kostil_look k_look;
    public kostil_traska k_traska;

    public List<cutscene_dof> cutscenes = new List<cutscene_dof>();

    public void work_on()
    {
        work = true;
    }


    public Vector3 out_cam_target_pos      //позиция, в которую смотрит камера   
    { get => _cam_holder.cam_target_pos; set => _cam_holder.cam_target_pos = value; }

    public Vector3 out_cam_pos             //позиция, где камера находится
    { get => _cam_holder.cam_pos; set => _cam_holder.cam_pos = value; }


    private GameObject main_next_current_object=null;

// Start is called before the first frame update
void Start()
    {
        main = this;
        if (work)
            Ini();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (work)
            Ini();
        else
            return;
        CRT_main_calc();
    }

    public void Ini()
    {
        if (ini)
            return;

        ini = true;
        _error_handler = GetComponent<CW2_error_handler>();
        _default_smooth_layer_settings = GetComponent<CW2_smooth_layer>();
        _roll = GetComponent<CW2_roll>();
        _look_offset = GetComponent<CW2_look_offset>();
        _shake = GetComponent<CW2_cam_shake>();
        _dof = GetComponent<CW2_depth_of_field>();

        #region check_critical_errors


        #region find_objects

        Transform _temp;
        //------------find room holder-------------

        if (!CW2_utility.Try_find_child(room_holder_name, out _temp, transform))
            _error_handler.Add(true, "CW2_main: critical error -> cannot find room holder");
        else
          if (!_temp.gameObject.TryGetComponent<CW2_room_holder>(out _room_holder))
            _error_handler.Add(true, "CW2_main: critical error -> cannot find CW2_room_holder class in room holder");

        if (!CW2_utility.Try_find_child(room_holder_name, out _temp, transform))
            _error_handler.Add(true, "CW2_main: critical error -> cannot find room holder");
        else
           if(!_temp.gameObject.TryGetComponent<CW2_room_holder>(out _room_holder))
                _error_handler.Add(true, "CW2_main: critical error -> cannot find CW2_room_holder class in room holder");

        //------------find cam_holder-------------
        if (!CW2_utility.Try_find_child(cam_holder_name, out _temp, transform))
            _error_handler.Add(true, "CW2_main: critical error -> cannot find cam_holder");
        else
           if (!_temp.gameObject.TryGetComponent<CW2_camera_holder>(out _cam_holder))
            _error_handler.Add(true, "CW2_main: critical error -> cannot find CW2_camera_holder class in cam_holder");

        //------------find transition holder-------------
        if (!CW2_utility.Try_find_child(transition_holder_name, out _temp, transform))
            _error_handler.Add(true, "CW2_main: critical error -> cannot find transition holder");
        else
           if (!_temp.gameObject.TryGetComponent<CW2_transition_holder>(out _transition_holder))
            _error_handler.Add(true, "CW2_main: critical error -> cannot find CW2_transition_holder class in _transition_holder");

        #endregion

        #endregion



        #region ini_other_objects
        _room_holder.Ini(_error_handler);
        _transition_holder.Ini(_error_handler);
        _cam_holder.Ini(_error_handler);
        _roll.cinemachine_cam = _cam_holder.virtual_camera;
        _roll.Ini(_error_handler);

        _dof.Ini(_error_handler);
        #endregion

        player_neck = find_player();
        current = find_closest_room().gameObject;
        recalculate_actives_current_room();

        if (player_neck == null)
        {
            Debug.Log("CW2: player dosnt find!");
            ini = false;
            return;
        }
        k_look = _cam_holder.GetComponent<kostil_look>();
        k_traska = _cam_holder.GetComponent<kostil_traska>();
    }


    #region CRT camera cube + room cube + transitions + utilitis


    CW2_points CRT_main_calc()
    {


        out_cam_pos = last_cam_pos;
        out_cam_target_pos = last_cam_target;

        //if (_cam_holder.extrapolate != null)
        //   _cam_holder.extrapolate.before_cycle(_cam_holder._cam_pos, _cam_holder._cam_target_pos);

        CW2_room temp1;
        CW2_transition temp2;




        #region defines
        Vector3 look_target = player_neck.transform.position;
        look_target.y = player.transform.position.y + player_y_offset;
        Vector3 player_pos = player_neck.transform.position;
        player_pos.y = look_target.y;

        #endregion


        #region look offset
        Vector3 look_offset_out;
        {
            bool change = false;
            Vector2 v2 = Vector2.zero;
            //if (Input.GetKey(KeyCode.H))
            //{
            //    v2.y += 1f;
            //    change = true;
            //}
            //if (Input.GetKey(KeyCode.N))
            //{
            //    v2.y -= 1f;
            //    change = true;
            //}
            //if (Input.GetKey(KeyCode.M))
            //{
            //    v2.x += 1f;
            //    change = true;
            //}
            //if (Input.GetKey(KeyCode.B))
            //{
            //    v2.x -= 1f;
            //    change = true;
            //}
            //v2.Normalize();

            if (change)
            {
                _look_offset.current_offset_new = b_math.My_lerp(_look_offset.current_offset_new, v2, _look_offset.to_max_offset_time);
                _look_offset.time_from_last_change = 0f;
            }
            else
            {
                const float treshhold = 0.001f;
                if (_look_offset.time_from_last_change < treshhold)
                    _look_offset.time_from_last_change = 1.01f * treshhold;
                else
                {
                    _look_offset.time_from_last_change += Time.deltaTime;
                    if (_look_offset.time_from_last_change > _look_offset.time_to_change)
                        _look_offset.current_offset_new = b_math.My_lerp(_look_offset.current_offset_new, v2, _look_offset.to_max_offset_time);
                }
            }
            Vector3 offset = new Vector3(
                _look_offset.current_offset_new.x * _look_offset.look_power.x,
                _look_offset.current_offset_new.y * _look_offset.look_power.y,
                0f
                );
            look_target += offset;



            v2 = Vector2.zero;
            //float distance_between_cam_and_target = (out_cam_target_pos - out_cam_pos).magnitude;
            //look_offset(_look_offset, v2, look_target, distance_between_cam_and_target,out look_offset_out, change);
            look_offset_out = look_target;
        }
        #endregion

        #region main upd actives
        for (int i = 0; i < actives.Count; i++)
        {
            if (actives[i].TryGetComponent<CW2_room>(out temp1))
                CRT_room_active_upd(temp1, player_pos, look_offset_out);
            else
                if (actives[i].TryGetComponent<CW2_transition>(out temp2))
                CRT_transition_active_upd(temp2, player_pos);
        }
        #endregion

        #region main upd current
        if (current.TryGetComponent<CW2_room>(out temp1))
        {
            CRT_room_current_upd(temp1, look_offset_out);
            _dof.debug_settings = get_dof_settings_from_room(temp1);
        }
        else
           if (current.TryGetComponent<CW2_transition>(out temp2))
        {
            CRT_transition_current_upd(temp2, player_pos, false);
            _dof.debug_settings = get_dof_settings_from_transition(temp2);
        }
        #endregion

        #region change_current
        if (main_next_current_object != null)
        {
            current = main_next_current_object;
            CW2_room temp_room;
            CW2_transition temp_trans;
            if (current.TryGetComponent<CW2_room>(out temp_room))
            {
                current = temp_room.gameObject;
                recalculate_actives_current_room();
            }
            if (current.TryGetComponent<CW2_transition>(out temp_trans))
            {
                current = temp_trans.gameObject;
                recalculate_actives_current_transition();
            }
        }
        #endregion

        #region postprocess
        if (_shake != null)
        {
            //if (Input.GetKeyDown(KeyCode.U))
            //    _shake.intencity = 1f;
            _shake.calc(new CW2_points(out_cam_pos, out_cam_target_pos));
            out_cam_pos = _shake.out_points.cam_pos;
        }
        //roll_upd();

        //if (_cam_holder.extrapolate != null)
        //    _cam_holder.extrapolate.after_cycle(_cam_holder._cam_pos, _cam_holder._cam_target_pos);

        last_cam_pos = out_cam_pos;
        last_cam_target = out_cam_target_pos;

        CW2_points points_raw = new CW2_points(last_cam_pos, last_cam_target);
        CW2_points traska = k_traska.Upd(points_raw);
        CW2_points look = k_look.Upd(traska);

        out_cam_pos = look.cam_pos;
        out_cam_target_pos = look.room_pos;

        depth_of_field();
        #endregion

        return null;
    }

    #region room

    #region active room

    void CRT_room_active_upd(CW2_room room, Vector3 player_pos, Vector3 look_pos)
    {
        Vector3 clamp_pos;
        clamp_pos = CRT_box_collider_clamp(room.room_cube, player_pos);
        Vector3 clamp_pos_look_offset = CRT_box_collider_clamp(room.room_cube, look_pos);
        CW2_smooth_layer smooth_layer = room.smooth_layer;

        smooth_layer.Upd(clamp_pos, smooth_layer.out_cam_target_pos, clamp_pos_look_offset, smooth_layer.out_look_offset_pos, Time.deltaTime);

        out_cam_target_pos = smooth_layer.out_look_offset_pos;

        out_cam_target_pos = smooth_layer.out_cam_target_pos;
        room.points.room_pos = out_cam_target_pos;

        room.points.room_pos = out_cam_target_pos;

        room.points.cam_pos = CRT_percent_to_pos_in_collider_clamped(
            room.cam_cube,
            CRT_pos_to_percent_in_collider_clamped(room.room_cube, smooth_layer.out_cam_target_pos)
            );
    }
    void CRT_room_active_start(CW2_room room, Vector3 player_pos)
    {
        room.smooth_layer.Reset(player_neck.transform.position);
    }
    void CRT_room_active_end(CW2_room room, Vector3 player_pos)
    {

    }

    #endregion

    #region current room
    void CRT_room_current_upd(CW2_room room, Vector3 player_pos)
    {
        out_cam_target_pos = room.points.room_pos;
        out_cam_pos = room.points.cam_pos;
    }
    void CRT_room_current_start(CW2_room room, Vector3 player_pos)
    {
        CRT_room_set_points(room, player_neck.transform.position);
    }
    void CRT_room_current_end(CW2_room room, Vector3 player_pos)
    {

    }
    #endregion


    #endregion

    #region transition

    #region active transition
    void CRT_transition_active_upd(CW2_transition trans, Vector3 player_pos)
    {
        if (trans.gameObject == current)
            return;
        if (CRT_is_point_in_cube(trans.transition_box, player_pos))
            main_next_current_object = trans.gameObject;
    }
    void CRT_transition_active_start(CW2_transition trans, Vector3 player_pos)
    {
        CRT_transition_reset(trans, player_pos);
    }
    void CRT_transition_active_end(CW2_transition trans, Vector3 player_pos)
    { }
    #endregion

    #region current transition
    void CRT_transition_current_upd(CW2_transition trans, Vector3 player_pos, bool reset)
    {
        BoxCollider collider = trans.transition_box;
        //Vector3 center = collider.transform.position + collider.center;

        float center = 0f, room1 = 0f, room2 = 0f, player_axis_pos = 0f, room1_axis = 0f, room2_axis = 0f;

        float time_change = 0f;

        float _new_pos = trans.position;
        if (!reset)
            time_change = trans.settings_change_time;
        if (time_change <= 0f)
            time_change = 0.000000001f;
        float change = Time.deltaTime / time_change;


        float size = 0f;
        if (trans.axis == CW2_axis.x)
        {
            player_axis_pos = player_pos.x;
            center = collider.transform.position.x + collider.center.x;
            size = collider.size.x;
            room1_axis = trans.room1.transform.position.x + collider.center.x;
            room2_axis = trans.room2.transform.position.x + collider.center.x;
        }

        if (trans.axis == CW2_axis.y)
        {
            player_axis_pos = player_pos.y;
            center = collider.transform.position.y + collider.center.y;
            size = collider.size.y;
            room1_axis = trans.room1.transform.position.y + collider.center.y;
            room2_axis = trans.room2.transform.position.y + collider.center.y;
        }
        if (trans.axis == CW2_axis.z)
        {
            player_axis_pos = player_pos.z;
            center = collider.transform.position.z + collider.center.z;
            size = collider.size.z;
            room1_axis = trans.room1.transform.position.z + collider.center.z;
            room2_axis = trans.room2.transform.position.z + collider.center.z;
        }
        if (size < 0)
            size *= -1f;
        room1 = center - size;
        room2 = center + size;

        //if(Mathf.Abs(room1_axis - room1)>Mathf.Abs(room1_axis-room2))
        if (room1_axis - room1 < room1_axis - room2)
        {
            float temp = room1;
            room1 = room2;
            room2 = temp;
        }

        if (!trans.плавно)
        {
            if (player_axis_pos < center)
                _new_pos -= change;//to 1 room (to 0f)
            else
                _new_pos += change;//to 2 room (to 1f)

        }
        else
        {
            float pos = Mathf.Clamp(player_axis_pos, Mathf.Min(room1, room2), Mathf.Max(room1, room2));
            float current_pos01 = Mathf.Abs(pos - room1) / Mathf.Abs(room1 - room2);
            if (_new_pos < current_pos01)
            {
                _new_pos += change;//to 1 room (to 0f)
                if (_new_pos > current_pos01)
                    _new_pos = current_pos01;
            }
            else
            {
                _new_pos -= change;//to 2 room (to 1f)
                if (_new_pos < current_pos01)
                    _new_pos = current_pos01;
            }
        }


        _new_pos = Mathf.Clamp01(_new_pos);
        trans.position = _new_pos;

        float blend1 = 1f - trans.position,
              blend2 = trans.position;

        if (trans.use_curve)
        {
            float curve_sample = trans.curve.Evaluate(_new_pos);
            curve_sample = Mathf.Clamp01(curve_sample);
            blend2 = curve_sample;
            blend1 = 1f - curve_sample;
        }


        trans.points.room_pos = trans.room1.points.room_pos * blend1 + trans.room2.points.room_pos * blend2;
        trans.points.cam_pos = trans.room1.points.cam_pos * blend1 + trans.room2.points.cam_pos * blend2;

        out_cam_target_pos = trans.points.room_pos;
        out_cam_pos = trans.points.cam_pos;

        if (!CRT_is_point_in_cube(trans.transition_box, player_pos) & !reset)
        {
            if (trans.position == 0f)
                main_next_current_object = trans.room1.gameObject;
            if (trans.position == 1f)
                main_next_current_object = trans.room2.gameObject;
        }
    }
    void CRT_transition_current_start(CW2_transition trans, Vector3 player_pos)
    {
        //CRT_transition_reset(trans,);
    }
    void CRT_transition_current_end(CW2_transition trans, Vector3 player_pos)
    {

    }

    #endregion

    #endregion


    #region utilitys


    #region room utility
    void CRT_room_set_points(CW2_room room, Vector3 point_in_room)
    {
        room.points.room_pos = CRT_box_collider_clamp(room.room_cube, player_neck.transform.position);
        room.points.cam_pos = CRT_percent_to_pos_in_collider_clamped(
            room.cam_cube,
            CRT_pos_to_percent_in_collider_clamped(room.room_cube, room.points.room_pos)
            );
    }
    bool CRT_is_point_in_cube(BoxCollider collider, Vector3 pos)
    {
        Vector3 center = collider.center + collider.transform.position;
        Vector3 size = CRT_collider_size_normalized(collider);
        Vector3 difference_between_center_and_pos = center - pos;
        if (Mathf.Abs(difference_between_center_and_pos.x) > size.x / 2f)
            return false;
        if (Mathf.Abs(difference_between_center_and_pos.y) > size.y / 2f)
            return false;
        if (Mathf.Abs(difference_between_center_and_pos.z) > size.z / 2f)
            return false;

        return true;
    }
    Vector3 CRT_collider_size_normalized(BoxCollider collider)
    {
        Vector3 size = collider.size;
        if (size.x < 0)
        {
            collider.size = new Vector3(size.x * -1f, size.y, size.z);
            size = collider.size;
        }
        if (size.y < 0)
        {
            collider.size = new Vector3(size.x, size.y * -1f, size.z);
            size = collider.size;
        }
        if (size.z < 0)
        {
            collider.size = new Vector3(size.x, size.y, size.z * -1f);
            size = collider.size;
        }
        return size;
    }
    public Vector3 CRT_box_collider_clamp(BoxCollider collider, Vector3 pos)
    {
        Vector3 center = collider.center + collider.transform.position;
        Vector3 size = CRT_collider_size_normalized(collider);
        return new Vector3(
            Mathf.Clamp(pos.x, center.x - (size.x / 2f), center.x + (size.x / 2f)),
            Mathf.Clamp(pos.y, center.y - (size.y / 2f), center.y + (size.y / 2f)),
            Mathf.Clamp(pos.z, center.z - (size.z / 2f), center.z + (size.z / 2f))
            );
    }
    Vector3 CRT_pos_to_percent_in_collider_clamped(BoxCollider collider, Vector3 pos)
    {
        Vector3 center = collider.center + collider.transform.position;
        Vector3 size = CRT_collider_size_normalized(collider);
        return new Vector3(
            Mathf.Clamp01((pos.x - (center.x - size.x / 2f)) / size.x),
            Mathf.Clamp01((pos.y - (center.y - size.y / 2f)) / size.y),
            Mathf.Clamp01((pos.z - (center.z - size.z / 2f)) / size.z)
            );
    }
    Vector3 CRT_percent_to_pos_in_collider_clamped(BoxCollider collider, Vector3 percent3D)
    {
        Vector3 center = collider.center + collider.transform.position;
        Vector3 size = CRT_collider_size_normalized(collider);
        percent3D = new Vector3(
            Mathf.Clamp01(percent3D.x),
            Mathf.Clamp01(percent3D.y),
            Mathf.Clamp01(percent3D.z)
            );

        return new Vector3(
            center.x + ((percent3D.x * 2f - 1f) * (size.x / 2f)),
            center.y + ((percent3D.y * 2f - 1f) * (size.y / 2f)),
            center.z + ((percent3D.z * 2f - 1f) * (size.z / 2f))
            );
    }

    void recalculate_actives_current_room()
    {
        CW2_room room;
        if (!current.TryGetComponent<CW2_room>(out room))
        {
            Debug.LogError("try to recalculate room, but current hasnt cw2_room class!");
            return;
        }
        List<GameObject> add_list = new List<GameObject>(),
                            remove_list = new List<GameObject>(),
                            stay_list = new List<GameObject>();

        #region add to lists

        for (int i = 0; i < room.transition_list.Count; i++)
        {
            if (actives.Contains(room.transition_list[i].gameObject))
                stay_list.Add(room.transition_list[i].gameObject);
            else
                add_list.Add(room.transition_list[i].gameObject);
        }
        for (int i = 0; i < actives.Count; i++)
        {
            if (!stay_list.Contains(actives[i]))
                remove_list.Add(actives[i]);
        }
        if (actives.Contains(room.gameObject))
        {
            if (!stay_list.Contains(room.gameObject))
                stay_list.Add(room.gameObject);
        }
        else
            add_list.Add(room.gameObject);
        #endregion

        #region start and end
        {
            CW2_room room_temp;
            CW2_transition transition_temp;
            for (int i = 0; i < add_list.Count; i++)
            {

                if (add_list[i].TryGetComponent<CW2_transition>(out transition_temp))
                    CRT_transition_active_start(transition_temp, player_neck.transform.position);
                else
                    if (add_list[i].TryGetComponent<CW2_room>(out room_temp))
                    CRT_room_active_start(room_temp, player_neck.transform.position);
                stay_list.Add(add_list[i]);
            }

            for (int i = 0; i < remove_list.Count; i++)
            {
                if (remove_list[i].TryGetComponent<CW2_transition>(out transition_temp))
                    CRT_transition_active_end(transition_temp, player_neck.transform.position);
                else
                   if (remove_list[i].TryGetComponent<CW2_room>(out room_temp))
                    CRT_room_active_end(room_temp, player_neck.transform.position);
            }
        }
        #endregion

        actives = stay_list;
    }
    void recalculate_actives_current_transition()
    {
        CW2_transition trans;
        if (!current.TryGetComponent<CW2_transition>(out trans))
        {
            Debug.LogError("try to recalculate room, but current hasnt cw2_room class!");
            return;
        }
        List<GameObject> add_list = new List<GameObject>(),
                            remove_list = new List<GameObject>(),
                            stay_list = new List<GameObject>();

        #region add to lists
        List<GameObject> temp_rooms = new List<GameObject>();
        temp_rooms.Add(trans.room1.gameObject);
        temp_rooms.Add(trans.room2.gameObject);
        for (int i = 0; i < temp_rooms.Count; i++)
        {
            if (actives.Contains(temp_rooms[i].gameObject))
                stay_list.Add(temp_rooms[i].gameObject);
            else
                add_list.Add(temp_rooms[i].gameObject);
        }
        for (int i = 0; i < actives.Count; i++)
        {
            if (!stay_list.Contains(actives[i]))
                remove_list.Add(actives[i]);
        }
        #endregion

        #region start and end
        {
            CW2_room room_temp;
            CW2_transition transition_temp;
            for (int i = 0; i < add_list.Count; i++)
            {

                if (add_list[i].TryGetComponent<CW2_transition>(out transition_temp))
                    CRT_transition_active_start(transition_temp, player_neck.transform.position);
                else
                    if (add_list[i].TryGetComponent<CW2_room>(out room_temp))
                    CRT_room_active_start(room_temp, player_neck.transform.position);
                stay_list.Add(add_list[i]);
            }

            for (int i = 0; i < remove_list.Count; i++)
            {
                if (remove_list[i].TryGetComponent<CW2_transition>(out transition_temp))
                    CRT_transition_active_end(transition_temp, player_neck.transform.position);
                else
                   if (remove_list[i].TryGetComponent<CW2_room>(out room_temp))
                    CRT_room_active_end(room_temp, player_neck.transform.position);
            }
        }
        #endregion

        actives = stay_list;
    }
    #endregion


    #region transition utility

    void CRT_normalize_rooms_in_transition(CW2_transition trans)
    {
        BoxCollider collider1 = trans.room1.room_cube,
                    collider2 = trans.room2.room_cube;
        Vector3 center_box_collider_room1 = collider1.center + collider1.transform.position,
                center_box_collider_room2 = collider2.center + collider2.transform.position;
        if (trans.axis == CW2_axis.x)
            if (center_box_collider_room1.x > center_box_collider_room2.x)
            {
                //swap
                CW2_room temp = trans.room1;
                trans.room1 = trans.room2;
                trans.room2 = temp;
            }

        if (trans.axis == CW2_axis.y)
            if (center_box_collider_room1.y > center_box_collider_room2.y)
            {
                //swap
                CW2_room temp = trans.room1;
                trans.room1 = trans.room2;
                trans.room2 = temp;
            }

        if (trans.axis == CW2_axis.z)
            if (center_box_collider_room1.z > center_box_collider_room2.z)
            {
                //swap
                CW2_room temp = trans.room1;
                trans.room1 = trans.room2;
                trans.room2 = temp;
            }
    }

    void CRT_transition_reset(CW2_transition trans, Vector3 player_pos)
    {
        CRT_normalize_rooms_in_transition(trans);


        BoxCollider collider = trans.transition_box;
        Vector3 center = collider.transform.position + collider.center;

        if (trans.axis == CW2_axis.x)
            if (player_pos.x < center.x)  //to 1 room (to 0f)
                trans.position = 0f;
            else  //to 2 room (to 1f)
                trans.position = 1f;

        if (trans.axis == CW2_axis.y)
            if (player_pos.y < center.y)  //to 1 room (to 0f)
                trans.position = 0f;
            else  //to 2 room (to 1f)
                trans.position = 1f;

        if (trans.axis == CW2_axis.z)
            if (player_pos.z < center.z)  //to 1 room (to 0f)
                trans.position = 0f;
            else  //to 2 room (to 1f)
                trans.position = 1f;
    }


    #endregion

    #region global utilitys

    //fix this
    GameObject find_player()
    {
        if (player_neck == null || !player_neck.activeSelf)
        {
            PlayerController[] go_list = GameObject.FindObjectsOfType<PlayerController>();//.FindGameObjectsWithTag("Player");
            if (go_list.Length == 0)
                return null;
            for (int i = 0; i < go_list.Length; i++)
                if (go_list[i].CompareTag("Player"))
                {
                    player = go_list[i].gameObject;
                    return find_neck(go_list[i].gameObject);
                }
            player = go_list[0].gameObject;
            return find_neck(go_list[0].gameObject);
        }
        else
        {
            player = player_neck;
            return find_neck(player);
        }
    }
    public void fix_NaN(ref Vector3 val)
    {
        if (float.IsNaN(val.x))
        {
            Debug.Log("fix x");
            val.x = 0;
        }
        if (float.IsNaN(val.y))
        {
            Debug.Log("fix y");
            val.y = 0;
        }
        if (float.IsNaN(val.z))
        {
            Debug.Log("fix z");
            val.z = 0;
        }
    }

    GameObject find_neck(GameObject player)
    {
        Transform temp = null, final = null;
        if (player == null)
            return null;
        else
            final = player.transform;
        int count = player.transform.childCount;

        for (int i = 0; i < way_to_Neck.Length; i++)
            if (CW2_utility.Try_find_child(way_to_Neck[i], out temp, final))
                final = temp;
            else
                return final.gameObject;

        return final.gameObject;
    }
    CW2_room find_closest_room()
    {
        Vector3 player_pos = player_neck.transform.position;

        float dist = 9999999f;
        CW2_room closest = null;

        for (int i = 0; i < _room_holder.rooms.Count; i++)
        {
            CW2_room temp_room = _room_holder.rooms[i];
            float temp_dist = Vector3.Distance(CRT_box_collider_clamp(temp_room.room_cube, player_pos), player_pos);
            if (temp_dist < dist)
            {
                dist = temp_dist;
                closest = temp_room;
            }
        }

        return closest;
    }

    public void reset_pos(bool force_reset)
    {
        Debug.Log("reset_pos start");
        CW2_room closest_room;
        if (!force_reset)
        {
            closest_room = find_closest_room();
            CW2_transition temp;
            CW2_room temp2;

            if (current.TryGetComponent<CW2_transition>(out temp))
            {
                if (!closest_room.transition_list.Contains(temp))
                    force_reset = true;
            }
            else
                if (current.TryGetComponent<CW2_room>(out temp2))
            {
                if (temp2 != closest_room)
                    force_reset = true;
            }
        }
        if (force_reset)
        {
            actives.Clear();
            Debug.Log("reset_pos force_reset");
            current = find_closest_room().gameObject;
            recalculate_actives_current_room();
        }
    }

    public CW2_DOF_settings get_dof_settings_from_room(CW2_room room)
    {
        CW2_DOF_settings_holder holder;
        if (room.TryGetComponent<CW2_DOF_settings_holder>(out holder))
            return new CW2_DOF_settings(holder.settings);
        else
            return new CW2_DOF_settings(_dof.default_settings);
    }
    public CW2_DOF_settings get_dof_settings_from_transition(CW2_transition transition)
    {
        return CW2_DOF_settings.blend(get_dof_settings_from_room(transition.room2), get_dof_settings_from_room(transition.room1), transition.position);
    }
    #endregion



    #endregion

    #region look_offset

    void look_offset(CW2_look_offset offset_settings, Vector2 vector_to, Vector3 look_pos, float distance_between_cam_and_target, out Vector3 look_offset_out, bool change)
    {
        #region change current

        float change_spd = 1f / offset_settings.to_max_offset_time;
        offset_settings.current_offset_old = b_math.My_lerp(offset_settings.current_offset_old, vector_to, change_spd);
        #endregion

        #region change offset
        look_offset_out = look_pos;
        look_offset_out.x += distance_between_cam_and_target * offset_settings.distance_to_offset * offset_settings.current_offset_old.x;
        look_offset_out.y += distance_between_cam_and_target * offset_settings.distance_to_offset * offset_settings.current_offset_old.y;
        #endregion
    }


    #endregion

    #region camera_post_process

    void roll_upd()
    {
        //TODO: заглушка, чтобы убрать вращение камеры
        return;
        if (_roll.current_change_side)
        {
            _roll.current_change += Time.deltaTime / _roll.change_time;
            if (_roll.current_change > _roll.max_change)
            {
                _roll.current_change = _roll.max_change - (_roll.current_change - _roll.max_change);
                _roll.current_change_side = false;
            }
        }
        else
        {
            _roll.current_change -= Time.deltaTime / _roll.change_time;
            if (_roll.current_change < -_roll.max_change)
            {
                _roll.current_change = -_roll.max_change + (_roll.current_change + _roll.max_change);
                _roll.current_change_side = true;
            }
        }


        _roll.cinemachine_cam.m_Lens.Dutch = _roll.current_change;
    }

    void depth_of_field()
    {
        bool dof_std = false;
        bool dof_cutscene = false;
        cutscene_dof cdof = null;

        if (cutscenes.Count > 0)
        {
            bool clear = false;

            for (int i = 0; i < cutscenes.Count; i++)
            {
                if (cutscenes[i] == null)
                    clear = true;
                else
                {
                    if (cutscenes[i].added == false)
                        clear = true;
                    else
                        cdof = cutscenes[i];
                }
            }
            if (cdof != null)
            {
                dof_cutscene = true;
            }
            else
            {
                dof_std = true;
            }
            if (clear)
            {
                List<cutscene_dof> list_new = new List<cutscene_dof>();
                for (int i = 0; i < cutscenes.Count; i++)
                {
                    if (cutscenes[i] != null)
                        if (cutscenes[i].added != false)
                            list_new.Add(cutscenes[i]);
                }
                cutscenes = list_new;
            }
        }
        else
        {
            dof_std = true;
        }

        if ((dof_std & dof_cutscene) | (!dof_std & !dof_cutscene))
        {
            Debug.LogWarning("dof error");
        }

        if (dof_std)
        {
            Vector3 player_pos = player_neck.transform.position;
            float dist = Vector3.Distance(player_pos, out_cam_pos);
            _dof.debug_distance = dist;
            _dof.set();
        }

        if (dof_cutscene)
        {
            _dof.debug_distance = cdof.get_distance();
            _dof.set();
        }
    }

    #endregion

    #endregion
}
