﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW2_camera_holder : MonoBehaviour
{
    // Start is called before the first frame update
    const string _camera_name = "Main Camera";
    public GameObject _camera;
    const string _virual_camera_settings_name = "CM vcam1 MAIN";
    public GameObject _virual_camera_settings;
    const string _cam_pos_name = "cam_pos";
    public GameObject _cam_pos;
    const string _cam_target_pos_name = "cam_target_pos";
    public GameObject _cam_target_pos;

    public Vector3 cam_pos { get => _cam_pos.transform.position; set => _cam_pos.transform.position = value; }
    public Vector3 cam_target_pos { get => _cam_target_pos.transform.position; set => _cam_target_pos.transform.position = value; }
    public Camera camera { get => _camera.GetComponent<Camera>();}
    public CinemachineVirtualCamera virtual_camera { get => _virual_camera_settings.GetComponent<CinemachineVirtualCamera>(); }

    public CW2_extrapolate extrapolate
    { 
        get 
        {
            CW2_extrapolate temp;
            if (gameObject.TryGetComponent<CW2_extrapolate>(out temp))
                if(temp.enabled)
                    return temp;            
            return null;
        } 
    }

    public void Ini(CW2_error_handler _error_handler)
    {
        #region check_critical_errors

        #region find gameobjects
        Transform _temp;
        //------------find _camera-------------
        if (_camera == null)
        {
            if (!CW2_utility.Try_find_child(_camera_name, out _temp, transform))
                _error_handler.Add(true, "CW2_cam_holder: critical error -> cannot find camera, name=" + this.name);
            else
                _camera = _temp.gameObject;
        }

        {
            Camera temp;
            if (_camera.TryGetComponent<Camera>(out temp))
                ;
            else
                _error_handler.Add(true, "CW2_cam_holder: critical error -> bad camera. name=" + this.name);
        }

        //------------find _virual_camera_settings-------------
        if (_virual_camera_settings == null)
        {
            if (!CW2_utility.Try_find_child(_virual_camera_settings_name, out _temp, transform))
                _error_handler.Add(true, "CW2_cam_holder: critical error -> cannot find virtual_camera, name=" + this.name);
            else
                _virual_camera_settings = _temp.gameObject;
        }

        {
            CinemachineVirtualCamera temp;
            if (_virual_camera_settings.TryGetComponent<CinemachineVirtualCamera>(out temp))
                ;
            else
                _error_handler.Add(true, "CW2_cam_holder: critical error -> bad virtual camera setting. name=" + this.name);
        }

        //------------find _cam_pos--------
        if (_cam_pos == null)
        {
            if (!CW2_utility.Try_find_child(_cam_pos_name, out _temp, transform))
                _error_handler.Add(true, "CW2_cam_holder: critical error -> cannot find _cam_pos, name=" + this.name);
            else
                _cam_pos = _temp.gameObject;
        }

        //------------find _cam_target_pos--------
        if (_cam_target_pos == null)
        {
            if (!CW2_utility.Try_find_child(_cam_target_pos_name, out _temp, transform))
                _error_handler.Add(true, "CW2_cam_holder: critical error -> cannot find _cam_target_pos, name=" + this.name);
            else
                _cam_target_pos = _temp.gameObject;
        }

        #endregion

        #endregion
    }
}
