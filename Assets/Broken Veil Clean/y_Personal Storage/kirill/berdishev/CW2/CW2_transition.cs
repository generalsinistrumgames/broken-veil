﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW2_transition : MonoBehaviour
{
    // Start is called before the first frame update
    BoxCollider bx;
    public BoxCollider transition_box { get => bx; }

    public CW2_axis axis = CW2_axis.x;

    public bool плавно = false;
    public bool use_curve = false;
    public AnimationCurve curve = new AnimationCurve(new Keyframe[] { new Keyframe(0, 0, 0.7f, 0.7f), new Keyframe(1, 1, 0.7f, 0.7f) });

    public CW2_room room1, room2;

    public float settings_change_time = 0.3f;

    //0f - room1, 1f - room2
    public float position = 0f;
    public float position_out = 0f;
    public CW2_points points = new CW2_points();

    public void Ini(CW2_error_handler _error_handler)
    {
        #region check_critical_errors
        if (TryGetComponent<BoxCollider>(out bx))
            bx.enabled = false;
        else
            _error_handler.Add(true, "CW2_transition: critical error -> transition has'nt box collider. transition name=" + this.name);
        #endregion

        #region check_non-critical_errors

        #region rotation
        if (Mathf.Abs(Quaternion.Angle(transform.rotation, Quaternion.Euler(0, 0, 0))) > 0.01f)
        {
            _error_handler.Add(false, "CW2_room: non-critical error -> wrong rotation on transition. transition name=" + this.name);
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        #endregion
        #region scale
        if (transform.localScale != new Vector3(1f, 1f, 1f))
        {
            _error_handler.Add(false, "CW2_transition: non-critical error -> wrong scale on transition. transition name=" + this.name);
            transform.localScale = new Vector3(1f, 1f, 1f);
        }
        #endregion
        #region colliders off
        bx.enabled = false;
        #endregion
        #region curve_keys_0_element
        if (curve.keys.Length == 0)
            curve.keys = new Keyframe[] { new Keyframe(0, 0,0.7f, 0.7f), new Keyframe(1, 1, 0.7f, 0.7f) };
        #endregion

            #endregion

        room1.transition_list.Add(this);
        room2.transition_list.Add(this);
    }
}
