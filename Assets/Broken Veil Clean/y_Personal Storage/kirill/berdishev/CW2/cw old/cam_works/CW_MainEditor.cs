﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;

//[CustomEditor(typeof(CW_Cam_movement))]
//[CanEditMultipleObjects]
public class CW_MainEditor /*: Editor*/
{
    CW_Cam_movement gameObject;
    CW_room_fabric fabric;
    void OnSceneGUI()
    {
        if (!gameObject.enabled)
            return;
        if (!fabric.global_draw)
            return;
        List<Room_data> fabric_rooms = fabric.scene_rooms;
        List<CW_Transition> fabric_trans = fabric.scene_transitions;
        for (int i = 0; i < fabric_rooms.Count; i++)
            draw(fabric_rooms[i]);
        for (int i = 0; i < fabric_trans.Count; i++)
            draw(fabric_trans[i]);
    }
    void draw(Drawable val)
    {

        if (val.Is_draw())
        {
            Cube[] mass = val.Get_cubes();
            for (int i = 0; i < mass.Length; i++) ;
                //draw_cube(mass[i]);
        }
    }
    //void draw_cube(Cube cube)
    //{        
    //    cube.Upd_vertexes();
    //    Handles.color = cube.editor_main_color;
    //    Handles.DrawLine(cube.Vertex[0], cube.Vertex[1]);
    //    Handles.DrawLine(cube.Vertex[0], cube.Vertex[2]);
    //    Handles.DrawLine(cube.Vertex[0], cube.Vertex[4]);
    //    Handles.DrawLine(cube.Vertex[1], cube.Vertex[5]);

    //    Handles.DrawLine(cube.Vertex[1], cube.Vertex[3]);
    //    Handles.DrawLine(cube.Vertex[2], cube.Vertex[3]);
    //    Handles.DrawLine(cube.Vertex[2], cube.Vertex[6]);
    //    Handles.DrawLine(cube.Vertex[3], cube.Vertex[7]);

    //    Handles.DrawLine(cube.Vertex[4], cube.Vertex[5]);
    //    Handles.DrawLine(cube.Vertex[4], cube.Vertex[6]);
    //    Handles.DrawLine(cube.Vertex[5], cube.Vertex[7]);
    //    Handles.DrawLine(cube.Vertex[6], cube.Vertex[7]);

    //    if (!cube.editor_center_draw)
    //        return;

    //    Vector3 c = cube.Center;
    //    Handles.color = cube.editor_center_color;

    //    float f = 0.25f;
    //    Handles.DrawLine(c, new Vector3(c.x, c.y + f, c.z));
    //    Handles.DrawLine(c, new Vector3(c.x, c.y - f, c.z));
    //    Handles.DrawLine(c, new Vector3(c.x + f, c.y, c.z));
    //    Handles.DrawLine(c, new Vector3(c.x - f, c.y, c.z));
    //    Handles.DrawLine(c, new Vector3(c.x, c.y, c.z + f));
    //    Handles.DrawLine(c, new Vector3(c.x, c.y, c.z - f));
    //}


    void OnEnable()
    {
        //gameObject = (CW_Cam_movement)target;
        fabric = gameObject.GetComponent<CW_room_fabric>();
    }
}

public interface Drawable
{
    Cube[] Get_cubes();
    bool Is_draw();
}
