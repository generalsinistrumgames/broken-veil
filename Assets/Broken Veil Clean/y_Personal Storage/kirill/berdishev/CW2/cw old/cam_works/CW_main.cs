﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW_main : MonoBehaviour
{
    public GameObject cube;
    public float spd = 0.5f;
    public float spd_perc = 0.2f;
    //[HideInInspector]
    //public CW_MainEditor gui;
    // Start is called before the first frame update
    public Vector2 cam_x=new Vector2(-2.5f,1.3f);
    public Vector2 cam_y=new Vector2(2.2f,0.6f);

    public Vector2 room_x = new Vector2(3.1f,-4.4f),
                   room_z = new Vector2(-5.4f,-0.1f);

    void Start()
    {
       // gui = new CW_MainEditor();
    }

    // Update is called once per frame
    void Update()
    {
        float z = gameObject.transform.position.z,
              x = Mathf.Clamp(cube.transform.position.x, cam_x.x, cam_x.y),
              y = Mathf.Clamp(cube.transform.position.y, cam_y.y, cam_y.x);
        cube.transform.position = new Vector3(x, y, z);
        gameObject.transform.position = Smooth_move(gameObject.transform.position, cube.transform.position, spd, spd_perc);
    }
    public Vector3 Smooth_move(Vector3 current_pos,Vector3 final_pos,float spd_per_sec, float spd_percent)
    {
        return b_math.My_lerp(current_pos,final_pos,spd_per_sec,spd_percent);
    }
}


public interface Cam_controller
{
    void Start(Vector3 target_pos,Vector3 look_at);
    void End();
    Cam_controller Upd(Vector3 target_pos,Vector3 look_at);
    Vector3 Get_smooth_cam_pos();
    Vector3 Get_smooth_room_pos();
    void Activate_transition();
    void Deactivate_transition();
    List<CW_Transition> Get_all_transition();
    void Reset(Vector3 target_pos,Vector3 look_at);
}
public interface Point_Of_Interest
{
    void upd();
    Vector3 pos
    {
        get; set;
    }

    float max_dist
    {
        get; set;
    }

    bool is_active
    {
        get; set;
    }

    float weight
    {
        get; set;
    }
}