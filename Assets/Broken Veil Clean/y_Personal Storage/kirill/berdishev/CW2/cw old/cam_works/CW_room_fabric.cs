﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class CW_room_fabric : MonoBehaviour
{
    public int error_count = 0;
    [SerializeField]
    public List<Room_data> scene_rooms = new List<Room_data>();
    [SerializeField]
    public List<CW_Transition> scene_transitions = new List<CW_Transition>();

    public int default_room_id = 0;
    public Cam_controller default_room;
    public bool is_ini = false;
    public bool global_draw = true;


    void Start()
    {
        ini();
    }
    public void ini()
    {
        Debug.Log("фабрика начинает свою работу");
        default_room = scene_rooms[default_room_id];
        if (is_ini)
            return;
        error_count = 0;
        for (int i = 0; i < scene_rooms.Count; i++)
        {
            if (scene_rooms[i].fabric_name == "")
            {
                error_count++;
                Debug.LogError("Room fabric warning - ключ комнаты не задан id=" + i.ToString());
            }
            Set_room_to_transitions(scene_rooms[i]);
        }
        for (int i = 0; i < scene_transitions.Count; i++)
        {
            if (scene_transitions[i].fabric_name == "")
            {
                error_count++;
                Debug.LogError("Room fabric warning - ключ перехода не задан id=" + i.ToString());
            }
            Set_trasitions_to_rooms(scene_transitions[i]);
        }

        if (error_count == 0)
            Debug.Log("фабрика успешно завершила работу");
        else
            Debug.LogError("фабрика не завершила работу успешно, кол-во ошибок=" + error_count.ToString());

        is_ini = true;
    }
    public void Set_room_to_transitions(Room_data room)
    {
        for (int i = 0; i < room.fabric_transition_Names.Count; i++)
        {
            CW_Transition t = Search_Transition(room.fabric_transition_Names[i]);
            // names
            if (!t.fabric_room_names.Contains(room.fabric_name))
                t.fabric_room_names.Add(room.fabric_name);
            //t r
            if (t.room1 != room & t.room2 != room)
            {
                if (t.room1.fabric_name == "")
                    t.room1 = room;
                else if (t.room2.fabric_name == "")
                    t.room2 = room;
                else
                {
                    error_count++;
                    Debug.LogError("Room fabric error - все комнаты в переходе заняты ключ перехода={" + t.fabric_name + "}" + " ключ заменяемой комнаты={" + room.fabric_name + "}" + " ключи комнат в переходе={" + t.room1.fabric_name + "} и {" + t.room2.fabric_name + "}");
                }
            }
            //r t
            if (!room.room_transitions.Contains(t))
                room.room_transitions.Add(t);
        }
    }
    public void Set_trasitions_to_rooms(CW_Transition trans)
    {
        for (int i = 0; i < trans.fabric_room_names.Count; i++)
        {
            Room_data room = Search_Room(trans.fabric_room_names[i]);
            //r t
            if(!room.room_transitions.Contains(trans))
                room.room_transitions.Add(trans);
            //room names
            if (!room.fabric_transition_Names.Contains(trans.name))
                room.fabric_transition_Names.Add(trans.name);
            //t r
            if (trans.room1 != room & trans.room2 != room)
            {
                if (trans.room1.fabric_name == "")
                    trans.room1 = room;
                else if (trans.room2.fabric_name == "")
                    trans.room2 = room;
                else
                {
                    error_count++;
                    Debug.LogError("Room fabric error - все комнаты в переходе заняты ключ перехода={" + trans.fabric_name + "}" + " ключ заменяемой комнаты={" + room.fabric_name + "}" + " ключи комнат в переходе={" + trans.room1.fabric_name + "} и {" + trans.room2.fabric_name + "}");
                }
            }
        }
    }

    //public void Set(Room_data to,String key)
    //{
    //    CW_Transition from;
    //    Cam_controller temp;
    //    bool sucsess = try_search(key, out temp);
    //    if(sucsess)
    //        if(temp is CW_Transition)            
    //            from = temp as CW_Transition;
    //        else
    //        {
    //            sucsess = false;
    //            Debug.LogError("fabric error set-room, ключ не transition, key="+key);
    //            return;
    //        }
    //    else
    //    {
    //        Debug.LogError("fabric error set-room, ключ не найден, key=" + key);
    //        return;
    //    }

    //    //transition list<names>
    //    sucsess = true;
    //    for(int i=0;i<from.fabric_room_names.Count;i++)
    //        if (from.fabric_room_names[i] == key)
    //            sucsess = false;
    //    if (sucsess)
    //        from.fabric_room_names.Add(key);
    //    //transition rooms
    //    sucsess = true;
    //    for (int i = 0; i < from.fabric_room_names.Count; i++)
    //        if (from.fabric_room_names[i] == key)
    //            sucsess = false;
    //    if (sucsess)
    //        from.fabric_room_names.Add(key);

    //}
    public Room_data Search_Room(String str)
    {
        Room_data final=null;
        bool is_searched=false,double_key_error=false;
        for (int i = 0; i < scene_rooms.Count; i++)
            if (scene_rooms[i].fabric_name == str)
                if (!is_searched)
                {
                    is_searched = true;
                    final = scene_rooms[i];
                }
                else
                    double_key_error = true;
        if (!is_searched)
        {
            Debug.LogWarning("Room fabric warning - ключ не найден в комнатах. key=" + str);
            error_count++;
        }
        if (double_key_error)
        {
            Debug.LogWarning("Room fabric warning - ключ дублируется в комнатах. key=" + str);
            error_count++;
        }
        return final;
    }
    public CW_Transition Search_Transition(String str)
    {
        CW_Transition final = null;
        bool is_searched = false, double_key_error = false;
        for (int i = 0; i < scene_transitions.Count; i++)
            if (scene_transitions[i].fabric_name == str)
                if (!is_searched)
                {
                    is_searched = true;
                    final = scene_transitions[i];
                }
                else
                    double_key_error = true;
        if (!is_searched)
        {
            Debug.LogWarning("Room fabric warning - ключ не найден в переходах. key={" + str+"}");
            error_count++;
        }
        if (double_key_error)
        {
            Debug.LogWarning("Room fabric warning - ключ дублируется в переходах. key={" + str+"}");
            error_count++;
        }
        return final;
    }
    public bool try_search(String str,out Cam_controller cc)
    {
        Cam_controller result = null;
        bool error = true;

        for (int i = 0; i < scene_rooms.Count; i++)
            if (scene_rooms[i].fabric_name == str)
            {
                error = false;
                result = scene_rooms[i];
            }
        for (int i = 0; i < scene_transitions.Count; i++)
            if (scene_transitions[i].fabric_name == str)
            {
                error = false;
                result = scene_rooms[i];
            }
        cc = result;
        return error;
    }
}
