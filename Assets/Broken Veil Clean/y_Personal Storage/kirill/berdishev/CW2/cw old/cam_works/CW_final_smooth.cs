﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW_final_smooth : MonoBehaviour
{
    int length = 5;
    public bool is_first_frame = true;
    public Vector3[] points;
    public Vector3 velocity;
    public float spd_change = 0.05f;
    // Start is called before the first frame update
    void Start()
    {
        points = new Vector3[length];
        for (int i = 0; i < length; i++)
            points[i].x = float.NaN;

    }
    public void upd(Vector3 v3)
    {
        if(is_first_frame)
        {
            is_first_frame = true;
            points[0] = v3;
            velocity = new Vector3();
        }
        else
        {
            for(int i=0;i<points.Length;i++)
            {

            }
        }
    }

    public Vector3 smooth_point()
    {
        Vector3 result = new Vector3();
        int num = 0;
        for(int i=points.Length-1;i==-1;i++)
            if (!float.IsNaN(points[i].x))
            {
                result += points[i];
                
            }
        result /= points.Length;
        return result;
    }
}
