﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CW_Transition : MonoBehaviour, Cam_controller, Drawable
{
    public bool editor_is_draw = true;

    public bool is_active = false;
    public bool is_work = false;
    public bool is_lock = false;
                       //axis:   x        y        z
    public Room_data room1;//   left              forward  
    public Room_data room2;//  right              backward

    public GameObject player;

    public Axis axis = Axis.x;

    public float room_blend_value = 0;
    public float room1_blend_final;
    public float room2_blend_final;

    public float room1_blend;
    public float room2_blend;

    public Vector3 target_smooth_pos;
    public Vector3 cam_smooth_pos;

    float bound;
    public BoxCollider bc;

    public Blend_controller blend_controller=new Blend_controller();

    #region fabric_params 
    [Header("fabric params")]
    public string fabric_name = "";
    public List<String> fabric_room_names = new List<String>();
    #endregion

    #region editor
    public Color editor_color = Color.cyan;
    public bool Is_draw()
    {
        return editor_is_draw;
    }
    public Cube[] Get_cubes()
    {
        Cube c = new Cube(new Vector2(gameObject.transform.position.x - bc.size.x / 2f, gameObject.transform.position.x + bc.size.x / 2f),
            new Vector2(gameObject.transform.position.y - bc.size.y / 2f, gameObject.transform.position.y + bc.size.y / 2f),
            new Vector2(gameObject.transform.position.z - bc.size.z / 2f, gameObject.transform.position.z + bc.size.z / 2f)
            );
        c.editor_main_color = editor_color;
        c.editor_center_draw = false;

        return new Cube[]{c};
    }
    #endregion

    void Upd_values(Vector3 target_pos)
    {
        if (axis == Axis.x)
        {
            Vector2 r1x = room1.room_cube[Axis.x];
            Vector2 r2x = room2.room_cube[Axis.x];
            if ((r1x.x + r1x.y) / 2f < (r2x.x + r2x.y) / 2f)
            {
                Room_data temp = room1;
                room1 = room2;
                room2 = temp;
            }
            room_blend_value = target_pos.x - gameObject.transform.position.x;
            bound = bc.size.x / 2f;
        }
        if (axis == Axis.z)
        {
            Vector2 r1z = room1.room_cube[Axis.z];
            Vector2 r2z = room2.room_cube[Axis.z];
            if ((r1z.x + r1z.y) / 2f < (r2z.x + r2z.y) / 2f)
            {
                Room_data temp = room1;
                room1 = room2;
                room2 = temp;
            }
            room_blend_value = target_pos.z - gameObject.transform.position.z;
            bound = bc.size.z / 2f;
        }
        if (axis == Axis.y)
        {
            Vector2 r1y = room1.room_cube[Axis.y];
            Vector2 r2y = room2.room_cube[Axis.y];
            if ((r1y.x + r1y.y) / 2f < (r2y.x + r2y.y) / 2f)
            {
                Room_data temp = room1;
                room1 = room2;
                room2 = temp;
            }
            room_blend_value = target_pos.y - gameObject.transform.position.y;
            bound = bc.size.y / 2f;
        }
        room_blend_value = Mathf.Clamp(room_blend_value, -bound, bound);
        room1_blend = (room_blend_value + bound) / (2 * bound);
        room2_blend = 1f - room1_blend;

        room1_blend_final = room1_blend;
        room2_blend_final = room2_blend;

        blend_controller.Get_blended_val(room_blend_value,bound,ref room1_blend_final,ref room2_blend_final);
    }
    void Upd_rooms(Vector3 target_pos,Vector3 look_at)
    {
        //Debug.Log(target_pos);
        room1.Upd(target_pos,look_at);
        room2.Upd(target_pos,look_at);
    }
    void Upd_smooth_val()
    {
        target_smooth_pos = Vector3.Lerp(room1.Get_smooth_room_pos(), room2.Get_smooth_room_pos(), room2_blend_final);
        cam_smooth_pos = Vector3.Lerp(room1.Get_smooth_cam_pos(), room2.Get_smooth_cam_pos(), room2_blend_final);
    }
    public Room_data Get_closest_room()
    {
        if (room1_blend > 0.5f)
            return room1;
        return room2;
    }
    public void Start()
    {

    }
    public void Start(Vector3 target_pos,Vector3 look_at)
    {
        bc = gameObject.GetComponent<BoxCollider>();
        Activate_transition();
        is_work = true;
        Upd_values(target_pos);
        if (room1 == Get_closest_room())
            room2.Reset(target_pos,look_at);
        else
            room1.Reset(target_pos,look_at);
        blend_controller.is_work = false;
        blend_controller.t = 0f;
        //Upd_values(target_pos);     
        //Upd_values(target_pos);
        //Upd(target_pos);
        Upd(target_pos,look_at);
    }
    public void End()
    {
        Deactivate_transition();
        is_work = false;
        is_active = false;
        blend_controller.is_work = false;
    }
    public Cam_controller Upd(Vector3 target_pos,Vector3 look_at)
    {
        Upd_values(target_pos);
        Upd_rooms(target_pos,look_at);
        Upd_smooth_val();
        //Debug.Log("r1 blend="+room1_blend_final.ToString()+" cam pos="+cam_smooth_pos.ToString()+ " room pos=" + target_smooth_pos.ToString());
        //Debug.Log("r2 blend=" + room2_blend_final.ToString() );
        if (b_math.eq(0f, room1_blend_final) | b_math.eq(1f, room1_blend_final))
            is_lock = false;
        else
            is_lock = true;
        if (!is_work & !is_lock)
        {
            Room_data rd = Get_closest_room();            
            return rd;
        }
        return this;
    }
    public Vector3 Get_smooth_cam_pos()
    {
        return cam_smooth_pos;
    }
    public Vector3 Get_smooth_room_pos()
    {
        return target_smooth_pos;
    }
    public void Activate_transition()
    {
        this.is_active = true;
    }
    public void Deactivate_transition()
    {
        this.is_active = false;
    }
    public List<CW_Transition> Get_all_transition()
    {
        return new List<CW_Transition> {this};
    }
    public void Reset(Vector3 target_pos,Vector3 look_at)
    {
        room1.Reset(target_pos,look_at);
        room2.Reset(target_pos,look_at);
    }
    public void OnTriggerEnter(Collider other)
    {
        if (!this.enabled|!is_active)
            return;
        is_work = true;        
    }
    public void OnTriggerExit(Collider other)
    {
        if (!this.enabled|!is_work)
            return;
        is_work = false;
    }
}
[Serializable]
public class Blend_controller
{

    public bool is_work = false;
    public bool time_mode = true; 
    public float t = 0;
    public float max_t = 0.7f;

    public float test_r1,test_r2;
    public void Get_blended_val(float room_blend_val,float bound, ref float r1_blend_val, ref float r2_blend_val)
    {
        if (!is_work)
        {
            if (room_blend_val < 0f)
                t = max_t; //room2
            else
                t = 0f;    //room1
            is_work = true;
        }

        if (time_mode)
        {
            if (room_blend_val < 0f)
                t += Time.deltaTime;
            else
                t -= Time.deltaTime;
            t = Mathf.Clamp(t, 0f, max_t);
            r2_blend_val = t / max_t;
            r1_blend_val = 1f - r2_blend_val;
            test_r2 = r2_blend_val;
            test_r1 = r1_blend_val;
        }
    }
}
