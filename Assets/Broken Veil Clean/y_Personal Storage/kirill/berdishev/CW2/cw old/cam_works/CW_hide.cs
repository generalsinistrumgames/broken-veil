﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW_hide : MonoBehaviour
{
    [SerializeField]
    public GameObject Up_wall_segment;
    public MeshRenderer mr;
    //public Material full_transparent;
    ////[SerializeField]
    ////public float Max_time = 1.4f;

    //public Material[] original;


    ////public float delta = 1f;//0f-transparent  1f-original
    //public Material[] mat;
    public bool hidden = false;

    void Start()
    {
        mr = Up_wall_segment.GetComponent<MeshRenderer>();
        //original = Up_wall_segment.GetComponent<MeshRenderer>().materials.Clone() as Material[];
        //mat = original.Clone() as Material[];
    }

    // Update is called once per frame
    void Update()
    {
        if (hidden)
            mr.enabled = false;
        else
            mr.enabled = true;
        //for (int i = 0; i < original.Length; i++)
        //{
        //    if (hidden)
        //        delta -= (1f) * Time.deltaTime / Max_time;
        //    else
        //        delta += (1f) * Time.deltaTime / Max_time;

        //    delta = Mathf.Clamp(delta, 0f, 1f);
        //    //mat[i].color = new Color(delta, mat[i].color.r, mat[i].color.g, mat[i].color.b);
        //    if (delta == 0f)
        //        mat[i] = full_transparent;
        //    else
        //    if (delta == 1f)
        //        mat[i] = original[i];
        //    else
        //    {
        //        if (mat[i] != original[i] & mat[i] != full_transparent)
        //            mat[i].color = new Color(mat[i].color.r, mat[i].color.b, mat[i].color.g, delta);
        //        else
        //        {
        //            mat[i] = Material.Instantiate<Material>(original[i]);
        //            mat[i].color = new Color(mat[i].color.r, mat[i].color.b, mat[i].color.g, delta);
        //        }
        //    }
        //}
        //Up_wall_segment.GetComponent<MeshRenderer>().materials = mat;
    }
}
