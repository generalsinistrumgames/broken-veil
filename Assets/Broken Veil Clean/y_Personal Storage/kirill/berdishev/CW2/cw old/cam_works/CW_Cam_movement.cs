﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CW_Cam_movement : MonoBehaviour
{
    [Header("cam movings param")]
    public GameObject target;
    public GameObject go_cam_look_at;
    public GameObject go_cam_follow;
    public GameObject go_player_look_at;
    public Vector3  cam_look_at, 
                    cam_follow;


    [Header("room params")]
    [SerializeField]
    public Cam_controller current_room;
    public CW_room_fabric fabric;

    public String test_room_name = "";
    [HideInInspector]
    Vector3 test_cam,test_room;
    Cam_controller test_cc;
    //public Vector2 cam_x = new Vector2(-2.5f, 1.3f);
    //public Vector2 cam_y = new Vector2( 0.6f, 2.2f);
    //public Vector2 cam_z = new Vector2(-16.0f, 15.7f);

    //public Vector2 room_x = new Vector2(3.1f, -4.4f),
    //               room_y = new Vector2(0.3f, 2.0f),
    //               room_z = new Vector2(-0.1f, -5.4f);

    //public List<Room_data> rooms;
    //public CW_Transition test1;
    //public Room_data test_default_room;
    // Start is called before the first frame update
    void Start()
    {
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player");
        }

        fabric = gameObject.GetComponent<CW_room_fabric>();
        fabric.ini();

        current_room =fabric.default_room;
        test_cc = current_room;
        current_room.Start(target.transform.position, Vector3.zero);
        //current_room.Activate_transition();
        current_room.Reset(target.transform.position, target.transform.position);
        Upd_target_pos();
        Upd_cam_pos();
    }

    // Update is called once per frame
    void Update()
    {    
        //Debug.Log("fps="+(1f/Time.deltaTime).ToString()+" deltatime="+Time.deltaTime);
        Cam_controller cc_buffer = current_room;
        Vector3 target_pos = target.transform.position;
        Vector3 look_at= go_player_look_at.transform.position;
        Upd_target_pos();
        Upd_cam_pos();

        current_room = current_room.Upd(target_pos, look_at);

        if (cc_buffer!=current_room)
        {           
            cc_buffer.End();
            if (current_room is Room_data)
                if (cc_buffer is CW_Transition)
                {
                    CW_Transition tr = cc_buffer as CW_Transition;
                    if (tr.room1 == current_room)
                        tr.room2.hide_objects(current_room as Room_data);
                    if (tr.room2 == current_room)
                        tr.room1.hide_objects(current_room as Room_data);
                    if (!(tr.room1 == current_room)|| !(tr.room2 == current_room))
                        Debug.LogError("hide sistem error2");
                }
                else
                    Debug.LogError("hide sistem error");
            current_room.Start(target_pos, look_at);
        }    
        if(current_room is Room_data)
            test_room_name = (current_room as Room_data).fabric_name;
        if (current_room is CW_Transition)
            test_room_name = (current_room as CW_Transition).fabric_name;
    }
    void Upd_target_pos()
    {
        //cam_look_at.transform.position = current_room.Get_smooth_room_pos();
        cam_look_at = current_room.Get_smooth_room_pos();
        go_cam_look_at.transform.position = cam_look_at;
    }
    void Upd_cam_pos()
    {       
       //cam_follow.transform.position = current_room.Get_smooth_cam_pos();
       cam_follow = current_room.Get_smooth_cam_pos();
       go_cam_follow.transform.position = cam_follow;
    }
}

[Serializable]
public class Cube
{
    [HideInInspector]
    private Vector3[] vertex=new Vector3[] {Vector3.zero, Vector3.zero , Vector3.zero , Vector3.zero,
                                     Vector3.zero, Vector3.zero , Vector3.zero , Vector3.zero };
    public Vector3[] Vertex { get { return vertex; } }
    //public static Vector3[] zero_vertex = { Vector3.zero, Vector3.zero , Vector3.zero , Vector3.zero,
    //                                 Vector3.zero, Vector3.zero , Vector3.zero , Vector3.zero};

    public Color editor_main_color;
    public Color editor_center_color;
    public bool editor_center_draw;

    //public static Vector2[] zero_bounds = { Vector2.zero,Vector2.zero,Vector2.zero};

    [SerializeField]
    Vector2[] bounds=new Vector2[] { Vector2.zero, Vector2.zero, Vector2.zero };
    public Vector2[] Bounds
    {
        get
        {
            return (Vector2[])bounds.Clone();
        }
        set
        {
            if (value.Length != bounds.Length)
            {
                Debug.LogWarning("bounds length should be "+3.ToString()+" not " +value.Length.ToString());
                return;                
            }
            bounds = value;
        }
    }
    public Vector2 this[Axis indexer]
    {
        get
        {
            return bounds[(int)indexer];
        }
        set
        {
            if (indexer == Axis.x)
                bounds[0] = value;
            if (indexer == Axis.y)
                bounds[1] = value;
            if (indexer == Axis.z)
                bounds[2] = value;
        }
    }

    //      v[0]--------v[1]
    //     /|            /|
    //    / |           / |
    //   /  |          /  |
    //  /  v[2]-------/--v[3]
    // /    /        /   /
    // v[4]/--------v[5]/
    // |  /         |  /
    // | /          | /
    // |/           |/
    // v[6]--------v[7]

    public void Upd_vertexes()
    {
        if (editor_center_color.a == 0)
            editor_center_color = Color.red;
        if (editor_main_color == Color.clear)
        {
            Debug.Log("main color was=" + editor_main_color.a);
            editor_main_color = Color.blue;
        }
        if (vertex is null||vertex.Length!=8)
            vertex = new Vector3[] {Vector3.zero, Vector3.zero , Vector3.zero , Vector3.zero,
                                     Vector3.zero, Vector3.zero , Vector3.zero , Vector3.zero};
        Normolize_bounds();
        Vector2 bounds_x = Get_Bounds(Axis.x),
                bounds_y = Get_Bounds(Axis.y),
                bounds_z = Get_Bounds(Axis.z);
        ;
        //x
        for (int i = 0; i < vertex.Length; i++)
            if (i % 2 == 0)//0,2,4,6
                vertex[i].x = bounds_x.x;
            else
                vertex[i].x = bounds_x.y;
        //y
        for (int i = 0; i < vertex.Length; i++)
            if (i == 0 | i == 1 | i == 4 | i == 5)//0,1,4,5
                vertex[i].y = bounds_y.x;
            else
                vertex[i].y = bounds_y.y;
        //z
        for (int i = 0; i < vertex.Length; i++)
            if (i < 4)//0,1,2,3
                vertex[i].z = bounds_z.y;
            else
                vertex[i].z = bounds_z.x;
    }
    public void Normolize_bounds()
    {
        for(int i=0;i< bounds.Length; i++)
            if (bounds[i].x > bounds[i].y)
                bounds[i] = new Vector2(bounds[i].y, bounds[i].x);
    }
    public Vector2 Get_Bounds(Axis axis) 
    {
        return this[axis];
    }
    public void Set_bounds(Axis axis,Vector2 value)
    {
        this[axis] = value;
    }    
    public Vector3 Center
    {
        get 
        {
            Vector3 fin = Vector3.zero;
            for (int i = 0; i < Vertex.Length; i++)
                fin += Vertex[i];
            return fin / 8f;
        }
        set {
            Vector3 delta=Center-value;
            for (int i = 0; i < Vertex.Length; i++)
                Vertex[i] += delta;
        }
    }
    public float Width //x
    {
        get { return Get_Bounds(Axis.x).y - Get_Bounds(Axis.x).x; }
        set
        {
            if (value < 0)
                Width = -value;
            else
            {
                Vector2 h = Get_Bounds(Axis.x);
                float f = (h.x + h.y) / 2f;
                Set_bounds(Axis.x,new Vector2(f - value / 2f, f + value / 2f));
            }
        }
    }//x
    public float Height //y
    {
        get { return Get_Bounds(Axis.y).y - Get_Bounds(Axis.y).x; }
        set
        {
            if (value < 0)
                Height = -value;
            else
            {
                Vector2 h = Get_Bounds(Axis.y);
                float f = (h.x + h.y) / 2f;
                Set_bounds(Axis.y, new Vector2(f - value / 2f, f + value / 2f));            
            }
        }
    }//y
    public float Length //z
    {
        get { return Get_Bounds(Axis.z).y-Get_Bounds(Axis.z).x; }
        set
        {
            if (value < 0)
                Length = -value;
            else
            {
                Vector2 h = Get_Bounds(Axis.z);
                float f = (h.x + h.y) / 2f;
                Set_bounds(Axis.z, new Vector2(f - value / 2f, f + value / 2f));
            }
        }
    }//z
    public Cube(Vector2 bounds_x, Vector2 bounds_y, Vector2 bounds_z)
    {
        editor_center_draw = true;
        editor_main_color = Color.green;
        editor_center_color = Color.red;
        //vertex = (Vector3[])Cube.zero_vertex.Clone();
        //bounds = (Vector2[])zero_bounds.Clone();
        Set_bounds(Axis.x, bounds_x);
        Set_bounds(Axis.y, bounds_y);
        Set_bounds(Axis.z, bounds_z);
    }
    public Cube(Vector3 center,float width, float height, float length)
    {
        editor_center_draw = true;
        editor_main_color = Color.green;
        editor_center_color = Color.red;
        //vertex = (Vector3[])Cube.zero_vertex.Clone();
        //bounds = (Vector2[])zero_bounds.Clone();
        Center = center;
        Width = width;
        Height = height;
        Length = length;
    }
    public static Vector3 Pos_in_cube(Vector3 pos,Cube cube)
    {
        return new Vector3(
            cube.Get_Bounds(Axis.x).x + Mathf.Clamp01(pos.x) * cube.Width,
            cube.Get_Bounds(Axis.y).x + Mathf.Clamp01(pos.y) * cube.Height,
            cube.Get_Bounds(Axis.z).x + Mathf.Clamp01(pos.z) * cube.Length
            );
    }
    public Vector3 Pos_in_cube(Vector3 pos)
    {
        return Cube.Pos_in_cube(pos,this);
    }
}

[Serializable]
public class Room_data: Cam_controller,Drawable
{
    public bool editor_is_draw = true;

    public Cube room_cube;
    public Cube cam_cube;

    public bool[] cam_inverse_axes = { false, false, false };

    [HideInInspector]
    public Vector3 target_pos_destination_clamp;//Clamp по границам комнаты
    [HideInInspector]
    public Vector3 target_pos_destination;
    public Vector3 target_smooth_pos;

    [HideInInspector]
    public Vector3 player_look_pos_destination_clamp;//Clamp по границам комнаты
    [HideInInspector]
    public Vector3 player_look_pos_destination;
    public Vector3 player_look_smooth_pos;


    public cam_movement_param Target_Movement_Param;

    [HideInInspector]
    public Vector3 cam_pos_destination_clamp;//Clamp по границам куба для камеры
    [HideInInspector]
    public Vector3 cam_pos_destination;
    public Vector3 cam_smooth_pos;
    public cam_movement_param Cam_Movement_Param;
   
    #region fabric_params
    [Header("fabric params")]
    public string fabric_name = "";
    public List<String> fabric_transition_Names = new List<String>();


    //комнаты, которые находятся рядом с текущей комнатой, и hide_system_disable_objects которых не желательно скрывать
    public List<Room_data> hide_system_active_rooms;
    //игровые объекты, которые неплохо было бы скрыть при выходе из текущей комнаты
    public List<GameObject> hide_system_disable_objects;
    #endregion

    #region editor
    public Color editor_cam_color = Color.green;
    public Color editor_room_color = Color.yellow;
    public bool Is_draw()
    {
        return editor_is_draw;
    }

    public Cube[] Get_cubes()
    {
        if (editor_cam_color.a<1f)
            editor_cam_color.a = 1f;
        if (editor_room_color.a < 1f)
            editor_room_color.a = 1f;
        cam_cube.editor_main_color = editor_cam_color;
        room_cube.editor_main_color = editor_room_color;
        return new Cube[] {cam_cube,room_cube };
    }
    #endregion

    public Vector3 Get_smooth_cam_pos() 
    {
        return cam_smooth_pos;
    }
    public Vector3 Get_smooth_room_pos() 
    {
        //return target_smooth_pos; 
        return player_look_smooth_pos;
    }
    public void Start(Vector3 target_pos,Vector3 look_at)
    {
        //Reset(target_pos);
        Activate_transition();
    }
    public void End()
    {
        Deactivate_transition();
    }
    public Cam_controller Upd(Vector3 target_pos,Vector3 look_at)
    {
        target_pos_destination = target_pos;
        target_pos_destination_clamp = Room_clamp(target_pos_destination);
        target_smooth_pos = Target_Movement_Param.Smooth_move(target_smooth_pos, target_pos_destination_clamp);

        cam_pos_destination = project_cam(target_pos_destination_clamp, room_cube, cam_cube, cam_inverse_axes);
        cam_pos_destination_clamp = Cam_clamp(cam_pos_destination);
        cam_smooth_pos = Cam_Movement_Param.Smooth_move(cam_smooth_pos, cam_pos_destination_clamp);

        player_look_pos_destination = look_at;
        player_look_pos_destination_clamp = Room_clamp(player_look_pos_destination);
        player_look_smooth_pos = Target_Movement_Param.Smooth_move(player_look_smooth_pos, player_look_pos_destination_clamp);

        for (int i=0;i< room_transitions.Count;i++)
            if(room_transitions[i].is_work)
                return room_transitions[i];
        return this;
    }
    public Vector3 Room_clamp(Vector3 target)
    {
        return b_math.Clamp(target, room_cube.Get_Bounds(Axis.x), room_cube.Get_Bounds(Axis.y), room_cube.Get_Bounds(Axis.z));
    }
    public Vector3 Cam_clamp(Vector3 target)
    {
       return b_math.Clamp(target, cam_cube.Get_Bounds(Axis.x), cam_cube.Get_Bounds(Axis.y), cam_cube.Get_Bounds(Axis.z));
    }
    public static Vector3 project_cam(Vector3 target_pos, Cube room_cube, Cube cam_cube, bool[] inverse)
    {
        bool inv_x = false, inv_y = false, inv_z = false;
        if (inverse.Length > 0)
            inv_x = inverse[0];
        if (inverse.Length > 1)
            inv_y = inverse[1];
        if (inverse.Length > 2)
            inv_z = inverse[2];
        Vector3 target_project = project_cube(target_pos, room_cube, true);
        if (inv_x)
            target_project.x = 1 - target_project.x;
        if (inv_y)
            target_project.y = 1 - target_project.y;
        if (inv_z)
            target_project.z = 1 - target_project.z;

        return cam_cube.Pos_in_cube(target_project);
    }
    public static Vector3 project_cube(Vector3 target, Cube cube, bool clamp)
    {
        Vector2 c_x = cube.Get_Bounds(Axis.x);
        Vector2 c_y = cube.Get_Bounds(Axis.y);
        Vector2 c_z = cube.Get_Bounds(Axis.z);

        Vector3 fin = new Vector3(
            (target.x - c_x.x) / (cube.Width != 0 ? cube.Width : 99999999f),
            (target.y - c_y.x) / (cube.Height != 0 ? cube.Height : 99999999f),
            (target.z - c_z.x) / (cube.Length != 0 ? cube.Length : 99999999f));

        if (clamp)
        {
            fin.x = Mathf.Clamp01(fin.x);
            fin.y = Mathf.Clamp01(fin.y);
            fin.z = Mathf.Clamp01(fin.z);
        }
        return fin;
    }
    public void Reset(Vector3 target_pos,Vector3 look_at)
    {
        //Debug.Log("reset room name="+fabric_name);

        target_pos_destination = target_pos;
        target_pos_destination_clamp = Room_clamp(target_pos_destination);
        target_smooth_pos = target_pos_destination_clamp;

        cam_pos_destination = project_cam(target_pos_destination_clamp, room_cube, cam_cube, cam_inverse_axes);
        cam_pos_destination_clamp = Cam_clamp(cam_pos_destination);
        cam_smooth_pos = cam_pos_destination_clamp;

        player_look_pos_destination = look_at;
        player_look_pos_destination_clamp = Room_clamp(player_look_pos_destination);
        player_look_smooth_pos = player_look_pos_destination_clamp;
    }

    [SerializeField]
    public List<CW_Transition> room_transitions = new List<CW_Transition>();
    public void Activate_transition()
    {
        for (int i = 0; i < room_transitions.Count; i++)
            room_transitions[i].is_active = true;
    }
    public void Deactivate_transition()
    {
        for (int i = 0; i < room_transitions.Count; i++)
            room_transitions[i].is_active = false;
    }
    public List<CW_Transition> Get_all_transition()
    {
        return room_transitions;
    }

    public void hide_objects(Room_data new_room)
    {
        List<List<GameObject>> obj_lists=new List<List<GameObject>>();
        for(int i=0;i<hide_system_active_rooms.Count;i++)
            obj_lists.Add(hide_system_active_rooms[i].hide_system_disable_objects);

        for(int i=0;i<hide_system_disable_objects.Count;i++)
        {

        }
    }
}

[Serializable]
public struct cam_movement_param
{
    public float linear_per_second;
    public float geometric_per_sec;

    public cam_movement_param(float linear, float geom)
    {
        linear_per_second = linear;
        geometric_per_sec = geom;
    }
    public cam_movement_param(float linear)
    {
        linear_per_second = linear;
        geometric_per_sec = 0f;
    }

    public Vector3 Smooth_move(Vector3 current_pos, Vector3 final_pos)
    {
        return b_math.My_lerp(current_pos, final_pos, linear_per_second, geometric_per_sec);
    }
}
public enum Axis
{
    x,
    y,
    z
}