using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kostil_look : MonoBehaviour
{
    public Vector2 look;
    public float down_str = 1f;
    public float up_str = 1f;
    public float left_str = 1f;
    public float right_str = 1f;

    public KeyCode up=KeyCode.H;
    public KeyCode down = KeyCode.N;
    public KeyCode right = KeyCode.B;
    public KeyCode left = KeyCode.M;

    [Range(0.001f,0.5f)]
    public float lerp_str = 0.06f;


    [Range(0f, 1f)]
    public float cam_str =0.65f;
    public float look_cd=0.4f;
    public float look_cd_time = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Upd_look(float t)
    {
        const float max_dt = 0.01666f;
        if (t > max_dt)
        {
            Upd_look(t - max_dt);
            t = max_dt;
        }
        bool change = false;
        Vector2 input_v2 = new Vector2();
        if (Input.GetKey(up))
        {
            input_v2.y = 1f;
            change = true;
        }
        if (Input.GetKey(down))
        {
            input_v2.y = -1f;
            change = true;
        }

        if (Input.GetKey(left))
        { 
            input_v2.x = 1f;
            change = true;
        }
        if (Input.GetKey(right))
        {
            input_v2.x = -1f;
            change = true;
        }

        if(change)
        {
            look_cd_time = look_cd;
            look = Vector2.Lerp(look, input_v2, lerp_str * (t / max_dt));
        }
        else
        {
            if (look_cd_time > 0f)
                look_cd_time -= t;
            else
                look = Vector2.Lerp(look, Vector2.zero, lerp_str * (t / max_dt));
        }

        if (look.magnitude > 1f)
            look = look.normalized;
    }

    public CW2_points Upd(CW2_points raw)
    {
        CW2_points result = new CW2_points(raw.cam_pos,raw.room_pos);
        Upd_look(Time.deltaTime);



        float distance = Vector3.Distance(raw.room_pos, raw.cam_pos);
        Vector3 offset = new Vector3(0,0,0f);
        if (look.y > 0f)
            offset.y = look.y * up_str;
        if (look.y < 0f)
            offset.y = look.y * down_str;

        if (look.x > 0f)
            offset.x = look.x * left_str;
        if (look.x < 0f)
            offset.x = look.x * right_str;


        Vector3 fin_pos = distance * offset;
        Vector3 fin_target = distance * offset;
    
        Quaternion q = Quaternion.FromToRotation(raw.cam_pos, raw.room_pos);
        result.room_pos += q* fin_target;
        result.cam_pos += q* fin_pos* cam_str;


        return result;
    }

}
