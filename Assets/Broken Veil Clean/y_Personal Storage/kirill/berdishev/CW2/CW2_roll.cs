﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CW2_roll : MonoBehaviour
{
    public float max_change = 5f;
    public float change_time = 3f;
    public float current_change = 0f;
    public bool current_change_side = true;
    //
    // Сводка:
    //     true - positive change, false - negative
    public CinemachineVirtualCamera cinemachine_cam;

    public void Ini(CW2_error_handler _error_handler)
    {

    }
}
