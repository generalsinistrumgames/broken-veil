﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW2_look_offset : MonoBehaviour
{
    public Vector2 current_offset_old=Vector2.zero;
    public float distance_to_offset=0.2f;
    public float to_max_offset_time = 0.8f;

    public Vector2 current_offset_new = new Vector2();
    public Vector2 look_power = new Vector2(1.4f,0.8f);
    public float time_from_last_change = 0f;
    public float time_to_change = 0.6f;
}
