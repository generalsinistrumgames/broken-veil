﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW2_room_holder : MonoBehaviour
{
    public List<CW2_room> rooms = new List<CW2_room>(); 


    public void Ini(CW2_error_handler _error_handler)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform tr = transform.GetChild(i);
            CW2_room room;
            if (tr.gameObject.TryGetComponent<CW2_room>(out room))
                rooms.Add(room);
        }
        
        for(int i=0;i<rooms.Count;i++)
            rooms[i].Ini(_error_handler);

        return;
    }
}
