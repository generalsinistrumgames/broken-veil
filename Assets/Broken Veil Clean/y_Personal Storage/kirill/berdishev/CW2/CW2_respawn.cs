﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW2_respawn : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="Player")
            CW2_Main.main.reset_pos( force_reset:false );
    }
}
