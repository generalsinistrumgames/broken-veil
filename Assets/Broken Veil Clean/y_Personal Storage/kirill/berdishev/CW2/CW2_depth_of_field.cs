﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

public class CW2_depth_of_field : MonoBehaviour
{
    //public float near_blur_start = 2.1f;
    //public float near_blur_end = 0.45f;
    //[Range(0f,8f)]
    //public float near_blur_strengh = 4f;

    //[Space]

    //public float far_blur_start = 0.45f;
    //public float far_blur_end = 2.2f;
    //[Range(0f, 16f)]
    //public float far_blur_strengh = 4f;

    [SerializeField]
    public CW2_DOF_settings default_settings = new CW2_DOF_settings();

    [Space]
    public DOF_quality_settings quality = DOF_quality_settings._default;
    [Space]
    [Space]
    [Space]
    public float debug_last_far_str = 0f;
    public float debug_last_near_str = 0f;
    public float debug_distance = 0f;
    public CW2_DOF_settings debug_settings = new CW2_DOF_settings();


    DOF_quality_settings last_quality = DOF_quality_settings._default;

    public List<DepthOfField> debug_dofComponents=new List<DepthOfField>();

    public void Ini(CW2_error_handler _error_handler)
    {

        debug_settings = default_settings;
        Volume[] volume = GameObject.FindObjectsOfType<Volume>();
        //Volume[] volume = _volume_profile_go.GetComponent<Volume>();
        DepthOfField tmp;
        debug_dofComponents.Clear();
        for(int i=0;i<volume.Length;i++)
            if (volume[i].profile.TryGet<DepthOfField>(out tmp))
            {   
                debug_dofComponents.Add( tmp);
            }
            //else
            //{
            //    volume[i].profile.Add<DepthOfField>();
            //    volume[i].profile.TryGet<DepthOfField>(out tmp);
            //    debug_dofComponents.Add(tmp);
            //} 
    }

    public void set()
    {
        //kostil
        bool error = false;
        if (debug_dofComponents.Count == 0)
            error = true;
        else
            for(int i=0;i< debug_dofComponents.Count;i++)
                if (debug_dofComponents[i] == null)
                    error = true;
        if (error)
            Ini(new CW2_error_handler());
        //kostil off

        if (quality == DOF_quality_settings.dont_work)
            return;

        if (quality == DOF_quality_settings.off)
            return; 
        set_quality();
        float
            NFS = debug_distance - debug_settings.near_blur_start,
            NFE = debug_distance - debug_settings.near_blur_end,
            FFS = debug_distance + debug_settings.far_blur_start, 
            FFE = debug_distance + debug_settings.far_blur_end;
        for (int i = 0; i < debug_dofComponents.Count; i++)
        {
            debug_dofComponents[i].nearFocusStart.value = NFS;
            debug_dofComponents[i].nearFocusEnd.value = NFE;
            debug_dofComponents[i].farFocusStart.value = FFS;
            debug_dofComponents[i].farFocusEnd.value = FFE;

            if(debug_last_far_str!= debug_settings.far_blur_strengh)
            {
                debug_last_far_str = debug_settings.far_blur_strengh;
                debug_dofComponents[i].farMaxBlur = debug_settings.far_blur_strengh;
            }
            if (debug_last_near_str != debug_settings.near_blur_strengh)
            {
                debug_last_near_str = debug_settings.near_blur_strengh;
                debug_dofComponents[i].nearMaxBlur = debug_settings.near_blur_strengh;
            }
        }
    }

    public void set_quality()
    {
        if (quality == DOF_quality_settings.dont_work)
            return;
        if (quality == DOF_quality_settings.off)
            return;
        if (quality == DOF_quality_settings._default)
            quality = DOF_quality_settings.medium;
        else
            if (last_quality == quality)
                return;

 //       for(int i = 0; i < dofComponents.Count; i++)
 //       {
 //            if(dofComponents[i].quality==ScalableSettingLevelParameter.Level.
 //       }

        if (quality == DOF_quality_settings.off)
            for (int i = 0; i < debug_dofComponents.Count; i++)
                debug_dofComponents[i].active = false;

        if (quality == DOF_quality_settings.low)
            for (int i = 0; i < debug_dofComponents.Count; i++)
            {
                debug_dofComponents[i].active = true;
                debug_dofComponents[i].resolution = DepthOfFieldResolution.Quarter;
                debug_dofComponents[i].nearSampleCount = 1;
                debug_dofComponents[i].nearMaxBlur = debug_settings.near_blur_strengh;
                debug_dofComponents[i].farSampleCount = 1;
                debug_dofComponents[i].farMaxBlur = debug_settings.far_blur_strengh;
            }

        if (quality == DOF_quality_settings.medium)
            for (int i = 0; i < debug_dofComponents.Count; i++)
            {
                debug_dofComponents[i].active = true;
                debug_dofComponents[i].resolution = DepthOfFieldResolution.Half;
                debug_dofComponents[i].nearSampleCount = 2;
                debug_dofComponents[i].nearMaxBlur = debug_settings.near_blur_strengh;
                debug_dofComponents[i].farSampleCount = 3;
                debug_dofComponents[i].farMaxBlur = debug_settings.far_blur_strengh;
            }

        if (quality == DOF_quality_settings.high)
            for (int i = 0; i < debug_dofComponents.Count; i++)
            {
                debug_dofComponents[i].active = true;
                debug_dofComponents[i].resolution = DepthOfFieldResolution.Half;
                debug_dofComponents[i].nearSampleCount = 4;
                debug_dofComponents[i].nearMaxBlur = debug_settings.near_blur_strengh;
                debug_dofComponents[i].farSampleCount = 6;
                debug_dofComponents[i].farMaxBlur = debug_settings.far_blur_strengh;
            }

        if (quality == DOF_quality_settings.super)
            for (int i = 0; i < debug_dofComponents.Count; i++)
            {
                debug_dofComponents[i].active = true;
                debug_dofComponents[i].resolution = DepthOfFieldResolution.Full;
                debug_dofComponents[i].nearSampleCount = 5;
                debug_dofComponents[i].nearMaxBlur = debug_settings.near_blur_strengh;
                debug_dofComponents[i].farSampleCount = 9;
                debug_dofComponents[i].farMaxBlur = debug_settings.far_blur_strengh;
            }

        if (quality == DOF_quality_settings.ultra)
            for (int i = 0; i < debug_dofComponents.Count; i++)
            {
                debug_dofComponents[i].active = true;
                debug_dofComponents[i].resolution = DepthOfFieldResolution.Full;
                debug_dofComponents[i].nearSampleCount = 8;
                debug_dofComponents[i].nearMaxBlur = debug_settings.near_blur_strengh;
                debug_dofComponents[i].farSampleCount = 16;
                debug_dofComponents[i].farMaxBlur = debug_settings.far_blur_strengh;
            }

        last_quality = quality;
    }
}

[Serializable]
public class CW2_DOF_settings
{
    public float near_blur_start = 2.1f;
    public float near_blur_end = 0.45f;
    [Range(0f, 8f)]
    public float near_blur_strengh = 4f;

    [Space]

    public float far_blur_start = 0.45f;
    public float far_blur_end = 2.2f;
    [Range(0f, 16f)]
    public float far_blur_strengh = 4f;

    public static CW2_DOF_settings blend(CW2_DOF_settings settings1, CW2_DOF_settings settings2,float settings1_blend_factor)
    {

        settings1_blend_factor = Mathf.Clamp01(settings1_blend_factor);
        CW2_DOF_settings new_settings = new CW2_DOF_settings();

        new_settings.near_blur_start = blend_val(settings1.near_blur_start, settings2.near_blur_start, settings1_blend_factor);
        new_settings.near_blur_end = blend_val(settings1.near_blur_end, settings2.near_blur_end, settings1_blend_factor);
        new_settings.near_blur_strengh = blend_val(settings1.near_blur_strengh, settings2.near_blur_strengh, settings1_blend_factor);

        new_settings.far_blur_start = blend_val(settings1.far_blur_start, settings2.far_blur_start, settings1_blend_factor);
        new_settings.far_blur_end = blend_val(settings1.far_blur_end, settings2.far_blur_end, settings1_blend_factor);
        new_settings.far_blur_strengh = blend_val(settings1.far_blur_strengh, settings2.far_blur_strengh, settings1_blend_factor);


        return new_settings;
    }
    static float blend_val(float val1,float val2,float factor)
    {
        return val1 * factor + val2 * (1f - factor);
    }

    public CW2_DOF_settings()
    { }
    public CW2_DOF_settings(CW2_DOF_settings target)
    {
    near_blur_start = target.near_blur_start;
    near_blur_end = target.near_blur_end;
    near_blur_strengh = target.near_blur_strengh;
    far_blur_start = target.far_blur_start;
    far_blur_end = target.far_blur_end;
    far_blur_strengh = target.far_blur_strengh;
}
}

public enum DOF_quality_settings
{
    off,
    low,
    medium,
    high,
    super,
    ultra,
    _default,
    dont_work
}