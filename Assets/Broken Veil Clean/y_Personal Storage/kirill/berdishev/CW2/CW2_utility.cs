﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CW2_utility
{
    public static bool Try_find_child(string name,out Transform _out, Transform _target)
    {
        _out = _target.Find(name);
        if(_out != null)
            return true;
        return false;
    }
}


public enum CW2_axis
{
    x,
    y,
    z
}
