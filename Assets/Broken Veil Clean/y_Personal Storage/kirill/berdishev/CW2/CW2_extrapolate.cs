using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CW2_extrapolate : MonoBehaviour
{
    public bool error = false;
    public bool first_frame = true;
    public string cam_pos_name = "extrapoly_cam_pos";
    public GameObject e_cam_pos;
    public string target_pos_name = "extrapoly_target_pos";
    public GameObject e_target_pos;

    public Vector3 last_cam_pos = new Vector3();
    public Vector3 last_target_pos = new Vector3();

    public Vector3 local_cam_pos_offset = new Vector3();
    public Vector3 local_target_pos_offset = new Vector3();

    public Vector3 final_cam_pos_offset = new Vector3();
    public Vector3 final_target_pos_offset = new Vector3();

    public Vector3 max_offset_cam = new Vector3(2f, 0.2f, 0.8f);
    public Vector3 max_offset_target = new Vector3(2f, 0.2f, 0.8f);
    [Range(0f,1f)]
    public float lastpos_cam_smooth_value = 0.08f;
    [Range(0f, 1f)]
    public float lastpos_target_smooth_value = 0.08f;
    [Space]
    public float multi_mod = 2f;

    public Rigidbody player;
    public CW2_Main cw2;

    // Start is called before the first frame update
    void Start()
    {
        first_frame = true;
        Transform temp;
        if (!CW2_utility.Try_find_child(cam_pos_name, out temp, transform))
            error = true;
        else
            e_cam_pos = temp.gameObject;

        if (!CW2_utility.Try_find_child(target_pos_name, out temp, transform))
            error = true;
        else
            e_target_pos = temp.gameObject;
    }

    // Update is called once per frame
    public void before_cycle(GameObject cam_pos, GameObject target_pos)
    {
        if (error)
            return;
        last_cam_pos = cam_pos.transform.position;
        last_target_pos = target_pos.transform.position;
    }
    public void after_cycle(GameObject cam_pos, GameObject target_pos)
    {
        if (error)
            return;
        calc(cam_pos, target_pos);
    }
    public void swap(ref GameObject g1,ref GameObject g2)
    {
        GameObject temp = g1;
        g1 = g2;
        g2 = temp;
    }
    public void calc(GameObject cam_pos, GameObject target_pos)
    {
        
        if (Time.deltaTime < 0.0001f)
            return;
        if(first_frame)
        {
            CW2_Main[] go_list = GameObject.FindObjectsOfType<CW2_Main>();//.FindGameObjectsWithTag("Player");
            cw2 = go_list[0];
            if (player == null)
                player = cw2.player.GetComponent<Rigidbody>();
            e_cam_pos.transform.position = cam_pos.transform.position;
            e_target_pos.transform.position = target_pos.transform.position;
            first_frame = false;
            return;
        }
        float change_s=1f/Time.deltaTime;

        Vector3 delta_cam = (cam_pos.transform.position - last_cam_pos)* change_s;
        Vector3 delta_tar = (target_pos.transform.position - last_target_pos) * change_s;

        Vector3 velocity = player.velocity;
        velocity.y = 0f;
        delta_cam = (delta_cam + velocity) / 2f;
        delta_tar = (delta_tar*4 + velocity) / 5f;

        delta_tar.y = player.transform.position.y;

        delta_cam = clamp_vector(delta_cam, max_offset_cam);
        delta_tar = clamp_vector(delta_tar, max_offset_target);        

        local_cam_pos_offset = (delta_cam * multi_mod) * lastpos_cam_smooth_value + local_cam_pos_offset * (1f - lastpos_cam_smooth_value);
        local_target_pos_offset = (delta_tar * multi_mod) * lastpos_target_smooth_value + local_target_pos_offset * (1f - lastpos_target_smooth_value);

        

        e_cam_pos.transform.position = cam_pos.transform.position + local_cam_pos_offset;
        e_target_pos.transform.position = target_pos.transform.position + local_target_pos_offset;

        //e_cam_pos.transform.position = cam_pos.transform.position + local_cam_pos_offset;
        //e_target_pos.transform.position = target_pos.transform.position + local_target_pos_offset;
        //e_cam_pos.transform.position= (cam_pos.transform.position+delta_cam* multi_mod)* lastpos_value+ e_cam_pos.transform.position* (1f-lastpos_value);
        //e_target_pos.transform.position= (target_pos.transform.position + delta_tar* multi_mod)* lastpos_value+ e_target_pos.transform.position * (1f - lastpos_value);
    }

    public Vector3 clamp_vector(Vector3 target,Vector3 clamp)
    {
        Vector3 fin = target;
        if (fin.x > 0)
        {
            if (fin.x > clamp.x)
                fin.x = clamp.x;
        }
        else
        {
            if (fin.x < (clamp.x)*-1f)
                fin.x = clamp.x * -1f;
        }

        if (fin.y > 0)
        {
            if (fin.y > clamp.y)
                fin.y = clamp.y;
        }
        else
        {
            if (fin.y < (clamp.y) * -1f)
                fin.y = clamp.y * -1f;
        }

        if (fin.z > 0)
        {
            if (fin.z > clamp.z)
                fin.z = clamp.z;
        }
        else
        {
            if (fin.z < (clamp.z) * -1f)
                fin.z = clamp.z * -1f;
        }
        
        return fin;
    }
}
