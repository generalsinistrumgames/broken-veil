﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

public class flashlight : MonoBehaviour
{
    public Light target_light;
    public AnimationCurve intencity_change;
    public float base_intencity_candela;
    public float change_intencity_time = 1.5f;
    
    public float intencity_change_val = 0f;

    public bool pingpong = false;


    // Start is called before the first frame update
    void Start()
    {
        if (target_light == null)
            TryGetComponent<Light>(out target_light);
        base_intencity_candela = target_light.intensity;
    }

    // Update is called once per frame
    void Update()
    {  
        intencity_change_val += (Time.deltaTime / change_intencity_time);
        if (!pingpong)
        {

            if (intencity_change_val > 1f)
                intencity_change_val = intencity_change_val % 1f;

            target_light.intensity = intencity_change.Evaluate(intencity_change_val) * base_intencity_candela;
            //Debug.Log("intensity=" + target_light.intensity);
        }
        else
        {
            if (intencity_change_val > 2f)
                intencity_change_val = intencity_change_val % 2f;
            float val;
            if (intencity_change_val < 1f)
                val = intencity_change_val;
            else
                val = 1f- (intencity_change_val-1f);
            target_light.intensity = intencity_change.Evaluate(val) * base_intencity_candela;
        }
    }
}
