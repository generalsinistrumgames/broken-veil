﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class metric : MonoBehaviour
{
    public metric other;


    public Vector3 output;
    // Start is called before the first frame update
    void Start()
    {
        output = gameObject.transform.position - other.transform.position;
        output.x = Mathf.Abs(output.x);
        output.y = Mathf.Abs(output.y);
        output.z = Mathf.Abs(output.z);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
