﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide_transparancy : MonoBehaviour
{
    public bool editor_draw = true;
    public bool inverse = false;
    [Range(0f,1f)]
    public float door_to_collider_percent = 0.2f;
    public List<GameObject> transparancy_objects=new List<GameObject>();

    float prev_alpha_multi = 1f;
    bool start = true;
    public float alpha_multi = 1f;
    CapsuleCollider collider;
    public GameObject player_ref;
    // Start is called before the first frame update


    void Start()
    {
        collider = GetComponent<CapsuleCollider>();

        player_ref = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        collider = GetComponent<CapsuleCollider>();
        if (Vector3.Distance(player_ref.transform.position, collider.transform.position + collider.center) > collider.radius * 1.2f)
            return;
        alpha_multi = get_blend_val_from_pos(player_ref.transform.position);
        //if (!work)
        //    return;
        if (inverse)
        {
            if (player_ref.transform.position.z < gameObject.transform.position.z)
                alpha_multi = 0f;
        }
        else
        {
            if (player_ref.transform.position.z > gameObject.transform.position.z)
                alpha_multi = 0f;
        }
        if (start)
        {
            start = false;
            prev_alpha_multi = alpha_multi;
        }
        for(int i=0;i<transparancy_objects.Count;i++)
        {
            set_transparency(transparancy_objects[i], alpha_multi);
        }
    }

    void set_transparency(GameObject gameObject,float alpha)
    {
        if (alpha < 0)
            alpha = 0;
        if (alpha > 1)
            alpha = 1;

        var block = new MaterialPropertyBlock();
        MeshRenderer mesh_renderer = gameObject.GetComponent<MeshRenderer>();
        const float turn_moment = 0.02f;
        //if (prev_alpha_multi < turn_moment)
        //{
        //    if (alpha > turn_moment)
        //        mesh_renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        //}
        //else
        //{
        //    if(alpha < turn_moment)
        //        mesh_renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
        //}



        //else
        //    mesh_renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        Color c = mesh_renderer.materials[0].color;
        c.a = alpha;
        block.SetColor("_BaseColor", c);
        mesh_renderer.SetPropertyBlock(block);
    }

    float get_blend_val_from_pos(Vector3 pos)
    {
        float result = 0f;
        Vector3 center = transform.position + collider.center;
        center.y = pos.y;


        result = Vector3.Distance(pos, center)/collider.radius; 
        if (result > 1f)
            result = 1f;

        Vector3[] door_points = 
            {
            new Vector3(center.x - collider.radius * door_to_collider_percent, center.y, center.z),
            new Vector3(center.x + collider.radius * door_to_collider_percent, center.y, center.z),
            new Vector3(Mathf.Clamp(pos.x,center.x - collider.radius * door_to_collider_percent,center.x + collider.radius * door_to_collider_percent), center.y, center.z)
        };
        float result2 = Vector3.Distance(door_points[nearliest(door_points, pos)],pos)/ collider.radius/(1f-door_to_collider_percent);
        if (1f - door_to_collider_percent < 0.001f)
            result2 = 1f;
        if (result2 > 1f)
            result2 = 1f;
        

        float fin = 0f;
        if(result2>result)
        {
            fin = result*result*result2;
        }
        else
        {
            fin = result2*result2;
        }

        if (fin > 1f)
            fin = 1f;
        return fin;
    }

    int nearliest(Vector3[] mass,Vector3 target)
    {
        if (mass.Length == 1)
            return 0;
        float result_dist = Vector3.Distance(mass[0],target);
        int result = 0;
        for(int i=1;i<mass.Length;i++)
        {
            float _f = Vector3.Distance(mass[i], target);
            if ( result_dist> _f )
            {
                result_dist = _f;
                result = i;
            }
        }

        return result;
    }
}
