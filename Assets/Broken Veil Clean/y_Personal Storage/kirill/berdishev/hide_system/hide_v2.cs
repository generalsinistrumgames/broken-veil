﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hide_v2 : MonoBehaviour
{
    public GameObject player;
    public float hide_val = 1f; //0-full transparacy
    public float hide_time = 1.5f;
    public bool inverse = false;
    public CW2_axis axis = CW2_axis.z;
    public BoxCollider bx;
    public GameObject[] transparancy_go;
    public GameObject ebanutaya_stenka_ot_antona;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        bx.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = player.transform.position;
        if (
            !(hide_val == 0f || hide_val == 1f)
            || is_point_in_cube(bx, pos))
        {
            float change = Time.deltaTime / hide_time;
            float pos_val = 0f;
            float center_val = 0f;

            if (axis == CW2_axis.x)
            {
                pos_val = pos.x;
                center_val = bx.transform.position.x + bx.center.x;
            }
            if (axis == CW2_axis.y)
            {
                pos_val = pos.y;
                center_val = bx.transform.position.y + bx.center.y;
            }
            if (axis == CW2_axis.z)
            {
                pos_val = pos.z;
                center_val = bx.transform.position.z + bx.center.z;
            }

            if (pos_val < center_val ^ inverse)
                hide_val += change;
            else
                hide_val -= change;

            if (hide_val > 1f)
                hide_val = 1f;
            if (hide_val < 0f)
                hide_val = 0f;
            for(int i=0;i<transparancy_go.Length;i++)
                if(transparancy_go[i]!=ebanutaya_stenka_ot_antona)
                    apply_transparancy(transparancy_go[i]);
            apply_ebanutaya_stenka(ebanutaya_stenka_ot_antona);
        }
    }
    void apply_ebanutaya_stenka(GameObject go)
    {
        var block = new MaterialPropertyBlock();
        MeshRenderer mesh_renderer;
        if (!go.TryGetComponent<MeshRenderer>(out mesh_renderer))
            return;
        for (int i = 0; i < mesh_renderer.materials.Length; i++)
        {
            Color c = mesh_renderer.materials[i].color;
            c.a = hide_val;
            //mesh_renderer.material.shader = Shader.Find("_Color");
            //mesh_renderer.material.SetColor("_Color", new Color(c.r, c.g, c.b, hide_val));
            //mesh_renderer.materials[0].color =new Color(c.r,c.g,c.b, hide_val); 
            //block.SetColor("_BaseColor", c);
            //mesh_renderer.materials[0].SetColor("_BaseColor", new Color(c.r, c.g, c.b, hide_val));
            //Debug.Log("set_block");
            //mesh_renderer.SetPropertyBlock(block);

            //Renderer nRend = go.GetComponent<Renderer>();
            Material _mat = mesh_renderer.materials[i];
            _mat.SetColor("_BaseColor", new Color(c.r, c.g, c.b, hide_val));
        }
    }
    void apply_transparancy(GameObject go)
    {
        var block = new MaterialPropertyBlock();
        MeshRenderer mesh_renderer;
        if(! go.TryGetComponent<MeshRenderer>(out mesh_renderer))
            return;
        Color c = mesh_renderer.materials[0].color;
        c.a = hide_val;
        //mesh_renderer.material.shader = Shader.Find("_Color");
        //mesh_renderer.material.SetColor("_Color", new Color(c.r, c.g, c.b, hide_val));
        //mesh_renderer.materials[0].color =new Color(c.r,c.g,c.b, hide_val); 
        //block.SetColor("_BaseColor", c);
        //mesh_renderer.materials[0].SetColor("_BaseColor", new Color(c.r, c.g, c.b, hide_val));
        //Debug.Log("set_block");
        //mesh_renderer.SetPropertyBlock(block);

        Renderer nRend = go.GetComponent<Renderer>();
        Material _mat = nRend.material;
        _mat.SetColor("_BaseColor", new Color(c.r, c.g, c.b, hide_val));
    }

    bool is_point_in_cube(BoxCollider collider, Vector3 pos)
    {
        Vector3 center = collider.center + collider.transform.position;
        Vector3 size = CRT_collider_size_normalized(collider);
        Vector3 difference_between_center_and_pos = center - pos;
        if (Mathf.Abs(difference_between_center_and_pos.x) > size.x / 2f)
            return false;
        if (Mathf.Abs(difference_between_center_and_pos.y) > size.y / 2f)
            return false;
        if (Mathf.Abs(difference_between_center_and_pos.z) > size.z / 2f)
            return false;

        return true;
    }

    Vector3 CRT_collider_size_normalized(BoxCollider collider)
    {
        Vector3 size = collider.size;
        if (size.x < 0)
        {
            collider.size = new Vector3(size.x * -1f, size.y, size.z);
            size = collider.size;
        }
        if (size.y < 0)
        {
            collider.size = new Vector3(size.x, size.y * -1f, size.z);
            size = collider.size;
        }
        if (size.z < 0)
        {
            collider.size = new Vector3(size.x, size.y, size.z * -1f);
            size = collider.size;
        }
        return size;
    }
}
