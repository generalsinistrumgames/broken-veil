﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor;
using UnityEngine;

//[CustomEditor(typeof(Hide_transparancy))]
public class Hide_transparancy_editor /*: Editor*/
{
    Hide_transparancy target_object;
    CapsuleCollider collider;
    // Start is called before the first frame update
    void OnEnable()
    {
        //target_object = (Hide_transparancy)target;
        collider = target_object.gameObject.GetComponent<CapsuleCollider>();
    }

    void OnSceneGUI()
    {
        if (!target_object.enabled)
            return;
        if (!target_object.editor_draw)
            return;
        //Draw();
        draw_arrow();
    }

    //void Draw()
    //{
    //    Vector3[,] pos = Get_draw_pos();
    //    Handles.color = Color.red;
    //    Handles.DrawLine(pos[0,0], pos[0,1]);
    //    Handles.DrawLine(pos[0,0], pos[0,2]);
    //    Handles.DrawLine(pos[0,2], pos[0,3]);
    //    Handles.DrawLine(pos[0,3], pos[0,1]);

    //    Handles.color = Color.green;
    //    Handles.DrawLine(pos[1, 0], pos[1, 1]);
    //    Handles.DrawLine(pos[1, 0], pos[1, 2]);
    //    Handles.DrawLine(pos[1, 2], pos[1, 3]);
    //    Handles.DrawLine(pos[1, 3], pos[1, 1]);
    //}

    void draw_arrow()
    {
        Vector3 collider_wall_pos = collider.center;
        collider_wall_pos.z += collider.radius;
        const float arrow_mod_x = 0.6f, arrow_mod_z = 0.6f;
        Vector3 collider_arrow_l = new Vector3(collider.radius * arrow_mod_x, collider_wall_pos.y, collider_wall_pos.z * arrow_mod_z);
        Vector3 collider_arrow_r = new Vector3(collider_arrow_l.x * -1, collider_arrow_l.y, collider_arrow_l.z);
        Vector3 global_pos = target_object.gameObject.transform.position;

        if (target_object.inverse)
        {
            collider_wall_pos.z *= -1f;
            collider_arrow_l *= -1f;
            collider_arrow_r *= -1f;
        }

        collider_wall_pos += global_pos;
        collider_arrow_l += global_pos;
        collider_arrow_r += global_pos;

        //Handles.color = Color.blue;
        //Handles.DrawLine(global_pos + collider.center, collider_wall_pos);
        //Handles.DrawLine(collider_arrow_l, collider_wall_pos);
        //Handles.DrawLine(collider_arrow_r, collider_wall_pos);
    }
    Vector3[,] Get_draw_pos()
    {
        Vector3[,] pos = new Vector3[2,4];
        for (int i = 0; i < 4; i++)
        {
            pos[0, i] = collider.center+ target_object.transform.position;
            pos[1, i] = pos[0, i];
            if (i >= 2)
            {
                pos[0, i].x += collider.radius;
                pos[1, i].x += collider.radius * target_object.door_to_collider_percent;
            }
            else
            {
                pos[0, i].x -= collider.radius;
                pos[1, i].x -= collider.radius * target_object.door_to_collider_percent;
            }
            if (i % 2 == 0)
            {
                pos[0, i].y += collider.height / 2f;
                pos[1, i].y = pos[0, i].y;
            }
            else
            {
                pos[0, i].y -= collider.height / 2f;
                pos[1, i].y = pos[0, i].y;
            }
        }
        return pos;
    }
}