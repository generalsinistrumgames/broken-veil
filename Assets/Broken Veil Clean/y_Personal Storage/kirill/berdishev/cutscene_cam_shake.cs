using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class cutscene_cam_shake : MonoBehaviour
{

    public UnityEngine.Playables.PlayableDirector cutscene_timeline;
    public CinemachineVirtualCamera cutscene_Vcam;

    [Header("debug parameters")]

    public Transform real_cam_pos;
    public Transform real_cam_target;

    public Transform cam_pos;
    public Transform cam_target;

    [Header("traska_param")]
    public float intencity = 0f;
    [Range(0f, 1f)]
    public float str_per_distance_pos = 0.384f;
    [Range(0f, 0.25f)]
    public float intencity_change = 0.0327f;
    [Range(0f, 1f)]
    public float str_per_distance_target = 0.329f;
    public Vector3 cam_pos_new_rnd = new Vector3();
    public Vector3 cam_pos_old_rnd = new Vector3();
    public Vector3 cam_target_new_rnd = new Vector3();
    public Vector3 cam_target_old_rnd = new Vector3();
    public bool cam_pos_shake = true;
    public bool cam_target_shake = true;
    public bool synchronize = true;
    public float time = 0f;
    [Range(0f, 1f)]
    public float time_interval = 0.036f;
    bool intencity_change_frame = false;

    // Start is called before the first frame update
    void Start()
    {
        real_cam_pos = cutscene_Vcam.Follow;
        real_cam_target = cutscene_Vcam.LookAt;

        cam_pos = Instantiate<GameObject>(real_cam_pos.gameObject, gameObject.transform).transform;
        cam_target = Instantiate<GameObject>(real_cam_target.gameObject, gameObject.transform).transform;

        cutscene_Vcam.Follow = cam_pos;
        cutscene_Vcam.LookAt = cam_target;
    }

    public void impulse100()
    {
        impulse(1f, intencity_change);
    }

    public void impulse075()
    {
        impulse(0.75f, intencity_change);
    }

    public void impulse050()
    {
        impulse(0.5f, intencity_change);
    }

    public void impulse025()
    {
        impulse(0.25f, intencity_change);
    }


    public void impulse(float _intencity, float _intencity_change)
    {
        intencity = _intencity;
        intencity_change = _intencity_change;
        intencity_change_frame = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(cutscene_timeline.state==PlayState.Playing)
        {
            cam_pos.position = real_cam_pos.position;
            cam_target.position = real_cam_target.position;

            if (!intencity_change_frame & intencity < 0.001f)
            {
                intencity = 0f;
            }
            //cam_target.transform.position = player.transform.position;
            //cam_pos.transform.position = cam_target.transform.position + offset;
            else
                intencity = Mathf.Lerp(intencity, 0f, intencity_change);
            float distance = Vector3.Distance(cam_target.position, cam_pos.position);
            time -= Time.deltaTime;
            if (time < 0)
            {
                gavno:
                time += time_interval;
                cam_pos_old_rnd = cam_pos_new_rnd;
                cam_target_old_rnd = cam_target_new_rnd;
                generate_new_pos();
                if (time < 0)
                    goto gavno;
            }
            Vector3 fin_pos = new Vector3();
            Vector3 fin_target = new Vector3();
            float lerp_param = time / time_interval;
            if (synchronize && cam_pos_shake && cam_target_shake)
            {
                fin_pos = distance * intencity * str_per_distance_pos * Vector3.Lerp(cam_pos_new_rnd, cam_pos_old_rnd, lerp_param);
                fin_target = distance * intencity * str_per_distance_target * Vector3.Lerp(cam_pos_new_rnd, cam_pos_old_rnd, lerp_param);
            }
            else
            {
                if (cam_pos_shake)
                    fin_pos = distance * intencity * str_per_distance_pos * Vector3.Lerp(cam_pos_new_rnd, cam_pos_old_rnd, lerp_param);
                if (cam_target_shake)
                    fin_target = distance * intencity * str_per_distance_target * Vector3.Lerp(cam_target_new_rnd, cam_target_old_rnd, lerp_param);
            }
            Quaternion q = Quaternion.FromToRotation(cam_pos.position, cam_target.position);
            cam_target.position += q * fin_target;
            cam_pos.position += q * fin_pos;
        }
    }

    void generate_new_pos()
    {
        cam_pos_new_rnd = new Vector3(Random.value - 0.5f, Random.value - 0.5f, 0f);
        cam_target_new_rnd = new Vector3(Random.value - 0.5f, Random.value - 0.5f, 0f);
    }
}
