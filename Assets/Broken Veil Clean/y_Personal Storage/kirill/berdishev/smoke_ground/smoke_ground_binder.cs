﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class smoke_ground_binder : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        transform.localScale = new Vector3(1f, 1f, 1f);

        BoxCollider bc = GetComponent<BoxCollider>();
        bc.enabled = false;
        VisualEffect ve=gameObject.GetComponent<VisualEffect>();
        ve.SetVector3("center",transform.position+bc.center);
        ve.SetVector3("size", bc.size);
    }
}
