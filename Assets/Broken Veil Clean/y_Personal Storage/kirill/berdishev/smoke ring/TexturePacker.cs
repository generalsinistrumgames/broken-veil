﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEditor;
//using System.IO;

//public class TexturePacker : EditorWindow
//{

//    public Texture2D metalic, ambientOcclusion, detailMask, smoothness, maskMap;
//    public string textureName = "Untitled";
//    public int width, height;
//    public bool inverseSmoothness;
//    private string path
//    {

//        get
//        {

//            string a = "";
//            if(metalic != null)
//            {

//                a = AssetDatabase.GetAssetPath((Object)metalic);
//                a = a.Substring(0, a.IndexOf(((Object)metalic).name));
//                return a;

//            }
//            if (ambientOcclusion != null)
//            {

//                a = AssetDatabase.GetAssetPath((Object)ambientOcclusion);
//                a = a.Substring(0, a.IndexOf(((Object)ambientOcclusion).name));
//                return a;

//            }
//            if (detailMask != null)
//            {

//                a = AssetDatabase.GetAssetPath((Object)detailMask);
//                a = a.Substring(0, a.IndexOf(((Object)detailMask).name));
//                return a;

//            }
//            if (smoothness != null)
//            {

//                a = AssetDatabase.GetAssetPath((Object)smoothness);
//                a = a.Substring(0, a.IndexOf(((Object)smoothness).name));
//                return a;

//            }
//            return a;

//        }

//    }
    
//    [MenuItem("Window/Tools/Texture Channel Packer")]
//    public static void ShowWindow()
//    {

//        GetWindow<TexturePacker>("Texture Channel Packer");

//    }

//    public void OnGUI()
//    {

//        metalic = ShowTexGUI("Metalic", metalic);
//        ambientOcclusion = ShowTexGUI("Ambient Occlusion", ambientOcclusion);
//        detailMask = ShowTexGUI("Detail Mask", detailMask);
//        smoothness = ShowTexGUI("Smoothness", smoothness);
//        textureName = EditorGUILayout.TextField("Name", textureName);
//        width = EditorGUILayout.IntField("Width", width);
//        height = EditorGUILayout.IntField("Height", height);
//        inverseSmoothness = EditorGUILayout.Toggle("Inverse Smoothness", inverseSmoothness);

//        if(GUILayout.Button("Pack Textures"))
//        {

//            PackTextures();
//            Debug.Log(path);

//        }
//        if(GUILayout.Button("Clear"))
//        {

//            metalic = null;
//            ambientOcclusion = null;
//            detailMask = null;
//            smoothness = null;
//            maskMap = null;
//            name = "Untitled";
//            width = 0;
//            height = 0;
//            inverseSmoothness = false;

//        }

//    }

//    private void PackTextures()
//    {

//        maskMap = new Texture2D(width, height);
//        maskMap.SetPixels(ColorArray());

//        byte[] tex = maskMap.EncodeToPNG();

//        FileStream stream = new FileStream(path + textureName + ".png", FileMode.OpenOrCreate, FileAccess.ReadWrite);
//        BinaryWriter writer = new BinaryWriter(stream);
//        for (int j = 0; j < tex.Length; j++)
//            writer.Write(tex[j]);

//        writer.Close();
//        stream.Close();

//        AssetDatabase.ImportAsset(path + textureName + ".png", ImportAssetOptions.ForceUpdate);
//        AssetDatabase.Refresh();

//    }

//    private Color[] ColorArray()
//    {

//        Color[] cl = new Color[width * height];

//        for(int j = 0;j < cl.Length;j++)
//        {

//            cl[j] = new Color();

//            if (metalic != null)
//                cl[j].r = metalic.GetPixel(j % width, j / width).r;
//            else
//                cl[j].r = 1;
//            if (ambientOcclusion != null)
//                cl[j].g = ambientOcclusion.GetPixel(j % width, j / width).r;
//            else
//                cl[j].g = 1;
//            if (detailMask != null)
//                cl[j].b = detailMask.GetPixel(j % width, j / width).r;
//            else
//                cl[j].b = 1;
//            if (smoothness != null)
//            {

//                if (!inverseSmoothness)
//                    cl[j].a = smoothness.GetPixel(j % width, j / width).r;
//                else
//                    cl[j].a = 1 - smoothness.GetPixel(j % width, j / width).r;

//            }
//            else
//                cl[j].a = 1;

//        }

//        return cl;

//    }

//    public Texture2D ShowTexGUI(string fieldName,Texture2D texture)
//    {

//        return (Texture2D)EditorGUILayout.ObjectField(fieldName, texture, typeof(Texture2D), false);

//    }

//}
