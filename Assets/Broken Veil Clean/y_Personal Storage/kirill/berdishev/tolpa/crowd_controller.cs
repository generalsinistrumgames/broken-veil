using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;

public class crowd_controller : MonoBehaviour
{
    public GameObject gen_list;
    public GameObject crowd_container;
    public GameObject point_container;
    public GameObject[] point_list;
    public OrderedDictionary free_point_list;
    public int free_point_count = 0;



    public int free_point_list_index = 0;
    public int Free_point_list_index 
    { 
        get 
        { 
            free_point_list_index++;
            return free_point_list_index; 
        } 
    }

    // Start is called before the first frame update
    void Start()
    {
        free_point_list = new OrderedDictionary();
        point_list = new GameObject[ point_container.transform.childCount];
        for (int i = 0; i < point_list.Length; i++)
        {
            GameObject go = point_container.transform.GetChild(i).gameObject;
            point_list[i] = go;
            free_point(go);
            bool b = true;
            while (b)
                b = normalization_rotation(go);
        }
        crowd_gen[] gen_mass = gen_list.GetComponentsInChildren<crowd_gen>(true);
        for(int i=0;i<gen_mass.Length;i++)
            gen_mass[i].generate(point_list[(int)(Random.value * (float)gen_mass.Length)].transform.position);
        TolpaV2_entity[] crowd_mass = crowd_container.GetComponentsInChildren<TolpaV2_entity>(true);
        for(int i=0;i< crowd_mass.Length;i++)
        {
            crowd_mass[i].Free_point();
            crowd_mass[i].destination_point = get_random_point();
            crowd_mass[i].gameObject.transform.position = crowd_mass[i].destination_point.transform.position;
        }
    }

    private void Update()
    {
        free_point_count = free_point_list.Count;
    }
    bool normalization_rotation(GameObject go)
    {
        float y_rotation = go.transform.rotation.eulerAngles.y;
        bool change = false;
        if (y_rotation > 180f)
        {
            y_rotation -= 360f;
            change = true;
        }
        if (y_rotation < -180f)
        {
            y_rotation += 360f;
            change = true;
        }
        if (change)
            go.transform.rotation = Quaternion.Euler(new Vector3(0f, y_rotation, 0f));
        return change;
    }

    public GameObject get_random_point()
    {
        if (free_point_list.Count == 0)
            return null;
        int index = (int)((float)(free_point_list.Count) * Random.value);
        GameObject go = free_point_list[index] as GameObject;
        free_point_list.RemoveAt(index);
        return go;
    }

    public void free_point(GameObject point)
    {
        free_point_list.Add(Free_point_list_index, point);
    }
}
