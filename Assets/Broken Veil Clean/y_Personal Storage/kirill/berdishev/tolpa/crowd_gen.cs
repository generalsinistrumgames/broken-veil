using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class crowd_gen : MonoBehaviour
{
    public crowd_controller crowd;

    public int number_gen=1;
    public bool mat_control = true;

    public Renderer hair;
    public Renderer body;
    public Renderer coat1;
    public Renderer coat2;

    public Material[] hair_mats;
    public Material[] body_mats;
    public Material[] coat1_mats;
    public Material[] coat2_mats;

    public void generate(Vector3 pos)
    {
        while (number_gen > 0)
        {
            GameObject go = Instantiate(gameObject, pos, new Quaternion(),crowd.crowd_container.transform);
            crowd_gen gen = go.GetComponent<crowd_gen>();
            if (mat_control)
            {
                gen.hair.materials[0] = hair_mats[(int)(Random.value * (float)hair_mats.Length)];
                gen.body.materials[0] = body_mats[(int)(Random.value * (float)body_mats.Length)];
                gen.coat1.materials[0] = coat1_mats[(int)(Random.value * (float)coat1_mats.Length)];
                gen.coat2.materials[0] = coat2_mats[(int)(Random.value * (float)coat2_mats.Length)];
            }
            gen.remove();
            go.SetActive(true);
            number_gen -= 1;
        }
        gameObject.SetActive(false);
    }

    public void remove()
    {
        Destroy(this);
    }
}
