using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TolpaV2_entity : MonoBehaviour
{
    crowd_controller controller;
    public NavMeshAgent agent;
    public Animator anim_controller;
    public bool is_visible = false;
    public bool is_moving = false;
    public tolpaV2_state state = tolpaV2_state.idle;
    public tolpaV2_state last_state = tolpaV2_state.ini;
    public GameObject destination_point = null;
    public Vector3 destination = new Vector3();
    public bool move = false;
    public float idle_time = 0f;
    public Vector2 idle_parameters = new Vector2(7f,13f);
    public float turn_per_second = 180f;
    public Vector3 turning_point = new Vector3();
    public float anim_parameter = 0f;//0-idle,1-moving
    [Range(0f,1f)]
    public float anim_change = 0.11f;
    public float y_rotation = 0f;
    public float target_y_rotaion = 0f;
    // Start is called before the first frame update
    void Start()
    {
        agent = gameObject.GetComponent<NavMeshAgent>();
        controller = GameObject.FindObjectOfType<crowd_controller>();
    }

    // Update is called once per frame
    void Update()
    {
        switch(last_state)
        {
            case tolpaV2_state.ini:
                Move_new();                
                break;


            case tolpaV2_state.moving:
                if (!move)
                    Move_new();
                else
                {
                    if (!Is_moving())
                    {
                        state = tolpaV2_state.turning;
                        Set_turning_point();
                    }
                    else
                        Anim_parameter_change(1f);
                }
                if (!move)
                    Anim_parameter_change(0f);
                break;



            case tolpaV2_state.turning:               
                Turning();
                Anim_parameter_change(0f);
                break;


            case tolpaV2_state.idle:
                Anim_parameter_change(0f);
                idle_time -= Time.deltaTime;
                if(idle_time<0f)
                {
                    Free_point();
                    Move_new();
                }
                break;
        }

        last_state = state;
        Anim();
    }
    void Anim_parameter_change(float val)
    {
        anim_parameter = Mathf.Lerp(anim_parameter,val,anim_change);
    }
    private void Anim()
    {
        anim_controller.SetFloat("move",anim_parameter);
    }
    void Set_turning_point()
    {
        Vector3 player_pos = gameObject.transform.position;
        turning_point = destination_point.transform.forward;
        turning_point.y = player_pos.y;
    }
    void Turning()
    {   
        y_rotation = gameObject.transform.rotation.eulerAngles.y;
        target_y_rotaion = destination_point.transform.rotation.eulerAngles.y;
        float frame_turn_spd = turn_per_second * Time.deltaTime;
        float closest_point = Closest_point(y_rotation, target_y_rotaion);
        float difference = y_rotation - closest_point;

        if (Mathf.Abs(difference) < frame_turn_spd)
        {
            y_rotation = target_y_rotaion;
            Apply_rotation();
            state = tolpaV2_state.idle;
            Get_idle_time();
            return;
        }
        else
        {
            if(difference>0)
                y_rotation -= frame_turn_spd;
            else
                y_rotation += frame_turn_spd;
            normalization_rotation();
        }
        Apply_rotation();
    }
    void normalization_rotation()
    {
        if (y_rotation > 180f)
            y_rotation -= 360f;
        if (y_rotation < -180f)
            y_rotation += 360f;
    }
    void Apply_rotation()
    {
        gameObject.transform.rotation =Quaternion.Euler(new Vector3(0f, y_rotation, 0f));
    }
    float Closest_point(float entity_rot_y,float point_rot_y)
    {
        float[] y_sample = new float[] { point_rot_y, point_rot_y-360f, point_rot_y+360f };
        float difference = 9999999f;
        int index = 0;
        for(int i=0;i<y_sample.Length;i++)
        {
            float d = Mathf.Abs(entity_rot_y-y_sample[i]);
            if (d < difference)
            {
                difference = d;
                index = i;
            }
        }
        return y_sample[index];
    }
    void Get_idle_time()
    {
        idle_time = Mathf.Lerp(idle_parameters.x,idle_parameters.y,Random.value);
    }
    void Move_new()
    {
        state = tolpaV2_state.moving;
        destination_point = controller.get_random_point();
        if(destination_point!=null)
        {
            destination = destination_point.transform.position;
            destination.y = gameObject.transform.position.y;
            Go_to_point(destination);
            move = true;
        }
    }
    public void Free_point()
    {
        if(destination_point!=null)
            controller.free_point(destination_point);
        destination_point = null;
    }

    public bool Is_moving()
    {
        is_moving = agent.hasPath;
        return is_moving;
    }

    public void Go_to_point(Vector3 destination)
    {
        agent.SetDestination(destination);
    }

    public enum tolpaV2_state
    {
        moving,
        idle,
        turning,
        ini
    }
}
