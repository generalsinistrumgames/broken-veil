﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class fly : MonoBehaviour
{
    public Vector3 center = Vector3.zero;
    public float cicle = 0f;
    public Vector3 rad = new Vector3(0,0f,2f);
    public float vfx_str = 8f;
    public float circles_per_second = 1f;
    public VisualEffect vfx;
    public GameObject GO;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //vfx.SetVector3("const_direction", Vector3.zero);
        cicle += circles_per_second * Time.deltaTime;
        GO.transform.position = get_pos();
        GO.transform.LookAt(get_direction()+GO.transform.position);
        vfx.SetVector3("const_direction", get_direction());
        Debug.DrawLine(center, GO.transform.position, Color.blue);
        Debug.DrawLine(GO.transform.position, GO.transform.position+ get_direction(), Color.green);
    }

    Vector3 get_pos()
    {
        if (cicle > 1f)
        {
            cicle -= 1f;
            return get_pos();
        }
        if (cicle < 0f)
        {
            cicle += 1f;
            return get_pos();
        }
        return center+( Quaternion.Euler(0f,cicle*360f,0f) * rad);
    }

    Vector3 get_direction()
    {
        const float angle = 180f;
        return (Quaternion.Euler(0f, cicle * 360f, 0f)*Quaternion.Euler(0,angle,0))*new Vector3(0,0, vfx_str);
    }
}
