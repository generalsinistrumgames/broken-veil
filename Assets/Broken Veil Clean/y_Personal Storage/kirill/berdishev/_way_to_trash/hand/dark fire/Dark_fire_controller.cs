﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.VFX;
using UnityEngine;

public class Dark_fire_controller : MonoBehaviour
{
    public BoxCollider area;
    public VisualEffect particle_system_prefab;
    public Particle_system_controller[] particle_systems;
    public float lifetime1 = 2f;
    public float lifetime2 = 4f;
    public int max_particle_system_count = 12;

    // Start is called before the first frame update
    void Start()
    {
        if(lifetime1>lifetime2)
        {
            float temp = lifetime2;
            lifetime2 = lifetime1;
            lifetime1 = temp;
        }
        particle_systems = new Particle_system_controller[max_particle_system_count];
        for(int i=0;i<max_particle_system_count;i++)
        {
            particle_systems[i] = new Particle_system_controller();
            particle_systems[i].Ini(particle_system_prefab, Get_random_pos(), this,i,Random.Range(lifetime1,lifetime2));
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < max_particle_system_count; i++)
        {
            particle_systems[i].Update();
            if (particle_systems[i].is_free)
                particle_systems[i].Reset(Get_random_pos());
        }
    }

    Vector3 Get_random_pos()
    {
        Vector3 fin = new Vector3(
            area.size.x * Random.Range(-1f, 1f)/2f,
            area.size.y * Random.Range(-1f, 1f)/2f,
            area.size.z * Random.Range(-1f, 1f)/2f
            );
        fin += area.center + transform.position;
        return fin;
    }
}


[SerializeField]
public class Particle_system_controller
{
    public VisualEffect particle_system;
    public float lifetime;
    public bool is_free = true;

    private float life = 0f;

    public void Ini(VisualEffect prefab,Vector3 pos,Dark_fire_controller parent,int phase,float new_lifetime)
    {
        lifetime = new_lifetime;

        particle_system = VisualEffect.Instantiate(prefab,parent.transform);
        particle_system.SetFloat("phase", (float)phase);
        Reset(pos);
    }
    public void Reset(Vector3 pos)
    {
        particle_system.transform.position = pos;
        is_free = false;
        life = 0;
        particle_system.SetFloat("phase", 1+particle_system.GetFloat("phase"));
    }
    public void Update()
    {
        if (is_free)
            return;
        life += Time.deltaTime;
        if(life>lifetime)
        {
            is_free = true;
            life = 0;
            return;
        }
        particle_system.SetFloat("spawn_multi_lifetime", Mathf.PingPong(life, lifetime / 2f));
    }
}
