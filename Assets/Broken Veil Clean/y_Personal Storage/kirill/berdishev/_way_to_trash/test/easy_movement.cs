﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class easy_movement : MonoBehaviour
{
    public float spd = 0.6f;
    public float rot_spd = 0.8f;
    public float max_speed = 6f;
    [Range(0f,1f)]
    public float look_change_factor = 0.96f;
    public float look_max_distance=1.4f;

    public Vector2 input_v = new Vector2();
    public Vector2 look_input_v = new Vector2();
    public Vector2 mouse_input_v = new Vector2();
    public float mouse_multi = 2.4f;
    public GameObject go_look;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Vector2 look_input = new Vector2(Input.GetAxis("Axis2x"), Input.GetAxis("Axis2y"));

        Vector2 mouse_input_v2 = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"))* mouse_multi;
        if (input.magnitude > 1f)
            input = input.normalized;
        if (look_input.magnitude > 1f)
            look_input = look_input.normalized;
        input_v = input;
        if (mouse_input_v2.magnitude > 1f)
            mouse_input_v2 = mouse_input_v2.normalized;
        mouse_input_v = mouse_input_v2;
        if (mouse_input_v2.magnitude > look_input.magnitude)
            look_input = mouse_input_v2;

        //look_input_v = look_input*(1f- look_change_factor) +look_input_v* look_change_factor;
        look_input_v = look_input_v + look_input * 0.05f;        
        if (look_input_v.magnitude > 1f)
            look_input_v = look_input_v.normalized;
        go_look.transform.position = gameObject.transform.position + (new Vector3(look_input_v.x, look_input_v.y, 0f)*look_max_distance);
        //Rigidbody rb = gameObject.GetComponent<Rigidbody>();
        //float y = 0f;
        //if (Input.GetKey(KeyCode.Space))
        //    y = 13f;
        //rb.velocity += new Vector3(input.x*Time.deltaTime*spd,y*Time.deltaTime, input.y * Time.deltaTime*spd);
        //Vector3 speed_limiter = new Vector3(rb.velocity.x,0,rb.velocity.z);
        //if (speed_limiter.magnitude > max_speed)
        //{
        //    speed_limiter=speed_limiter.normalized * max_speed;
        //    rb.velocity = new Vector3(speed_limiter.x, rb.velocity.y, speed_limiter.z);
        //}
    }
}
