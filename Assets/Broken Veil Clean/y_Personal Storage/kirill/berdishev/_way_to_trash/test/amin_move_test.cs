﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class amin_move_test : MonoBehaviour
{
    public bool is_done = false;
    public bool is_work = false;
    public Vector3[] mass;
    public float time=0f;
    public float max_time = 0.84f;
    public GameObject target;

    public float test_d;
    public float test_result;
    // Start is called before the first frame update
    void Start()
    {
        target = gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if(is_done)
        {
            is_done = false;
            is_work = false;
            target.GetComponent<Rigidbody>().isKinematic = false;
        }
        if (!is_work & Input.GetKey(KeyCode.Escape))
        {
            Time.timeScale = 1f;
            time = 0f;
            target.GetComponent<Rigidbody>().isKinematic = true;
            is_work = true;
            mass[0] = target.transform.position;
            for(int ind=1;ind<mass.Length;ind++)
            {
                mass[ind] = mass[ind - 1]+Vector3.forward * 0.4f;
                if (ind % 2 == 1)
                    mass[ind] += Vector3.up * 0.4f;
                else
                    mass[ind] -= Vector3.up * 0.4f;
            }
        }
        if (!is_work)
            return;
        float t = time / max_time;
        if (t < 0f)
            t = 0f;
        if (t > 1f)
            t = 1f;
        float result = t*(float)(mass.Length-1);
        float d= result-Mathf.Floor(result);
        int i = Mathf.FloorToInt(result);
        if (i==mass.Length-1)
        {
            time =max_time;
            is_done = true;
            target.transform.position = mass[mass.Length - 1];
            return;
        }
        target.transform.position = Vector3.Lerp(mass[i],mass[i+1],d);
        time += Time.deltaTime;

        test_d = d;
        test_result = result;
    }
}
