﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test_lateupd : MonoBehaviour
{
    // Start is called before the first frame update
    MeshRenderer mr;
    void Start()
    {
        mr = gameObject.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        mr.enabled = true;
    }

    private void LateUpdate()
    {
        mr.enabled = false;
    }
}
