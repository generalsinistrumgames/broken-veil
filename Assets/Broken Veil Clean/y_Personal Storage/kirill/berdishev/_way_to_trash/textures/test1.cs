﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test1 : MonoBehaviour
{
    public Material mat;
    public bool hidden = false;
    public float alpha = 1f;
    public float time = 1.4f;
    public float min_a = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (hidden)
        {
            alpha -=(1f-min_a)*Time.deltaTime/time;
            alpha = Mathf.Clamp(alpha, min_a, 1f);
            mat.color = new Color(alpha, mat.color.r, mat.color.g, mat.color.b);
        }
        else
        {
            alpha += (1f - min_a) * Time.deltaTime / time;
            alpha = Mathf.Clamp(alpha,min_a,1f);
            mat.color = new Color(alpha, mat.color.r, mat.color.g, mat.color.b);            
        }
    }
}
