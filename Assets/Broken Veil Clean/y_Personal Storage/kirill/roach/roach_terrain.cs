using GlobalScripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class roach_terrain : MonoBehaviour
{
    public GameObject roach;
    public Terrain terr;
    public Animator anim;
    public GameObject player;
    public NavMeshAgent agent;
    [Range(0.2f, 4f)]
    public float scare_distance = 1.4f;
    public bool Is_moving = false;

    public float walk_spd = 1.5f;
    public float run_spd = 3.5f;
    public Vector2 idle_wait_in_seconds = new Vector2(2.5f, 12.5f);
    public float wait_time = 0f;

    public float anim_state = 0f;
    public roach_state state = roach_state.idle;
    [Range(0f,1f)]
    public float walk_after_run_chance = 0.75f;
    public float dist = 0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 player_pos;
        if (player == null)
        {
            PlayerController[] pc = GameObject.FindObjectsOfType<PlayerController>();
            if (pc.Length != 0)
                player = pc[0].gameObject;
            player_pos = new Vector3();
        }
        else
        {
            player_pos = player.transform.position;
        }
        if(terr == null)
        {
            Terrain[] terrmass = GameObject.FindObjectsOfType<Terrain>();
            if (terrmass.Length != 0)
                terr = terrmass[0];
        }
        Vector3 roach_pos = gameObject.transform.position;
        player_pos.y = 0f;
        roach_pos.y = 0f;
        dist = Vector3.Distance(player_pos, roach_pos);
        roach_state last_state = state;
        if (last_state == roach_state.idle)
        {
            
            if (dist < scare_distance)
            {
                state = roach_state.run;
                Vector3 offset = new Vector3();
                offset.x = Random.Range(-2.5f, 2.5f);
                offset.z = Random.Range(-2.5f, 2.5f);
                offset += transform.position;
                go(offset);
            }
            else
            {
                wait_time -= Time.deltaTime;
                if (wait_time < 0f)
                {
                    state = roach_state.walk;
                    Vector3 offset = new Vector3();
                    offset.x = Random.Range(-1.5f, 1.5f);
                    offset.z = Random.Range(-1.5f, 1.5f);
                    offset += transform.position;
                    go(offset);
                }
            }
        }
        if (last_state == roach_state.walk)
        {
            if (dist < scare_distance)
            {
                state = roach_state.run;
                Vector3 offset = new Vector3();
                offset.x = Random.Range(-2.5f, 2.5f);
                offset.z = Random.Range(-2.5f, 2.5f);
                offset += transform.position;
                go(offset);
            }
            else
            {
                if (!is_moving())
                {
                    state = roach_state.idle;
                    wait_time = Mathf.Lerp(idle_wait_in_seconds.x, idle_wait_in_seconds.y, Random.value);
                }
            }
        }
        if (last_state == roach_state.run)
        {
            if(!is_moving())
            {
                if (dist < scare_distance)
                {
                    Vector3 offset = new Vector3();
                    offset.x = Random.Range(-2.5f, 2.5f);
                    offset.z = Random.Range(-2.5f, 2.5f);
                    offset += transform.position;
                    go(offset);
                }
                else 
                {
                    if (Random.value < walk_after_run_chance)
                    {
                        state = roach_state.idle;
                        wait_time = Mathf.Lerp(idle_wait_in_seconds.x, idle_wait_in_seconds.y, Random.value);
                    }
                    else 
                    {
                        state = roach_state.walk;
                        Vector3 offset = new Vector3();
                        offset.x = Random.Range(-1.5f, 1.5f);
                        offset.z = Random.Range(-1.5f, 1.5f);
                        offset += transform.position;
                        go(offset);
                    }
                }
            }
        }



        if(state == roach_state.idle)
            anim_state=Mathf.Lerp(anim_state, 0f,0.1f);
        if (state == roach_state.walk)
            anim_state = Mathf.Lerp(anim_state, 1f, 0.1f);
        if (state == roach_state.run)
            anim_state = Mathf.Lerp(anim_state, 2f, 0.1f);
        anim.SetFloat("walk_state", anim_state);
        set_spd();
        set_pos();
    }

    public void set_spd()
    {
        if (state == roach_state.idle)
            return;
        if (state == roach_state.walk)
            agent.speed = walk_spd;
        if (state == roach_state.run)
            agent.speed = run_spd;
    }
    public void go(Vector3 destination)
    {
        agent.SetDestination(destination);
    }
    public bool is_moving()
    {
        Is_moving = agent.hasPath;
        return Is_moving;
    }

    public void set_pos()
    {
        Vector3 pos = transform.position;
        if(terr!=null)
            pos.y = terr.SampleHeight(pos)+terr.transform.position.y;
        roach.transform.position = pos;
    }

    public enum roach_state
    {
        idle,
        walk,
        run,
    }
}
