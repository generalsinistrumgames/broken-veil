using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEngine;

public class tolpa_optimize : MonoBehaviour
{
    public List<tolpa_entity> tolpa = new List<tolpa_entity>();

    public int visible_count = 0;
    public int total_count = 0;

    public int debug_dummy_on = 0;
    public int debug_skinned_on = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {   
        visible_count = 0;
        total_count = tolpa.Count;

        debug_dummy_on = 0;
        debug_skinned_on = 0;

        for (int i=0;i< total_count; i++)
        {
            if (tolpa[i].mode == tolpa_visible_mode.person_on)
                debug_skinned_on++;
            else
                debug_dummy_on++;
        }
        Debug.Log(visible_count);
    }    
}
