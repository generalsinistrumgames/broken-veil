using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debug_visible : MonoBehaviour
{    
    public Renderer r;
    public bool is_visible=false;
    // Start is called before the first frame update
    void Start()
    {
        r = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        is_visible = r.isVisible;
    }
}
