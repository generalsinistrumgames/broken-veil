using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static tolpa_optimize;

public class tolpa_find : MonoBehaviour
{
    public GameObject container;
    public tolpa_optimize crowd_controller;
    // Start is called before the first frame update
    void Start()
    {
        tolpa_optimize temp=null;
        if (crowd_controller == null)
            gameObject.TryGetComponent<tolpa_optimize>(out temp);
        if (temp != null)
        {
            crowd_controller = temp;
            for (int i=0;i<container.transform.childCount;i++)
            {
                GameObject tempgo = container.transform.GetChild(i).gameObject;
                tolpa_entity te = tempgo.AddComponent<tolpa_entity>();
                crowd_controller.tolpa.Add(te);
                if (!crowd_controller.enabled)
                    te.enabled = false;
            }
        }


    }
}
