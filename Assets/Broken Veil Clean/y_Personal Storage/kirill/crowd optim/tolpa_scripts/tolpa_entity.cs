using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tolpa_entity : MonoBehaviour
{
    public tolpa_visible_mode mode = tolpa_visible_mode.person_on;

    public Animator anim;
    public Renderer coat;
    public bool is_coat_visible = false;
    public Renderer[] geometry;
    public float time_cd = 0f;
    public const float max_time_cd = 0.35f;


    // Start is called before the first frame update
    void Start()
    {

        List<Renderer> skinned_renders = new List<Renderer>();

        #region defines
        GameObject GO = gameObject;
        int index = 0;
        for (int i = 0; i < GO.transform.childCount; i++)
        {
            if (GO.transform.GetChild(i).name.StartsWith("persons"))
                index = i;
        }
        anim = GO.transform.GetChild(index).GetComponent<Animator>();

        index = 0;
        for (int i = 0; i < anim.transform.childCount; i++)
        {
            if (anim.transform.GetChild(i).name.StartsWith("Geometry"))
                index = i;
        }
        Transform geometry_t = anim.transform.GetChild(index);


        index = 0;
        for (int i = 0; i < geometry_t.transform.childCount; i++)
        {
            Renderer r_temp;
            if (geometry_t.transform.GetChild(i).gameObject.TryGetComponent<Renderer>(out r_temp))
                skinned_renders.Add(r_temp);
            if (geometry_t.transform.GetChild(i).name.StartsWith("coat"))
                index = i;
        }
        coat = geometry_t.transform.GetChild(index).GetComponent<Renderer>();


        geometry = skinned_renders.ToArray();
        #endregion

        #region ini

        time_cd = 0.1f;
        mode = tolpa_visible_mode.person_on;
        #endregion


        foreach (Renderer r in geometry)
            r.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
    }

    // Update is called once per frame
    void Update()
    {
        is_coat_visible = coat.isVisible;
        if(mode == tolpa_visible_mode.person_on)
        {            
            if(!is_coat_visible)
                if(time_cd<0)
                {
                    mode = tolpa_visible_mode.person_off;
                    anim.enabled = false;
                    return;
                }

            time_cd -= Time.deltaTime;
        }
        if(mode == tolpa_visible_mode.person_off)
        {
            if (is_coat_visible)
            {
                mode = tolpa_visible_mode.person_on;
                time_cd = max_time_cd;
                anim.enabled = true;
                return;
            }
        }
    }
}

public enum tolpa_visible_mode
{
    person_on,
    person_off
}
