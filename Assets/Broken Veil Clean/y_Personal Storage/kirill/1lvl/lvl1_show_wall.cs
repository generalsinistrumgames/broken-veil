using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;

public class lvl1_show_wall : MonoBehaviour
{
    public GameObject wall;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        bool work = false;
        GameObject go = other.gameObject;
        if (go.tag == "Player")
            work = true;
        else
        {
            PlayerController pc;
            if (go.TryGetComponent<PlayerController>(out pc))
                work = true;
        }

        if(work)
        {
            if (wall != null)
                wall.SetActive(true);
            else
                Debug.LogError("wall GameObj hast assigned");

            Collider[] col = gameObject.GetComponents<Collider>();

            for (int i = 0; i < col.Length; i++)
                col[i].enabled = false;

            this.enabled = false;
        }

    }
}
