﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotherFootsteps : MonoBehaviour
{
    [Header("Sound Event")]
    public string EventPlayName;

    public void MotherFootstepsSound()
    {
        AkSoundEngine.PostEvent(EventPlayName, gameObject);
    }
}