using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrannyRoomEnterTrigger : MonoBehaviour
{
    public Security security;
    [Space]
    public NavMeshAgentControl securityAgentControl;
    [Space]
    public Transform targetTransform;
    [Space]
    public Player player;


    //smoothdamp testing
    //public float maxsPeed;
    //[Space]
    //public float timeZeroToMax;
    //[Space]
    //public float acceleratePerSecond;
    //[Space]
    //public float forwardVelocity;
    //[Space]
    //public Transform target;
    //public float smoothTime = 0.3F;
    //private Vector3 velocity = Vector3.zero;

    private void OnTriggerEnter(Collider other)
    {     
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (player = null)
            {
                player = playerScript;
            }

            if (security.chasingPlayer)
            {
                security.StopAllCoroutines();

                StartCoroutine(SecurityControl());
            }     
        }
    }

    public IEnumerator SecurityControl()
    {
        yield return security.StartCoroutine(security.MoveToAgentTarget(targetTransform));

        security.enabled = false;

        Grandma grandma = FindObjectOfType<Grandma>();

        grandma.GrandmaTriggered();
    }

    //private void Start()
    //{
    //    //acceleratePerSecond = maxsPeed / timeZeroToMax;
    //}

    //void Update()
    //{
    //    //    //Define a target position above and behind the target transform
    //    //    Vector3 targetPosition = targetTransform.TransformPoint(new Vector3(1, 1, -1));

    //    //    // Smoothly move the camera towards that target position
    //    //    transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
    //}
}