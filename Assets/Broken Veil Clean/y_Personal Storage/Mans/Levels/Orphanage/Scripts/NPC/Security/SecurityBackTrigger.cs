﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityBackTrigger : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] SecurityEyes securityEyes;

    [Header("Check Crawl")]
    [SerializeField] bool playerDetected;
    [Space]
    [SerializeField] float compareFloat;

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (!playerDetected)
            {            
                if (playerScript.GetComponent<Animator>().GetFloat("Movement") >= compareFloat)
                {
                    playerDetected = true;

                    securityEyes.player = playerScript;

                    //securityEyes.PlayerDetected();
                }
            }
        }
    }
}