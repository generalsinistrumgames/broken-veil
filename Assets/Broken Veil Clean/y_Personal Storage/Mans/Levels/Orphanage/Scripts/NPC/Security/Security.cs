﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Events;

public class Security : EnemyWithFOV
{
    public enum PlayerCatchPose
    {
        low,
        mid,
        high
    }

    [Header("Security Refs")]
    [Space]
    MoveAndRotateTowards _moveAndRotateTowards;
    [Space]
    [SerializeField] NavMeshAgentControl navMeshAgentControl;
    [Space]
    [SerializeField] PlayableDirector _playableDirector;
    [Space]
    public Rigidbody _cigarRb;
    [Space]
    [SerializeField] Animator _chairAnim;
    [Space]
    [SerializeField] Collider _enemyCollider;
    [Space]
    [SerializeField] List<Collider> _ignorePhysicsColliders;

    [Header("Chase Player")]
    public PlayerCatchPose playerCatchPose;
    [Space]
    public bool chasingPlayer;
    [Space]
    [SerializeField] float _rotateSpeed;
    [Space]
    public float chasePlayerSpeed;
    [Space]
    public float chasePlayerStoppingDistance;
    [Space]
    public float chasePlayerAcceleration;

    [Header("Sound Events")]
    public string chairSoundEventName;
    [Space]
    public string securitySoundEventName;

    [Header("Animation Control")]
    [SerializeField] bool _animStateDelayEnds;
    [Space]
    [SerializeField] float _animNormalizedDelayValue;
    [Space]
    [SerializeField] string _animationTag;

    [Header("Broom Control")]
    [SerializeField] Animator _broomAnimator;
    [Space]
    [SerializeField] GameObject _securityRightHand;
    [Space]
    [SerializeField] GameObject _broom;
    [Space]
    [SerializeField] Rigidbody _broomRb;
    [Space]
    [SerializeField] bool _broomInHand;
    [Space]
    [SerializeField] bool _broomInLoop;
    [Space]
    [SerializeField] Vector3 _broomToParentPosition;
    [Space]
    [SerializeField] Quaternion _broomToParentRotation;

    [Header("Agent Control")]
    public float moveSpeedMultiplier;
    [Space]
    public float turnSpeedMultiplier;
    [Space]
    public float checkNoiseSpeed;
    [Space]
    public float checkNoiseStopDistance;
    [Space]
    public float checkNoiseAcceleration;
    [Space]
    public Transform[] transformTargets;
    [Space]
    public int currentTargetIndex;

    Coroutine movingToCheckNoiseCoroutine;

    [Header("To Parent Player")]
    [SerializeField] Transform _toParentPlayerRoot;
    [Space]
    [SerializeField] Transform _playerToParentTransform;
    [Space]
    [SerializeField] Transform[] _playerToParentTransformOffsets;
    [Space]
    [SerializeField] float _playerSetPositionSpeed;
    [Space]
    [SerializeField] float _playerSetRotationSpeed;
    [Space]
    [SerializeField] float rotThreshold = 0.97f;

    // check if player turned back or facing guard
    [Space]
    [SerializeField] bool playerIsFacingGuard;
    [Space]
    public bool usePlayerCatchFromBackReaction = true;

    [Header("Catch Player Cut Scene")]
    [SerializeField] UnityEvent _OnPlayPlayerCatchCutScene;

    [Header("Granny Room Enter Trigger")]
    public GrannyRoomEnterTrigger grannyRoomEnterTrigger;

    private void Awake()
    {
        _enemyCollider = GetComponent<Collider>();

        foreach (var collider in _ignorePhysicsColliders)
        {
            Physics.IgnoreCollision(_enemyCollider, collider);
        }
    }

    public override void Start()
    {
        base.Start();

        _moveAndRotateTowards = new MoveAndRotateTowards();

        if (_playableDirector == null)
        {
            _playableDirector = GetComponentInParent<PlayableDirector>();
        }

        navMeshAgentControl = GetComponent<NavMeshAgentControl>();

        //navMeshAgentControl.moveSpeedMultiplier = moveSpeedMultiplier / 2;

        //navMeshAgentControl.turnSpeedMultiplier = turnSpeedMultiplier / 2;

        SetNavMeshParametrs(checkNoiseSpeed, checkNoiseAcceleration, checkNoiseStopDistance);

        //movingToCheckNoiseCoroutine = MoveToAgentTarget(transformTargets[currentTargetIndex]);
    }

    //private new void Update()
    //{
    //    Vector3 direction = (new Vector3(transform.position.x, 0, transform.position.z) - new Vector3(_playerTransform.position.x, 0, _playerTransform.position.z)).normalized;

    //    float dotForward = Vector3.Dot(_playerTransform.forward, direction);  // check facing towards

    //    if (dotForward >= 0.5f)
    //    {
    //        Debug.Log("Security Catch Low");
    //    }
    //    else
    //    {
    //        Debug.Log("Security Catch Low Back");
    //    }
    //}

    public override IEnumerator DelayPlayerDetectionCoroutine()
    {
        grannyRoomEnterTrigger.gameObject.SetActive(true);

        yield return base.DelayPlayerDetectionCoroutine();

        chasingPlayer = true;

        if (_playableDirector.state == PlayState.Playing)
        {
            _playableDirector.Stop();

            Debug.Log("Triggered playable");
        }

        navMeshAgentControl.moveSpeedMultiplier = moveSpeedMultiplier;

        navMeshAgentControl.turnSpeedMultiplier = turnSpeedMultiplier;

        SetNavMeshParametrs(chasePlayerSpeed, chasePlayerAcceleration, chasePlayerStoppingDistance);

        StartCoroutine(CheckAnimationDelayedStateRoutine());
    }

    IEnumerator CheckAnimationDelayedStateRoutine()
    {
        Debug.Log("security routine starts");

        if (navMeshAgentControl.agent.isActiveAndEnabled)
        {
            navMeshAgentControl.agent.isStopped = true;
        }

        SetAnimationState();

        bool animDelayEnds = false;

        while (!animDelayEnds)
        {
            animDelayEnds = DelayedAnimationStateEnds(_animationTag, _animNormalizedDelayValue);

            yield return null;
        }

        if (animDelayEnds)
        {
            if (!navMeshAgentControl.agent.isActiveAndEnabled)
            {
                navMeshAgentControl.agent.enabled = true;
            }

            if (movingToCheckNoiseCoroutine != null)
            {
                StopCoroutine(movingToCheckNoiseCoroutine);

                navMeshAgentControl.agent.ResetPath();

                navMeshAgentControl.agent.velocity = Vector3.zero;
            }

            //StartCoroutine(MoveToAgentTarget(_player.transform, false));

            StartCoroutine(MoveAndRotateRoutine());

            Debug.Log("security routine starts ends");
        }
    }

    public void SetAnimationState()
    {
        if (_enemyAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Idle Smoking"))
        {
            _enemyAnimator.SetTrigger("Stand Up");

            _animationTag = "Standing";

            // chair anim and sound play
            _chairAnim.SetTrigger("Move Chair");

            AkSoundEngine.PostEvent(chairSoundEventName, gameObject);

            DropCigar();

            // stand anim normalized duration
            _animNormalizedDelayValue = 0.9f;
        }
        else
        {
            _enemyAnimator.SetTrigger("Trigger Player");

            _animationTag = "Moving";

            if (_broomInHand)
            {
                DropBroom();
            }

            _animNormalizedDelayValue = 0.1f;
        }

        AkSoundEngine.PostEvent(securitySoundEventName, gameObject);
    }

    public bool DelayedAnimationStateEnds(string animationTag, float delayTime)
    {
        if (_enemyAnimator.GetCurrentAnimatorStateInfo(0).IsTag(animationTag) && _enemyAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= delayTime)
        {
            Debug.Log("Animation Delay Ends");

            return true;
        }
        else
        {
            Debug.Log("Animation Delay working");

            return false;
        }
    }

    IEnumerator MoveAndRotateRoutine()
    {
        yield return MoveToAgentTarget(_player.transform, false);

        _player.DisablePlayerMovement();

        _player.GetComponent<Collider>().enabled = false;

        bool isFacingPlayer = false;

        //bool playerIsFacing = false;

        while (!isFacingPlayer /*|| !playerIsFacing*/)
        {
            if (!isFacingPlayer)
            {
                isFacingPlayer = _moveAndRotateTowards.RotateTowards(_enemyTransform, _playerTransform, _rotateSpeed);
            }

            //if (!playerIsFacing)
            //{
            //    playerIsFacing = _moveAndRotateTowards.RotateTowards(_playerTransform, _enemyTransform, _rotateSpeed);
            //}

            Debug.Log("Rotate together While is working");

            yield return null;
        }

        // play security catch anim nd wait till some delay
        string animationTriggerAndStateName = null;

        switch (playerCatchPose)
        {
            case PlayerCatchPose.low:
                animationTriggerAndStateName = "Security Catch Low";

                _enemyAnimator.SetTrigger(animationTriggerAndStateName);
                break;

            case PlayerCatchPose.mid:
                animationTriggerAndStateName = "Security Catch Mid";

                _enemyAnimator.SetTrigger(animationTriggerAndStateName);

                _playerToParentTransform = _playerToParentTransformOffsets[2];
                break;

            case PlayerCatchPose.high:
                animationTriggerAndStateName = "Security Catch High";

                _enemyAnimator.SetTrigger(animationTriggerAndStateName);

                _playerToParentTransform = _playerToParentTransformOffsets[3];
                break;
        }

        yield return MoveAndRotatePlayerInEnemyHands();

        bool animDelayEnds = false;

        while (!animDelayEnds)
        {
            animDelayEnds = DelayedAnimationStateEnds(animationTriggerAndStateName, 0.7f);

            yield return null;
        }

        _enemyAnimator.enabled = false;

         _player.animator.enabled = false;

        _OnPlayPlayerCatchCutScene?.Invoke();   

        yield return new WaitForSeconds(1.5f);

        _player.GetComponent<PlayerRenderControl>().SetPlayerRender(false);

        gameObject.SetActive(false);
    }

    IEnumerator MoveAndRotatePlayerInEnemyHands()
    {
        var _animatorSwapController = _player.GetComponent<AnimatorSwapController>();

        _animatorSwapController.SwapAnimator(PlayerAnimatorControllers.secondaryAnimator);

        //_playerTransform.parent = _toParentPlayerRoot;

        if (usePlayerCatchFromBackReaction)
        {  
            Vector3 directionToPlayer = (new Vector3(_enemyTransform.position.x, 0, _enemyTransform.position.z) - new Vector3(_playerTransform.position.x, 0, _playerTransform.position.z)).normalized;

            float guardDotForward = Vector3.Dot(_playerTransform.forward, directionToPlayer);  // check facing towards

            if (guardDotForward >= 0)
            {
                SetPlayerFacingCatchReaction();
            }
            else
            {
                _playerToParentTransform = _playerToParentTransformOffsets[1];

                _player.animator.SetTrigger("Security Catch Low Back");

                playerIsFacingGuard = false;
            }

            Debug.Log("Guard Dot forward " + guardDotForward);
        }
        else
        {
            SetPlayerFacingCatchReaction();
        }
  
        Vector3 directionFromPlayer;

        float playerDotForward;

        bool targetPosReached = false;

        bool targetRotReached = false;

        while (!targetPosReached || !targetRotReached)
        {
            if (!targetPosReached)
            {
                Vector3 targetPos = new Vector3(_playerToParentTransform.position.x, _playerTransform.position.y, _playerToParentTransform.position.z);

                targetPosReached = _moveAndRotateTowards.MoveToTargetPosition(_playerTransform, targetPos, _playerSetPositionSpeed);
            }

            if (playerIsFacingGuard)
            {
                directionFromPlayer = (new Vector3(_enemyTransform.position.x, 0, _enemyTransform.position.z) - new Vector3(_playerTransform.position.x, 0, _playerTransform.position.z)).normalized;
            }
            else
            {
                directionFromPlayer = (new Vector3(_playerTransform.position.x, 0, _playerTransform.position.z) - new Vector3(_enemyTransform.position.x, 0, _enemyTransform.position.z)).normalized;
            }

            float strength = Mathf.Min(_playerSetRotationSpeed * Time.deltaTime, 1);

            _playerTransform.rotation = Quaternion.Lerp(_playerTransform.rotation, Quaternion.LookRotation(directionFromPlayer), strength); // check facing towards

            playerDotForward = Vector3.Dot(_playerTransform.forward, directionFromPlayer);

            if (playerDotForward >= rotThreshold)
            {
                targetRotReached = true;
            }

            Debug.Log("Player Dot forward " + playerDotForward);

            //Quaternion targetRot = Quaternion.Euler(_playerToParentTransform.localRotation.x, _playerToParentTransform.localRotation.y, _playerToParentTransform.localRotation.z);

            //if (Quaternion.Angle(_player.transform.localRotation, _playerToParentTransform.localRotation) <= 0.1f)
            //{
            //    Debug.Log("Rotation Reached");

            //    targetRotReached = true;
            //}
            //else
            //{
            //    _player.transform.localRotation = Quaternion.RotateTowards(_player.transform.localRotation, _playerToParentTransform.localRotation, _playerSetRotationSpeed * Time.deltaTime);
            //}

            yield return null;
        }

        Debug.Log("MoveAndRotatePlayerInEnemyHands Coroutine Ends");
    }

    public void SetPlayerFacingCatchReaction()
    {
        _player.animator.SetTrigger("Security Catch Low");

        _playerToParentTransform = _playerToParentTransformOffsets[0];

        playerIsFacingGuard = true;
    }

    public void SetIdleStandingAnimation()
    {
        _enemyAnimator.SetTrigger("Blend Tree");
    }

    public void SetBroomAnimation()
    {
        _enemyAnimator.SetTrigger("Broom");
    }

    public void TakeBroomInHand()
    {
        _broomInHand = true;

        _broom.transform.parent = _securityRightHand.transform;

        _broom.transform.localPosition = _broomToParentPosition;

        _broom.transform.localRotation = _broomToParentRotation;
    }

    public void EnableNavMeshAgent()
    {
        navMeshAgentControl.agent.enabled = true;

        if (currentTargetIndex > transformTargets.Length)
        {
            return;
        }

        navMeshAgentControl.agent.SetDestination(transformTargets[currentTargetIndex].position);

        movingToCheckNoiseCoroutine = StartCoroutine(MoveToAgentTarget(transformTargets[currentTargetIndex]));
    }

    private void SetNavMeshParametrs(float AISpeed, float AIAcceleration, float AIStoppingDistance)
    {
        navMeshAgentControl.agent.speed = AISpeed;

        navMeshAgentControl.agent.acceleration = AIAcceleration;

        navMeshAgentControl.agent.stoppingDistance = AIStoppingDistance;
    }

    public IEnumerator MoveToAgentTarget(Transform target, bool useStopIdleAnim = true)
    {
        navMeshAgentControl.agent.isStopped = false;

        navMeshAgentControl.targetDistanceReached = false;

        navMeshAgentControl.useIdleStopAnim = useStopIdleAnim;

        while (true)
        {
            Debug.Log("moveToAgentTarge workingggg");

            navMeshAgentControl.SetAgentPathDestination(target.position);

            if (navMeshAgentControl.targetDistanceReached)
            {
                navMeshAgentControl.agent.isStopped = true;

                Debug.Log("Target reached");

                if (!chasingPlayer)
                {
                    currentTargetIndex++;
                }

                yield break;
            }

            yield return null;
        }
    }

    public void StartCheckNoiseTimeline()
    {
        if (!_playerWasDetected)
        {
            _playableDirector.Play();
        }
    }

    private void DropCigar()
    {
        if (_cigarRb != null)
        {
            _cigarRb.isKinematic = false;

            _cigarRb.velocity = Vector3.zero;

            _cigarRb.transform.parent = null;
        }
    }

    public void DropBroom()
    {
        Debug.Log("Broom Dropped");

        _broomAnimator.SetBool("Broom On", false);

        _broomInHand = false;

        _broomRb.velocity = Vector3.zero;

        _broomRb.isKinematic = false;

        _broomRb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        _broom.transform.parent = null;
    }

    public void SetBroomLoopAnim()
    {
        _broomInLoop = !_broomInLoop;

        if (_broomInLoop)
        {
            _broomAnimator.SetBool("Broom On", true);
        }
        else
        {
            _broomAnimator.SetBool("Broom On", false);
        }
    }

    public void SecurityTriggered()
    {
        _enemyFOVEnabled = false;

        _player = FindObjectOfType<Player>();

        _playerTransform = _player.transform;

        StartCoroutine(DelayPlayerDetectionCoroutine());
    }
}