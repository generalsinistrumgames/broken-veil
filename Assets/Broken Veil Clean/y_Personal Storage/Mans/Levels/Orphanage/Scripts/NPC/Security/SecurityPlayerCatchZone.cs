using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityPlayerCatchZone : MonoBehaviour
{
    public NavMeshAgentControl navMeshAgentControl;

    private void Start()
    {
        if (navMeshAgentControl == null)
        {
            navMeshAgentControl = GetComponentInParent<NavMeshAgentControl>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            player.DisablePlayerMovement();

            navMeshAgentControl.agent.velocity = Vector3.zero;

            //navMeshAgentControl.agent.isStopped = true;

            //navMeshAgentControl.enabled = false;
        }
    }
}