using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityTrigger : MonoBehaviour
{
    [SerializeField] Security _security;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            _security.SecurityTriggered();

            gameObject.SetActive(false);
        }
    }
}