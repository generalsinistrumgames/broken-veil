﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecuritySmoking : MonoBehaviour
{
    [SerializeField] ParticleSystem smoke;

    public void ActivateSmoking()
    {
        smoke.Play();
    }
}