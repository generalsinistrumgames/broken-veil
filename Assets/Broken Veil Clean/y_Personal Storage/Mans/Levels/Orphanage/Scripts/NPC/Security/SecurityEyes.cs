﻿using UnityEngine;
using System.Collections.Generic;
using Cinemachine;

public class SecurityEyes : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] TimelineController timelineController;
    [Space]
    public Player player;
    [Space]
    public CinemachineDollyCart securityCart;
    [Space]
    public GameObject broom;
    [Space]
    public GameObject cigar;
    [Space]
    [SerializeField] Security securityScript;
    [Space]
    [SerializeField] Animator chairAnim;

    [Header("Colliders")]
    [SerializeField] BoxCollider eyesCollider;

    [Header("Disable References")]
    [SerializeField] List<MonoBehaviour> disableScripts = new List<MonoBehaviour>();

    [Header("Player Detect")]
    public bool playerDetected;
    [Space]
    [SerializeField] float maxNearDistance;

    [Header("Ray Offsets")]
    [SerializeField] float rotateSpeed;
    [Space]
    [SerializeField] float yOffset;
    [Space]
    [SerializeField] float zOffset;

    [Header("Sound Event")]
    public string EventName;

    [Header("Game Over Time")]
    [SerializeField] float gameOverTime;

    //    void Start()
    //    {
    //        securityCart = GetComponentInParent<CinemachineDollyCart>();

    //        securityScript = GetComponentInParent<Security>();

    //        timelineController.GetComponentInParent<TimelineController>();

    //        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    //    }

    //    private void Update()
    //    {
    //        if (playerDetected)
    //        {
    //            Debug.Log("Security rotating towards player");

    //            securityScript.transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(new Vector3(player.transform.position.x, securityScript.transform.position.y, player.transform.position.z) - securityScript.transform.position, Vector3.up), rotateSpeed * Time.deltaTime);

    //            player.transform.rotation = Quaternion.Slerp(player.transform.rotation, Quaternion.LookRotation(new Vector3(transform.position.x, player.transform.position.y, transform.position.z) - player.transform.position, Vector3.up), rotateSpeed * Time.deltaTime);
    //        }
    //    }

    //    private void OnTriggerStay(Collider other)
    //    {
    //        if (!playerDetected)
    //        {
    //            if (other.TryGetComponent<Player>(out var playerScript))
    //            {
    //                if (player == null)
    //                {
    //                    player = playerScript;
    //                }

    //                Ray ray = new Ray(transform.position, (player.transform.position + Vector3.up) - transform.position);

    //                RaycastHit hit;

    //                Physics.Raycast(ray, out hit);

    //                Debug.DrawRay(transform.position, (player.transform.position + Vector3.up) - transform.position, Color.red);

    //                Debug.Log(hit.transform.name + " Detected");

    //                if (hit.transform.CompareTag("Player"))
    //                {
    //                    timelineController.StopTimeline();

    //                    PlayerDetected();
    //                }
    //            }
    //        }
    //    }

    //    public void PlayerDetected()
    //    {
    //        DisableSecurityCart();

    //        playerDetected = true;

    //        player.enabled = false;

    //        player.GetComponent<Rigidbody>().isKinematic = true;

    //        securityScript.enabled = false;

    //        if (securityScript._animator.GetCurrentAnimatorStateInfo(0).IsTag("Idle Smoking"))
    //        {
    //            securityScript._animator.SetTrigger("Stand Up");

    //            chairAnim.SetTrigger("Move Chair");

    //            AkSoundEngine.PostEvent(EventName, gameObject);

    //            if (cigar != null)
    //            {
    //                cigar.GetComponent<Rigidbody>().velocity = Vector3.zero;

    //                cigar.GetComponent<Rigidbody>().isKinematic = false;

    //                cigar.transform.parent = null;
    //            }
    //        }
    //        else
    //        {
    //            securityScript._animator.SetTrigger("Trigger Player");

    //            if (broom != null)
    //            {
    //                broom.GetComponent<Rigidbody>().velocity = Vector3.zero;

    //                broom.GetComponent<Rigidbody>().isKinematic = false;

    //                broom.transform.parent = null;
    //            }
    //        }

    //        //foreach (var MonoBehaviour in disableScripts)
    //        //{
    //        //    MonoBehaviour.enabled = false;
    //        //}

    //        GameOver.Instance.SetGameOver(gameOverTime);

    //        AkSoundEngine.PostEvent("Play_checkpoint_stop_vo", gameObject);
    //    }

    //    public void DisableSecurityCart()
    //    {
    //        securityCart.enabled = false;
    //    }
}