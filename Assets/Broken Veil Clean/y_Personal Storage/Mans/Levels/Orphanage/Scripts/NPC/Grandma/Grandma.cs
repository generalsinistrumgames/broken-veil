﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.Playables;
using UnityEngine.Events;

public class Grandma : EnemyWithFOV
{
    [Header("References")]
    MoveAndRotateTowards _moveAndRotateTowards = new MoveAndRotateTowards();
    [Space]
    [SerializeField] PlayableDirector _playableDirector;
    [Space]
    [SerializeField] WwiseGrannySounds _wwiseGrannySounds;
    [Space]
    [SerializeField] Collider chairCollider;

    [Header("Rotation Control")]
    [SerializeField] float _rotatePlayerSpeed;

    [Header("Animation Control")]
    [SerializeField] string _standIdleAnimName;
    [Space]
    [SerializeField] string _watchTvIdleAnimName;
    [Space]
    [SerializeField] string _standUpAnimName;
    [Space]
    [SerializeField] string _currentAnimName;
    [Space]
    [SerializeField] float _animNormalizedDelayValue;

    [Header("Game Over Trigger")]
    [SerializeField] float _gameOverDelay;
    [Space]
    [SerializeField] FloatVariable _gameOverSO;
    [Space]
    [SerializeField] UnityEvent OnGameOver;

    private void Awake()
    {
        Physics.IgnoreCollision(GetComponent<Collider>(), chairCollider); // disable collision with chair
    }

    public override void Start()
    {
        base.Start();

        _playableDirector = GetComponentInParent<PlayableDirector>();

        _wwiseGrannySounds = GetComponent<WwiseGrannySounds>();
    }

    public override IEnumerator DelayPlayerDetectionCoroutine()
    {
        yield return base.DelayPlayerDetectionCoroutine();

        _player.DisablePlayerMovement();

        if (_playableDirector.state == PlayState.Playing)
        {
            _playableDirector.Stop();

            Debug.Log("Triggered playable");
        }

        StartCoroutine(RotatePlayerTowardsGranny());
    }

    private void SetAnimationState()
    {
        if (_enemyAnimator.GetCurrentAnimatorStateInfo(0).IsName(_watchTvIdleAnimName))
        {
            _enemyAnimator.SetTrigger("Stand Up");

            _currentAnimName = _standUpAnimName;

            _animNormalizedDelayValue = 0.8f;
        }
        else
        {
            _enemyAnimator.SetTrigger("Stand Idle");

            _currentAnimName = _standIdleAnimName;

            _animNormalizedDelayValue = 0f;
        }
    }

    public bool DelayedAnimationStateEnds(string animationName, float animDelayTime)
    {
        if (_enemyAnimator.GetCurrentAnimatorStateInfo(0).IsName(animationName) && _enemyAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= animDelayTime)
        {
            Debug.Log("DelayedAnimationStateEnds ends");

            return true;
        }
        else
        {
            Debug.Log("DelayedAnimationStateEnds");

            return false;
        }
    }

    bool GrandmaIsFacingPlayer()
    {
        Debug.Log("Rotating animator");

        //Vector3 direction = (new Vector3(target.position.x, 0, target.position.z) - new Vector3(transform.position.x, 0, transform.position.z)).normalized;

        Vector3 direction = (_playerTransform.position - _enemyTransform.position).normalized;

        //var offsettedDirection = (direction * 0.3f); // removed

        //float strength = Mathf.Min(rotationSpeed * Time.deltaTime, 1);

        //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(direction), strength);

        //draw a ray pointing to our target
        Debug.DrawRay(_enemyTransform.position, direction, Color.cyan, 0.1f);

        var dotRight = Vector3.Dot(_enemyTransform.right, direction);

        var dotForward = Vector3.Dot(_enemyTransform.forward, direction);

        _enemyAnimator.SetFloat("Rotation Right", dotRight);

        if (dotRight > -0.1f && dotRight < 0.1f && dotForward > 0)  // dotforward is checks if facing target
        {
            Debug.Log("Rotating animator ends");

            return true;
        }
        else
        {
            return false;
        }
    }

    IEnumerator CheckAnimationDelayedStateRoutine()
    {
        SetAnimationState();

        bool animDelayEnds = false;

        while (!animDelayEnds)
        {
            animDelayEnds = DelayedAnimationStateEnds(_currentAnimName, _animNormalizedDelayValue);

            yield return null;
        }

        _enemyAnimator.SetTrigger("Pointing To Player");

        StartCoroutine(RotateTowardsPlayerCoroutine());
    }

    IEnumerator RotatePlayerTowardsGranny()
    {
        bool playerIsFacing = false;

        while (!playerIsFacing)
        {
            playerIsFacing = _moveAndRotateTowards.RotateTowards(_playerTransform, _enemyTransform, _rotatePlayerSpeed);

            yield return null;
        }

        StartCoroutine(CheckAnimationDelayedStateRoutine());
    }

    IEnumerator RotateTowardsPlayerCoroutine()
    {
        bool isFacingPlayer = false;

        //bool playerIsFacing = false;

        while (!isFacingPlayer /*|| !playerIsFacing*/)
        {
            if (!isFacingPlayer)
            {
                isFacingPlayer = GrandmaIsFacingPlayer();
            }

            //if (!playerIsFacing)
            //{
            //    playerIsFacing = _moveAndRotateTowards.RotateTowards(_playerTransform, _enemyTransform, _rotatePlayerSpeed);
            //}

            Debug.Log("While is working");

            yield return null;
        }

        //_wwiseGrannySounds.GrannyTriggerPlayerSound();

        Debug.Log("Facing Player Trigger Game Over");

        _gameOverSO.Value = _gameOverDelay;

        OnGameOver?.Invoke();
    }

    public void GrandmaTriggered()
    {
        _enemyFOVEnabled = false;

        _player = FindObjectOfType<Player>();

        _playerTransform = _player.transform;

        StartCoroutine(DelayPlayerDetectionCoroutine());
    }

    #region Signal Methods
    public void SetStandIdleAnim()
    {
        _enemyAnimator.SetTrigger("Stand Idle");
    }

    public void SetWatchingTvIdleAnim()
    {
        _enemyAnimator.SetTrigger("Watching TV Idle");
    }

    public void GrannyFOVControl()
    {
        _enemyFOVEnabled = !_enemyFOVEnabled;
    }

    public void SetDetectionDelay(float detectDelay)
    {
        _detectDelay = detectDelay;
    }

    public void ResetDetectionDelay(float detectDelay)
    {
        _detectDelay = 0;
    }
    #endregion
}