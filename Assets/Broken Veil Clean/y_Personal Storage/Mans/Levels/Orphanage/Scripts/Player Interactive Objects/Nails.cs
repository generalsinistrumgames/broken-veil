﻿using System.Collections.Generic;
using UnityEngine;

public class Nails : MonoBehaviour
{
    [Header("Randomize Falling")]
    [SerializeField] Vector3 force;

    [Header("Nails List")]
    public List<GameObject> nailsList = new List<GameObject>();

    public void NailsFall()
    {
        transform.parent = null;

        foreach (var nail in nailsList)
        {
            nail.GetComponent<Rigidbody>().isKinematic = false;

            nail.GetComponent<Rigidbody>().useGravity = true;

            nail.GetComponent<Rigidbody>().AddForce(force, ForceMode.Impulse);

            nail.transform.localRotation = Quaternion.Euler(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180));
        }
    }
}