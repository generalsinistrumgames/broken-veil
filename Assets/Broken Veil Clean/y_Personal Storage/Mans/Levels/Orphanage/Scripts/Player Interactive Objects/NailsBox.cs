﻿using UnityEngine;

public class NailsBox : MonoBehaviour
{
    [Header("Nails Box Interaction With Player")]
    [SerializeField] KeyCode dropBoxKey;   //temporary
    [Space]
    [SerializeField] Rigidbody rb;
    [Space]
    [SerializeField] Vector3 force;
    [Space]
    [SerializeField] bool rbIsOn = true;
    [Space]
    [SerializeField] BoxCollider triggerCollider;

    [Header("Sound Event")]
    [SerializeField] string pushEvent;
 
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    //temporary for audio test
    private void Update()
    {
        if (Input.GetKeyDown(dropBoxKey))
        {
            ThrowBox();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (rbIsOn)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (other.TryGetComponent<Player>(out var playerScript))
                {
                    ThrowBox();
                }
            }
        } 
    }

    void ThrowBox()
    {
        rbIsOn = false;

        rb.isKinematic = false;

        rb.AddForce(force, ForceMode.Impulse);
   
        Destroy(triggerCollider);

        //AkSoundEngine.PostEvent(pushEvent, gameObject);
    }
}