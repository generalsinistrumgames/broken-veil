﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutItemsInPlayerHand : MonoBehaviour
{
    [SerializeField] Rigidbody rb;

    [Header("Colliders")]
    [SerializeField] BoxCollider scoopTriggerCollider;
    [Space]
    [SerializeField] MeshCollider scoopCollider;

    [Header("To Parent Root")]
    public bool scoopInPlayerHand;
    [Space]
    [SerializeField] GameObject toParentObject;
    [Space]
    [SerializeField] Vector3 toParentPosition;
    [Space]
    [SerializeField] Quaternion toParentRotation;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        toParentObject = GameObject.FindGameObjectWithTag("PlayerRightHand");
    }

    private void Update()
    {
        if (scoopInPlayerHand)
        {
            if (Input.GetMouseButton(0))
            {
                PutInHand();
            }

            if (Input.GetMouseButtonUp(0))
            {
                ThrowOnGround();
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (Input.GetMouseButton(0))
            {
                PutInHand();        
            }
        }
    }

    public void PutInHand()
    {
        rb.useGravity = false;

        rb.velocity = Vector3.zero;

        scoopInPlayerHand = true;

        // triggers
        scoopTriggerCollider.enabled = false;

        scoopCollider.enabled = false;

        transform.SetParent(toParentObject.transform, false);

        transform.localPosition = toParentPosition;

        transform.localRotation = toParentRotation;
    }

    public void ThrowOnGround()
    {
        scoopInPlayerHand = false;

        rb.useGravity = true;
        rb.velocity = Vector3.zero;

        transform.SetParent(null, true);
        // triggers
        scoopCollider.enabled = true;

        scoopTriggerCollider.enabled = true;      
    }
}