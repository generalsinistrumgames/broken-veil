﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swing : MonoBehaviour
{
    [SerializeField] Animation swingAnimation;

    [Header("Timer")]
    [SerializeField] bool timerStart; 
    [Space]
    [SerializeField] float time;
    [Space]
    [SerializeField] float swingTime;

    private void Update()
    {
        if (timerStart)
        {
            time += Time.deltaTime;

            if (time >= swingTime)
            {
                //
                swingAnimation.Stop();
                timerStart = false;
                time -= time;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            // play anim
            swingAnimation.Play();
        }
    }
}