﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DigTrigger : MonoBehaviour
{
    [Header("Move And Rotate To Target")]
    MoveAndRotateTowards moveAndRotate = new MoveAndRotateTowards();
    [Space]
    [SerializeField] float _moveSpeed, _rotateSpeed;
    [Space]
    [SerializeField] Transform _targetPos;
    [Space]
    [SerializeField] Quaternion _targetRot;

    [Header("Playable Control")]
    [SerializeField] UnityEngine.Playables.PlayableDirector _sandboxPlayable;

    [Header("References")]
    [SerializeField] Scoop _scoop;
    [Space]
    [SerializeField] Player _player;
    [Space]
    [SerializeField] Animator _playerAnimator;
    [Space]
    [SerializeField] bool _playerWithinCollider;
    [Space]
    [SerializeField] Collider _sandCollider;

    [Header("Prefab Instantiate Control")]
    [SerializeField] GameObject _badgeParent;
    [Space]
    [SerializeField] GameObject _badgePrefab;

    [Header("Take Badge")]
    [SerializeField] GameObject _badgeObj;
    [Space]
    [SerializeField] Transform _takePosAndRot;
    [Space]
    [SerializeField] float hideBadgeDelay;

    [Header("Dig Control")]
    [SerializeField] bool _digging;

    [Header("Sound Event")]
    [SerializeField] string _playDigEvent;

    private void Update()
    {
        if (_playerWithinCollider && _scoop.itemInPlayerHand && !_digging)
        {
            if (_player.PlayerIsGrounded())
            {
                StartCoroutine(SetPosition());
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (_player == null)
            {
                _player = playerScript;

                _playerAnimator = playerScript.animator;

                PlayableDirectorUtility.DynamicAnimatorBinding(_sandboxPlayable, _playerAnimator, "Player");
            }

            _playerWithinCollider = true; 
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            _playerWithinCollider = false;
        }
    }

    public IEnumerator SetPosition()
    {
        Debug.Log("Start Digging");

        _player.DisablePlayerMovement(true);

        _digging = true;

        _scoop._canDropItem = false;

        Physics.IgnoreCollision(_scoop._itemCollider, _sandCollider);
    
        bool targetPosReached = false;

        bool targetRotReached = false;

        while (!targetPosReached || !targetRotReached)
        {
            _targetPos.position = new Vector3(_targetPos.localPosition.x, _player.transform.localPosition.y, _targetPos.localPosition.z);

            if (!targetPosReached)
            {
                targetPosReached = moveAndRotate.MoveToTargetPosition(_player.transform, _targetPos, _moveSpeed);
            }

            if (!targetRotReached)
            {
                targetRotReached = moveAndRotate.RotateToTargetRotation(_player.transform, _targetRot, _rotateSpeed);
            }

            yield return null;
        }

        _sandboxPlayable.Play();
    }

    public void GetBadge()
    { 
        _badgeObj = Instantiate(_badgePrefab, _badgeParent.transform.localPosition, _badgeParent.transform.localRotation, _badgeParent.transform);
    }

    public void StopDigging()
    {
        Physics.IgnoreCollision(_scoop._itemCollider, _sandCollider, false);

        //_scoop.transform.position = _scoop.transform.position + offset;

        _scoop._knockItemFromHands = true;

        _scoop._canDropItem = true;

        _scoop._canTakeItem = false;

        Debug.Log("Stop Digging");
    }

    public void TakeBadgeInHand()
    {
        var playerItemsIK = _player.GetComponent<PlayerItemsIKControl>();

        _badgeObj.transform.SetParent(playerItemsIK._rightArmItemContainer.transform);

        _badgeObj.transform.localPosition = _takePosAndRot.localPosition;

        _badgeObj.transform.localRotation = _takePosAndRot.localRotation;

        Destroy(_badgeObj, hideBadgeDelay);
    }

    public void EnablePlayerMovement()
    {
        _player.EnablePlayerMovement(false);

        _scoop._knockItemFromHands = false;

        _scoop._canTakeItem = true;

        this.enabled = false;
    }
}