﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairsTrigger : MonoBehaviour
{
    [SerializeField] Player player;
    [SerializeField] float hangTime;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (player == null)
            {
                player = playerScript;
            }

            StartCoroutine(StartHanging());
        }
    }

    IEnumerator StartHanging()
    {
        yield return new WaitForSecondsRealtime(hangTime);

        StopAllCoroutines();
    }
}
