﻿using UnityEngine;
public class BoxTrigger : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] TimelineController timelineController;
    [Space]
    [SerializeField] Nails nailsScript;

    [Header("Broken Box")]
    [SerializeField] bool boxIsBroken;

    [Header("Box Mass Control")]
    [SerializeField] float boxMass;

    [Header("Sound Event")]
    [Space]
    [SerializeField] string fallEvent;

    private void OnTriggerEnter(Collider other)
    {
        if (!boxIsBroken)
        {
            if (other.TryGetComponent<NailsBox>(out var boxScript))
            {
                boxIsBroken = true;

                boxScript.GetComponent<Rigidbody>().mass = boxMass;

                // play fall sounds
         
                nailsScript.NailsFall();

                timelineController.StartTimeline();

                Destroy(gameObject);

                //AkSoundEngine.PostEvent(fallEvent, gameObject);
            }
        }
    }
}