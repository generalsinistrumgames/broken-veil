﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;

public class SandTrigger : MonoBehaviour
{
    [SerializeField] PlayerController playerController;
    [Space]
    [SerializeField] float playerDefaultSpeed;
    [Space]
    [SerializeField] float playerOnSandSpeed;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<PlayerController>(out var playerControllerScript))
        {
            if (playerController == null)
            {
                playerController = playerControllerScript;
            }

            playerDefaultSpeed = playerController.SpeedFactor;

            playerController.SpeedFactor = playerOnSandSpeed;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<PlayerController>(out var playerControllerScript))
        {
            playerController.SpeedFactor = playerDefaultSpeed;
        }
    }
}