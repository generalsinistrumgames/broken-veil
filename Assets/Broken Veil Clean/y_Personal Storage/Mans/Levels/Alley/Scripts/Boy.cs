﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Boy : MonoBehaviour
{
    [Header("Boy")]
    public NavMeshAgent boyAgent;
    [Space]
    public bool boyMoving;
    [Space]
    [SerializeField] float rotateSpeed;

    [Header("Point To Move")]
    public Vector3 pointToMove;

    private void Start()
    {
        boyAgent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        if (boyMoving)
        {
            MoveBoy(pointToMove);
        }
    }

    public void MoveBoy(Vector3 position)
    {
        boyAgent.SetDestination(position);

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(position + Vector3.up - transform.position, Vector3.up), rotateSpeed * Time.deltaTime);

        Debug.Log("Boy is Moving");
    }
  
    private void OnTriggerEnter(Collider other)
    {
        if ((other.TryGetComponent<Board>(out var boardScript)))
        {
            Debug.Log("Board Hitted Boy");

            transform.rotation = Quaternion.Euler(0, 190, 0);
        }
    }
}