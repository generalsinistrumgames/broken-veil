﻿using System;
using System.Collections;
using UnityEngine;

public class ResetGameobjectOffsets : MonoBehaviour
{
    [Header("Reset Offset Control")]
    public Vector3 resetOffset;
    [Space]
    public GameObject[] allObjects;
    [Space]
    public float delayToReset;
    [Space]
    public bool IsMinus = true;

    void Start()
    {
        GetAllGameObjects();
    }

    public void GetAllGameObjects()
    {
        // check if this script already worked
        if (allObjects.Length > 0)
        {
            Debug.Log("Script already worked" + gameObject.name + " destroing " + this + " script");

            Destroy(this);
        }
        else
        {
            // hard getting all gameObjects in scene
            //allObjects = FindObjectsOfType<GameObject>();

            //define array length
            allObjects = new GameObject[transform.childCount];

            //getting objects in child of root
            for (int i = 0; i < allObjects.Length; i++)
            {
                allObjects[i] = transform.GetChild(i).gameObject;
            }

            StartCoroutine(ResetChildOffsets());
        }
    }

    public IEnumerator ResetChildOffsets()
    {
        int offsetResettedValue = 0;

        for (int i = 0; i < allObjects.Length; i++)
        {
            Vector3 pos;

            if (IsMinus)
            {
                pos = allObjects[i].transform.position - resetOffset;
            }
            else
            {
                pos = allObjects[i].transform.position + resetOffset;
            }

            var result = Math.Round(pos.y, 5); // how much digits will returbed to set in position

            pos.y = (float)result;

            Debug.Log(pos.y);

            allObjects[i].transform.position = pos;

            // stop on all resetting ends
            offsetResettedValue++;

            yield return new WaitForSeconds(delayToReset);

            if (offsetResettedValue == allObjects.Length)
            {
                Debug.Log("Stop resetting offsets in " + gameObject.name);

                StopAllCoroutines();
            }
        }
    }
}