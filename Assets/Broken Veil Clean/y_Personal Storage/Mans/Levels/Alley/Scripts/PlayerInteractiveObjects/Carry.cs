﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Carry : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] GameObject player;

    [Header("Colliders")]
    [SerializeField] BoxCollider objectTriggerCollider;
    [Space]
    [SerializeField] MeshCollider objectCollider;

    [Header("To Parent Root")]
    public bool objectInPlayerHand;
    [Space]
    [SerializeField] GameObject toParentObject;
    [Space]
    [SerializeField] Vector3 toParentPosition;
    [Space]
    [SerializeField] Quaternion toParentRotation;

    [Header("Force")]
    [SerializeField] Rigidbody rb;
    [Space]
    [SerializeField] Vector2 force;

    public virtual void Start()
    {
        rb = GetComponent<Rigidbody>();

        toParentObject = GameObject.FindGameObjectWithTag("PlayerRightHand");
    }

    private void Update()
    {
        if (objectInPlayerHand)
        {
            if (Input.GetMouseButton(0))
            {
                PutInHand();

                if (Input.GetKeyDown(KeyCode.Return))
                {
                    ThrowForward();
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                ThrowOnGround();
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!objectInPlayerHand)
        {
            if (other.TryGetComponent<Player>(out var playerScript))
            {
                if (player == null)
                {
                    player = playerScript.gameObject;
                }

                if (Input.GetMouseButtonDown(0))
                {
                    PutInHand();
                }
            }
        }
    }

    public void PutInHand()
    {
        Debug.Log("Put In Hand");

        objectInPlayerHand = true;

        // triggers
        objectTriggerCollider.enabled = false;

        objectCollider.enabled = false;

        rb.useGravity = false;

        rb.velocity = Vector3.zero;

        transform.SetParent(toParentObject.transform, false);

        transform.localPosition = toParentPosition;

        transform.localRotation = toParentRotation;
    }

    public void ThrowOnGround()
    {
        Debug.Log("Throw On Ground");

        objectInPlayerHand = false;

        rb.useGravity = true;

        rb.velocity = Vector3.zero;

        transform.SetParent(null, true);

        // triggers
        objectCollider.enabled = true;

        objectTriggerCollider.enabled = true;
    }

    public void ThrowForward()
    {
        Debug.Log("Throw Forward");

        objectInPlayerHand = false;

        transform.SetParent(null, true);

        // triggers
        objectCollider.enabled = true;

        objectTriggerCollider.enabled = true;

        rb.velocity = Vector3.zero;

        rb.useGravity = true;

        // add force
        rb.AddForce(player.transform.forward * force.x + player.transform.up * force.y, ForceMode.Impulse);


        string b = Input.GetKeyDown(KeyCode.A).ToString();
       
    }
}