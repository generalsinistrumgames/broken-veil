﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bug : MonoBehaviour
{
    [SerializeField] GameObject laser;
    [Space]
    [SerializeField] Vector3 laserPosOffset;
    [Space]
    [SerializeField] Quaternion laserRotOffset;
    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                GameObject playerHand = GameObject.FindGameObjectWithTag("PlayerRightHand");

                Instantiate(laser.transform, playerHand.transform.position + laserPosOffset, laserRotOffset, playerHand.transform);

                Destroy(this);
            }
        }
    }
}