﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Laser : MonoBehaviour
{
    [SerializeField] float maxLaserLength;

    LineRenderer lineRenderer;
 
    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            lineRenderer.enabled = true;

            lineRenderer.SetPosition(0, transform.position);

            RaycastHit hit;

            if (Physics.Raycast(transform.position, transform.up, out hit))
            {
                if (hit.collider)
                {
                    lineRenderer.SetPosition(1, hit.point);
                }
            }

            else
            {
                lineRenderer.SetPosition(1, transform.up * maxLaserLength);
            }

            //Debug.DrawRay(transform.position, transform.up, Color.red, 1f);
        }

        if (Input.GetMouseButtonUp(0))
        {
            lineRenderer.enabled = false;
        }
    }
}
