﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    [Header("Board Rigidbody")]
    [SerializeField] Rigidbody rb;
    [Space]
    [SerializeField] Vector3 force;

    [Header("Board Pull")]
    [SerializeField] bool canPull;
    [Space]
    [SerializeField] int maxPullAmount;
    [Space]
    [SerializeField] int _pulledAmount;

    public int PulledAmount
    {
        get
        {
            return _pulledAmount;
        }

        set
        {
            _pulledAmount = value;

            if (_pulledAmount >= maxPullAmount)
            {
                rb.isKinematic = false;

                rb.AddForce(force, ForceMode.Impulse);

                this.enabled = false;

                //Destroy(this);
            }
        }
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (canPull)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                PulledAmount++;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {  
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            canPull = true;
        }  
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            canPull = false;
        }
    }
}