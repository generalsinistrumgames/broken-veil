﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InBasementTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            playerScript.enabled = true;

            Destroy(gameObject);
        }
    }
}
