﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoysStopNearBasement : MonoBehaviour
{
    [SerializeField] GameObject boysBug;
    [Space]
    [SerializeField] Vector3 lukeNewPos;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Boy>(out var boyScript))
        {
            boyScript.boyMoving = false;

            boyScript.boyAgent.isStopped = true;

            Debug.Log("Boy stopped");
        }

        if (other.TryGetComponent<Player>(out var playerScript))
        {
            playerScript.gameObject.transform.SetParent(null);

            StartCoroutine(OpenLuke());
        }
    }

    IEnumerator OpenLuke()
    {
        yield return new WaitForSecondsRealtime(2f);

        transform.position += lukeNewPos;

        boysBug.transform.localPosition += Vector3.forward;

        boysBug.GetComponent<Rigidbody>().isKinematic = false;

        Destroy(this);

        StopAllCoroutines();
    }
}
