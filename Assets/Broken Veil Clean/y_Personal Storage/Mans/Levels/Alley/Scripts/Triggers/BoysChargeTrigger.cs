﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoysChargeTrigger : MonoBehaviour
{
    [Header("Boys Manager")]
    [SerializeField] Boy[] boysArray = new Boy[] { };

    [Header("Time Delays")]
    [SerializeField] float[] delaysMoveToPlayer = new float[] { };
    [Space]
    [SerializeField] float[] delaysMoveToBasement = new float[] { };

    [Header("Near Distance")]
    [SerializeField] GameObject basement;
    [Space]
    [SerializeField] float stopNearPlayerDistance;
    [Space]
    [SerializeField] float stopNearBasementDistance;

    [Header("Move Manager")]
    [SerializeField] bool chasingPlayer;
    [Space]
    [SerializeField] bool moveToBasement;

    [Header("Lug Player")]
    [SerializeField] Player player;
    [Space]
    [SerializeField] GameObject toParentObject;

    IEnumerator moveBoysToPlayer;

    IEnumerator moveBoysToBasement;

    private void Start()
    {
        moveBoysToPlayer = MoveBoysToPlayer();

        moveBoysToBasement = MoveBoysToBasement();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (player == null)
            {
                player = playerScript;

                player.enabled = false;
            }

            // start moving boys to player
            StartCoroutine(moveBoysToPlayer);
        }

        if (other.TryGetComponent<Boy>(out var boyScript))
        {
            boyScript.boyMoving = false;
        }
    }

    IEnumerator MoveBoysToPlayer()
    {
        Debug.Log("Start Moving Boys To Player");

        foreach (var boy in boysArray)
        {
            boy.pointToMove = player.transform.position;

            boy.boyAgent.stoppingDistance = stopNearPlayerDistance;
        }

        yield return new WaitForSecondsRealtime(delaysMoveToPlayer[0]);

        boysArray[0].boyMoving = true;

        yield return new WaitForSecondsRealtime(delaysMoveToPlayer[1]);

        boysArray[1].boyMoving = true;

        yield return new WaitForSecondsRealtime(delaysMoveToPlayer[2]);

        boysArray[2].boyMoving = true;

        // delay before moving to basement
        yield return new WaitForSecondsRealtime(delaysMoveToPlayer[3]);

        StopMovingBoys();

        Debug.Log("Stop Moving Boys To Player");
        StopCoroutine(moveBoysToPlayer);

        // parent player to boy hands
        player.transform.SetParent(toParentObject.transform, true);

        // start moving boys to basement
        StartCoroutine(moveBoysToBasement);
    }

    IEnumerator MoveBoysToBasement()
    {
        Debug.Log("Start Moving To Basement");

        foreach (var boy in boysArray)
        {
            boy.pointToMove = basement.transform.position;

            boy.boyAgent.stoppingDistance = stopNearBasementDistance;
        }

        yield return new WaitForSecondsRealtime(delaysMoveToBasement[0]);

        boysArray[0].boyMoving = true;

        yield return new WaitForSecondsRealtime(delaysMoveToBasement[1]);

        boysArray[1].boyMoving = true;

        yield return new WaitForSecondsRealtime(delaysMoveToBasement[2]);

        boysArray[2].boyMoving = true;

        StopCoroutine(moveBoysToBasement);

        Debug.Log("Stop Moving To Basement");
    }

    void StopMovingBoys()
    {
        foreach (var boy in boysArray)
        {
            boy.boyMoving = false;
        }
    }
}