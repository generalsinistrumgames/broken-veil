﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartConveyorTrigger : MonoBehaviour
{
    [SerializeField] Color color;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Hook>(out var hookScript))
        {
            PathManager.Instance.EnableCartsInList();
  
            GetComponent<MeshRenderer>().materials[0].SetColor("_BaseColor", color);

            Destroy(this);
        }
    }
}