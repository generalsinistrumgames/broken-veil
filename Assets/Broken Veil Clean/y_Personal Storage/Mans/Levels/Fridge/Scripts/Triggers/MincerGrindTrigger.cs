﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class MincerGrindTrigger : MonoBehaviour
{
    [Header("Grind Pig")]
    [SerializeField] GameObject pig;
    [Space]
    [SerializeField] Rigidbody pigRb;

    [Header("Create Pig")]
    [SerializeField] CinemachinePath conveyorPath;
    [Space]
    [SerializeField] GameObject pigPrefab;
    [Space]
    [SerializeField] Vector3 offset;
    [Space]
    [SerializeField] GameObject pigPrefabParent;

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<PigCarcase>(out var pigCarcase))
        {
            pig = pigCarcase.gameObject;

            pigRb = pig.GetComponent<Rigidbody>();

            pig.transform.parent = null;

            pigRb.isKinematic = false;

            CreatePigOnHook();

            //Destroy(GetComponent<BoxCollider>());
        }
    }

    void CreatePigOnHook()
    {
        GameObject obj = Instantiate(pigPrefab, pigPrefabParent.transform.position, Quaternion.identity, pigPrefabParent.transform);

        obj.GetComponent<CinemachineDollyCart>().m_Path = conveyorPath;

        Debug.Log("Pig Created");
    }
}