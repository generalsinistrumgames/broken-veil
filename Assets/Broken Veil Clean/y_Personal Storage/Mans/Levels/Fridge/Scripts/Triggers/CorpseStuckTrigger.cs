﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorpseStuckTrigger : MonoBehaviour
{
    [SerializeField] Conveyor conveyor;
    [Space]
    [SerializeField] float animationDuration;
    [Space]

    [Header("Corpse Manager")]
    [SerializeField] GameObject corpse;
    [Space]
    [SerializeField] Rigidbody corpseRb;
    [Space]
    [SerializeField] Vector3 fallForce;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Corpse>(out var corpseScript))
        {
            corpse = corpseScript.gameObject;

            corpse.transform.parent = null;

            corpseRb = corpse.GetComponent<Rigidbody>();

            PathManager.Instance.DisableCartsInList();

            StartCoroutine(CorpseFall());
        }
    }

    IEnumerator CorpseFall()
    {
        yield return new WaitForSecondsRealtime(animationDuration);

        corpseRb.isKinematic = false;

        corpseRb.AddForce(fallForce, ForceMode.Force);

        corpseRb.mass = 1000;

        PathManager.Instance.EnableCartsInList();

        StopAllCoroutines();
    }
}