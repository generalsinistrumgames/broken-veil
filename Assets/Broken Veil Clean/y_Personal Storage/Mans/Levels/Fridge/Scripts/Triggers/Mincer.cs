﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mincer : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<PigCarcase>(out var pigCarcase))
        {
            DestroyObjects(pigCarcase.gameObject);
        }

        if (other.TryGetComponent<Player>(out var playerScript))
        {
            DestroyObjects(playerScript.gameObject);
        }
    }

    void DestroyObjects(GameObject obj)
    {
        Destroy(obj);
    }
}