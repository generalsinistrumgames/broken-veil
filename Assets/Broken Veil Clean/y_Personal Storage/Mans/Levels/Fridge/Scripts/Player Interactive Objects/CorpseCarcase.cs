﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorpseCarcase : MonoBehaviour
{
    [SerializeField] Rigidbody rb;

    [Header("Player")]
    [SerializeField] GameObject player;
    [Space]
    [SerializeField] Rigidbody playerRb;
    [Space]
    [SerializeField] Player playerScript;
    [Space]
    [SerializeField] bool playerHanging;

    [Header("To Parent Player")]
    [SerializeField] Vector3 positionOffset;
    [Space]
    [SerializeField] Vector3 rotationOffset;

    [Header("Timer")]
    [SerializeField] bool timerStart;
    [Space]
    [SerializeField] float time;
    [Space]
    [SerializeField] float maxHangTime;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (timerStart)
        {
            time += Time.deltaTime;

            if (time >= maxHangTime)
            {
                playerHanging = false;

                playerScript.enabled = true;

                transform.parent = null;

                player.transform.parent = null;

                rb.mass = 100f;

                rb.isKinematic = false;

                rb.AddForce(transform.right * 1f);

                timerStart = false;

                time -= time;

                playerRb.useGravity = true;
            }
        }

        if (playerHanging)
        {
            if (Input.GetMouseButton(0))
            {
                playerRb.useGravity = false;

                timerStart = true;

                playerHanging = true;

                player.GetComponent<Animator>().SetFloat("Movement", 0);

                player.transform.SetParent(transform, true);

                playerScript.enabled = false;

                player.transform.position = transform.position + positionOffset;
            }

            if (Input.GetMouseButtonUp(0))
            {
                timerStart = false;

                playerHanging = false;

                player.transform.SetParent(null, true);

                playerScript.enabled = true;

                playerRb.useGravity = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!playerHanging)
        {
            if (other.TryGetComponent<Player>(out var playerComponent))
            {
                if (player == null)
                {
                    playerScript = playerComponent;

                    player = playerScript.gameObject;

                    playerRb = player.GetComponent<Rigidbody>();
                }

                if (Input.GetMouseButtonDown(0))
                {
                    playerHanging = true;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            Destroy(this);
        }
    }
}