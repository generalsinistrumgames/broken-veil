﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigCarcase : MonoBehaviour
{
    [SerializeField] Rigidbody pigCarcaseRb;

    [Header("Player")]
    [SerializeField] GameObject player;
    [Space]
    [SerializeField] Player playerScript;
    [Space]
    [SerializeField] Rigidbody playerRb;
    [Space]
    [SerializeField] bool playerHanging;

    [Header("To Parent Player Offsets")]
    [SerializeField] Vector3 positionOffset;
    [Space]
    [SerializeField] Vector3 rotationOffset;

    private void Start()
    {
        pigCarcaseRb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (playerHanging)
        {
            if (Input.GetMouseButton(0))
            {
                playerRb.useGravity = false;

                player.GetComponent<Animator>().SetFloat("Movement", 0);

                player.transform.SetParent(transform, true);

                playerScript.enabled = false;

                player.transform.position = transform.position + positionOffset;
            }

            if (Input.GetMouseButtonUp(0))
            {
                playerHanging = false;

                player.transform.SetParent(null, true);

                playerScript.enabled = true;

                playerRb.useGravity = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!playerHanging)
        {
            if (other.TryGetComponent<Player>(out var playerComponent))
            {
                if (player == null)
                {
                    playerScript = playerComponent;

                    player = playerScript.gameObject;

                    playerRb = player.GetComponent<Rigidbody>();
                }

                if (Input.GetMouseButtonDown(0))
                {
                    playerHanging = true;
                }
            }
        }
    }
}