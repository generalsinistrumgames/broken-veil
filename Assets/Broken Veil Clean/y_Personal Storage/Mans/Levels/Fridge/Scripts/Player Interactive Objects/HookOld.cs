﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookOld : MonoBehaviour
{
    [Header("Physics")]
    public bool hookInPlayerHand;
    [Space]
    [SerializeField] Rigidbody rb;
    [Space]
    [SerializeField] Vector2 force;

    [Header("Colliders")]

    [SerializeField] BoxCollider hookTriggerCollider;
    [Space]
    [SerializeField] BoxCollider hookCollider;

    [Header("To Parent Root")]
    [SerializeField] GameObject player;
    [Space]
    [SerializeField] GameObject toParentObject;
    [Space]
    [SerializeField] Vector3 toParentPosition;
    [Space]
    [SerializeField] Quaternion toParentRotation;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        toParentObject = GameObject.FindGameObjectWithTag("PlayerRightHand");
    }

    private void Update()
    {
        if (hookInPlayerHand)
        {
            if (Input.GetMouseButton(0))
            {
                PutInHand();

                if (Input.GetKey(KeyCode.Return))
                {
                    ThrowHookForward();
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                ThrowOnGround();
            }  
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!hookInPlayerHand)
        {
            if (other.TryGetComponent<Player>(out var playerScript))
            {
                if (player == null)
                {
                    player = playerScript.gameObject;
                }

                if (Input.GetMouseButtonDown(0))
                {
                    PutInHand();
                }
            }
        }   
    }

    void PutInHand()
    {
        hookInPlayerHand = true;

        rb.useGravity = false;

        // triggers
        hookTriggerCollider.enabled = false;

        hookCollider.enabled = false;

        transform.SetParent(toParentObject.transform, false);

        transform.localPosition = toParentPosition;

        transform.localRotation = toParentRotation;
    }

    void ThrowOnGround()
    {
        hookInPlayerHand = false;

        rb.useGravity = true;

        transform.SetParent(null, true);

        // triggers
        hookCollider.enabled = true;

        hookTriggerCollider.enabled = true;
    }

    void ThrowHookForward()
    {
        hookInPlayerHand = false;

        rb.useGravity = true;

        transform.SetParent(null, true);

        // triggers
        hookCollider.enabled = true;

        hookTriggerCollider.enabled = true;

        // add force
        rb.AddForce(player.transform.forward * force.x + player.transform.up * force.y, ForceMode.Impulse);
    }
}