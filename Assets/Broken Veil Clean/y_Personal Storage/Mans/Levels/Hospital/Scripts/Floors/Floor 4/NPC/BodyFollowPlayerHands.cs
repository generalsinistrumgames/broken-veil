﻿using UnityEngine;

public class BodyFollowPlayerHands : MonoBehaviour
{
    [Header("Disable Scripts")]
    [SerializeField] BodyFollowPlayer bodyFollowPlayer;

    private void Start()
    {
        bodyFollowPlayer = GetComponentInParent<BodyFollowPlayer>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            playerScript.enabled = false;

            // disable scripts
            bodyFollowPlayer.enabled = false;
            enabled = false;

            // gameOver
        }
    }
}