﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BodyFollowPlayer : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] NavMeshAgent agent;
    [Space]
    [SerializeField] Player player;
    [Space]
    [SerializeField] BoxCollider eyesTrigger;


    [SerializeField] bool isFollowingPlayer;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (isFollowingPlayer)
        {
            agent.SetDestination(player.transform.position);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            player = playerScript;

            eyesTrigger.enabled = false;

            isFollowingPlayer = true;
        }
    }
}
