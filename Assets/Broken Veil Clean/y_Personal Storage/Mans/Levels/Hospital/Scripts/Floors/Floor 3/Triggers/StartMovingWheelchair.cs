﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMovingWheelchair : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] Wheelchair wheelchair;
    [Space]
    [SerializeField] StopMovingWheelchair stopMovingWheelchair;

    [Header("Colliders")]
    [SerializeField] BoxCollider startMovingWheelchairCollider;
    [Space]
    [SerializeField] BoxCollider stopMovingWheelchairCollider;

    [SerializeField] float animationDuration;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            wheelchair.MoveToTarget(transform.position);
        }

        if (other.TryGetComponent<Wheelchair>(out var WheelchairScript))
        {
            startMovingWheelchairCollider.enabled = false;

            stopMovingWheelchairCollider.enabled = true;

            wheelchair.wheelchairAgent.isStopped = true;

            wheelchair.wheelchairAgent.velocity = Vector3.zero;

            StartCoroutine(WaitRotation());
        }
    }

    IEnumerator WaitRotation()
    {     
        yield return new WaitForSecondsRealtime(animationDuration);

        wheelchair.MoveToTarget(stopMovingWheelchair.transform.position);

        wheelchair.wheelchairAgent.isStopped = false;

        StopAllCoroutines();
    }
}