﻿using UnityEngine;
using UnityEngine.AI;

public class StopFollowingBodiesTrigger : MonoBehaviour
{
    [SerializeField] NavMeshAgent[] agents;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            foreach (NavMeshAgent agent in agents)
            {
                agent.isStopped = true;
            }

            Destroy(gameObject);
        }
    }
}