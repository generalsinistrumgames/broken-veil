﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullOutTheSocketTrigger : MonoBehaviour
{
    [Header("Socket")]
    [SerializeField] TriggerPatientInBed triggerPatientInBed;
    [Space]
    [SerializeField] Rigidbody socketRb;
    [Space]
    [SerializeField] Vector3 socketRbForce;

    [Header("Set Of False Teeth")]
    [SerializeField] Rigidbody setOfFalseTeethRb;
    [Space]
    [SerializeField] Vector3 setOfFalseTeethRbForce;
    [Space]
    [SerializeField] float delayBeforeTeethFalls;

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                triggerPatientInBed.socketActive = false;

                socketRb.isKinematic = false;

                socketRb.AddForce(socketRbForce, ForceMode.Acceleration);

                StartCoroutine(StartTimer());
               
            }
        }

        IEnumerator StartTimer()
        {
            yield return new WaitForSecondsRealtime(delayBeforeTeethFalls);

            setOfFalseTeethRb.AddForce(setOfFalseTeethRbForce, ForceMode.Impulse);

            Destroy(gameObject);
        }
    }
}