﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Wheelchair : MonoBehaviour
{
    public NavMeshAgent wheelchairAgent;
    [Space]
    public bool isMoving;

    [Header("Rotate")]
    public GameObject faceTarget;
    [Space]
    [SerializeField] float rotateSpeed;

    void Start()
    {
        wheelchairAgent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (isMoving)
        {
            FaceToTarget();
        }
    }

    public void MoveToTarget(Vector3 destination)
    {
        wheelchairAgent.SetDestination(destination);
    }

    public void FaceToTarget()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(faceTarget.transform.position - transform.position, Vector3.up), rotateSpeed * Time.deltaTime);
    }
}