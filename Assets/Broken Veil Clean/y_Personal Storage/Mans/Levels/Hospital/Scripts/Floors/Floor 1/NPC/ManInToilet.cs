﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ManInToilet : MonoBehaviour
{
    public NavMeshAgent menAgent;
    [Space]
    public bool isMoving;

    [Header("Rotate")]
    public GameObject faceTarget;
    [Space]
    [SerializeField] float rotateSpeed;

    private void Start()
    {
        menAgent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (isMoving)
        {
            FaceToTarget();
        }
    }

    public void MoveToTarget(Transform destination)
    {
        menAgent.SetDestination(destination.position);
    }

    public void FaceToTarget()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(faceTarget.transform.position - transform.position, Vector3.up), rotateSpeed * Time.deltaTime);
    }
}