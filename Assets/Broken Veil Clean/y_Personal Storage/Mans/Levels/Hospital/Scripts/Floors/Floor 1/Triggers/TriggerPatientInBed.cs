﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPatientInBed : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] Player player;
    [Space]
    [SerializeField] GameObject patientInBed;
    [Space]
    [SerializeField] WomamAtReception womamAtReception;

    [Space]
    [SerializeField] float delayBeforeDeath;
    [Space]
    public bool socketActive = true;


    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            player = playerScript;

            StartCoroutine(StartTimer());

            GetComponent<BoxCollider>().enabled = false;
        }
    }

    public IEnumerator StartTimer()
    {
        yield return new WaitForSecondsRealtime(delayBeforeDeath);

        if (socketActive)
        {
            womamAtReception.MoveToTarget(player.transform);

            womamAtReception.womanAgent.stoppingDistance = 2f;

            player.enabled = false;
        }

        else
        {
            Destroy(gameObject);
        }
    }
}