﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Gastroenterologist : MonoBehaviour
{
    public NavMeshAgent gastroenterologist;
    [Space]
    public bool isMoving;
    [Space]
    [SerializeField] bool moveToBodies;
    [Space]
    [SerializeField] Transform bodiesPoint;

    [Header("Rotate")]
    public GameObject faceTarget;
    [Space]
    [SerializeField] float rotateSpeed;

    private void Start()
    {
        gastroenterologist = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (isMoving)
        {
            FaceToTarget();
        }

        if (moveToBodies)
        {
            gastroenterologist.SetDestination(bodiesPoint.position);

            moveToBodies = false;
        }
    }

    public void MoveToTarget(Transform destination)
    {
        gastroenterologist.SetDestination(destination.position);
    }

    public void FaceToTarget()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(faceTarget.transform.position - transform.position, Vector3.up), rotateSpeed * Time.deltaTime);
    }
}