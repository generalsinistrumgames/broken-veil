﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TearScotchTapeTrigger : MonoBehaviour
{
    [SerializeField] Rigidbody doorRb;
    [Space]
    public bool canTear;
    [Space]
    [SerializeField] int needtearValue;
    [Space]
    [SerializeField] int toarValue;

    private void Update()
    {
        if (canTear)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                toarValue++;

                if (toarValue >= needtearValue)
                {
                    doorRb.isKinematic = false;

                    canTear = false;

                    Destroy(gameObject);
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            canTear = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            canTear = false;
        }
    }
}