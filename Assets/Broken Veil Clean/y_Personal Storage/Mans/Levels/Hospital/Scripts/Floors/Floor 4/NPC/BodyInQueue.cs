﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyInQueue : MonoBehaviour
{
    [Header("Refs")]
    public GameObject player;

    [Header("Rotation")]
    public bool rotateTowardsPlayer;
    [Space]
    public float rotateSpeed;

    void Update()
    {
        if (rotateTowardsPlayer)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(new Vector3(transform.position.x, transform.position.y, player.transform.position.z) - transform.position, Vector3.up), rotateSpeed * Time.deltaTime);

            if (transform.rotation == player.transform.rotation)
            {
                rotateTowardsPlayer = false;
            }
        }
    }
}