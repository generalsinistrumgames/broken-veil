﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class SetCameraViewInRooms : MonoBehaviour
{
    [SerializeField] CinemachineVirtualCamera virtualCamera;
    [Space]
    [SerializeField] Vector3 defaultCameraViewTransform;
    [Space]
    [SerializeField] Vector3 virtualCameraViewTransform;

    private void Start()
    {
        virtualCamera = GameObject.FindGameObjectWithTag("CM Virtual Camera").GetComponent<CinemachineVirtualCamera>(); 
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            defaultCameraViewTransform = virtualCamera.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset;

            virtualCamera.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = virtualCameraViewTransform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            virtualCamera.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = defaultCameraViewTransform;
        }
    }
}