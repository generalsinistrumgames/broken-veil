﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMenInToilet : MonoBehaviour
{
    [SerializeField] ManInToilet manInToilet;
    [Space]
    [SerializeField] Transform manTarget;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            manInToilet.MoveToTarget(manTarget);

            Destroy(gameObject);
        }
    }
}