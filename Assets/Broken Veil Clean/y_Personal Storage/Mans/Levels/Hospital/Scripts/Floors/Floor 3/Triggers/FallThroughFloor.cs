﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallThroughFloor : MonoBehaviour
{
    [SerializeField] Rigidbody floorRb;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            floorRb.isKinematic = false;

            Destroy(gameObject);
        }
    }
}