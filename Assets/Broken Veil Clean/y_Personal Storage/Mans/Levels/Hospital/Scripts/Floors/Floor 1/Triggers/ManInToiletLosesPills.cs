﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManInToiletLosesPills : MonoBehaviour
{
    [SerializeField] GameObject pills;
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<ManInToilet>(out var manInToilet))
        {
            pills.transform.parent = null;

            pills.GetComponent<Rigidbody>().isKinematic = false;

            Destroy(gameObject);
        }
    }
}