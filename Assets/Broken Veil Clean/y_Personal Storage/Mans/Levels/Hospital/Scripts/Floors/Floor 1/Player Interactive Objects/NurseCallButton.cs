﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NurseCallButton : MonoBehaviour
{
    [SerializeField] WomamAtReception womamAtReception;
    [Space]
    [SerializeField] Transform targetPosition;
    [Space]
    [SerializeField] GameObject sheet;
    [Space]
    [SerializeField] Material buttomMaterial;

    private void Start()
    {
        buttomMaterial = GetComponent<MeshRenderer>().material;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<SetOfFalseTeeth>(out var setOfFalseTeeth))
        {
            womamAtReception.MoveToTarget(targetPosition);

            buttomMaterial.SetColor("_BaseColor", Color.red);

            sheet.SetActive(true);

            Destroy(this);
        }
    }
}