﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmookyPlace : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] Player player;

    [Header("Inside Place")]
    [SerializeField] bool insideSmookyPlace;

    [Header("Timer")]
    [SerializeField] float timer;

    private void Update()
    {
        if (insideSmookyPlace)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                player.enabled = false;

                this.enabled = false;

                Debug.Log("Player is Dead");
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (player == null)
            {
                player = playerScript;
            }

            insideSmookyPlace = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        insideSmookyPlace = false;
    }
}