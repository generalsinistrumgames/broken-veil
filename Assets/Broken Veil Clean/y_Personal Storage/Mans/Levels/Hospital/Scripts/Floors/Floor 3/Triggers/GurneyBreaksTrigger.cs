﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GurneyBreaksTrigger : MonoBehaviour
{
    [SerializeField] GameObject[] wheels;
    [Space]
    [SerializeField] Rigidbody gurneyRb;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Gurney>(out var gurneyScript))
        {
            foreach (GameObject wheel in wheels)
            {
                Destroy(wheel); 
            }

            StartCoroutine(DisableRigidboy());
        }
    }

    IEnumerator DisableRigidboy()
    {
        yield return new WaitForSecondsRealtime(1f);

        gurneyRb.isKinematic = true;

        StopAllCoroutines();
    }
}