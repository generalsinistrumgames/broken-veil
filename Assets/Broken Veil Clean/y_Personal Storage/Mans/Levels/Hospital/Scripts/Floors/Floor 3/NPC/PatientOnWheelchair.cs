﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatientOnWheelchair : MonoBehaviour
{
    [SerializeField] Wheelchair wheelchair;

    private void Start()
    {
        wheelchair = GetComponentInParent<Wheelchair>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            wheelchair.wheelchairAgent.isStopped = true;

            //wheelchair.wheelchairAgent.enabled = false;

            wheelchair.wheelchairAgent.velocity = Vector3.zero;

            playerScript.gameObject.SetActive(false);
        }
    }
}