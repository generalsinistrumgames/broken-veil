﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopMovingWheelchair : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] Wheelchair wheelchair;
    [Space]
    [SerializeField] StartMovingWheelchair startMovingWheelchair;

    [Header("Colliders")]
    [SerializeField] BoxCollider stopMovingWheelchairCollider;
    [Space]
    [SerializeField] BoxCollider startMovingWheelchairCollider;

    [SerializeField] float animationDuration;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Wheelchair>(out var WheelchairScript))
        {         
            stopMovingWheelchairCollider.enabled = false;

            startMovingWheelchairCollider.enabled = true;

            wheelchair.wheelchairAgent.isStopped = true;

            wheelchair.wheelchairAgent.velocity = Vector3.zero;

            StartCoroutine(WaitRotation());
        }
    }

    IEnumerator WaitRotation()
    {
        yield return new WaitForSecondsRealtime(animationDuration);

        wheelchair.MoveToTarget(startMovingWheelchair.transform.position);

        wheelchair.wheelchairAgent.isStopped = false;

        StopAllCoroutines();
    }
}