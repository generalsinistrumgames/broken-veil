﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBodyInQueue : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] BodyInQueue bodyInQueue;
    [Space]
    [SerializeField] GameObject bodyEyes;

    [Header("Rigidbody")]
    [SerializeField] Rigidbody bodyRb;
    [Space]
    [SerializeField] Vector3 force;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            bodyInQueue.player = playerScript.gameObject;

            bodyEyes.SetActive(true);

            bodyRb.isKinematic = false;

            bodyRb.AddForce(force, ForceMode.Acceleration);

            //bodyInQueue.rotateTowardsPlayer = true;

            Destroy(gameObject);
        }
    }
}