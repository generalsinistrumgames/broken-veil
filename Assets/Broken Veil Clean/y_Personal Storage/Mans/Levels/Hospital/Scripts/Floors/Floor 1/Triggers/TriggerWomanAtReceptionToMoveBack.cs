﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerWomanAtReceptionToMoveBack : MonoBehaviour
{
    [SerializeField] WomamAtReception womamAtReception;
    [Space]
    [SerializeField] Transform womanTargetAtReception;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            womamAtReception.MoveToTarget(womanTargetAtReception);

            Destroy(gameObject);
        }
    }
}