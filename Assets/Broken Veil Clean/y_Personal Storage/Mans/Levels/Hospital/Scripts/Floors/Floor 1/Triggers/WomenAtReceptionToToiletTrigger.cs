﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WomenAtReceptionToToiletTrigger : MonoBehaviour
{
    [SerializeField] WomamAtReception womamAtReception;
    [Space]
    [SerializeField] Transform womanTarget;
    [Space]
    [SerializeField] Transform pillsPutTarget;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Pills>(out var pillsScript))
        {
            pillsScript.transform.position = pillsPutTarget.transform.position;

            StartCoroutine(DelayBeforeMoveToToilet());     
        }

        IEnumerator DelayBeforeMoveToToilet()
        {
            yield return new WaitForSecondsRealtime(3f);

            womamAtReception.MoveToTarget(womanTarget);

            Destroy(gameObject);
        }
    }
}