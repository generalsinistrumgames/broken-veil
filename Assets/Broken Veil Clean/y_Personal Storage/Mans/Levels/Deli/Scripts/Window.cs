﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : MonoBehaviour
{
    [SerializeField] GameObject destroyedWindowPrefab;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            DestroyWindow();
        }
    }


    void DestroyWindow()
    {
        Instantiate(destroyedWindowPrefab, transform.position, transform.rotation);

        Destroy(gameObject);

        Debug.Log("Window Destroyed");
    }
}