﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButcherHands : MonoBehaviour
{
    [Header("Disable Butcher Scripts")]
    [SerializeField] Butcher butcher;
    [Space]
    [SerializeField] ButcherEyes butcherEyes;

    private void Start()
    {
        butcher = GetComponentInParent<Butcher>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            playerScript.gameObject.SetActive(false);

            // disable scripts
            //butcher.enabled = false;

            butcher.chasingPlayer = false;
            butcher.navMeshAgent.isStopped = true;
            enabled = false;

            // gameOver
        }

        if (true)
        {

        }
    }
}
