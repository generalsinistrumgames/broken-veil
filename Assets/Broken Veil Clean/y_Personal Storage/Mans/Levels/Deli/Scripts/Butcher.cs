﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Butcher : MonoBehaviour
{
    [Header("NavMesh")]
    public NavMeshAgent navMeshAgent;
    [Space]
    public float runSpeed;

    [Header("Security Hands")]
    public GameObject securityRightHand;
    [Space]
    public GameObject securityLeftHand;

    [Header("Rotation")]
    public float rotateSpeed;

    [Header("Timer")]
    public bool timerStart;
    public float time;

    [Header("Animation Duration")]
    public float animDuration;

    [Header("Player Detect")]
    [Space]
    public GameObject player;
    public bool chasingPlayer;
    [Space]
    [SerializeField] float maxNearDistance;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (chasingPlayer)
        {
            MoveToTarget(player.gameObject.transform);

            Debug.Log("Chasing Player");
        }
    }

    public void MoveToTarget(Transform pointToMove)
    {
        navMeshAgent.isStopped = false;

        navMeshAgent.speed = runSpeed;

        navMeshAgent.SetDestination(pointToMove.position);

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(pointToMove.position + Vector3.up - transform.position, Vector3.up), rotateSpeed * Time.deltaTime);

        // Check if we've reached the destination
        //if (!navMeshAgent.pathPending)
        //{
        //    if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
        //    {
        //        if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f)
        //        {

        //        }
        //    }
        //}
    }

    public IEnumerator RotateSecurity(Quaternion rotation)
    {
        bool rotateEnds = false;

        while (!rotateEnds)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotateSpeed * Time.deltaTime);

            if (transform.rotation == rotation)
            {
                rotateEnds = true;
                Debug.Log("asfefsfsefsef");
                StopAllCoroutines();
            }

            yield return new WaitForSecondsRealtime(0.1f);
        }
    }
}