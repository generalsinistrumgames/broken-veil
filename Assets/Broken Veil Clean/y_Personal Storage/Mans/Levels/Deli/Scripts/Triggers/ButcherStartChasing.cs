﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButcherStartChasing : MonoBehaviour
{
    [SerializeField] Butcher butcher;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            butcher.player = playerScript.gameObject;

            butcher.chasingPlayer = true;

            Destroy(gameObject);
        }
    }
}