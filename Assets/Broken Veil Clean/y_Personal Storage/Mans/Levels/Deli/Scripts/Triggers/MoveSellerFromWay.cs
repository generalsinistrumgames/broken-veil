﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSellerFromWay : MonoBehaviour
{
    [SerializeField] GameObject seller;
    [Space]
    [SerializeField] Transform targetPosition;
    [Space]
    [SerializeField] bool movingFromWay;
    [Space]
    [SerializeField] float speed;

    private void Update()
    {
        if (movingFromWay)
        {
            seller.transform.localPosition = Vector3.MoveTowards(seller.transform.localPosition, targetPosition.position + seller.transform.up, speed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Butcher>(out var butcherScript))
        {
            movingFromWay = true;
        }
    }
}