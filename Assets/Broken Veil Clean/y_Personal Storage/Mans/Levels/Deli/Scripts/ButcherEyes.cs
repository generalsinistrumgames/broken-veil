﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButcherEyes : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] GameObject player;
    [Space]
    [SerializeField] Butcher butcherScript;

    [Header("Colliders")]
    [SerializeField] BoxCollider eyesCollider;
    [Space]
    //[SerializeField] BoxCollider handsCollider;

    [Header("Player Detect")]
    public bool chasingPlayer;
    [Space]
    [SerializeField] float maxNearDistance;

    [Header("Ray Offsets")]
    [SerializeField] float yOffset;
    [Space]
    [SerializeField] float zOffset;

    void Start()
    {
        butcherScript = GetComponentInParent<Butcher>();
    }

    void Update()
    {
        if (chasingPlayer)
        {
            butcherScript.MoveToTarget(player.gameObject.transform);

            Debug.Log("Chasing Player");
        }
    }

    void OnTriggerStay(Collider other)
    {
        //if (!chasingPlayer)
        //{
            if (other.TryGetComponent<Player>(out var playerScript))
            {
                if (player == null)
                {
                    player = playerScript.gameObject;
                }

                // draw ray
                Ray ray = new Ray(transform.position + transform.forward * zOffset, player.transform.position - transform.position + Vector3.up * yOffset);

                RaycastHit hit;

                Physics.Raycast(ray, out hit);

                Debug.DrawRay(transform.position + transform.forward * zOffset, player.transform.position - transform.position + Vector3.up * yOffset, Color.red);

                Debug.Log(hit.transform.name);

                if (hit.transform.CompareTag("Player"))
                {
                    butcherScript.navMeshAgent.speed = butcherScript.runSpeed;

                    // add offset between positions
                    butcherScript.navMeshAgent.stoppingDistance = maxNearDistance;

                    chasingPlayer = true;

                    eyesCollider.enabled = false;

                    //handsCollider.enabled = true;
                }
            //}
        }
    }
}
