﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValveFallPipe : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] Player player;
    [Space]
    [SerializeField] Rigidbody pipeRb;
    [Space]
    [SerializeField] SphereCollider trigger;

    [Header("Rotate")]
    [SerializeField] Quaternion targetRotation;
    [Space]
    [SerializeField] bool canRotate = true;
    [Space]
    [SerializeField] bool rotating;
    [Space]
    [SerializeField] float rotationSpeed;

    private void OnTriggerStay(Collider other)
    {
        if (canRotate)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (other.TryGetComponent<Player>(out var playerScript))
                {
                    if (player == null)
                    {
                        player = playerScript;  
                    }

                    player.enabled = false;

                    player.GetComponent<Animator>().SetFloat("Movement", 0);

                    trigger.enabled = false;

                    rotating = true;

                    canRotate = false;
                }
            }
        }
    }

    private void Update()
    {
        if (rotating)
        {
            RotateToTarget();
        }
    }


    void RotateToTarget()
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);

        if (transform.rotation == targetRotation)
        {
            Debug.Log("Valve Turned");

            rotating = false;

            player.enabled = true;

            pipeRb.isKinematic = false;

            pipeRb.gameObject.transform.localRotation = Quaternion.Euler(pipeRb.gameObject.transform.localRotation.x + 45f, pipeRb.gameObject.transform.localRotation.y, pipeRb.gameObject.transform.localRotation.z);

            Destroy(this);
        }
    }
}