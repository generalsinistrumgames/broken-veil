﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stick : MonoBehaviour
{
    [SerializeField] FloatingBox floatingBox;

    [Header("Physics")]
    public bool stickInPlayerHand;
    [Space]
    public Rigidbody rb;
    [Space]
    [SerializeField] Vector2 force;

    [Header("Colliders")]

    [SerializeField] BoxCollider stickTriggerCollider;
    [Space]
    [SerializeField] BoxCollider stickCollider;

    [Header("To Parent Root")]
    [SerializeField] GameObject player;
    [Space]
    [SerializeField] GameObject toParentObject;
    [Space]
    [SerializeField] Vector3 toParentPosition;
    [Space]
    [SerializeField] Quaternion toParentRotation;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        toParentObject = GameObject.FindGameObjectWithTag("PlayerRightHand");
    }

    private void Update()
    {
        if (stickInPlayerHand)
        {
            if (Input.GetMouseButton(0))
            {
                PutInHand();
            }     
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!stickInPlayerHand)
        {
            if (other.TryGetComponent<Player>(out var playerScript))
            {
                if (player == null)
                {
                    player = playerScript.gameObject;
                }

                if (Input.GetMouseButtonDown(0))
                {
                    PutInHand();

                    playerScript.enabled = false;

                    floatingBox.boxMovementEnabled = true;

                }
            }
        }
    }

    void PutInHand()
    {
        stickInPlayerHand = true;

        rb.useGravity = false;

        // triggers
        stickTriggerCollider.enabled = false;

        stickCollider.enabled = false;

        transform.SetParent(toParentObject.transform, false);

        transform.localPosition = toParentPosition;

        transform.localRotation = toParentRotation;
    }

    public void ThrowOnGround()
    {
        stickInPlayerHand = false;

        rb.useGravity = true;

        transform.SetParent(null, true);

        // triggers
        stickCollider.enabled = true;

        stickTriggerCollider.enabled = true;
    }
}