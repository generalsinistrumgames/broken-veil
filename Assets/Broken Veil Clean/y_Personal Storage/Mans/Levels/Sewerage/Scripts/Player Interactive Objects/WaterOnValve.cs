﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterOnValve : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] WaterFillContainer waterFillContainer;
    [Space]
    [SerializeField] Animation pipeShiverAnim;
    [Space]
    [SerializeField] Player player;
    [Space]
    [SerializeField] SphereCollider trigger;

    [Header("Rotate")]
    [SerializeField] bool canRotate = true;
    [Space]
    [SerializeField] bool opened;
    [Space]
    [SerializeField] bool rotating;
    [Space]
    [SerializeField] float rotationSpeed;
    [Space]
    [SerializeField] Quaternion targetRotation;
    [Space]
    [SerializeField] Quaternion defaultRotation;

    private void Start()
    {
        defaultRotation = transform.rotation;
    }

    private void OnTriggerStay(Collider other)
    {
        if (canRotate)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (other.TryGetComponent<Player>(out var playerScript))
                {
                    if (player == null)
                    {
                        player = playerScript;
                    }

                    player.enabled = false;

                    player.GetComponent<Animator>().SetFloat("Movement", 0);

                    trigger.enabled = false;

                    rotating = true;

                    canRotate = false;
                }
            }
        }
    }

    private void Update()
    {
        if (rotating)
        {
            if (opened)
            {

                RotateValve(defaultRotation);
            }
            else
            {

                RotateValve(targetRotation);
            }
        }
    }

    void RotateValve(Quaternion rotation)
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, rotationSpeed * Time.deltaTime);

        if (transform.rotation == rotation)
        {
            Debug.Log("Valve Turned");

            rotating = false;

            player.enabled = true;

            if (opened)
            {
                pipeShiverAnim.Stop();

                waterFillContainer.TopPipeOpened = false;
            }

            else
            {
                pipeShiverAnim.Play();

                waterFillContainer.TopPipeOpened = true;
            }

            trigger.enabled = true;

            opened = !opened;

            canRotate = true;
        }
    }
}