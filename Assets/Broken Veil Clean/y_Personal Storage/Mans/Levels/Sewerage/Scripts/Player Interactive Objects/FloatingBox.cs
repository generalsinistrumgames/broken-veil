﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingBox : MonoBehaviour
{
    [Header("Player")]
    public Player player;

    [Header("Movement")]
    public bool boxIsMovingToTarget;
    [Space]
    public bool boxMovementEnabled;
    [Space]
    public Vector3 movement;

    [Header("Can moove")]
    public Rigidbody boxRb;
    [Space]
    [SerializeField] float xOffset;
    [Space]
    public float speed;

    [Header("Target Position")]
    public Transform targetPosition;

    private void Start()
    {
        xOffset = transform.localScale.x / 2;
    }
    private void Update()
    {
        if (boxMovementEnabled)
        {
            movement.z = Input.GetAxis("Horizontal"); 
        }
    }

    private void FixedUpdate()
    {
        if (boxMovementEnabled)
        {
            MoveBox(-movement.normalized);
        }

        if (boxIsMovingToTarget)
        {
            MoveBoxToTarget();
        }
    }

    void MoveBox(Vector3 direction)
    {
        boxRb.MovePosition(transform.position + direction * (speed * Time.fixedDeltaTime));
    }

    void MoveBoxToTarget()
    {      
        boxRb.MovePosition(Vector3.MoveTowards(transform.position, new Vector3(targetPosition.position.x - xOffset, targetPosition.position.y, targetPosition.position.z), speed * Time.fixedDeltaTime));

        CheckDistance();
    }

    void CheckDistance()
    {
        float distance = (Vector3.Distance(boxRb.transform.position, new Vector3(targetPosition.position.x - xOffset, targetPosition.position.y, targetPosition.position.z)));

        Debug.Log(Mathf.Clamp01(distance));

        if (distance <= 0.01)
        {
            Debug.Log("Distance completed");

            boxIsMovingToTarget = false;

        }
    }
}