﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LukeFall : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] Player player;
    [Space]
    [SerializeField] Rigidbody lukeRb;
    [Space]
    [SerializeField] Animation lukeFallAnim;

    [Header("Check Jumps")]
    [SerializeField] bool canCountJumps;
    [Space]
    [SerializeField] int jumpedAmount;

    private void Start()
    {
        lukeFallAnim = GetComponent<Animation>();

        lukeRb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (canCountJumps)
        {
            if (Input.GetButtonDown("Jump"))
            {
                jumpedAmount++;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (player == null)
            {
                player = playerScript;
            }

            canCountJumps = true;

            if (jumpedAmount >= 2)
            {
                // set falling animation
                //

                //

                player.GetComponent<Animator>().SetFloat("Movement", 0);

                lukeFallAnim.Play();

                lukeRb.isKinematic = false;

                Destroy(this);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        canCountJumps = false;
    }
}