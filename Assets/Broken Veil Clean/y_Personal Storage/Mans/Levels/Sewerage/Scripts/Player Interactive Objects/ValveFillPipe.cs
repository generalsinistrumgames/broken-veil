﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValveFillPipe : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] WaterFillContainer waterFillContainer;
    [Space]
    [SerializeField] Animation pipeShiverAnim;
    [Space]
    [SerializeField] Player player;
    [Space]
    [SerializeField] SphereCollider trigger;

    [Header("Rotate")]
    [SerializeField] bool canRotate = true;
    [Space]
    public bool opened = true;
    [Space]
    [SerializeField] bool rotating;
    [Space]
    [SerializeField] float rotationSpeed;
    [Space]
    [SerializeField] Quaternion targetRotation;
    [Space]
    [SerializeField] Quaternion defaultRotation;

    private void Start()
    {
        defaultRotation = transform.rotation;
    }

    private void OnTriggerStay(Collider other)
    {
        if (canRotate)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                if (other.TryGetComponent<Player>(out var playerScript))
                {
                    if (player == null)
                    {
                        player = playerScript;
                    }

                    player.enabled = false;

                    player.GetComponent<Animator>().SetFloat("Movement", 0);

                    trigger.enabled = false;

                    rotating = true;

                    canRotate = false;
                }
            }
        }
    }
    private void Update()
    {
        if (rotating)
        {
            if (opened)
            {
                RotateValve(targetRotation);
            }
            else
            {
                RotateValve(defaultRotation);
            }
        }
    }

    void RotateValve(Quaternion rotation)
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, rotationSpeed * Time.deltaTime);

        if (transform.rotation == rotation)
        {
            rotating = false;

            player.enabled = true;

            if (opened)
            {
                Debug.Log("Valve Opened");

                pipeShiverAnim.Stop();

                opened = false;

                waterFillContainer.LowerPipeOpened = false;
            }

            else
            {
                Debug.Log("Valve Closed");

                pipeShiverAnim.Play();
   
                opened = true;

                waterFillContainer.LowerPipeOpened = true;
            } 

            trigger.enabled = true;

            canRotate = true;
        }
    }
}