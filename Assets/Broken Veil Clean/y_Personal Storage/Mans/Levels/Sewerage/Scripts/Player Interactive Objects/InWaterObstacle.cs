﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InWaterObstacle : MonoBehaviour
{
    [Header("Force")]
    [SerializeField] Rigidbody rb;
    [Space]
    [SerializeField] Vector3 force;

    [Header("Refs")]
    [SerializeField] FloatingBox floatingBox;
    [Space]
    [SerializeField] Player player;

    [Header("bools")]
    [SerializeField] bool boxPushed;

    [Header("Timer")]
    [SerializeField] float timer;
    [Space]
    [SerializeField] bool timerStarted;
    [Space]
    [SerializeField] float setBackDefaultSpeedTime;
    [Space]
    [SerializeField] float timePassedAfterPush;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (timerStarted)
        {
            timer -= Time.deltaTime;

            if (timer <= 0.0f)
            {
                timePassedAfterPush += 0.1f * Time.deltaTime;

                floatingBox.speed = Mathf.Lerp(floatingBox.speed, 1.1f, timePassedAfterPush);

                if (floatingBox.speed >= 1.0f)
                {
                    timerStarted = false;

                    timer = setBackDefaultSpeedTime;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<FloatingBox>(out var floatingBoxScript))
        {
            if (floatingBox == null)
            {
                floatingBox = floatingBoxScript;

                player = floatingBox.player;
            }

            rb.AddForce(force, ForceMode.Impulse);

            floatingBox.speed = 0.25f;

            timerStarted = true;
        } 
    }
}