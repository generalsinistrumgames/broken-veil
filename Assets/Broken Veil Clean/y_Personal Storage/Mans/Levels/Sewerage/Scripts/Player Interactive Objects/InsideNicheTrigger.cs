﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideNicheTrigger : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] Player player;
    [Space]
    [SerializeField] Flashlight flashlightScript;

    [Header("Timer")]
    [SerializeField] bool timerStarted;
    [Space]
    [SerializeField] float timer;
    [Space]
    [SerializeField] float deathTime;

    private void Update()
    {
        if (timerStarted)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                timerStarted = false;

                Destroy(player.gameObject);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (player == null)
            {
                player = playerScript;

                flashlightScript = player.GetComponentInChildren<Flashlight>(true);

                flashlightScript.gameObject.SetActive(true);
            }

            flashlightScript.canUseFlashlight = true;

            if (flashlightScript.flashlight.enabled)
            {
                timer = deathTime;

                timerStarted = false;
            }

            else
            {
                timerStarted = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        flashlightScript.canUseFlashlight = false;

        timer = deathTime;

        timerStarted = false;
    }
}