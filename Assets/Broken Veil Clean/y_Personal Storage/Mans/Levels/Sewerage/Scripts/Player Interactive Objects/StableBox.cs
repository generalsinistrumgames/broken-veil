﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StableBox : MonoBehaviour
{
    [SerializeField] Vector3 targetPos;
    [Space]
    [SerializeField] float delayTime;
    [Space]
    [SerializeField] float speed;

    public void StartBoxUpCoroutine(float targetPos)
    {
        StartCoroutine(MovementCoroutine(targetPos));
    }

    public IEnumerator MovementCoroutine(float yTargetPos)
    {
        yield return new WaitForSecondsRealtime(delayTime);

        targetPos = new Vector3(transform.position.x, yTargetPos, transform.position.z);

        bool arrived = false;

        while (!arrived)
        {
            Debug.Log("Moving Up");

            transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);

            if (Vector3.Distance(transform.position, targetPos) <= 0.1)
            {
                arrived = true;
            }

            yield return null;
        }

        if (arrived)
        {
            Debug.Log("Box arrived");

            StopAllCoroutines();
        }
    }
}