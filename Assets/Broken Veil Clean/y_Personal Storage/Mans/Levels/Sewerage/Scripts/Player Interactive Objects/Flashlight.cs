﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour
{
    public Light flashlight;
    [Space]
    [SerializeField] KeyCode turnOnButton;
    [Space]
    public bool canUseFlashlight;
    [Space]
    public bool flashlightTurnedOn;

    void Update()
    {
        if (canUseFlashlight)
        {
            if (Input.GetKey(turnOnButton))
            {
                flashlight.enabled = true;
            }

            else if ((Input.GetKeyUp(turnOnButton)))
            {
                flashlight.enabled = false;
            }
        }
    }
}