﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnstableBox : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] Player player;
    [Space]
    [SerializeField] BoxCollider boxTrigger;

    [Header("Sink Manager")]
    [SerializeField] Vector3 targetPos;
    [Space]
    [SerializeField] bool playerOnBox;
    [Space]
    [SerializeField] float sinkSpeed;
    [Space]
    [SerializeField] float sinkDepth;
    
    [Header("Timer")]
    [SerializeField] float timer;
    [Space]
    [SerializeField] float defaultTimerValue;

    private void Update()
    {
        if (playerOnBox)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                StartCoroutine(MovementCoroutine());

                boxTrigger.enabled = false;

                playerOnBox = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (player == null)
            {
                player = playerScript;
            }

            playerOnBox = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            playerOnBox = false;

            timer = defaultTimerValue;
        }
    }

    public IEnumerator MovementCoroutine()
    {
        targetPos = transform.position + (sinkDepth * Vector3.down);

        bool arrived = false;

        while (!arrived)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPos, sinkSpeed * Time.deltaTime);

            if (Vector3.Distance(transform.position, targetPos) <= 0.1f)
            {
                arrived = true;
            }

            yield return null;
        }

        if (arrived)
        {
            StopAllCoroutines();

            enabled = false;
        }
    }
}