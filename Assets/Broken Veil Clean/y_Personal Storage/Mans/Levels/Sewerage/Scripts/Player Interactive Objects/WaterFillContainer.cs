﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterFillContainer : MonoBehaviour
{
    [Header("Pipes")]
    public bool topPipeOpened;
    [Space]
    public bool lowerPipeOpened = true;

    public bool TopPipeOpened
    {
        get
        {
            return topPipeOpened;
        }

        set
        {
            topPipeOpened = value;

            CheckIfValvesSetup();
        }
    }

    public bool LowerPipeOpened
    {
        get
        {
            return lowerPipeOpened;
        }

        set
        {
            lowerPipeOpened = value;

            CheckIfValvesSetup();
        }
    }

    [Space]
    public bool[] lukesClosed = new bool[3];

    [Header("Box Up")]
    [SerializeField] StableBox stableBox;
    [Space]
    [SerializeField] Transform[] upPositions = new Transform[3];
    [Space]
    [SerializeField] float pivotOffset;
    [Space]
    [SerializeField] GameObject midCoverTrigger;

    public void CheckIfValvesSetup()
    {
        Debug.Log("Check pipes");

        if (topPipeOpened && !lowerPipeOpened)
        {
            stableBox.StartBoxUpCoroutine(upPositions[0].position.y - pivotOffset);

            // enable mid trigger
            midCoverTrigger.SetActive(true);
        }
    }

    public void StopFillContainer()
    {
        StopAllCoroutines();
    }

    public void StartFillContainer()
    {
        StartCoroutine(FillContainer());
    }

    public IEnumerator FillContainer()
    {
        Debug.Log("fill container");

        //yield return new WaitForSecondsRealtime(fillTimeSteps[0]);

        // lower cover
        if (lukesClosed[0])
        {
            stableBox.StartBoxUpCoroutine(upPositions[1].position.y - pivotOffset);

            yield return stableBox.MovementCoroutine(upPositions[1].position.y - pivotOffset);

            // for exit from if
            lukesClosed[0] = false;
        }

        // mid cover
        else if (lukesClosed[1])
        {
            stableBox.StartBoxUpCoroutine(upPositions[2].position.y - pivotOffset);

            yield return stableBox.MovementCoroutine(upPositions[2].position.y - pivotOffset);

            // for exit from if
            lukesClosed[1] = false;

            StopAllCoroutines();   
        }  
    }
}