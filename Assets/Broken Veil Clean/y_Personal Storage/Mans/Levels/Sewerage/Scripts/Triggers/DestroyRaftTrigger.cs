﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyRaftTrigger : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] FloatingBox floatingBox;
    [Space]
    [SerializeField] Player player;

    [Header("Delay")]
    [SerializeField] float destroyDelay;


    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<FloatingBox>(out var floatingBoxScript))
        {
            if (floatingBox == null)
            {
                floatingBox = floatingBoxScript;

                player = floatingBox.player;
            }

            floatingBox.boxRb.constraints = RigidbodyConstraints.None;

            floatingBox.boxRb.isKinematic = false;

            Destroy(floatingBox.gameObject, destroyDelay);

            Destroy(player.gameObject, destroyDelay);

            Destroy(gameObject, destroyDelay + 1f);
        }
    }
}
