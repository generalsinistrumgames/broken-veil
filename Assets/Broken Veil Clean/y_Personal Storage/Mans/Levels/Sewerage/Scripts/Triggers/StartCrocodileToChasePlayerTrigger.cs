﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartCrocodileToChasePlayerTrigger : MonoBehaviour
{
    [SerializeField] Crocodile crocodile;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<FloatingBox>(out var floatingBox))
        {
            crocodile.followTarget = floatingBox.gameObject.transform;

            crocodile.chasingPlayer = true;

            Destroy(gameObject);
        }
    }
}