﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NearHookTrigger : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] Player player;
    [Space]
    [SerializeField] FloatingBox floatingBox;

    [Header("Rigidbody")]
    [SerializeField] Rigidbody hookRb;
    [Space]
    [SerializeField] Rigidbody gridRb;
    [Space]
    [SerializeField] BoxCollider trigger;

    [Header("Delay")]
    [SerializeField] float destructionDelay;
    [Space]
    [SerializeField] float delay;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<FloatingBox>(out var floatingBoxScript))
        {
            floatingBox = floatingBoxScript;

            player = floatingBox.player;

            floatingBox.boxMovementEnabled = false;

            player.enabled = false;

            StartCoroutine(DestroyStuff());
        }
    }

    IEnumerator DestroyStuff()
    {
        yield return new WaitForSecondsRealtime(delay);

        hookRb.isKinematic = false;

        gridRb.isKinematic = false;

        trigger.enabled = false;

        // move box forward
        floatingBox.movement.x = -1f;

        floatingBox.boxMovementEnabled = true;

        yield return new WaitForSecondsRealtime(destructionDelay);

        Destroy(hookRb.gameObject);

        Destroy(gridRb.gameObject);

        StopAllCoroutines();

        Destroy(gameObject);
    }
}