﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenCoverTrigger : MonoBehaviour
{
    [Header("Lukes")]
    public Lukes lukes;
    public enum Lukes
    {
        Top,
        Mid
    }

    public bool[] lukesClosed = new bool[2];
    [Space]

    [SerializeField] WaterFillContainer waterFillContainer;
    [Space]
    [SerializeField] Animation anim;
    [Space]
    [SerializeField] bool coverOpened;

    private void OnTriggerStay(Collider other)
    {
        if (!coverOpened)
        {
            if (other.TryGetComponent<Player>(out var playerScript))
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    if (lukesClosed[(int)Lukes.Mid])
                    {
                        anim.Play("LukeCoverOpen");

                        coverOpened = true;

                        // midle luke
                        waterFillContainer.lukesClosed[0] = true;

                        waterFillContainer.StartFillContainer();
                    }

                    else if (lukesClosed[(int)Lukes.Top])
                    {
                        anim.Play("LukeCoverOpen");

                        coverOpened = true;

                        // top luke
                        waterFillContainer.lukesClosed[1] = true;

                        waterFillContainer.StartFillContainer();
                    }
                }
            }
        }  
    }
}