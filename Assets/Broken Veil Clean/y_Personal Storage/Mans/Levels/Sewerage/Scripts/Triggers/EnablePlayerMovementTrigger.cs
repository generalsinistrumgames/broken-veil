﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnablePlayerMovementTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            playerScript.enabled = true;

            // set fall anim
            //playerScript.GetComponent<Animator>().SetFloat("Movement", 0);

            Destroy(gameObject);
        }
    }
}
