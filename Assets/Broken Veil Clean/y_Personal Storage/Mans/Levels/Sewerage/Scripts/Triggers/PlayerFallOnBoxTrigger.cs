﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFallOnBoxTrigger : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] FloatingBox floatingBox;
    [Space]
    [SerializeField] Player player;
    [Space]
    [SerializeField] Stick stick;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<FloatingBox>(out var floatingBoxScript))
        {
            if (floatingBox == null)
            {
                floatingBox = floatingBoxScript;

                player = floatingBox.player;

                stick.rb.isKinematic = false;

                stick.ThrowOnGround();

                player.enabled = true;

                Destroy(gameObject);
            }
        }
    }
}