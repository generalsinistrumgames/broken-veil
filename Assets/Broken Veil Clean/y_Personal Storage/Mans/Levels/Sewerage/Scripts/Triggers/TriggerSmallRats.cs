﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSmallRats : MonoBehaviour
{
    [SerializeField] SmallRat[] smallRats = new SmallRat[3];

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            foreach (var smallRat in smallRats)
            {
                smallRat.player = playerScript;

                smallRat.movingToTarget = true;
            }

            Destroy(gameObject);
        }
    }
}