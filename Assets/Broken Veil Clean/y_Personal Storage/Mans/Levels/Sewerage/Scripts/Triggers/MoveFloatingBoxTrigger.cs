﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFloatingBoxTrigger : MonoBehaviour
{
    [SerializeField] FloatingBox floatingBox;
    [Space]
    [SerializeField] Player player;
    [Space]
    [SerializeField] float speed;
    [Space]
    [SerializeField] Transform targetMovePos;
    [Space]
    [SerializeField] bool canMoveBox;
    [Space]
    [SerializeField] bool boxMoved;

    private void Update()
    {
        if (!boxMoved)
        {
            if (canMoveBox)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    floatingBox.boxIsMovingToTarget = true;

                    //player.enabled = false;

                    //player.GetComponent<Animator>().SetFloat("Movement", 0);

                    boxMoved = true;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (player == null)
            {                
                player = playerScript;

                floatingBox.player = playerScript;
            }

            canMoveBox = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        canMoveBox = false;
    }
}