﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrestWireTrigger : MonoBehaviour
{
    [Header("Stable Box")]
    [SerializeField] StableBox stableBox;
    [Space]
    [SerializeField] float upTargetValue;

    [Header("Button")]
    [SerializeField] MeshRenderer buttonRenderer;
    [Space]
    [SerializeField] Color color;

    [Header("Wire Physics")]
    [Space]
    [SerializeField] Rigidbody wireRb;
    [Space]
    [SerializeField] Vector3 force;

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {             
                wireRb.isKinematic = false;

                wireRb.AddForce(force, ForceMode.Impulse);

                buttonRenderer.material.SetColor("_BaseColor", color);

                stableBox.StartBoxUpCoroutine(upTargetValue);

                Destroy(this);
            }
        }
    }
}