﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBigRat : MonoBehaviour
{
    [SerializeField] BigRat bigRat;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            bigRat.player = playerScript;

            bigRat.movingToTarget = true;

            Destroy(gameObject);
        }
    }
}