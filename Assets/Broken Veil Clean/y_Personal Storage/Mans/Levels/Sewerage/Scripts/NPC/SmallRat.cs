﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SmallRat : MonoBehaviour
{
    [SerializeField] float maxNearDistance;

    public Player player;

    [Header("Rat")]
    [SerializeField] NavMeshAgent ratAgent;

    [Header("Rotation")]
    public float rotateSpeed;

    public bool movingToTarget;

    private void Start()
    {
        ratAgent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        if (movingToTarget)
        {
            MoveToTarget(player.transform);
        }
    }

    public void MoveToTarget(Transform pointToMove)
    {
        //ratAgent.speed = runSpeed;

        ratAgent.SetDestination(pointToMove.position);

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(pointToMove.position - transform.position, Vector3.up), rotateSpeed * Time.deltaTime);

        // Check if we've reached the destination
        //if (!ratAgent.pathPending)
        //{
        //    if (ratAgent.remainingDistance <= ratAgent.stoppingDistance)
        //    {
        //        if (!ratAgent.hasPath || ratAgent.velocity.sqrMagnitude == 0f)
        //        {
        //            testBool = false;
        //        }
        //    }
        //}
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            player.enabled = false;

            player.GetComponent<Animator>().SetFloat("Movement", 0);

            movingToTarget = false;

            ratAgent.isStopped = true;

            Debug.Log("Player is dead");
        }
    }
}