﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crocodile : MonoBehaviour
{
    [SerializeField] Player player;
    [Space]
    public Transform followTarget;
    [Space]
    public bool chasingPlayer;
    [Space]
    public float speed;
    [Space]
    public float rotateSpeed;
    [Space]
    [SerializeField] float xOffset;

    private void Start()
    {
        xOffset = transform.localScale.x / 2;
    }

    private void Update()
    {
        if (chasingPlayer)
        {
            ChasePlayer(followTarget);         
        }    
    }

    void ChasePlayer(Transform target)
    {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.position.x - xOffset, target.position.y, target.position.z), speed * Time.deltaTime);

        //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(target.position - transform.position, Vector3.up), rotateSpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, target.position) <= 0.1f)
        {
            chasingPlayer = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            playerScript.enabled = false;

            player.GetComponent<Animator>().SetFloat("Movement", 0);

            Destroy(playerScript.gameObject);

            Debug.Log("Player is dead");
        }
    }
}