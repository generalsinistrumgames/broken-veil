﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiltTheGurneyTrigger : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] Pathologist pathologist;
    [Space]
    [SerializeField] GameObject gurney;
    [Space]
    [SerializeField] Rigidbody patientRb;
    [Space]
    [SerializeField] Transform pathologistReturnPoint;

    [Header("Anim Durations")]
    [SerializeField] float[] animDutations;
    [Space]
    [SerializeField] Quaternion rotation;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Pathologist>(out var pathologistScript))
        {
            pathologist = pathologistScript;

            pathologist.pathologistAgent.isStopped = true;

            StartCoroutine(StartTiltGurney());
        }
    }

    IEnumerator StartTiltGurney()
    {
        yield return new WaitForSecondsRealtime(animDutations[0]);

        gurney.transform.localRotation = rotation;

        gurney.transform.parent = null;

        patientRb.isKinematic = false;

        yield return new WaitForSecondsRealtime(animDutations[1]);

        gurney.transform.localRotation = Quaternion.Euler(0, -90, gurney.transform.rotation.z - rotation.z);

        pathologist.pathologistAgent.SetDestination(pathologistReturnPoint.position);

        pathologist.pathologistAgent.isStopped = false;

        StopAllCoroutines();

        Destroy(gameObject);
    }
}