﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePathologistToDeadTrigger : MonoBehaviour
{
    [SerializeField] Pathologist pathologist;
    [Space]
    [SerializeField] GameObject deadBody;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            pathologist.pathologistAgent.SetDestination(deadBody.transform.position);

            Destroy(gameObject);
        }
    }
}