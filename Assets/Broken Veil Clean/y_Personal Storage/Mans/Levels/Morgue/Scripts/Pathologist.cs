﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Pathologist : MonoBehaviour
{
    public NavMeshAgent pathologistAgent;
    [Space]
    public bool isMoving;
    [Space]
    [SerializeField] bool moveToBodies;
    [Space]
    [SerializeField] Transform bodiesPoint;

    [Header("Rotate")]
    public GameObject faceTarget;
    [Space]
    [SerializeField] float rotateSpeed;

    private void Start()
    {
        pathologistAgent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (isMoving)
        {
            FaceToTarget();
        }

        if (moveToBodies)
        {
            pathologistAgent.SetDestination(bodiesPoint.position);

            moveToBodies = false;
        }
    }

    public void MoveToTarget(Transform destination)
    {
        pathologistAgent.SetDestination(destination.position);
    }

    public void FaceToTarget()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(faceTarget.transform.position - transform.position, Vector3.up), rotateSpeed * Time.deltaTime);
    }
}
