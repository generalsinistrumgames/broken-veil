using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasementExitTrigger : MonoBehaviour
{
    public AK.Wwise.Event basementExitSoundEvent;
    [Space]
    public GameObject handsLoopSoundObj;

    public void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            basementExitSoundEvent.Post(gameObject);

            handsLoopSoundObj.SetActive(true);

            gameObject.SetActive(false);
        }
    }
}