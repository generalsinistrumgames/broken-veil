using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCutScene : MonoBehaviour
{

    [SerializeField] ScenesLoadDataSO scenesLoadDataSO;
    [Space]
    [SerializeField] Animator _animator;
    [Space]
    [SerializeField] int _lookBackStateHash = Animator.StringToHash("Look Back");

    [Header("Timer")]
    [SerializeField] bool _timerIsRunning;
    [Space]
    [SerializeField] float _timeRemaining;
    [Space]
    [SerializeField] float _minLookBackDelay;
    [Space]
    [SerializeField] float _maxLookBackDelay;

    [Header("Hat Control")]
    [SerializeField] List<GameObject> _activateObjects;
    [Space]
    [SerializeField] List<GameObject> _deactivateObjects;

    void Start()
    {
        _timeRemaining = Random.Range(_minLookBackDelay, _maxLookBackDelay);

        _timerIsRunning = true;

        if (scenesLoadDataSO.currentScenesData.dataHasAssigned)
        {
            PutOnHat();
        }
    }

    void Update()
    {
        if (_timerIsRunning)
        {
            if (_timeRemaining > 0)
            {
                _timeRemaining -= Time.deltaTime;
            }
            else
            {              
                _timeRemaining = Random.Range(_minLookBackDelay, _maxLookBackDelay);

                LookBack();           
            }
        }
    }

    public void LookBack()
    {
        Debug.Log("Time has run out! Turning Back");

        _animator.SetTrigger(_lookBackStateHash);
    }

    public void PutOnHat()
    {
        foreach (var obj in _deactivateObjects)
        {
            obj.SetActive(false);
        }

        foreach (var obj in _activateObjects)
        {
            obj.SetActive(true);
        }  
    }
}