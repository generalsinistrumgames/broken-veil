using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

public class ExposureControl : MonoBehaviour
{
    public bool canControlExposure;
    [Space]
    public float exposureDefaultValue;
    [Space]
    public Volume globalVolume;
    [Space]
    public Exposure exposure;

    [Header("Set Exposure")]
    public AnimationCurve curve;

    void Start()
    {
        Exposure tempExposure;

        Volume[] volumes;

        volumes = FindObjectsOfType<Volume>();

        foreach (var volume in volumes)
        {
            if (volume.isGlobal)
            {
                if (volume.profile.TryGet<Exposure>(out tempExposure))
                {
                    globalVolume = volume;

                    exposure = tempExposure;
                }
            }
        }

        exposureDefaultValue = exposure.fixedExposure.value;
    }

    void LateUpdate()
    {
        if (canControlExposure)
        {
            exposure.fixedExposure.value = exposureDefaultValue;
        } 
    }
}