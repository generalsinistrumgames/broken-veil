using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BlendTurnAnims : MonoBehaviour
{
    public Transform from;
    [Space]
    public Transform target;
    [Space]
    public Animator animator;
    [Space]
    // position reached check
    public float posThreshold = 0.001f;
    [Space]
    // rotation reached check
    public float rotThreshold = 0.99f;
    [Space]
    // rotation dot
    public float dotRight;
    [Space]
    public float dotForward;
    [Space]
    public float rotationSpeed;
    [Space]
    public float offset;
    [Space]
    public bool checkRot;

    private void Start()
    {
        if (from == null)
        {
            from = GetComponent<Transform>();
        }

        if (animator == null)
        {
            animator = GetComponent<Animator>();
        }
    }

    void Update()
    {
        //OldCheckRot();

        if (checkRot)
        {
            PlayerAnimationTurns();
        }
    }

    private void OldCheckRot()
    {
        if (checkRot)
        {
            //Vector3 direction = (new Vector3(target.position.x, 0, target.position.z) - new Vector3(transform.position.x, 0, transform.position.z)).normalized;

            Vector3 direction = (target.position - transform.position).normalized;

            var offsettedDirection = (direction * offset);

            //float strength = Mathf.Min(rotationSpeed * Time.deltaTime, 1);

            //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(direction), strength);

            //draw a ray pointing to our target
            Debug.DrawRay(transform.position, offsettedDirection, Color.cyan, 0.1f);

            dotRight = Vector3.Dot(transform.right, offsettedDirection);

            dotForward = Vector3.Dot(transform.forward, offsettedDirection);

            animator.SetFloat("Rotation Right", dotRight);

            //if (dotRight > -0.1f && dotRight < 0.1f && dotForward > 0)  // dotforward is checks if facing target
            //{
            //    checkRot = false;
            //}

            //Debug.Log("Rotation Dot Is " + dot);

            //if (dotForward >= rotThreshold)
            //{
            //    Debug.Log("Rotation Facing");
            //}
            //else if (dotRight >= 0.5f && dotRight < rotThreshold)
            //{
            //    Debug.Log("Rotation Half Facing right");
            //}
            //else if (dotRight <= -rotThreshold && dotRight <= 0.5f)
            //{
            //    Debug.Log("Rotatio Facing left");
            //}
            //else if (dotRight <= -0.5f)
            //{
            //    Debug.Log("Rotation Half Facing left");
            //}
        }
    }

    public void PlayerAnimationTurns()
    {
        Vector3 direction = (new Vector3(target.position.x, 0, target.position.z) - new Vector3(from.position.x, 0, from.position.z)).normalized;

        //Vector3 direction = (target.position - from.position).normalized;

        //var offsettedDirection = (direction * 0.3f); // removed

        float strength = Mathf.Min(rotationSpeed * Time.deltaTime, 1);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(direction), strength);

        //draw a ray pointing to our target
        Debug.DrawRay(from.position, direction, Color.cyan);

        dotRight = Vector3.Dot(target.right, direction);

        //dotForward = Vector3.Dot(target.forward, direction);

        //animator.SetFloat("Idle Turn 180", dotForward);

        dotForward = Vector3.Dot(from.forward, direction);  // check facing towards item

        if (dotForward >= rotThreshold)
        {
            Debug.Log("Rotation Facing");

            animator.SetFloat("Idle Turn", 0);

            checkRot = false;
        }
        //else
        //{
        //    Debug.Log("Rotating animator");

        //    animator.SetFloat("Idle Turn", dotRight);

        //    //if (dotForward >= rotThreshold)
        //    //{
        //    //    Debug.Log("Rotation Facing");
        //    //}
        //    //else if (dotRight >= 0.5f && dotRight < rotThreshold)
        //    //{
        //    //    Debug.Log("Rotation Half Facing right");
        //    //}
        //    //else if (dotRight <= -rotThreshold && dotRight <= 0.5f)
        //    //{
        //    //    Debug.Log("Rotatio Facing left");
        //    //}
        //    //else if (dotRight <= -0.5f)
        //    //{
        //    //    Debug.Log("Rotation Half Facing left");
        //    //}
        //}

        //if (dotRight > -0.1f && dotRight < 0.1f && dotForward > 0)  // dotforward is checks if facing target
        //{
        //    Debug.Log("Rotating animator ends");

        //    return true;
        //}
        //else
        //{
        //    return false;
        //}
    }
}