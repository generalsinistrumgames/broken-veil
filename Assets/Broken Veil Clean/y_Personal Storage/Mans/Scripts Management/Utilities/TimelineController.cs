﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[RequireComponent(typeof (PlayableDirector))]
public class TimelineController : MonoBehaviour
{
    public PlayableDirector timelineDirector;

    private void Start()
    {
        timelineDirector = GetComponent<PlayableDirector>();
    }

    public void StartTimeline()
    {
        timelineDirector.Play();
    }

    public void ResumeTimeline()
    {
        timelineDirector.Resume();
    }

    public void StopTimeline()
    {
        timelineDirector.Stop();
    }
}