using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public static class PlayableDirectorUtility
{
    public static void DynamicAnimatorBinding(PlayableDirector playableDirector, Animator animator, string trackName)
    {
        foreach (var playableAssetOutput in playableDirector.playableAsset.outputs)
        {
            if (playableAssetOutput.streamName.Equals(trackName))
            {
                playableDirector.SetGenericBinding(playableAssetOutput.sourceObject, animator);
            }
        }
    }
}