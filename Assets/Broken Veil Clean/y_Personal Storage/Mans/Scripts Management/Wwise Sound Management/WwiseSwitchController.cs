﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class WwiseSwitchController : MonoBehaviour
{
    [Header("Player")]
    [SerializeField] GameObject player;

    [Header("Switches")]
    public AK.Wwise.Switch setSwitchMaterialType;
    [Space]
    public AK.Wwise.Switch unssetSwitchMaterialType;

    [Header("Debug")]
    public bool debugEnabled;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (player == null)
            {
                player = playerScript.gameObject;
            }

            setSwitchMaterialType.SetValue(player.gameObject);

            if (debugEnabled)
            {
                Debug.Log(setSwitchMaterialType.ToString() + " switch state setted");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            unssetSwitchMaterialType.SetValue(player.gameObject);

            if (debugEnabled)
            {
                Debug.Log(unssetSwitchMaterialType.ToString() + " switch state setted");
            }
        }
    }
}