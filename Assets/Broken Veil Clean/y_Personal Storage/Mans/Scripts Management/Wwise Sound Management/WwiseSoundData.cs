﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//[System.Serializable]
//public class WwiseSoundData
//{
//    [Header("Call Event")]
//    public bool playEventOnce;  // set loop audio once on trigger enter

//    public bool PlayEventOnce
//    {
//        get
//        {
//            return playEventOnce;
//        }

//        set { playEventLocked = playEventOnce; }
//    }

//    [Space]
//    public bool playEventLocked;

//    [Space]
//    public string EventPlayName;
//    [Space]
//    public bool useStopEvent; // stop event on trigger exit or not
//    [Space]
//    public string EventStopName;

//    [Header("Listener Control")]
//    public bool useDefaultListener;
//    [Space]
//    public AkGameObj akGameObj;
//    [Space]
//    public AkAudioListener akAudioListener;

//    [Header("Player Control")]
//    public GameObject player;
//    [Space]
//    public bool playerInCollider;

//    [Header("Debug Control")]
//    public bool debugEnabled;

//    private void Start()
//    {
//        akGameObj = GetComponent<AkGameObj>();
//    }

//    private void OnTriggerEnter(Collider other)
//    {
//        if (other.TryGetComponent<Player>(out var playerscript) && !playerInCollider)
//        {
//            playerInCollider = true;

//            if (player == null)
//            {
//                player = playerscript.gameObject;

//                akGameObj = GetComponent<AkGameObj>();

//                SetDefaultListener();
//            }

//            if (!playEventLocked)
//            {
//                if (playEventOnce)
//                {
//                    if (EventPlayName != null)
//                    {
//                        AkSoundEngine.PostEvent(EventPlayName, gameObject);
//                    }

//                    playEventLocked = true;
//                }

//                else
//                {
//                    if (EventPlayName != null)
//                    {
//                        AkSoundEngine.PostEvent(EventPlayName, gameObject);
//                    }
//                }

//                if (debugEnabled)
//                {
//                    Debug.Log(EventPlayName + " playing");

//                    Debug.Log(EventPlayName + " is locked? " + playEventLocked);
//                }
//            }
//        }
//    }

//    private void OnTriggerStay(Collider other)
//    {
//        if (other.TryGetComponent<Player>(out var playerscript) && !playerInCollider)
//        {
//            playerInCollider = true;

//            if (!playEventLocked)
//            {
//                if (playEventOnce)
//                {
//                    if (EventPlayName != null)
//                    {
//                        AkSoundEngine.PostEvent(EventPlayName, gameObject);

//                        playEventLocked = true;
//                    }
//                }

//                else
//                {
//                    if (EventPlayName != null)
//                    {
//                        AkSoundEngine.PostEvent(EventPlayName, gameObject);
//                    }
//                }

//                if (debugEnabled)
//                {
//                    Debug.Log(EventPlayName + " playing");

//                    Debug.Log(EventPlayName + " is locked? " + playEventLocked);
//                }
//            }
//        }
//    }

//    private void OnTriggerExit(Collider other)
//    {
//        if (other.TryGetComponent<Player>(out var playerscript) && playerInCollider)
//        {
//            playerInCollider = false;

//            if (useStopEvent)
//            {
//                if (EventStopName != null)
//                {
//                    AkSoundEngine.PostEvent(EventStopName, gameObject);
//                }

//                if (debugEnabled)
//                {
//                    Debug.Log(EventStopName + " stopped");

//                    Debug.Log(EventPlayName + " is locked? " + playEventLocked);
//                }
//            }
//        }
//    }

//    public void SetDefaultListener()
//    {
//        if (!useDefaultListener)
//        {
//            if (akAudioListener == null)
//            {
//                akAudioListener = player.GetComponent<AkAudioListener>();
//            }

//            akGameObj.AddListener(akAudioListener);
//        }
//    }

//    public bool IsPlayOnceEvent()
//    {
//        if (playEventOnce)
//        {
//            playEventLocked = true;

//            return true;
//        }

//        else
//        {
//            playEventLocked = false;

//            return false;
//        }
//    }
//}