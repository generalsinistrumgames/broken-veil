using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObjByTime : MonoBehaviour
{
    public List<GameObject> activateObjects;
    [Space]
    public float activationDelay;

    private void Start()
    {
        Invoke("ActivateByTime", activationDelay);
    }

    public void ActivateByTime()
    {
        foreach (var obj in activateObjects)
        {
            obj.SetActive(true);
        }
    }
}