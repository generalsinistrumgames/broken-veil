﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardSounds : MonoBehaviour
{
    public string idleEventName;
    [Space]
    public string footstepEventName;
    [Space]
    public string broomSoundEventName = "Play_sweep_nails";

    public void GuardIdleSound()
    {
        AkSoundEngine.PostEvent(idleEventName, gameObject);
    }

    public void GuardFootstepSound()
    {
        AkSoundEngine.PostEvent(footstepEventName, gameObject);
    }

    public void PlayGuardBroomSound()
    {
        AkSoundEngine.PostEvent(broomSoundEventName, gameObject);
    }
}