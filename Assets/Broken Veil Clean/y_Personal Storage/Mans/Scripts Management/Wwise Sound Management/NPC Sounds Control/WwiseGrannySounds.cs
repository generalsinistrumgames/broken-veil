﻿using UnityEngine;

public class WwiseGrannySounds : MonoBehaviour
{
    public string footstepEventName;
    [Space]
    public string playerTriggerEventName;

    public void GrannyFootstepSound()
    {
        AkSoundEngine.PostEvent(footstepEventName, gameObject);
    }

    public void GrannyTriggerPlayerSound()
    {
        AkSoundEngine.PostEvent(playerTriggerEventName, gameObject);
    }
}