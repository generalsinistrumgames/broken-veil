﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundsControl : MonoBehaviour
{
    [Header("Sound Events")]
    public AK.Wwise.Event footstepEvent;
    [Space]
    public AK.Wwise.Event landEvent;
    [Space]
    public AK.Wwise.Event climbEvent;
    [Space]
    public AK.Wwise.Event slideEvent;

    [Header("Switches")]
    public AK.Wwise.Switch runSwitch;
    [Space]
    public AK.Wwise.Switch sprintSwitch;
    [Space]
    public AK.Wwise.Switch sneakSwitch;

    public void PlayLandSound()
    {
        landEvent.Post(gameObject);
    }

    public void PlayFootstepSneakSound()
    {
        //AkSoundEngine.SetSwitch(switchGroup, setSneakSwitchName, gameObject);

        if (Input.GetAxis("Horizontal") != 0f)
        {
            sneakSwitch.SetValue(gameObject);

            footstepEvent.Post(gameObject);
        }
    }

    public void PlayFootstepRunSound()
    {
        //AkSoundEngine.SetSwitch(switchGroup, setRunSwitchName, gameObject);
        runSwitch.SetValue(gameObject);

        footstepEvent.Post(gameObject);
    }

    public void PlayFootstepSprintSound()
    {
        //AkSoundEngine.SetSwitch(switchGroup, setSprintSwitchName, gameObject);

        //sprintSwitch.SetValue(gameObject);

        //footstepEvent.Post(gameObject);
    }

    public void PlaySlideSound()
    {
        slideEvent.Post(gameObject);

        //AkSoundEngine.SetSwitch(switchGroup, setSprintSwitchName, gameObject);

        //AkSoundEngine.PostEvent(footstepEventName, gameObject);
    }

    public void PlayClimbSound()
    {
        climbEvent.Post(gameObject);
    }
}