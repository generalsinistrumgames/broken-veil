﻿using UnityEngine;

[System.Serializable]
public class WwiseAudioVolumeControl
{
    [Header("Audio Volume Control")]
    [Range(0, 100)] public float masterVolume;
    [Space]
    [Range(0, 100)] public float ambientVolume;
    [Space]
    [Range(0, 100)] public float sfxVolume;

    [Header("Wwise RTPC Value Names")]
    public string masterVolumeRTPC;
    public string ambientVolumeRTPC;
    public string sfxVolumeRTPC;

    [Header("Enable Control")]
    public bool canControlVolume;
}