﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WwiseAudioTrigger : MonoBehaviour
{
    [Header("Call Event")]
    public bool playEventOnce;  // set loop audio once on trigger enter

    public bool PlayEventOnce
    {
        get
        {
            return playEventOnce;
        }

        set { playEventLocked = playEventOnce; }
    }

    [Space]
    public bool playEventLocked;

    [Space]
    public string EventPlayName;
    [Space]
    public bool useStopEvent = true; // stop event on trigger exit or not
    [Space]
    public string EventStopName;

    [Space]
    public bool playerInCollider;

    [Header("Debug Control")]
    public bool debugEnabled;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerscript) && !playerInCollider)
        {
            playerInCollider = true;

            if (!playEventLocked)
            {
                if (playEventOnce)
                {
                    if (EventPlayName != null)
                    {
                        AkSoundEngine.PostEvent(EventPlayName, gameObject);
                    }

                    playEventLocked = true;
                }

                else
                {
                    if (EventPlayName != null)
                    {
                        AkSoundEngine.PostEvent(EventPlayName, gameObject);
                    }
                }

                if (debugEnabled)
                {
                    Debug.Log(EventPlayName + " playing");

                    Debug.Log(EventPlayName + " is locked? " + playEventLocked);
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerscript) && !playerInCollider)
        {
            playerInCollider = true;

            if (!playEventLocked)
            {
                if (playEventOnce)
                {
                    if (EventPlayName != null)
                    {
                        AkSoundEngine.PostEvent(EventPlayName, gameObject);

                        playEventLocked = true;
                    }
                }

                else
                {
                    if (EventPlayName != null)
                    {
                        AkSoundEngine.PostEvent(EventPlayName, gameObject);
                    }
                }

                if (debugEnabled)
                {
                    Debug.Log(EventPlayName + " playing");

                    Debug.Log(EventPlayName + " is locked? " + playEventLocked);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerscript) && playerInCollider)
        {
            playerInCollider = false;

            if (useStopEvent)
            {
                if (EventStopName != null)
                {
                    AkSoundEngine.PostEvent(EventStopName, gameObject);
                }

                if (debugEnabled)
                {
                    Debug.Log(EventStopName + " stopped");

                    Debug.Log(EventPlayName + " is locked? " + playEventLocked);
                }
            }
        }
    }

    public bool IsPlayOnceEvent()
    {
        if (playEventOnce)
        {
            playEventLocked = true;

            return true;
        }

        else
        {
            playEventLocked = false;

            return false;
        }
    }
}