using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WwiseOnTriggerSound : MonoBehaviour
{
    [Header("Call Event")]
    public string eventPlayName;
    [Space]
    public GameObject triggerSoundObject;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == triggerSoundObject)
        {
            AkSoundEngine.PostEvent(eventPlayName, gameObject);
        }
    }
}