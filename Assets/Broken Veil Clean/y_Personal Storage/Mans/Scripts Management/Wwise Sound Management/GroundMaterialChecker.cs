using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundMaterialChecker : MonoBehaviour
{
    public GameObject player;
    [Space]
    public AK.Wwise.Switch currentSwitchMaterial;
    [Space]
    public bool debugEnabled;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<SoundMaterial>(out var soundMaterial))
        {
            if (currentSwitchMaterial != soundMaterial.materialType)
            {
                currentSwitchMaterial = soundMaterial.materialType;

                currentSwitchMaterial.SetValue(player);

                Debug.Log("Switch " + currentSwitchMaterial.ToString() + " set");
            }
            else
            {
                Debug.Log("Switch " + currentSwitchMaterial.ToString() + " setted already");
            }
        }
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.TryGetComponent<SoundMaterial>(out var soundMaterial))
    //    {
    //        if (debugEnabled)
    //        {
    //            foreach (ContactPoint contact in collision.contacts)
    //            {
    //                Debug.DrawRay(contact.point, contact.normal, Color.yellow, 0.1f);
    //            }
    //        }
          
    //        Debug.Log("Switch " + currentSwitchMaterial.ToString() + " setted already");

    //        //if (collision.relativeVelocity.magnitude > 2)

    //        if (currentSwitchMaterial != soundMaterial.materialType)
    //        {
    //            currentSwitchMaterial = soundMaterial.materialType;

    //            currentSwitchMaterial.SetValue(player);

    //            Debug.Log("Switch " + currentSwitchMaterial.ToString() + " set");
    //        }
    //        else
    //        {
    //            Debug.Log("Switch " + currentSwitchMaterial.ToString() + " setted already");
    //        }
    //    }
    //}
}