using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHitCollisionSound : MonoBehaviour
{
    public enum Type
    {
        ground,
        ashaplt,
        concrete,
        tile,
        wood,
        ceramic,
        guts,
        metal,
        plastic
    }

    public Type objectMaterialType;
}