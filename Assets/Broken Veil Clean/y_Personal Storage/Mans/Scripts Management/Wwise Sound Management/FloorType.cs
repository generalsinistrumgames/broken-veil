using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorType : MonoBehaviour
{
    public enum Type
    {
        ashaplt,
        concrete,
        tile,
        wood,
        sand,
        ground,
        guts,
        ceramic,
        metal,
        plastic
    }

    public Type floorType;

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.TryGetComponent<PlayerFootstepsSoundSetter>(out var playerFootstepsSoundSetter))
    //    {
    //        playerFootstepsSoundSetter.SetSwitch(floorType.ToString());

    //        Debug.LogError("Switching");
    //    }
    //}
}