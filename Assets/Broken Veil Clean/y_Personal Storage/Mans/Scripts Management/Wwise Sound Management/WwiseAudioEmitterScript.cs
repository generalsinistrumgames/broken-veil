﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WwiseAudioEmitterScript : MonoBehaviour
{
    [Header("Call Event")]
    public bool playEventOnce;  // set loop audio once on trigger enter
    [Space]
    public string EventPlayName;
    [Space]
    public bool useStopEvent = true; // stop event on trigger exit or not
    [Space]
    public string EventStopName;

    [Header("Listener Control")]
    public bool useDefaultListener;
    [Space]
    public AkGameObj akGameObj;
    [Space]
    public AkAudioListener akAudioListener;

    [Header("Player Control")]
    public GameObject player;
    [Space]
    public bool playerInCollider;

    [Header("Debug Control")]
    public bool debugEnabled;
    [Space]
    public bool eventCalled;

    private void Start()
    {
        akGameObj = GetComponent<AkGameObj>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerscript) && !playerInCollider)
        {
            playerInCollider = true;

            if (player == null)
            {
                player = playerscript.gameObject;

                //SetDefaultListener();
            }

            CallEvent();
        }
    }

    //private void OnTriggerStay(Collider other)
    //{
    //    if (other.TryGetComponent<Player>(out var playerscript) && !playerInCollider)
    //    {
    //        playerInCollider = true;

    //        CallEvent();

    //        if (debugEnabled)
    //        {
    //            Debug.Log(EventPlayName + " playing");

    //            Debug.Log(EventPlayName + " is locked? " + playEventOnce);
    //        }
    //    }
    //}

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerscript) && playerInCollider)
        {
            playerInCollider = false;

            if (useStopEvent)
            {
                if (EventStopName != null)
                {
                    AkSoundEngine.PostEvent(EventStopName, gameObject);
                }

                if (debugEnabled)
                {
                    Debug.Log(EventStopName + " stopped");

                    Debug.Log(EventPlayName + " is locked? " + playEventOnce);
                }
            }
        }
    }

    public void SetDefaultListener()
    {
        if (useDefaultListener && akAudioListener == null)
        {
            akAudioListener = Camera.main.GetComponent<AkAudioListener>();
        }
        else
        {
            akAudioListener = player.GetComponent<AkAudioListener>();
        }

        //akGameObj.AddListener(akAudioListener);
    }

    public bool IsPlayOnceEvent()
    {
        if (playEventOnce)
        {
            return true;
        }

        else
        {
            return false;
        }
    }

    public void CallEvent()
    {
        if (playEventOnce && eventCalled)
        {
            return;
        }
        else
        {
            if (EventPlayName != null)
            {
                eventCalled = true;

                AkSoundEngine.PostEvent(EventPlayName, gameObject);

                if (debugEnabled)
                {
                    Debug.Log(EventPlayName + " playing");

                    Debug.Log(EventPlayName + " is locked? " + playEventOnce);
                }
            }
        }
    }

    public void PlaySound()
    {
        AkSoundEngine.PostEvent(EventPlayName, gameObject);
    }

    public void StopSound()
    {
        AkSoundEngine.PostEvent(EventStopName, gameObject);
    }
}