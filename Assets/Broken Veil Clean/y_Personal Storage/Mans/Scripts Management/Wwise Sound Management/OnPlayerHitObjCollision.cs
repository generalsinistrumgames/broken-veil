using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnPlayerHitObjCollision : MonoBehaviour
{
    public Rigidbody rb;
    [Space]
    public float collisionMagnitude = 1f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<Player>(out var player))
        {
            if (collision.relativeVelocity.magnitude > collisionMagnitude)
            {
                rb.isKinematic = false;
            }
        }
    }
}