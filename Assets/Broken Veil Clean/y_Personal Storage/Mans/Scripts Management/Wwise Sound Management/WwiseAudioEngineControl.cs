using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WwiseAudioEngineControl : MonoBehaviour
{
    public void PauseWwise()
    {
        AkSoundEngine.Suspend();
    }
    public void ResumeWwise()
    {
        AkSoundEngine.WakeupFromSuspend();
    } 
}