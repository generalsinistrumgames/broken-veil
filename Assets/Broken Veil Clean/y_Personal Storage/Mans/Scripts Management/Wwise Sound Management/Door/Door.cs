﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] bool _playerInCollider;
    [Space]
    [SerializeField] HingeJoint _hingeJoint;
    [Space]
    [SerializeField] JointSpring _jointSpring;

    [Header("Hinge Control")]
    [SerializeField] float _doorStrength;
    [Space]
    [SerializeField] float _pushStrength;
    [Space]
    [SerializeField] float _doorMinSpring;
    [Space]
    [SerializeField] float _defaultDoorStrenght;

    private void Start()
    {
        _hingeJoint = GetComponent<HingeJoint>();

        _jointSpring = _hingeJoint.spring;

        _defaultDoorStrenght = _doorStrength;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.A) && _playerInCollider)
        {
            if (_hingeJoint.useSpring)
            {
                if (_doorStrength >= _doorMinSpring)
                {
                    _doorStrength -= Time.deltaTime * _pushStrength;

                    _jointSpring.spring = _doorStrength;

                    _hingeJoint.spring = _jointSpring;
                }
            }
        }
        else if (Input.GetKeyUp(KeyCode.A) && _playerInCollider)
        {
            _doorStrength = _defaultDoorStrenght;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            _hingeJoint.useSpring = true;

            _playerInCollider = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            _doorStrength = _defaultDoorStrenght;

            _playerInCollider = false;
        }
    }
}