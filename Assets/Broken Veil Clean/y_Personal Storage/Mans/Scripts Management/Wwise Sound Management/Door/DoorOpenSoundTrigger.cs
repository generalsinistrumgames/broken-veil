using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpenSoundTrigger : MonoBehaviour
{
    public string openEventPlayName;
    [Space]
    public Rigidbody doorRb;
    [Space]
    public float triggerSoundMagnitude = 0.1f;

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent<Door>(out var doorScript))
        {
            if (doorRb.velocity.sqrMagnitude > triggerSoundMagnitude)
            {
                AkSoundEngine.PostEvent(openEventPlayName, gameObject);
            }
        }
    }
}