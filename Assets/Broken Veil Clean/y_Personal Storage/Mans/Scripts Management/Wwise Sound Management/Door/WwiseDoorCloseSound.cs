using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WwiseDoorCloseSound : MonoBehaviour
{
    [Header("Sound Event")]
    public string closeEventName;
    [Space]
    [SerializeField] bool _doorClosed;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Door>(out var doorScript) && !_doorClosed)
        {
            //AkSoundEngine.PostEvent(closeEventName, gameObject);

            Debug.Log("Door is closed");

            _doorClosed = true;
        }
    }
}