using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCloseSoundTrigger : MonoBehaviour
{
    public string closeEventPlayName;
    [Space]
    public Rigidbody doorRb;
    [Space]
    public float triggerSoundMagnitude = 0.1f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<Door>(out var doorScript))
        {
            if (doorRb.velocity.sqrMagnitude > triggerSoundMagnitude)
            {
                AkSoundEngine.PostEvent(closeEventPlayName, gameObject);
            }        
        }
    }
}