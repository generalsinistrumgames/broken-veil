﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WwiseDoorSoundsControl : MonoBehaviour
{
    [Header("Sound Events")]
    public string openEventPlayName;
    [Space]
    public string closeEventPlayName;

    [Header("Play Control")]
    [SerializeField] bool _doorClosed = true;
    [Space]
    public Rigidbody doorRb;
    [Space]
    public float hitMagnitude;

    public Collider openCollider;

    public Collider closeCollider;


    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Door>(out var doorScript) /*&& _doorClosed*/)
        {
            if (doorRb == null)
            {
                doorRb = doorScript.GetComponent<Rigidbody>();
            }

            _doorClosed = false;

            AkSoundEngine.PostEvent(openEventPlayName, gameObject);

            Debug.Log("Door is opened");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<Door>(out var doorScript) /*&& !_doorClosed*/)
        {
            if (doorRb.velocity.magnitude > hitMagnitude)
            {
                _doorClosed = true;

                AkSoundEngine.PostEvent(closeEventPlayName, gameObject);

                Debug.Log("Door is closed");
            }
        }
    }
}