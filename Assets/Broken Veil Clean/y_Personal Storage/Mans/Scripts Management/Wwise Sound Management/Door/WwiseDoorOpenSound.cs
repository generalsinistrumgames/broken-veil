﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WwiseDoorOpenSound : MonoBehaviour
{
    [Header("Sound Event")]
    public string openEventName;
    [Space]
    [SerializeField] bool _doorClosed;

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Door>(out var doorScript) && _doorClosed)
        {
            //AkSoundEngine.PostEvent(openEventName, gameObject);

            //uint eventId = AkSoundEngine.GetIDFromString(openEventName);

            //uint playingId = AkSoundEngine.GetEventIDFromPlayingID(eventId);

            //if (eventId == playingId)
            //{
            //    Debug.Log("Event is already playing");
            //}
            //else
            //{
            //    AkSoundEngine.PostEvent(openEventName, gameObject);
            //}

            Debug.Log("Door is opened");

            _doorClosed = false;
        }
    }

    static uint[] playingIds = new uint[50];

    //public static bool IsEventPlayingOnGameObject(string eventName, GameObject go)
    //{
    //    uint testEventId = AkSoundEngine.GetIDFromString(eventName);

    //    uint count = (uint)playingIds.Length;

    //    AKRESULT result = AkSoundEngine.GetPlayingIDsFromGameObject(go, ref count, playingIds);

    //    for (int i = 0; i < count; i++)
    //    {
    //        uint playingId = playingIds[i];
    //        uint eventId = AkSoundEngine.GetEventIDFromPlayingID(playingId);

    //        if (eventId == testEventId)
    //            return true;
    //    }

    //    return false;
    //}
}