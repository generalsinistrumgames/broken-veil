using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WwiseOnCollisionSound : MonoBehaviour
{
    [Header("Call Event")]
    public string eventPlayName;
    [Space]
    public float collisionMagnitude = 1f;
    [Space]
    public UnityEvent OnObjHit;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > collisionMagnitude)
        {
            if (collision.gameObject.TryGetComponent<Player>(out var player))
            {
                AkSoundEngine.PostEvent(eventPlayName, gameObject);

                OnObjHit?.Invoke();
            }
        }
    }
}