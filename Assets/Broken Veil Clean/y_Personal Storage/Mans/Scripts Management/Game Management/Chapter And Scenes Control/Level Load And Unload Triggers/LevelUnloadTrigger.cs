﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUnloadTrigger : MonoBehaviour
{
    [Header("Unload Scene Info")]
    [SerializeField] string unloadSceneName;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            SceneLoadManager.Instance.UnloadLevelInGame(unloadSceneName);
        }
    }
}