﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoadTrigger : MonoBehaviour
{
    [Header("Load Scene Info")]
    [SerializeField] string loadSceneName;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            SceneLoadManager.Instance.LoadSceneAsyncAdditive(loadSceneName);
        }
    }
}