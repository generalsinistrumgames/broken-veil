using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetScenesLoadData : MonoBehaviour
{
    [Header("Set To Load Data")]
    [SerializeField] ChapterDataSO _chapterDataSO;
    [Space]
    [SerializeField] SavePointSO _savePointSO;

    public void SetCurrentScenesLoadData(ScenesLoadDataSO scenesLoadDataSO)
    {
        scenesLoadDataSO.currentScenesData.currentChapterIndex = _chapterDataSO.chapterIndex;

        scenesLoadDataSO.currentScenesData.currentSavePointIndex = _savePointSO.savePointIndex;
    }
}