﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SetSavePointTrigger : MonoBehaviour
{
    [SerializeField] SavePointSO _savePoint;
    [Space]
    [SerializeField] ScenesLoadDataSO _scenesLoadDataSO;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            SetSavePoint();
        }
    }

    public void SetSavePoint()
    {
        if (_scenesLoadDataSO.currentSavePoint == _savePoint)
        {
            Debug.Log("This Save Point Already Assigned");
        }
        else
        {
            Debug.Log("This Save Point Assigned To Curent Save Point");

            _scenesLoadDataSO.currentScenesData.currentSavePointIndex = _savePoint.savePointIndex;

            _scenesLoadDataSO.currentSavePoint = _savePoint;

            SaveAndLoadManager.Instance.Save(_scenesLoadDataSO.saveFileName, _scenesLoadDataSO.currentScenesData);
        }  
    }
}