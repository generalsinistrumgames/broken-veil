using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetChapterToLoad : MonoBehaviour
{
    [SerializeField] ChapterDataSO _chapter;
    [Space]
    [SerializeField] SavePointSO _savePoint;
    [Space]
    [SerializeField] ScenesLoadDataSO _scenesLoadDataSO;

    public void LoadChapter()
    {
        _scenesLoadDataSO.currentChapter = _chapter;

        _scenesLoadDataSO.currentSavePoint = _savePoint;

        _scenesLoadDataSO.currentScenesData.currentChapterIndex = _chapter.chapterIndex;

        _scenesLoadDataSO.currentScenesData.currentSavePointIndex = _savePoint.savePointIndex;
   
        SaveAndLoadManager.Instance.Save(_scenesLoadDataSO.saveFileName, _scenesLoadDataSO.currentScenesData);
    }
}