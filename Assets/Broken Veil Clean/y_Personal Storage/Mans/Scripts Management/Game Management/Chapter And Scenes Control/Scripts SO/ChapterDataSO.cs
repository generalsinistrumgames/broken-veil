﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scene Management/Chapter Data", fileName = "Chapter Data")]
public class ChapterDataSO : ScriptableObject
{
    [Header("Chapter Info")]
    [ContextMenuItem("Reset Chapter Scenes To Default Load Presset", "ResetSceneToDefaultLoad")]
    public string chapterName;
    [Space]
    public int chapterIndex;
    [Space]
    public bool isCurrentChapter;

    [Space]
    public SceneDataSO chapterMainScene;

    [Header("Chapter Sub Scenes")]
    public List<SceneDataSO> sceneDatas;
    [Space]
    public List<SceneDataSO> newGameLoadScenes;

    [Space]
    public List<SavePointSO> chapterSavePoints;

    public void ResetScenesToDefaultLoad()
    {
        foreach (var scene in sceneDatas)
        {
            //scene.loadOnStart = false;
        }

        for (int i = 0; i < newGameLoadScenes.Count; i++)
        {
            foreach (var scene in sceneDatas)
            {
                if (scene.sceneIndex == newGameLoadScenes[i].sceneIndex)
                {
                    Debug.Log("Found " + scene.sceneName);

                    //scene.loadOnStart = true;
                }       
            }
        }

        Debug.Log("Reset");
    }
}