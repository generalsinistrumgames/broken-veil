using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scene Management/Scene Info", fileName = "Scene Info")]
public class SceneSO : ScriptableObject
{
    [Header("Scene Info")]
    public string sceneName;
    [Space]
    [Range(0, 100)] public int sceneIndex;
}