using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Save And Load/Save Point", fileName = "Save Point Info")]
public class SavePointSO : ScriptableObject
{
    [Header("Save Point Info")]
    public string savePointName;
    [Space]
    public int savePointIndex;
    [Space]
    public bool isCurrentSavePoint;

    [Header("Load On Start Scenes")]
    public List<SceneDataSO> _loadOnStartSceneDatas;

    [Header("Position And Rotation")]
    public Vector3 playerPosition;
    [Space]
    public Quaternion playerRotation;

    [Header("Trigger Event On Scenes Load")]
    public UnityEvent OnSavePointLoadEvent;
}