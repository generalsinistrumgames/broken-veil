using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Save And Load/Scenes Load Data", fileName = "Scenes Load Data")]
public class ScenesLoadDataSO : ScriptableObject
{
    [Header("All Game Chapters")]
    public List<ChapterDataSO> chapters;

    [Header("Game First Chapter And Save Point")]
    public ChapterDataSO gameFirstChapter;
    [Space]
    public SavePointSO gameFirstSavePoint;

    [Header("Current Scenes Data")]
    public ChapterDataSO currentChapter;
    [Space]
    public SavePointSO currentSavePoint;
    [Space]
    public CurrentScenesData currentScenesData;

    [Header("Save File Name")]
    public string saveFileName = "/currentScenesData.json";

    [System.Serializable]
    public class CurrentScenesData
    {
        [SerializeField] public int currentChapterIndex;
        [Space]
        [SerializeField] public int currentSavePointIndex;
        [Space]
        [SerializeField] public bool dataHasAssigned;
    }

    public void GetCurrentScenesData(SaveAndLoadManager saveAndLoadManager)
    {
        if (saveAndLoadManager.IsSaveFileExists(saveFileName))
        {
            saveAndLoadManager.Load(saveFileName, currentScenesData);

            currentChapter = chapters[currentScenesData.currentChapterIndex - 1];

            currentSavePoint = chapters[currentScenesData.currentChapterIndex - 1].chapterSavePoints[currentScenesData.currentSavePointIndex - 1];
        }
    }

    [ContextMenu("Reset Current Assign Data To Default Presset")]
    public void ResetSavedData()
    {
        currentChapter = null;

        currentSavePoint = null;

        currentScenesData.currentChapterIndex = 0;

        currentScenesData.currentSavePointIndex = 0;

        currentScenesData.dataHasAssigned = false;
    }

    public void SetNewGameLoadData()
    {
        currentChapter = chapters[gameFirstChapter.chapterIndex - 1];

        currentSavePoint = chapters[gameFirstChapter.chapterIndex - 1].chapterSavePoints[gameFirstSavePoint.savePointIndex - 1];

        currentScenesData.currentChapterIndex = gameFirstChapter.chapterIndex;

        currentScenesData.currentSavePointIndex = gameFirstSavePoint.savePointIndex;

        currentScenesData.dataHasAssigned = true;
    }
}