﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scene Management/Scene Data", fileName = "Scene Data")]
public class SceneDataSO : ScriptableObject
{
    [Header("Scene Info")]
    public string sceneName;
    [Space]
    [Range(0, 20)]
    public int sceneIndex;

    [Header("Load Control")]
    public bool sceneIsLoaded;

    [Header("Scene Unlocking")]
    public bool sceneLocked;
}