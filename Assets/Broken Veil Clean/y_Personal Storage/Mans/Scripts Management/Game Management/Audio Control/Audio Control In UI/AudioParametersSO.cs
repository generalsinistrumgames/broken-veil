using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Management/Audio Parameters", fileName = "Audio Parameters")]
public class AudioParametersSO : ScriptableObject
{
    [Header("Audio Default Volume")]
    public float defaultMasterVolumeValue;
    [Space]
    public float defaultAmbientVolumeValue;
    [Space]
    public float defaultSFXVolumeValue;

    [Header("Audio Default Volume")]
    public float currentMasterVolumeValue;
    [Space]
    public float currentAmbientVolumeValue;
    [Space]
    public float currentSFXVolumeValue;

    [Header("Wwise RTPC Value Names")]
    public string masterVolumeRTPC;
    [Space]
    public string ambientVolumeRTPC;
    [Space]
    public string sfxVolumeRTPC;

    [Header("Enable Control")]
    public bool canControlVolume = true;
}