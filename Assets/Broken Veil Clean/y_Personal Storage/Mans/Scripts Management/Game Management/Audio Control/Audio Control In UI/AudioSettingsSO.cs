using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Management/Audio Settings", fileName = "Audio Settings")]
public class AudioSettingsSO : ScriptableObject
{
    [Header("Audio Parameters")]
    public AudioParametersSO audioParametersSO;
    [Space]
    public string audioSettingsSaveFile = "/audioSettings.json";

    #region Audio Settings Control
    public void SetGloabalVolume(float volume)
    {
        if (audioParametersSO.canControlVolume)
        {
            audioParametersSO.currentMasterVolumeValue = volume;    

            AkSoundEngine.SetRTPCValue(audioParametersSO.masterVolumeRTPC, volume);
        }
    }

    public void SetAmbientVolume(float volume)
    {
        if (audioParametersSO.canControlVolume)
        {
            audioParametersSO.currentAmbientVolumeValue = volume;

            Debug.Log(volume);

            AkSoundEngine.SetRTPCValue(audioParametersSO.ambientVolumeRTPC, volume);
        }
    }

    public void SetSFXVolume(float volume)
    {
        if (audioParametersSO.canControlVolume)
        {
            audioParametersSO.currentSFXVolumeValue = volume;

            AkSoundEngine.SetRTPCValue(audioParametersSO.sfxVolumeRTPC, volume);
        }
    }

    public void SaveAudioSettings()
    {
        SaveAndLoadManager.Instance.Save(audioSettingsSaveFile, audioParametersSO);
    }
    #endregion
}