﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoSingleton<AudioManager>
{
    //public AudioManager(Player qwe, List<PlayerSounds> asd)
    //{
    //    PlayPlayerSounds(qwe, asd);
    //}
    
    //public AudioManager(Camera qwe, int asd)
    //{
    //    PlayPlayerSounds(qwe, asd);
    //}
    
    [Header("Player Sounds")]
    public List<PlayerSounds> playerSoundsList;

    [Header("Level Sounds")]
    public List<LevelSounds> levelSoundsList;

    [Header("Game Music")]
    public List<GameMusic> gameMusicList;

    [Header("Refs")]
    [SerializeField] AudioSource playerAudioSource;
    [Space]
    [SerializeField] AudioSource cameraAudioSource;

    //void OnEnable()
    //{
    //    Debug.Log("OnEnable called");
    //    SceneManager.sceneLoaded += OnSceneLoaded;
    //}

    //void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    //{
    //    playerAudioSource = GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>();

    //    cameraAudioSource = Camera.main.GetComponent<AudioSource>();

    //    Debug.Log("Scene: " + scene.name + " was loaded");
    //    Debug.Log("SceneLoadMode: " + mode);
    //}

    //void OnDisable()
    //{
    //    Debug.Log("OnDisable");
    //    SceneManager.sceneLoaded -= OnSceneLoaded;
    //}

    public void PlayPlayerSounds(int audioGroupindex, int audioClipindex)
    {
        playerAudioSource.clip = playerSoundsList[audioGroupindex].audioClips[audioClipindex];

        playerAudioSource.volume = playerSoundsList[audioGroupindex].volume;

        playerAudioSource.loop = playerSoundsList[audioGroupindex].isLooping;

        playerAudioSource.playOnAwake = playerSoundsList[audioGroupindex].playOnAwake;

        playerAudioSource.Play();
    }

    public void PlayLevelSounds(int audioGroupindex, int audioClipindex)
    {
        cameraAudioSource.clip = levelSoundsList[audioGroupindex].audioClips[audioClipindex];

        cameraAudioSource.volume = levelSoundsList[audioGroupindex].volume;

        cameraAudioSource.loop = levelSoundsList[audioGroupindex].isLooping;

        cameraAudioSource.playOnAwake = levelSoundsList[audioGroupindex].playOnAwake;

        cameraAudioSource.Play();
    }
}