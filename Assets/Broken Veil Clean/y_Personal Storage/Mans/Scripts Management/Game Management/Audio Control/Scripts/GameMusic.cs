﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameMusic
{
    public string gameMusicScene;
    [Space]
    public List<AudioClip> audioClips;
    [Space]
    [Range(0f, 1f)]
    public float volume;
    [Space]
    public bool isLooping;
    [Space]
    public bool playOnAwake;
}