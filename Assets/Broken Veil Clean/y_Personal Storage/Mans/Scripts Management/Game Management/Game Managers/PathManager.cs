﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PathManager : MonoSingleton<PathManager>
{
    [Header("Carts")]
    public List<CinemachineDollyCart> cartsList = new List<CinemachineDollyCart>();
    [Space]
    public CinemachineDollyCart[] cartsArray = new CinemachineDollyCart[] { };

    [Header("Paths")]
    public CinemachinePath[] pathsArray = new CinemachinePath[] { };
    [Space]
    public CinemachineSmoothPath[] smoothPathsArray = new CinemachineSmoothPath[] { };


    // carts list
    public void EnableCartsInList()
    {
        foreach (var CinemachineDollyCart in cartsList)
        {
            CinemachineDollyCart.enabled = true;
        }
    }

    public void DisableCartsInList()
    {
        foreach (var CinemachineDollyCart in cartsList)
        {
            CinemachineDollyCart.enabled = false;
        }
    }




    public void EnablePath()
    {
        foreach (var CinemachineDollyCart in cartsList)
        {
            CinemachineDollyCart.enabled = true;
        }
    }

    public void DisablePath()
    {
        foreach (var CinemachineDollyCart in cartsList)
        {
            CinemachineDollyCart.enabled = false;
        }
    }
}