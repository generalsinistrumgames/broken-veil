﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoSingleton<SoundManager>
{
    [Header("Sounds List")]
    public List<AudioSource> soundsList = new List<AudioSource>();

    [Header("Sounds Array")]
    public List<AudioSource> soundsArray = new List<AudioSource>();


    public void PlaySoundInList(int index)
    { 
        soundsList[index].Play();
    }

    public void PlaySoundInArray(int index)
    {
        soundsArray[index].Play();
    }
}