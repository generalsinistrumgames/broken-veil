﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggersManager : MonoSingleton<TriggersManager>
{
    [Header("Triggers List")]
    public List<GameObject> triggersList = new List<GameObject>();
    
    [Header("Triggers Array")]
    public GameObject[] triggersArray = new GameObject[] { };  
}
