﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class UIManager : MonoSingleton<UIManager>
{
    [Header("UI Camera Activity")]
    [SerializeField] Camera _UICamera;
    [Space]
    [SerializeField] bool _UICameraEnabled;

    [Header("UI Panels")]
    [SerializeField] GameObject _pauseMenu;
    [Space]
    [SerializeField] GameObject _settingsMenu;

    [Header("Menu Activity")]
    [SerializeField] KeyCode _backKey;
    [Space]
    [SerializeField] bool _isRunnigGameState;
    [Space]
    [SerializeField] bool _pauseMenuLocked;
    [Space]
    [SerializeField] bool _pauseMenuIsActive;
    [Space]
    [SerializeField] bool _mainMenuIsActive;
    [Space]
    [SerializeField] bool settingsMenuIsActive;

    [Header("Event")]
    public UnityEvent OnBackToMainMenuUI;

    private void Start()
    {
        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChange);
    }

    private void Update()
    {
        if (_isRunnigGameState && !_pauseMenuLocked)
        {
            if (Input.GetKeyDown(_backKey))
            {
                //if (_mainMenuIsActive)
                //{
                //    _mainMenuIsActive = false;

                //    ShowSettingsMenu(false);
                //}

                _pauseMenuIsActive = !_pauseMenuIsActive;

                ShowPauselMenu(_pauseMenuIsActive);

                SetGamePause(_pauseMenuIsActive);

                if (_settingsMenu.activeInHierarchy)
                {
                    ShowSettingsMenu(false);
                }

                //UICameraControl();
            }
        }
    }

    #region Game State Control
    public void SetGamePause(bool gamePaused)
    {
        if (GameManager.Instance.CurrentGameState != GameManager.GameState.Pregame)
        {
            if (gamePaused)
            {
                AkSoundEngine.Suspend();

                GameManager.Instance.TogglePause(GameManager.GameState.Paused);
            }
            else
            {
                GameManager.Instance.TogglePause(GameManager.GameState.Running);

                AkSoundEngine.WakeupFromSuspend();
            }
        }
    }

    public void ResumeGame()
    {
        ShowPauselMenu(false);

        AkSoundEngine.WakeupFromSuspend();

        //UICameraControl();
    }

    public void BackToMenu()
    {
        ShowPauselMenu(false);

        LockPauseMenu(true);

        _UICameraEnabled = false;

        AkSoundEngine.WakeupFromSuspend();
    }

    void HandleGameStateChange(GameManager.GameState currentState, GameManager.GameState previousState)
    {
        if (currentState == GameManager.GameState.Running || currentState == GameManager.GameState.Paused)
        {
            _isRunnigGameState = true;
        }
        else
        {
            _isRunnigGameState = false;
        }
    }
    #endregion

    #region Pause Menu Control
    public void ShowPauselMenu(bool active)
    {
        _pauseMenu.SetActive(active);

        _pauseMenuIsActive = active;
    }

    public void LockPauseMenu(bool active)
    {
        _pauseMenuLocked = active;

        _pauseMenuIsActive = false;
    }
    #endregion

    #region Settings Menu Control
    public void ShowSettingsMenu(bool active)
    {
        _settingsMenu.SetActive(active);

        settingsMenuIsActive = active;

        if (_isRunnigGameState)
        {
            ShowPauselMenu(!active);

            _pauseMenuIsActive = !active;
        }
        else
        {
            OnBackToMainMenuUI?.Invoke();

            //_mainMenuIsActive = true;
        }
    }
    #endregion

    #region UI Camera Control
    public void UICameraControl()
    {
        if (_pauseMenuIsActive || settingsMenuIsActive)
        {
            if (_UICameraEnabled)
            {
                return;
            }
            else
            {
                _UICameraEnabled = true;

                UICameraActivity(true);
            }
        }
        else
        {
            _UICameraEnabled = false;

            UICameraActivity(false);
        }
    }

    public void UICameraActivity(bool active)
    {
        _UICamera.gameObject.SetActive(active);
    }
    #endregion
}