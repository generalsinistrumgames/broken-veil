﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class SceneLoadManager : MonoSingleton<SceneLoadManager>
{
    [Header("Scenes Management Info")]
    public ScenesLoadDataSO scenesLoadDataSO;
    [Space]
    public SceneSO mainMenuScene;
    [Space]
    public string currentSceneName;

    List<AsyncOperation> _asyncLoadOperations = new List<AsyncOperation>();

    List<Scene> _loadedScenes = new List<Scene>();

    private void Start()
    {
        //scenesLoadDataSO.GetCurrentScenesData();        
    }

    //void OnApplicationQuit()
    //{
    //    SaveAndLoadManager.Instance.SaveGame(scenesLoadDataSO.saveFileName, scenesLoadDataSO);

    //    Debug.Log("Application ending after " + Time.time + " seconds");
    //}

    void OnEnable()
    {
        Debug.Log("OnEnable called");

        SceneManager.sceneLoaded += OnSceneLoaded;

        SceneManager.sceneUnloaded += OnSceneUnloaded;

        SavePointButton.OnLoadSavePoint += LoadSavePoint;
    }

    void OnDisable()
    {
        Debug.Log("OnDisable");

        SceneManager.sceneLoaded -= OnSceneLoaded;

        SceneManager.sceneUnloaded -= OnSceneUnloaded;

        SavePointButton.OnLoadSavePoint -= LoadSavePoint;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Scene: " + scene.name + " was loaded");

        Debug.Log("SceneLoadMode: " + mode.ToString());

        _loadedScenes.Add(scene);
    }
    void OnSceneUnloaded(Scene scene)
    {
        Debug.Log("Scene: " + scene.name + " was unloaded");

        _loadedScenes.Remove(scene);
    }

    public bool IsTargetSceneLoaded(string sceneName)
    {
        if (SceneManager.GetSceneByName(sceneName).isLoaded)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #region Scenes Load
    public void LoadSceneAsyncAdditive(string levelName)
    {
        if (!IsTargetSceneLoaded(levelName))
        {
            SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);

            currentSceneName = levelName;
        }
    }

    public void LoadSceneAsyncAdditive(SceneSO sceneSO)
    {
        if (!IsTargetSceneLoaded(sceneSO.sceneName))
        {
            SceneManager.LoadSceneAsync(sceneSO.sceneName, LoadSceneMode.Additive);

            currentSceneName = sceneSO.sceneName;
        }
    }

    public void LoadSceneAdditiveAndAsyncLoadOperation(string levelName)
    {
        if (!IsTargetSceneLoaded(levelName))
        {
            AsyncOperation ao = SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);

            //ao.allowSceneActivation = false;

            if (ao == null)
            {
                Debug.LogError(gameObject.name + " Unable to load level: " + levelName);

                return;
            }

            ao.completed += OnAsyncLoadOperationComplete;

            _asyncLoadOperations.Add(ao);

            currentSceneName = levelName;
        }
    }

    public void LoadSceneAdditiveAndAsyncLoadOperation(SceneSO sceneSO)
    {
        if (!IsTargetSceneLoaded(sceneSO.name))
        {
            AsyncOperation ao = SceneManager.LoadSceneAsync(sceneSO.name, LoadSceneMode.Additive);

            //ao.allowSceneActivation = false;

            if (ao == null)
            {
                Debug.LogError(gameObject.name + " Unable to load level: " + sceneSO.name);

                return;
            }

            ao.completed += OnAsyncLoadOperationComplete;

            _asyncLoadOperations.Add(ao);

            currentSceneName = sceneSO.name;
        }
    }

    void OnAsyncLoadOperationComplete(AsyncOperation ao)
    {
        if (_asyncLoadOperations.Contains(ao))
        {
            _asyncLoadOperations.Remove(ao);

            // scene fully loaded do some stuff here
            if (_asyncLoadOperations.Count == 0)
            {
                GameManager.Instance.UpdateGameState(GameManager.GameState.Running);

                if (scenesLoadDataSO.currentScenesData.dataHasAssigned)
                {
                    scenesLoadDataSO.currentSavePoint.OnSavePointLoadEvent?.Invoke();

                    Debug.Log(scenesLoadDataSO.currentSavePoint.savePointName + " OnSavePointLoadEvent Raised");
                }
            }
        }

        Debug.Log("On Async Load Complete");
    }
    #endregion

    #region Unload Scene
    public void UnloadLevelInGame(string levelName)
    {
        if (IsTargetSceneLoaded(levelName))
        {
            SceneManager.UnloadSceneAsync(levelName);
        }
    }

    public void UnloadLevel(string levelName)
    {
        AsyncOperation ao = SceneManager.UnloadSceneAsync(levelName);

        if (ao == null)
        {
            Debug.LogError(gameObject.name + " Unable to unload level: " + levelName);

            return;
        }

        ao.completed += OnAsyncUnloadOperationComplete;
    }

    void OnAsyncUnloadOperationComplete(AsyncOperation ao)
    {
        Debug.Log("On Async Unload Complete");
    }
    #endregion

    #region Listen Events
    public void LoadGameInitializationScene(SceneSO sceneSO)
    {
        LoadSceneAsyncAdditive(sceneSO.sceneName);

        //LoadSceneAdditiveAndAsyncLoadOperation(sceneSO.sceneName);
    }

    public void LoadCurrentChapterScenes()
    {
        foreach (var scene in _loadedScenes)
        {
            if (scene.buildIndex != 0) // ignore game root scene and unload others
            {
                UnloadLevel(scene.name);
            }
        }

        LoadSceneAdditiveAndAsyncLoadOperation(scenesLoadDataSO.currentChapter.chapterMainScene.sceneName);

        foreach (var scene in scenesLoadDataSO.currentSavePoint._loadOnStartSceneDatas)
        {
            LoadSceneAdditiveAndAsyncLoadOperation(scene.sceneName);
        }
    }

    #region From UI
    public void LoadNewGameScene(SceneSO sceneSO)
    {
        //LoadSceneAdditiveAndAsyncLoadOperation(scenesLoadDataSO.currentChapter.chapterMainScene.sceneName);

        LoadSceneAsyncAdditive(sceneSO.sceneName);

        UnloadLevel(mainMenuScene.sceneName);

        //foreach (var scene in scenesLoadDataSO.currentChapter.newGameLoadScenes)
        //{
        //    LoadSceneAdditiveAndAsyncLoadOperation(scene.sceneName);
        //} 
    }

    public void AllowScenesActivation()
    {
        StartCoroutine(CheckIfScenesAreReady());

        Debug.Log("AllowScenesActivation" + _asyncLoadOperations);
    }

    public IEnumerator CheckIfScenesAreReady()
    {
        foreach (var ao in _asyncLoadOperations)
        {
            ao.allowSceneActivation = true;
        }

        while (_asyncLoadOperations.Capacity > 0)
        {
            Debug.Log(_asyncLoadOperations.Capacity);

            yield return null;
        }

        UIFader.Instance.FadeOut();

        Debug.Log("CheckIfScenesAreReady Fade out");
    }

    public IEnumerator CheckIfSceneIsUnloaded(string sceneName)
    {
        AsyncOperation operation = SceneManager.UnloadSceneAsync(sceneName);

        while (!operation.isDone)
        {
            Debug.Log("Unloading " + sceneName);

            yield return null;
        }

        Debug.Log(sceneName + " Unloaded");
    }

    public void LoadResumeGameScenes()
    {
        UnloadLevel(mainMenuScene.sceneName);

        LoadSceneAdditiveAndAsyncLoadOperation(scenesLoadDataSO.currentChapter.chapterMainScene.sceneName);

        foreach (var scene in scenesLoadDataSO.currentSavePoint._loadOnStartSceneDatas)
        {
            LoadSceneAdditiveAndAsyncLoadOperation(scene.sceneName);
        }
    }

    public void LoadBackToMenuScene(SceneSO sceneSO)
    {
        AkSoundEngine.StopAll();

        GameManager.Instance.UpdateGameState(GameManager.GameState.Pregame);

        foreach (var scene in _loadedScenes)
        {
            if (scene.buildIndex != 0) // ignore game root scene and unload others
            {
                UnloadLevel(scene.name);
            }
        }

        LoadSceneAsyncAdditive(sceneSO.sceneName);
    }

    public void LoadSavePoint(ChapterDataSO chapterDataSO, SavePointSO savePointSO)
    {
        foreach (var scene in _loadedScenes)
        {
            if (scene.buildIndex != 0) // ignore game root scene and unload others
            {
                UnloadLevel(scene.name);
            }
        }

        LoadSceneAdditiveAndAsyncLoadOperation(chapterDataSO.chapterMainScene.sceneName);

        foreach (var scene in savePointSO._loadOnStartSceneDatas)
        {
            LoadSceneAdditiveAndAsyncLoadOperation(scene.sceneName);
        }
    }

    #endregion

    #endregion

    #region Game Over Reload Scenes
    public void ReloadGame()
    {

    }
    #endregion
}