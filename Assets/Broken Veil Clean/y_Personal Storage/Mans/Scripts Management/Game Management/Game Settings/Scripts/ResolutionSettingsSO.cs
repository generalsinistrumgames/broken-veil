﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Management/Resolution Settings", fileName = "Resolution Settings")]
public class ResolutionSettingsSO : ScriptableObject
{
    [Header("Resolution Control")]
    public List<string> options = new List<string>();
    [Space]
    public int currentResolutionIndex;
    [Space]
    public int currentResolutionWidth, currentResolutionHeight;

    [System.NonSerialized] public Resolution[] resolutions;

    [System.NonSerialized] public string resolutionSaveFile = "/resolutionSettings.json";

    public void GetResolutionsInfo()
    {
        options.Clear();

        resolutions = Screen.resolutions;

        for (int i = 0; i < resolutions.Length; i++)
        {         
            string option = resolutions[i].width + " x " + resolutions[i].height;

            options.Add(option);

            if (resolutions[i].width == Screen.width && resolutions[i].height == Screen.height)
            {
                currentResolutionIndex = i;

                currentResolutionWidth = Screen.width;

                currentResolutionHeight = Screen.height;
            }
        }

        Debug.Log("Resolution Settings Getted");
    }

    public void SetResolution(int resolutionIndex)
    {
        currentResolutionIndex = resolutionIndex;

        // dont set same resolition
        //if (currentResolutionIndex == resolutionIndex)
        //{
        //    return;
        //}

        Resolution resolution = resolutions[currentResolutionIndex];

        currentResolutionWidth = resolution.width;

        currentResolutionHeight = resolution.height;

        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);

        SaveAndLoadManager.Instance.Save(resolutionSaveFile, this);

        Debug.Log("Resolution Settings Setted");
    }
}