﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ShowMenuPanel : MonoBehaviour
{
    [Header("Event")]
    public UnityEvent OnCloseSettingsPanel;

    [Header("Refs")]
    public GameObject settingsPanel;
    [Space]
    public GameObject pauseMenuPanel;
    [Space]
    public SceneSO sceneSO;

    public void ControlMenuPanel(bool value)
    {
        settingsPanel.SetActive(value);

        if (!value)
        {
            if (SceneManager.GetSceneByName(sceneSO.sceneName).isLoaded)
            {
                pauseMenuPanel.SetActive(false);

                OnCloseSettingsPanel?.Invoke();

                GameManager.Instance.CurrentGameState = GameManager.GameState.Pregame;
            }
            else
            {
                pauseMenuPanel.SetActive(true);

                GameManager.Instance.CurrentGameState = GameManager.GameState.Paused;
            }
        }   
    }
}