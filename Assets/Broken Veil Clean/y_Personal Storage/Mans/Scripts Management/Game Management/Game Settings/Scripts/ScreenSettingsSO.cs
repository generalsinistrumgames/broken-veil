using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Management/Screen Settings", fileName = "Screen Settings")]
public class ScreenSettingsSO : ScriptableObject
{
    [Header("Full Screen Control")]
    public bool isFullScreen;

    [Header("VSync Control")]
    public bool isVsyncEnabled;

    [System.NonSerialized] public string screenSettingsSaveFile = "/screenSettings.json";

    #region Screen Settings
    public bool GetFullScreenValue()
    {
        isFullScreen = Screen.fullScreen;

        return isFullScreen;
    }

    public void SetFullScreen(bool isFullScreenValue)
    {
        isFullScreen = isFullScreenValue;

        //if (isFullScreen == isFullScreenValue)
        //{
        //    return;
        //}

        Screen.fullScreen = isFullScreen;
    }

    public bool GetVSyncValue()
    {
        if (QualitySettings.vSyncCount == 1)
        {
            isVsyncEnabled = true;
        }
        else if (QualitySettings.vSyncCount == 0)
        {
            isVsyncEnabled = false;
        }

        return isVsyncEnabled;
    }

    public void SetVSyncValue(bool VSyncIsOn)
    {
        isVsyncEnabled = VSyncIsOn;

        if (isVsyncEnabled)
        {
            QualitySettings.vSyncCount = 1;
        }
        else
        {
            QualitySettings.vSyncCount = 0;
        }
    }

    public void SaveScreenSettings()
    {
        SaveAndLoadManager.Instance.Save(screenSettingsSaveFile, this);
    }
    #endregion
}