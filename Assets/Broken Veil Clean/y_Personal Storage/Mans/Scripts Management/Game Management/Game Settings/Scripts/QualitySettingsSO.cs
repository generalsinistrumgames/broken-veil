using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Management/Quality Settings", fileName = "Quality Settings")]
public class QualitySettingsSO : ScriptableObject
{
    [Header("Quaility Control")]
    public List<string> options = new List<string>();
    [Space]
    public int currentQualityIndex;

    [System.NonSerialized] public string qualitySettingsSaveFile = "/quailtySettings.json";

    #region Quality Settings
    public void GetQualityLevels()
    {
        options.Clear();

        string[] qualityLevels = QualitySettings.names;

        currentQualityIndex = QualitySettings.GetQualityLevel();

        for (int i = 0; i < qualityLevels.Length; i++)
        {
            string option = qualityLevels[i];

            options.Add(option);     
        }

        Debug.Log("Game Settings Getted");
    }

    public void SetQuailityLevel(int qualityIndex)
    {
        currentQualityIndex = qualityIndex;

        //if (currentQualityIndex == qualityIndex)
        //{
        //    return;
        //}

        QualitySettings.SetQualityLevel(currentQualityIndex);

        SaveAndLoadManager.Instance.Save(qualitySettingsSaveFile, this);

        Debug.Log("Game Settings Setted");
    }
    #endregion
}