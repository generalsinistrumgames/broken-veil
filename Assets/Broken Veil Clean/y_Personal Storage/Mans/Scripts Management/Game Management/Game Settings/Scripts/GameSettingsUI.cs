﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameSettingsUI : MonoBehaviour
{
    [Header("Quality Settings")]
    public QualitySettingsSO qualitySettingsSO;

    [Header("Resolution Settings")]
    public ResolutionSettingsSO resolutionSettingsSO;

    [Header("Screen Settings")]
    public ScreenSettingsSO screenSettingsSO;

    [Header("Brightness Settings")]
    public BrightnessSettingsSO brightnessSettingsSO;
    [Space]
    public BrightnessControll brightnessControll;

    [Header("Brightness Slider")]
    public Slider brightnessSlider;

    [Header("Audio Settings")]
    public AudioSettingsSO audioSettingsSO;

    [Header("Dropdown Refs")]
    public TMP_Dropdown resolutionDropdown;
    [Space]
    public TMP_Dropdown qualityDropdown;

    [Header("Togle Refs")]
    public Toggle fullScreenToggle;
    [Space]
    public Toggle vSyncToggle;

    [Header("Audio")]
    public Slider masterVolumeSlider;
    [Space]
    public Slider ambientVolumeSlider;
    [Space]
    public Slider SFXVolumeSlider;

    private void Start()
    {   
        QualityLevelSettingsControl();

        ScreenSettingsControl();

        ResolutionSettingsControl();

        BrightnessSettingsControl();

        AudioSettingsControl();
    }

    public void QualityLevelSettingsControl()
    {
        if (SaveAndLoadManager.Instance.IsSaveFileExists(qualitySettingsSO.qualitySettingsSaveFile))
        {  
            SaveAndLoadManager.Instance.Load(qualitySettingsSO.qualitySettingsSaveFile, qualitySettingsSO);
        }
        else
        {
            qualitySettingsSO.GetQualityLevels();      
        }

        qualityDropdown.onValueChanged.AddListener(delegate
        {
            qualitySettingsSO.SetQuailityLevel(qualityDropdown.value);
        });

        FillDropdownInfo(qualityDropdown, qualitySettingsSO.options, qualitySettingsSO.currentQualityIndex);
    }

    public void ResolutionSettingsControl()
    {
        resolutionSettingsSO.GetResolutionsInfo();

        if (SaveAndLoadManager.Instance.IsSaveFileExists(resolutionSettingsSO.resolutionSaveFile))
        {       
            SaveAndLoadManager.Instance.Load(resolutionSettingsSO.resolutionSaveFile, resolutionSettingsSO);
        }
 
        resolutionDropdown.onValueChanged.AddListener(delegate
        {
            resolutionSettingsSO.SetResolution(resolutionDropdown.value);
        });

        FillDropdownInfo(resolutionDropdown, resolutionSettingsSO.options, resolutionSettingsSO.currentResolutionIndex);
    }

    public void ScreenSettingsControl()
    {      
        if (SaveAndLoadManager.Instance.IsSaveFileExists(screenSettingsSO.screenSettingsSaveFile))
        {
            SaveAndLoadManager.Instance.Load(screenSettingsSO.screenSettingsSaveFile, screenSettingsSO);
        }
        else
        {
            fullScreenToggle.isOn = screenSettingsSO.GetFullScreenValue();

            vSyncToggle.isOn = screenSettingsSO.GetVSyncValue();
        }

        fullScreenToggle.onValueChanged.AddListener(delegate
        {
            screenSettingsSO.SetFullScreen(fullScreenToggle.isOn);

            screenSettingsSO.SaveScreenSettings();
        });

        vSyncToggle.onValueChanged.AddListener(delegate
        {
            screenSettingsSO.SetVSyncValue(vSyncToggle.isOn);

            screenSettingsSO.SaveScreenSettings();
        });

        fullScreenToggle.isOn = screenSettingsSO.isFullScreen;

        vSyncToggle.isOn = screenSettingsSO.isVsyncEnabled;
    }

    public void BrightnessSettingsControl()
    {
        if (SaveAndLoadManager.Instance.IsSaveFileExists(brightnessSettingsSO.brightnessSettingsSaveFile))
        {
            SaveAndLoadManager.Instance.Load(brightnessSettingsSO.brightnessSettingsSaveFile, brightnessSettingsSO);
        }
        else
        {
            brightnessSettingsSO.brightnessValue = brightnessSettingsSO.defaultBrightness;
        }

        brightnessControll.GetVolumeReference();

        brightnessSlider.onValueChanged.AddListener(delegate
        {
            brightnessSettingsSO.SetBrightness(brightnessSlider.value);

            brightnessControll.SetBrightness(brightnessSlider.value);
        });

        brightnessSlider.value = brightnessSettingsSO.brightnessValue;
    }

    public void AudioSettingsControl()
    {
        if (SaveAndLoadManager.Instance.IsSaveFileExists(audioSettingsSO.audioSettingsSaveFile))
        {
            SaveAndLoadManager.Instance.Load(audioSettingsSO.audioSettingsSaveFile, audioSettingsSO.audioParametersSO);

            masterVolumeSlider.value = audioSettingsSO.audioParametersSO.currentMasterVolumeValue;

            ambientVolumeSlider.value = audioSettingsSO.audioParametersSO.currentAmbientVolumeValue;

            SFXVolumeSlider.value = audioSettingsSO.audioParametersSO.currentSFXVolumeValue;
        }
        else
        {      
            masterVolumeSlider.value = audioSettingsSO.audioParametersSO.defaultMasterVolumeValue;

            ambientVolumeSlider.value = audioSettingsSO.audioParametersSO.defaultAmbientVolumeValue;

            SFXVolumeSlider.value = audioSettingsSO.audioParametersSO.defaultSFXVolumeValue;        
        }

        audioSettingsSO.SetGloabalVolume(masterVolumeSlider.value);

        audioSettingsSO.SetAmbientVolume(ambientVolumeSlider.value);

        audioSettingsSO.SetSFXVolume(SFXVolumeSlider.value);

        masterVolumeSlider.onValueChanged.AddListener(delegate
        {
            audioSettingsSO.SetGloabalVolume(masterVolumeSlider.value);
        });

        ambientVolumeSlider.onValueChanged.AddListener(delegate
        {
            audioSettingsSO.SetAmbientVolume(ambientVolumeSlider.value);
        });

        SFXVolumeSlider.onValueChanged.AddListener(delegate
        {
            audioSettingsSO.SetSFXVolume(SFXVolumeSlider.value);
        });
    }

    public void FillDropdownInfo(TMP_Dropdown dropdown, List<string> options, int currentOptionIndex)
    {
        dropdown.ClearOptions();

        dropdown.AddOptions(options);

        dropdown.value = currentOptionIndex;

        dropdown.RefreshShownValue();
    }

    //public void SliderValueSetter()
    //{
    //    slider.OnDeselect
    //}

    //private void OnEnable()
    //{
    //    masterVolumeSlider.OnDeselect += SliderValueSetter();
    //}

    //void OnGUI()
    //{
    //    string[] names = QualitySettings.names;
    //    GUILayout.BeginVertical();
    //    for (int i = 0; i < names.Length; i++)
    //    {
    //        if (GUILayout.Button(names[i]))
    //        {
    //            QualitySettings.SetQualityLevel(i, true);
    //        }
    //    }
    //    GUILayout.EndVertical();
    //}
}