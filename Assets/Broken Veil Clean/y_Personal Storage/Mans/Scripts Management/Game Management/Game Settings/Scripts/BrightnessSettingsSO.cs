using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game Management/Brightness Settings", fileName = "Brightness Settings")]
public class BrightnessSettingsSO : ScriptableObject
{
    [Header("Screen Brightness")]
    public float brightnessValue;
    [Space]
    public float defaultBrightness;

    [System.NonSerialized] public string brightnessSettingsSaveFile = "/brightnessSettings.json";

    public void SetBrightness(float value)
    {
        brightnessValue = value;
    }

    public void SaveBrightnessSettings()
    {
        SaveAndLoadManager.Instance.Save(brightnessSettingsSaveFile, this);
    }
}