﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveAndLoadManager : MonoSingleton<SaveAndLoadManager>
{
    public ScenesLoadDataSO scenesLoadDataSO;
    [Space]
    public string globalSavesDirectory = "/global_saves";
    [Space]
    public string localSavesDirectory = "/local_saves";

    protected override void Awake()
    {
        base.Awake();

        scenesLoadDataSO.GetCurrentScenesData(this);
    }

    public void Save(string saveName, Object obj, string directory = "/global_saves")
    {
        if (!Directory.Exists(Application.persistentDataPath + directory))
        {
            Directory.CreateDirectory(Application.persistentDataPath + directory);
        }

        BinaryFormatter bf = new BinaryFormatter();

        FileStream file = File.Create(Application.persistentDataPath + directory + saveName);

        var json = JsonUtility.ToJson(obj, true);

        bf.Serialize(file, json);

        file.Close();

        Debug.Log(saveName + " Save File Created");
    }

    public void Load(string saveName, Object obj, string directory = "/global_saves")
    {
        BinaryFormatter bf = new BinaryFormatter();

        if (File.Exists((Application.persistentDataPath + directory) + saveName))
        {
            FileStream file = File.Open(Application.persistentDataPath + directory + saveName, FileMode.Open);

            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), obj);

            file.Close();

            Debug.Log(saveName + " Save File Loaded");
        }
        else
        {
            Debug.Log(saveName + " Save File Doesnt Exists");
        }
    }

    public bool IsDirectoryExists(string directory)
    {
        return Directory.Exists(Application.persistentDataPath + directory);
    }
    public bool IsSaveFileExists(string saveFileName, string directory = "/global_saves")
    {
        return File.Exists(Application.persistentDataPath + directory + saveFileName);
    }


    public void DeleteDirectory(string directory)
    {
        if (Directory.Exists(Application.persistentDataPath + directory))
        {
            Directory.Delete(Application.persistentDataPath + directory, true);
        }
    }

    public void DeleteSavedFile(string saveName, Object obj, string directory = "/global_saves")
    {
        if (File.Exists(Application.persistentDataPath + directory + saveName))
        {
            File.Delete(Application.persistentDataPath + directory + saveName);

            Debug.Log(saveName + " Save File Deleted");
        }
        else
        {
            Debug.Log(saveName + " Save File Doesnt Exists");
        }
    }

    #region C# object save
    public void Save(string saveName, object obj, string directory = "/global_saves")
    {
        if (!Directory.Exists(Application.persistentDataPath + directory))
        {
            Directory.CreateDirectory(Application.persistentDataPath + directory);
        }

        BinaryFormatter bf = new BinaryFormatter();

        FileStream file = File.Create(Application.persistentDataPath + directory + saveName);

        var json = JsonUtility.ToJson(obj, true);

        bf.Serialize(file, json);

        file.Close();

        Debug.Log(saveName + " Save File Created");
    }

    public void Load(string saveName, object obj, string directory = "/global_saves")
    {
        BinaryFormatter bf = new BinaryFormatter();

        if (File.Exists(Application.persistentDataPath + directory + saveName))
        {
            FileStream file = File.Open(Application.persistentDataPath + directory + saveName, FileMode.Open);

            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), obj);

            file.Close();

            Debug.Log(saveName + " Save File Loaded");
        }
        else
        {
            Debug.Log(saveName + " Save File Doesnt Exists");
        }
    }

    public void DeleteSavedFile(string saveName, object obj, string directory = "/global_saves")
    {
        if (File.Exists(Application.persistentDataPath + directory + saveName))
        {
            File.Delete(Application.persistentDataPath + directory + saveName);

            Debug.Log(saveName + " Save File Deleted");
        }
        else
        {
            Debug.Log(saveName + " Save File Doesnt Exists");
        }
    }
    #endregion

    //public void LoadGame(string saveName, Object obj)
    //{
    //    if (PlayerPrefs.HasKey(saveName))
    //    {
    //        JsonUtility.FromJsonOverwrite(PlayerPrefs.GetString(saveName), obj);

    //        Debug.Log(saveName + "Save File Is Loaded");
    //    }
    //    else
    //    {
    //        Debug.Log(saveName + "File Doesn't Exists. File Created");
    //    }
    //}


    //public void SaveGame(string saveName, Object obj)
    //{
    //    PlayerPrefs.SetString(saveName, JsonUtility.ToJson(obj, true));

    //    PlayerPrefs.Save();

    //    Debug.Log(saveName + "Save File Created");
    //}
}