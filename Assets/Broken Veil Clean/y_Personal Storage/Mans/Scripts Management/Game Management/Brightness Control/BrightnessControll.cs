using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

public class BrightnessControll : MonoBehaviour
{
    public Volume volume;
    [Space]
    public float brightnessValue;
    [Space]
    public bool canControlBrightness;
    [Space]
    public ColorAdjustments colorAdjustments;
    [Space]
    public BrightnessSettingsSO brightnessSettingsSO;

    private void Start()
    {
        GetVolumeReference();
    }

    private void Update()
    {
        if (canControlBrightness)
        {
            colorAdjustments.postExposure.value = brightnessValue;
        }
    }

    public void GetVolumeReference()
    {
        if (volume == null)
        {
            volume = GetComponent<Volume>();
        }

        ColorAdjustments cA;

        if (volume.profile.TryGet(out cA))
        {
            colorAdjustments = cA;
        }
    }
    public void SetBrightness(float value)
    {
        colorAdjustments.postExposure.value = value;
    }

    public void SetDefaultBrightness()
    {
        colorAdjustments.postExposure.value = brightnessSettingsSO.defaultBrightness;
    }
}