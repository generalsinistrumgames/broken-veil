using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrightnessSetter : MonoBehaviour
{
    public BrightnessControll brightnessControll;
    [Space]
    public BrightnessSettingsSO brightnessSettingsSO;
    [Space]
    public AnimationCurve brightnessCurve;
    [Space]
    public bool canSetBrightness;
    [Space]
    public bool resetBrightnessAfterCurveEnds = true;
    [Space]
    public float evaluateTime;

    int brightnessCurveLengthValue;

    private void Start()
    {
        brightnessControll = FindObjectOfType<BrightnessControll>();

        brightnessCurve.preWrapMode = WrapMode.Once;

        brightnessCurve.postWrapMode = WrapMode.Once;
      
        brightnessCurve.keys[0].value = brightnessSettingsSO.brightnessValue;

        brightnessCurveLengthValue = brightnessCurve.length - 1;
    }

    public void Update()
    {
        if (canSetBrightness)
        {       
            evaluateTime += Time.deltaTime;

            brightnessControll.brightnessValue = brightnessCurve.Evaluate(evaluateTime);

            if (resetBrightnessAfterCurveEnds)
            {
                if (evaluateTime > brightnessCurve[brightnessCurveLengthValue].time)
                {
                    canSetBrightness = false;

                    ResetBrightness();

                    brightnessControll.canControlBrightness = false;
               
                    evaluateTime = 0;
                }
            }       
        }
    }

    public void SetBrightness()
    {
        canSetBrightness = true;

        brightnessControll.canControlBrightness = true;
    }

    public void ResetBrightness()
    {
        brightnessControll.SetBrightness(brightnessSettingsSO.brightnessValue);

        brightnessControll.canControlBrightness = false;     
    }

    private void OnDisable()
    {
        ResetBrightness();
    }
}