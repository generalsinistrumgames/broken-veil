﻿using TMPro;
using UnityEngine;
using System.Collections.Generic;

public class ShowScenesLoadData : MonoBehaviour
{
    public ScenesLoadDataSO scenesLoadDataSO;

    [Header("Fill Chapters Data In UI")]
    public TextMeshProUGUI chapterName;
    [Space]
    public GameObject chapterPrefabParent;
    [Space]
    public GameObject chapterPrefab;

    [Header("Fill Save Points Data In UI")]
    public TextMeshProUGUI savePointName;
    [Space]
    public GameObject savePointPrefabParent;
    [Space]
    public GameObject savePointPrefab;

    [Header("Slider Control")]
    public Vector2 scrollVector2;

    private void OnEnable()
    {
        ChapterButton.OnSavePointCreate += CreateSavePointButtons;
    }

    private void OnDisable()
    {
        ChapterButton.OnSavePointCreate -= CreateSavePointButtons;
    }

    private void Start()
    {
        LoadChaptersInUI();
    }

    public void LoadChaptersInUI()
    {
        chapterName.text = scenesLoadDataSO.chapters[0].chapterName;

        for (int i = 0; i < scenesLoadDataSO.chapters.Count; i++)
        {          
            var chapterButtonPrefab = Instantiate(chapterPrefab, chapterPrefabParent.transform);

            var chapterButtonScript = chapterButtonPrefab.GetComponent<ChapterButton>();

            chapterButtonScript.chapter = scenesLoadDataSO.chapters[i];

            AddChapterSavePoints(chapterButtonScript);
        }
    }

    public void AddChapterSavePoints(ChapterButton chapterBtn)
    {
        chapterBtn.GetChapterSavePoints();
    }

    public void CreateSavePointButtons(ChapterDataSO chapter, List<SavePointSO> savePoints)
    {
        if (savePointPrefabParent.transform.childCount > 0)
        {
            foreach (Transform child in savePointPrefabParent.transform)
            {
                Destroy(child.gameObject);
            }
        }

        for (int i = 0; i < savePoints.Count; i++)
        {
            var savePointObject = Instantiate(savePointPrefab, savePointPrefabParent.transform);

            var savePointButtonScript = savePointObject.GetComponent<SavePointButton>();

            savePointButtonScript.savePoint = savePoints[i];

            savePointButtonScript.chapter = chapter;
        }
    }

    public void GetScrollVector(Vector2 value)
    {
        scrollVector2 = value;
    }
}