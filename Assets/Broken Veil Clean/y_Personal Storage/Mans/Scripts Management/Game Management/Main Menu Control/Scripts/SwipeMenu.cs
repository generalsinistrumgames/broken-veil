﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SwipeMenu : MonoBehaviour
{
    [Header("Text UI")]
    [SerializeField] Button[] chapterButtons;
    [Space]
    [SerializeField] TextMeshProUGUI[] chapterNames;
    [Space]
    [SerializeField] TextMeshProUGUI sceneName;

    [Header("Swipe Control")]
    [SerializeField] Scrollbar scrollbar;
    [Space]
    [SerializeField] float scrollPos = 0;
    [Space]
    [SerializeField] float[] positions;
    [Space]
    [SerializeField] float distance;

    [Header("Scale")]
    [SerializeField] float scaleTime;
    [Space]
    [SerializeField] float scaleValue;
    [Space]
    [SerializeField] float defaultScaleValue;

    private void OnEnable()
    {
        GetReferences();

        CalculatePositionsDistance();
    }

    void Start()
    {
        //scrollbar.onValueChanged.AddListener(SetScrollbarValue);
    }

    void Update()
    {
        GetCurrentButton();

        SetScales();
    }

    public void GetCurrentButton()
    {
        for (int i = 0; i < positions.Length; i++)
        {
            if (scrollPos < positions[i] + (distance / 2) && scrollPos > positions[i] - (distance / 2))
            {
                //scrollbar.value = Mathf.Lerp(scrollbar.value, positions[i], scaleTime);

                sceneName.text = chapterNames[i].text;

                Debug.Log("Current Selected Level " + i);
            }
        }
    }

    public void SetScales()
    {
        for (int i = 0; i < positions.Length; i++)
        {
            if (scrollPos < positions[i] + (distance / 2) && scrollPos > positions[i] - (distance / 2))
            {
                transform.GetChild(i).localScale = Vector2.Lerp(transform.GetChild(i).localScale, new Vector2(scaleValue, scaleValue), scaleTime);

                for (int j = 0; j < positions.Length; j++)
                {
                    if (j != i)
                    {
                        transform.GetChild(j).localScale = Vector2.Lerp(transform.GetChild(j).localScale, new Vector2(defaultScaleValue, defaultScaleValue), scaleTime);
                    }
                }
            }
        }
    }

    public void CalculatePositionsDistance()
    {
        positions = new float[transform.childCount];

        distance = 1f / (positions.Length - 1f);

        for (int i = 0; i < positions.Length; i++)
        {
            positions[i] = distance * i;
        }
    }

    public void SetScrollbarValue()
    {
        scrollPos = scrollbar.value;

        if (scrollPos <= positions[0] && scrollPos <= positions[1])
        {
            sceneName.text = chapterNames[0].text;
        }
        else if (scrollPos < positions[1])
        {
            sceneName.text = chapterNames[1].text;
        }
    }

    void GetReferences()
    {
        chapterButtons = transform.GetComponentsInChildren<Button>();

        chapterNames = new TextMeshProUGUI[chapterButtons.Length];

        for (int i = 0; i < chapterButtons.Length; i++)
        {
            chapterNames[i] = chapterButtons[i].transform.GetComponentInChildren<TextMeshProUGUI>();
        }

        sceneName.text = chapterNames[0].text;  // first scene name
    }
}