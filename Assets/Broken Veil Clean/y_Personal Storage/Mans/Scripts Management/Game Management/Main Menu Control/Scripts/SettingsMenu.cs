﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SettingsMenu : MonoBehaviour
{
    GameObject gettedButton;
    KeyController kfc;
    KeyCode currentKey;
    Button currentButton;
    bool editMode;
    public List<TextMeshProUGUI> listLabels;


    void Start()
    {
        kfc = gameObject.AddComponent<KeyController>();
        kfc.Start();
        kfc.load_keys();

        listLabels[0].text= kfc._KeyMass[(int)command.up][0].ToString();
        listLabels[1].text = kfc._KeyMass[(int)command.left][0].ToString();
        listLabels[2].text = kfc._KeyMass[(int)command.down][0].ToString();
        listLabels[3].text = kfc._KeyMass[(int)command.right][0].ToString();
        for (int i=4;i<Mathf.Min(listLabels.Count,kfc._KeyMass.GetLength(0));i++)
        {
            listLabels[i].text = kfc._KeyMass[i][0].ToString();
        }
    }

    public void Confirm()
    {
        Debug.Log((int)StringToKeyCode(listLabels[1].text));
        KeyCode[][] result = new KeyCode[(int)command.Length][];
        result[(int)command.up] = new KeyCode[] { StringToKeyCode(listLabels[0].text), KeyCode.UpArrow };
        result[(int)command.left] = new KeyCode[] { StringToKeyCode(listLabels[1].text), KeyCode.LeftArrow };
        result[(int)command.down] = new KeyCode[] { StringToKeyCode(listLabels[2].text), KeyCode.DownArrow };
        result[(int)command.right] = new KeyCode[] { StringToKeyCode(listLabels[3].text), KeyCode.RightArrow };
        result[(int)command.jump] = new KeyCode[] { StringToKeyCode(listLabels[4].text) };
        result[(int)command.run] = new KeyCode[] { StringToKeyCode(listLabels[5].text) };
        result[(int)command.laser] = new KeyCode[] { StringToKeyCode(listLabels[6].text) };
        result[(int)command.crouch] = new KeyCode[] { StringToKeyCode(listLabels[7].text) };
        result[(int)command.enter] = new KeyCode[] { StringToKeyCode(listLabels[8].text), KeyCode.Return };
        kfc.Setup_Keys(result);
        kfc.save_keys();
    }


    KeyCode StringToKeyCode(string thisKeyCode)
    {
        return (KeyCode)System.Enum.Parse(typeof(KeyCode), thisKeyCode);
    }
    private void Update()
    {
        Debug.Log(currentKey);
    }

    public void SetUpKey(Button button)
    {
        editMode = !editMode;
        if (editMode)
            currentButton = button;
        else
            currentButton = null;
    }


    private void OnGUI()
    {
        Event e = Event.current;
        if (e.isKey && editMode)
        {
            currentKey = e.keyCode;
            if (currentButton != null)
                currentButton.GetComponent<TextMeshProUGUI>().text = currentKey.ToString();
            //if (Input.GetMouseButton(0))
            //    currentButton.GetComponent<TextMeshProUGUI>().text = "Left Mouse";
            //if (Input.GetMouseButton(1))
            //    currentButton.GetComponent<TextMeshProUGUI>().text = "Right Mouse";
            editMode = false;
        }
    }
}
