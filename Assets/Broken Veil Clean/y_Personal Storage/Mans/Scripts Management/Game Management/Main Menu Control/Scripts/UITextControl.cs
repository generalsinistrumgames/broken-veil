using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UITextControl : MonoBehaviour
{
    [Header("Default Text Parameter")]
    public TextMeshProUGUI textMesh;
    [Space]
    public Color defaultTextColor;
    [Space]
    public float defaultTextSize;

    [Header("Set Text Parameter")]
    public float textSizeMultiplier;
    [Space]
    public Color onHoverTextColor;

    private void Start()
    {
        if (textMesh == null)
        {
            textMesh = GetComponent<TextMeshProUGUI>();
        }

        //defaultTextColor = textMesh.color;

        defaultTextSize = textMesh.fontSize;
    }

    public void OnTextHover()
    {
        //textMesh.color = onHoverTextColor;

        textMesh.enableAutoSizing = false;

        textMesh.fontSize = defaultTextSize;

        textMesh.fontSize *= textSizeMultiplier;
    }

    public void OnTextExit()
    {
        //textMesh.color = defaultTextColor;

        textMesh.enableAutoSizing = true;

        textMesh.fontSize /= textSizeMultiplier;
    }
}