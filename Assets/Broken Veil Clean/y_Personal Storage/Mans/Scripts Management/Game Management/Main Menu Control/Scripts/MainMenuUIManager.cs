﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenuUIManager : MonoBehaviour
{
    public ScenesLoadDataSO scenesLoadDataSO;
    [Space]
    public Button resumeBtn;

    [Header("Canvas")]
    public Canvas canvas;

    [Header("Chapter Data In UI")]
    public GameObject chaptersScrollView;
    [Space]
    public GameObject chaptersContent;

    [Header("Scenes Data In UI")]
    public GameObject scenesScrollView;
    [Space]
    public GameObject scenesContent;

    private void Start()
    {
        //AssignUICameraToCanvas();

        resumeBtn.interactable = scenesLoadDataSO.currentScenesData.dataHasAssigned;
    }

    public void OpenScenesWindow()
    {
        chaptersScrollView.SetActive(false);

        scenesScrollView.SetActive(true);
    }

    //public void AssignUICameraToCanvas()
    //{
    //    canvas.worldCamera = UICameraSetter.Instance.UICamera;
    //}
}