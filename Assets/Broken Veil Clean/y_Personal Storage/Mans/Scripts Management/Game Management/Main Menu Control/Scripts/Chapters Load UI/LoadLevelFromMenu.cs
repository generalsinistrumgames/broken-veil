﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;

public class LoadLevelFromMenu : MonoBehaviour
{
    [Header("Scene Info")]
    public string loadSceneName;
    [Space]
    public int loadSceneIndex;
    [Space]
    public int sceneOfChapterIndex;

    private void Start()
    {
        Button btn = GetComponent<Button>();

        btn.onClick.AddListener(LoadLevel);
    }

    public void LoadLevel()
    {
        //MainMenuUIManager.Instance.loadingScreen.SetActive(true);

        // send chapter index to automatically load chapte start scenes
        //SceneLoadManagerNew.Instance.LoadScenesFromUI();
    }
}