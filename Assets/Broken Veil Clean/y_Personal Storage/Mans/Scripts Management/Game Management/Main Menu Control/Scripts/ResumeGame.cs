using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumeGame : MonoBehaviour
{
    [SerializeField] ScenesLoadDataSO _scenesLoadDataSO;

    [Header("Fade Transition")]
    [SerializeField] SceneTransitionFader _sceneTransitionFader;

    public WwiseAudioEmitterScript wwiseAudioEmitter;

    public void SaveScenesInfoAndResumeGame()
    {
        wwiseAudioEmitter.StopSound();

        SaveAndLoadManager.Instance.Save(_scenesLoadDataSO.saveFileName, _scenesLoadDataSO.currentScenesData);

        _sceneTransitionFader.SendGameEventAndStartFading();
    }
}