﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class NewGame : MonoBehaviour
{
    [SerializeField] ScenesLoadDataSO _scenesLoadDataSO;
    [Space]
    [SerializeField] GameObject _resetDataPanel;

    [Header("Fade Transition")]
    [SerializeField] SceneTransitionFader _sceneTransitionFader;

    [Header("Menu Ambient")]
    public string playMenuAmbEventName;
    [Space]
    public string stopMenuAmbEventName;

    public WwiseAudioEmitterScript wwiseAudioEmitter;

    // assign game start data and load the game
    private void Start()
    {
        wwiseAudioEmitter.PlaySound();
    }

    public void StartNewGame()
    {
        if (_scenesLoadDataSO.currentScenesData.dataHasAssigned)
        {
            _resetDataPanel.SetActive(true);
        }
        else
        {
            ResetSavedScenesAndLoadNewGame();
        }
    }

    // reset data in scenesdataSO and start the game
    public void ResetSavedScenesAndLoadNewGame()
    {
        SaveAndLoadManager.Instance.DeleteDirectory(SaveAndLoadManager.Instance.localSavesDirectory);

        _scenesLoadDataSO.SetNewGameLoadData();

        SaveAndLoadManager.Instance.Save(_scenesLoadDataSO.saveFileName, _scenesLoadDataSO.currentScenesData);

        _sceneTransitionFader.SendGameEventAndStartFading();

        StopMenuAmbient();
    }

    public void StopMenuAmbient()
    {
        wwiseAudioEmitter.StopSound();
    }
}