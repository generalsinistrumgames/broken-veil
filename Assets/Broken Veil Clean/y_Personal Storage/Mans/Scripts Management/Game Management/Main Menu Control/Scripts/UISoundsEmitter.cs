using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISoundsEmitter : MonoBehaviour
{
    [Header("Play Btn Events")]
    public AK.Wwise.Event playClickBtnEvent;
    [Space]
    public AK.Wwise.Event playHoverBtnEvent;

    [Header("Stop Btn Events")]
    public AK.Wwise.Event stopClickBtnEvent;
    [Space]
    public AK.Wwise.Event stopHoverBtnEvent;

    #region Btn Events
    public void PlayBtnClickSound()
    {
        playClickBtnEvent.Post(gameObject);
    }

    public void PlayBtnHoverSound()
    {
        playHoverBtnEvent.Post(gameObject);
    }

    public void StopBtnClickSound()
    {
        stopClickBtnEvent.Post(gameObject);
    }

    public void StopBtnHoverSound()
    {
        stopHoverBtnEvent.Post(gameObject);
    }

    #endregion
}