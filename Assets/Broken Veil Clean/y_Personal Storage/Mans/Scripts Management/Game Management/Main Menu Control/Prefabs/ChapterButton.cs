using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChapterButton : MonoBehaviour
{
    public ChapterDataSO chapter;
    [Space]
    public List<SavePointSO> chapterSavePoints;

    public static event System.Action<ChapterDataSO, List<SavePointSO>> OnSavePointCreate;

    public void GetChapterSavePoints()
    {
        for (int i = 0; i < chapter.chapterSavePoints.Count; i++)
        {
            chapterSavePoints.Add(chapter.chapterSavePoints[i]);
        }
    }

    public void CreateSavePoints()
    {
        OnSavePointCreate?.Invoke(chapter, chapterSavePoints);
    }
}