using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePointButton : MonoBehaviour
{
    public ChapterDataSO chapter;
    [Space]
    public SavePointSO savePoint;

    public static event System.Action<ChapterDataSO, SavePointSO> OnLoadSavePoint;

    public void LoadSavePoint()
    {
        OnLoadSavePoint?.Invoke(chapter, savePoint);
    }
}