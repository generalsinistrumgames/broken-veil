using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameEventSender : MonoBehaviour
{
    public static event System.Action<GameEvent> OnSendGameEvent;

    public GameEvent gameEvent;

    public void SendGameEvent()
    {
        OnSendGameEvent?.Invoke(gameEvent);
    }
}