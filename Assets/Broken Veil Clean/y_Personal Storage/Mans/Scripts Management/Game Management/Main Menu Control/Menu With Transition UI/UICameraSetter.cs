using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICameraSetter : MonoSingleton<UICameraSetter>
{
    public Camera UICamera;
    [Space]
    public Canvas gameCanvas;
   
    public void SetUICamera()
    {
        gameCanvas.worldCamera = UICamera;
    }
}