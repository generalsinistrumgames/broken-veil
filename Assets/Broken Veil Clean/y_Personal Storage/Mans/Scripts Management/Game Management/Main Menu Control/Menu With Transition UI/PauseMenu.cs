using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] Button _resumeButton;
    [Space]
    [SerializeField] Button _restartButton;
    [Space]
    [SerializeField] Button _quitButton;
    [Space]
    [SerializeField] Button _settingsButton;

    private void Start()
    {
        _resumeButton.onClick.AddListener(HandleResumeClicked);

        //_quitButton.onClick.AddListener(HandleQuitClicked);

        //_settingsButton.onClick.AddListener(HandleSettingsClicked);
    }

    private void HandleResumeClicked()
    {
        GameManager.Instance.TogglePause(GameManager.GameState.Running);
    }

    //private void HandleSettingsClicked()
    //{
    //    GameManager.Instance.CurrentGameState = GameManager.GameState.Settings;
    //}

    //private void HandleQuitClicked()
    //{
    //    GameManager.Instance.ExitGame();
    //}
}