using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTransitionFader : MonoBehaviour
{
    public bool usePressToContinue;
    [Space]
    public bool leaveUICameraActive;
    [Space]
    public GameEvent gameEventToTrigger;
 
    public void SendGameEventAndStartFading()
    {
        UIFader.Instance.FadeIn(gameEventToTrigger, usePressToContinue, leaveUICameraActive);
    }

    public void SendGameEventAndStartFading(GameEvent gameEvent)
    {
        UIFader.Instance.FadeIn(gameEvent, usePressToContinue, leaveUICameraActive);
    }

    public void SendGameEventAndStartFadingOut()
    {
        gameEventToTrigger?.Raise();

        UIFader.Instance.FadeOut();
    }
}