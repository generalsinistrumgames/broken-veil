using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class UIFader : MonoSingleton<UIFader>
{
    [Header("Refs")]
    [SerializeField] Camera _UICamera;
    [Space]
    public TextMeshProUGUI pressAnyKeyText;

    [Header("Animation Control")]
    [SerializeField] Animator _UIFaderAnimator;
    [Space]
    [SerializeField] string _fadeInAnimTriggerName;
    [Space]
    [SerializeField] string _fadeOutAnimTriggerName;

    [Header("Fading Control")]
    public bool usePressToContinue;
    [Space]
    public bool canPressBtnToContinue;
    [Space]
    public bool leaveCameraActive;
    [Space]
    public GameEvent fadeInEndEvent;
    [Space]
    public GameEvent OnActivateScenes;

    [Header("Fade Events")]
    public UnityEvent OnFadeOutEnd;
    [Space]
    public UnityEvent OnFadeInStart;
 
    //private void Update()
    //{
    //    if (canPressBtnToContinue & Input.anyKeyDown)
    //    {
    //        //GameManager.Instance.UpdateGameState(GameManager.GameState.Running);

    //        usePressToContinue = false;

    //        canPressBtnToContinue = false;

    //        FadeOut();
    //    }
    //}

    public void OnFadeInComplete()
    {
        //OnActivateScenes?.Raise();

        fadeInEndEvent?.Raise();

        //if (!usePressToContinue)
        //{
        //    FadeOut();

        //    return;
        //}

        FadeOut();

        //pressAnyKeyText.gameObject.SetActive(true);

        //canPressBtnToContinue = usePressToContinue;

        Debug.LogWarning("Fade In Complete");
    }

    public void OnFadeOutComplete()
    {
        //GameManager.Instance.UpdateGameState(GameManager.GameState.Running);

        //OnActivateScenes?.Raise();

        OnFadeOutEnd?.Invoke();

        //if (!leaveCameraActive)
        //{
        //    SetUICameraActive(false);
        //}

        Debug.LogWarning("Fade Out Complete");
    }

    public void FadeIn(GameEvent triggerEvent, bool usePressBtn, bool useCamera)
    {
        //GameManager.Instance.UpdateGameState(GameManager.GameState.Pregame);

        OnFadeInStart?.Invoke();

        //SetUICameraActive(true);

        //usePressToContinue = usePressBtn;

        //leaveCameraActive = useCamera;

        fadeInEndEvent = triggerEvent;

        _UIFaderAnimator.SetTrigger(_fadeInAnimTriggerName);

        Debug.LogWarning("Fade In Called");
    }

    public void FadeOut()
    {
        //pressAnyKeyText.gameObject.SetActive(false);

        _UIFaderAnimator.SetTrigger(_fadeOutAnimTriggerName);

        Debug.LogWarning("Fade Out Called");
    }

    public void SetUICameraActive(bool active)
    {
        _UICamera.gameObject.SetActive(active);
    }
}