using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoSingleton<GameManager>
{
    public enum GameState
    {
        Pregame,
        Running,
        Paused,
    }

    [SerializeField] GameState _currentGameState = GameState.Pregame;

    public GameState CurrentGameState
    {
        get { return _currentGameState; }

        set { _currentGameState = value; }
    }

    [SerializeField] public Events.GameStateEvent OnGameStateChanged;

    [Header("Instantiate Other Managers")]
    public GameObject[] systemPrefabs = new GameObject[] { };
    [Space]
    public List<GameObject> _instancedSystemPrefabs = new List<GameObject>();

    [Header("Game Initialization")]
    public UnityEvent OnGameInitialization;

    [Header("Game Over Control In Game")]
    public bool useGameOver = true;
    [Space]
    public UnityEvent OnGameOver;
  
    private void Start()
    {
        DontDestroyOnLoad(gameObject);

        //InstantiateSystemPrefabs();

        //UpdateGameState(GameState.Pregame);

        OnGameInitialization?.Invoke();
    }

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.Escape))
    //    {
    //        if (_currentGameState != GameState.Pregame)
    //        {
    //            TogglePause();
    //        }
    //    }
    //}

    void InstantiateSystemPrefabs()
    {
        if (systemPrefabs.Length > 0)
        {
            GameObject prefabInstance;

            for (int i = 0; i < systemPrefabs.Length; ++i)
            {
                prefabInstance = Instantiate(systemPrefabs[i], transform);

                _instancedSystemPrefabs.Add(prefabInstance);
            }
        }
    }

    // destroy all system managers instantiated by game manager
    protected override void OnDestroy()
    {
        base.OnDestroy();

        for (int i = 0; i < _instancedSystemPrefabs.Count; ++i)
        {
            Destroy(_instancedSystemPrefabs[i]);
        }

        _instancedSystemPrefabs.Clear();
    }

    public void UpdateGameState(GameState state)
    {
        GameState previousGameState = _currentGameState;

        _currentGameState = state;

        switch (_currentGameState)
        {
            case GameState.Pregame:
                //AkSoundEngine.WakeupFromSuspend();
                Time.timeScale = 0f;
                break;
            case GameState.Running:
                //AkSoundEngine.WakeupFromSuspend();
                Time.timeScale = 1.0f;
                break;
            case GameState.Paused:
                //AkSoundEngine.Suspend();        
                Time.timeScale = 0.0f;
                break;
        }

        OnGameStateChanged?.Invoke(_currentGameState, previousGameState);
    }

    public void TogglePause(GameState gameState)
    {
        UpdateGameState(gameState);

        //UpdateGameState(_currentGameState == GameState.Running ? GameState.Paused : GameState.Running);
    }

    #region Listen Events

    public void ResumeGame()
    {
        _currentGameState = GameState.Running;

        TogglePause(_currentGameState);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void GameOver(FloatVariable delay)
    {
        if (useGameOver)
        {
            Debug.LogError("Game Over Mother Fucka");

            var _gameOverYield = new WaitForSeconds(delay.Value);

            StartCoroutine(GameOverRoutine(_gameOverYield));
        }
        else
        {
            Debug.Log("Game Over Disabled");
        }
    }

    public IEnumerator GameOverRoutine(WaitForSeconds waitForSeconds)
    {
        yield return waitForSeconds;

        AkSoundEngine.StopAll();

        OnGameOver?.Invoke();
    }

    public void StopAllGameManagerCoroutines()
    {
        StopAllCoroutines();
    }
    #endregion
}