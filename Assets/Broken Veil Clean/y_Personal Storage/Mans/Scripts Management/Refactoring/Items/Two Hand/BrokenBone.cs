using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenBone : ThrowableItem
{
    [Header("Hand Targets")]
    [SerializeField] GameObject _rightHandTarget;
    [Space]
    [SerializeField] GameObject _leftHandTarget;
    [Space]
    [Range(0f, 1f)] public float _IKWeight;

    [Header("Hit Sound Events")]
    public string boneHitCeramicSoundEvent = "Play_chopping_bone_ceramic";
    [Space]
    public string boneHitGutsSoundEvent = "Play_chopping_bone_guts";
    [Space]
    public string boneHitMetalSoundEvent = "Play_chopping_bone_metal";
    [Space]
    public string boneHitPlasticSoundEvent = "Play_chopping_bone_plastic";

    [Header("Put Sound Events")]
    public string bonePutCeramicSoundEvent = "Play_chopping_bone_put_ceramic";
    [Space]
    public string bonePutGutsSoundEvent = "Play_chopping_bone_put_guts";
    [Space]
    public string bonePutMetalSoundEvent = "Play_chopping_bone_put_metal";

    [Header("Take Sound Events")]
    public string boneGetSoundEvent = "Play_chopping_bone_get";
    [Space]
    public string boneGetGutsSoundEvent = "Play_chopping_bone_get_guts";

    [Space]
    public float hitMagnitude = 1f;
    [Space]
    public float putMagnitude = 0.1f;

    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);

        if (other.TryGetComponent<OpenVentGrill>(out var openVentGrill))
        {
            openVentGrill.canTryToOpenVentGrill = false;

            Debug.LogError("pppspspdas");
        }
    }

    protected override void OnTriggerExit(Collider other)
    {
        base.OnTriggerExit(other);

        if (other.TryGetComponent<OpenVentGrill>(out var openVentGrill))
        {
            openVentGrill.canTryToOpenVentGrill = true;

            Debug.LogError("afafafawf");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > hitMagnitude && !itemInPlayerHand)
        {
            if (collision.gameObject.TryGetComponent<ItemHitCollisionSound>(out var itemHitCollisionSound))
            {
                switch (itemHitCollisionSound.objectMaterialType)
                {
                    case ItemHitCollisionSound.Type.ceramic:
                        AkSoundEngine.PostEvent(boneHitCeramicSoundEvent, gameObject);
                        break;
                    case ItemHitCollisionSound.Type.guts:
                        AkSoundEngine.PostEvent(boneHitGutsSoundEvent, gameObject);
                        break;
                    case ItemHitCollisionSound.Type.metal:
                        AkSoundEngine.PostEvent(boneHitMetalSoundEvent, gameObject);
                        break;
                    case ItemHitCollisionSound.Type.plastic:
                        AkSoundEngine.PostEvent(boneHitPlasticSoundEvent, gameObject);
                        break;
                    default:
                        AkSoundEngine.PostEvent(boneHitCeramicSoundEvent, gameObject);
                        break;
                }
            }
        }

        //if (collision.relativeVelocity.magnitude > putMagnitude && collision.relativeVelocity.magnitude < hitMagnitude)
        //{
        if (collision.gameObject.TryGetComponent<FloorType>(out var floorType))
        {
            switch (floorType.floorType)
            {
                case FloorType.Type.guts:
                    takeItemSoundName = boneGetGutsSoundEvent;

                    dropItemSoundName = bonePutGutsSoundEvent;
                    break;
                case FloorType.Type.ceramic:
                    takeItemSoundName = boneGetSoundEvent;

                    dropItemSoundName = bonePutCeramicSoundEvent;
                    break;
                case FloorType.Type.metal:
                    takeItemSoundName = boneGetSoundEvent;

                    dropItemSoundName = bonePutMetalSoundEvent;
                    break;
                default:
                    takeItemSoundName = boneGetSoundEvent;

                    dropItemSoundName = bonePutCeramicSoundEvent;
                    break;
            }
        }
        //}
    }

    protected override void Update()
    {
        base.Update();

        if (_IKEnabled)
        {
            //_playerItemsIK._rightArm.weight = _IKWeight; // ��� ��������� ������� ���� � �������, ����� ����������� � ��������� �������� � _IKEnableWeight

            _playerItemsIK._rightArm.data.target.position = _rightHandTarget.transform.position;

            _playerItemsIK._rightArm.data.target.rotation = _rightHandTarget.transform.rotation;

            _playerItemsIK._leftArm.data.target.position = _leftHandTarget.transform.position;

            _playerItemsIK._leftArm.data.target.rotation = _leftHandTarget.transform.rotation;
        }
    }
}