using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : ThrowableItem
{
    [Header("Hand Targets")]
    [SerializeField] GameObject _rightHandTarget;
    [Space]
    [SerializeField] GameObject _leftHandTarget;

    [Header("Hit Sound Events")]
    public string ballHitSoundEvent = "Play_playground_ball_hit";
    [Space]
    public string ballHitMetalSoundEvent = "Play_playground_ball_metal";

    [Header("Hit Magnitudes")]
    public float playerHitBallMagnitude = 1f;
    [Space]
    public float hitMagnitude = 1f;

    protected override void Update()
    {
        base.Update();

        if (_IKEnabled)
        {
            _playerItemsIK._leftArm.data.target.position = _leftHandTarget.transform.position;

            _playerItemsIK._rightArm.data.target.position = _rightHandTarget.transform.position;

            _playerItemsIK._leftArm.data.target.rotation = _leftHandTarget.transform.rotation;

            _playerItemsIK._rightArm.data.target.rotation = _rightHandTarget.transform.rotation;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<Player>(out var player))
        {
            if (collision.relativeVelocity.magnitude > playerHitBallMagnitude)
            {
                AkSoundEngine.PostEvent(ballHitSoundEvent, gameObject);
            }
        }

        if (collision.relativeVelocity.magnitude > hitMagnitude && !itemInPlayerHand)
        {
            AkSoundEngine.PostEvent(ballHitSoundEvent, gameObject);
        }

        //if (collision.gameObject.TryGetComponent<ItemHitCollisionSound>(out var itemHitCollisionSound))
        //{
        //    if (collision.relativeVelocity.magnitude > hitMagnitude)
        //    {
        //        switch (itemHitCollisionSound.objectMaterialType)
        //        {
        //            case ItemHitCollisionSound.Type.metal:
        //                AkSoundEngine.PostEvent(ballHitMetalSoundEvent, gameObject);
        //                break;
        //        }
        //    }
        //}
    }
}