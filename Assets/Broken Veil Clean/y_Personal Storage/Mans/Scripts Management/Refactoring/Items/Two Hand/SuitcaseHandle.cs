using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuitcaseHandle : ThrowableItem
{
    [Header("Hand Targets")]
    [SerializeField] GameObject _rightHandTarget;
    [Space]
    [SerializeField] GameObject _leftHandTarget;

    [Header("Sound Events")]
    public string suitcaseHandleDrop = "Play_refrigerator_hook_hit";
    [Space]
    public float hitMagnitude = 1f;

    protected override void Update()
    {
        base.Update();

        if (_IKEnabled)
        {
            _playerItemsIK._leftArm.data.target.position = _leftHandTarget.transform.position;

            _playerItemsIK._rightArm.data.target.position = _rightHandTarget.transform.position;

            _playerItemsIK._leftArm.data.target.rotation = _leftHandTarget.transform.rotation;

            _playerItemsIK._rightArm.data.target.rotation = _rightHandTarget.transform.rotation;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > hitMagnitude && !itemInPlayerHand)
        {
            if (collision.gameObject.TryGetComponent<FloorType>(out var floorType))
            {
                AkSoundEngine.PostEvent(suitcaseHandleDrop, gameObject);
            }
        }
    }
}