﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoop : ThrowableItem
{
    [Header("Hand Targets")]
    [SerializeField] GameObject _rightHandTarget;
    [Space]
    [SerializeField] GameObject _leftHandTarget;
    [Space]
    [Range(0f, 1f)] [SerializeField] float _IKWeight;

    [Header("Hit Sound Events")]
    public string scoopHitMetalSoundEvent = "Play_scoop_metal_hit";
    [Space]
    public string scoopHitSoundEvent = "Paly_playground_scoop_drop";
    [Space]
    public float hitMagnitude = 1f;

    protected override void Update()
    {
        base.Update();

        if (_IKEnabled)
        {
            //_playerItemsIK._rightArm.weight = _IKWeight; // для настройки точного веса в апдейти, после закоммитить и перенести значение в _IKEnableWeight

            _playerItemsIK._leftArm.data.target.position = _leftHandTarget.transform.position;

            _playerItemsIK._rightArm.data.target.position = _rightHandTarget.transform.position;


            _playerItemsIK._rightArm.data.target.rotation = _rightHandTarget.transform.rotation;

            _playerItemsIK._leftArm.data.target.rotation = _leftHandTarget.transform.rotation;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > hitMagnitude && !itemInPlayerHand)
        {
            if (collision.gameObject.TryGetComponent<ItemHitCollisionSound>(out var itemHitCollisionSound))
            {
                AkSoundEngine.PostEvent(scoopHitMetalSoundEvent, gameObject);
            }
            else
            {
                AkSoundEngine.PostEvent(scoopHitSoundEvent, gameObject);
            }         
        }
    }
}