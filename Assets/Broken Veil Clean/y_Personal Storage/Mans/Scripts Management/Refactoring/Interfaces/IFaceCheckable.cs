﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFaceCheckable
{
    void CheckFacing(Transform from, Transform target);
}