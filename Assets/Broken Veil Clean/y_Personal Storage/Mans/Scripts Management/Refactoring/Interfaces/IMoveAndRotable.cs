﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMoveAndRotable
{
    bool MoveTowards(Transform from, Transform target, float moveSpeed = 0.1f, float offset = 0.1f, bool moveUp = false);
    bool RotateTowards(Transform from, Transform target, float rotationSpeed = 1f, bool rotateXAngle = false);  
}