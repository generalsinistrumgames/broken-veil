using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Enemy : MonoBehaviour
{
    public static Action OnEnemyPlayerDetect;

    [Header("Enemy")]
    [SerializeField] protected Animator _enemyAnimator;
    [Space]
    [SerializeField] protected Rigidbody _enemyRbody;
    [Space]
    [SerializeField] protected Transform _enemyTransform;
    [Space]
    protected Collider enemyEyesCollider;
    [Space]
    public float _detectDelay;
    [Space]
    [SerializeField] protected bool _playerWasDetected;
    [Space]
    public bool _playerWasCatched;

    public bool PlayerWasDetected
    {
        get { return _playerWasDetected; }

        set { _playerWasDetected = value; }
    }

    public virtual void Start()
    {
        _enemyTransform = transform;

        _enemyAnimator = GetComponent<Animator>();

        _enemyRbody = GetComponent<Rigidbody>();

        if (enemyEyesCollider == null)
        {
            var colliders = GetComponents<Collider>();

            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].isTrigger)
                {
                    enemyEyesCollider = colliders[i];
                }
            }
        }
    }

    public void EnemyDetectedPlayer()
    {
        _playerWasDetected = true;

        StartCoroutine(DelayPlayerDetectionCoroutine());
    }

    public virtual IEnumerator DelayPlayerDetectionCoroutine()
    {
        yield return new WaitForSeconds(_detectDelay);

        _playerWasCatched = true;

        Debug.Log("Player Was Detected " + "By " + gameObject.name);
    }
}