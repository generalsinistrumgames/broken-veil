using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class SleepingBoy : MonoBehaviour
{
    public enum SleepState
    {
        sleepLeft,
        sleepRight
    }
    public enum TurnState
    {
        turnLeft,
        turnRight
    }

    [SerializeField] SleepState sleepState;
    [Space]
    [SerializeField] TurnState turnState;
    [Space]
    [SerializeField] Animator animator;
    [Space]
    [SerializeField] int turnTimer;
    [Space]
    public bool flipTurns;
    [Space]
    public int defaultTurnDelay;

    void Start()
    {
        animator = GetComponent<Animator>();

        defaultTurnDelay = turnTimer;

        if (sleepState == SleepState.sleepLeft)
        {
            animator.SetTrigger("sleepLeft");
        }
        else
        {
            animator.SetTrigger("sleepRight");
        }


        StartCoroutine(StartCountdown());
    }

    public IEnumerator StartCountdown()
    {
        while (true)
        {
            //Debug.Log("Countdown: " + turnTimer);

            if (turnTimer >= 0)
            {
                turnTimer--;
            }
            else
            {           
                if (flipTurns)
                {
                    if (turnState == TurnState.turnLeft)
                    {
                        turnState = TurnState.turnRight;
                    }
                    else
                    {
                        turnState = TurnState.turnLeft;
                    }
                }
                         
                animator.SetTrigger(turnState.ToString());

                turnTimer = defaultTurnDelay * 2;

                turnTimer = Random.Range(defaultTurnDelay / 2, defaultTurnDelay);

                // remove this shit
                flipTurns = true;
            }

            yield return new WaitForSeconds(1.0f);
        }
    }
}