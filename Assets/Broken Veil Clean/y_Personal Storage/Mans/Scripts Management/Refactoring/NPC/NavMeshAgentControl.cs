﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class NavMeshAgentControl : MonoBehaviour
{
    public enum SpeedType
    {
        walk,
        run
    }

    public SpeedType speedType;
    [Space]
    public NavMeshAgent agent;
    [Space]
    public Animator animator;
    [Space]
    public Rigidbody rb;
    [Space]
    public bool targetDistanceReached;
    [Space]
    public float forwardAmount;
    [Space]
    public float turnAmount;
    [Space]
    public float stationaryTurnSpeed;
    [Space]
    public float movingTurnSpeed;
    [Space]
    public float animSpeedMultiplier;
    [Space]
    public float moveSpeedMultiplier;
    [Space]
    public float turnSpeedMultiplier;
    [Space]
    public float moveAcceleratePerSecond;
    [Space]
    private float acceleration = 0;
    [Space]
    public bool useIdleStopAnim = true;
    [Space]
    public float stopAnimValue;

    void Start()
    {
        if (agent == null)
        {
            agent = GetComponent<NavMeshAgent>();
        }

        animator = GetComponent<Animator>();

        rb = GetComponent<Rigidbody>();

        //agent.updateRotation = false;
    }

    //private void Update()
    //{
    //    CheckIfDistanceReached();
    //}

    public void SetAgentPathDestination(Vector3 target)
    {
        agent.SetDestination(target);

        if (agent.hasPath)
        {
            Move(agent.velocity);
        }

        CheckIfDistanceReached();
    }

    public bool CheckIfDistanceReached()
    {
        if (!agent.pathPending)
        {
            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                if (!agent.hasPath || agent.velocity.magnitude == 0f)
                {
                    //forwardAmount -= stopAcceleration * Time.deltaTime;

                    //forwardAmount = Mathf.Min(stopAcceleration, forwardAmount);

                    if (useIdleStopAnim)
                    {
                        animator.SetFloat("Forward", 0);
                    }
                    else
                    {
                        animator.SetFloat("Forward", forwardAmount);
                    }

                    targetDistanceReached = true;

                    return true;
                }
            }
        }

        targetDistanceReached = false;

        return false;
    }

    public void Move(Vector3 move)
    {
        // convert the world relative moveInput vector into a local-relative
        // turn amount and forward amount required to head in the desired
        // direction.
        if (move.magnitude > 1f) move.Normalize();
        move = transform.InverseTransformDirection(move);

        acceleration += moveAcceleratePerSecond * Time.deltaTime;

        if (useIdleStopAnim)
        {
            forwardAmount = Mathf.Min(acceleration, move.z * moveSpeedMultiplier);
        }
        else
        {
            forwardAmount = Mathf.Min(acceleration, moveSpeedMultiplier);
        }

        turnAmount = Mathf.Atan2(move.x, move.z) * turnSpeedMultiplier;

        //CheckIfDistanceReached(forwardAmount);

        ApplyExtraTurnRotation();

        // send input and other state parameters to the animator
        UpdateAnimator(move);
    }

    void ApplyExtraTurnRotation()
    {
        // help the character turn faster (this is in addition to root rotation in the animation)
        float turnSpeed = Mathf.Lerp(stationaryTurnSpeed, movingTurnSpeed, forwardAmount);
        transform.Rotate(0, turnAmount * turnSpeed * Time.deltaTime, 0);
    }

    void UpdateAnimator(Vector3 move)
    {
        // update the animator parameters
        animator.SetFloat("Forward", forwardAmount, 0.1f, Time.deltaTime);
        animator.SetFloat("Turn", turnAmount, 0.1f, Time.deltaTime);

        //the anim speed multiplier allows the overall speed of walking/ running to be tweaked in the inspector,
        // which affects the movement speed because of the root motion.
        //if (move.magnitude > 0)
        //{
        //    animator.speed = animSpeedMultiplier;
        //}
        //else
        //{
        //    animator.speed = 1;
        //}
    }

    //public void OnAnimatorMove()
    //{
    //    // we implement this function to override the default root motion.
    //    // this allows us to modify the positional speed before it's applied.
    //    if (Time.deltaTime > 0)
    //    {
    //        Vector3 v = (animator.deltaPosition * moveSpeedMultiplier) / Time.deltaTime;

    //        // we preserve the existing y part of the current velocity.
    //        v.y = rb.velocity.y;
    //        rb.velocity = v;
    //    }
    //}
}