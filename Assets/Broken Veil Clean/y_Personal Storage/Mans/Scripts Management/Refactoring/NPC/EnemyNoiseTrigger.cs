﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyNoiseTrigger : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] EnemyWithFOV _enemyWithFOV;
    [Space]
    [SerializeField] Player _player;
    [Space]
    [SerializeField] Animator _playerAnimator;
    [Space]
    [SerializeField] Transform _playerTransform;

    [Header("Noise Trigger")]
    [SerializeField] bool _playerWasHeard;
    [Space]
    [SerializeField] bool _playerWithinHearingRadius;
    [Space]
    [SerializeField] string _playerSneakStateName;
    [Space]
    [SerializeField] int _playerSneakStateHashedValue;

    public void Start()
    {
        if (_enemyWithFOV == null)
        {
            _enemyWithFOV = GetComponentInParent<EnemyWithFOV>();
        }

        _playerSneakStateHashedValue = Animator.StringToHash(_playerSneakStateName);
    }

    public void Update()
    {
        if (_playerWithinHearingRadius && !_playerWasHeard)
        {
            CheckIfPlayerDoesntCrawling();
        }

    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (_playerAnimator == null)
            {
                _playerAnimator = playerScript.animator;

                _player = playerScript;

                _playerTransform = playerScript.transform;
            }

            _playerWithinHearingRadius = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            _playerWithinHearingRadius = false;
        }
    }

    public void CheckIfPlayerDoesntCrawling()
    {
        if (_playerAnimator.GetCurrentAnimatorStateInfo(0).fullPathHash != _playerSneakStateHashedValue)
        {
            _playerWasHeard = true;

            if (!_enemyWithFOV.PlayerWasDetected)
            {
                _enemyWithFOV._player = _player;

                _enemyWithFOV._playerTransform = _playerTransform;

                _enemyWithFOV.EnemyDetectedPlayer();

                Debug.Log("Player Was Heard");
            }
            else
            {
                this.enabled = false;
            }
        }
    }
}