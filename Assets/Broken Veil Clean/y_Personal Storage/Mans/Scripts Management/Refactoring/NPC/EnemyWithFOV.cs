﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyWithFOV : Enemy
{
    [Header("Enemy With FOV")]
    public Player _player;
    [Space]
    public Transform _playerTransform;
    [Space]
    public bool _enemyFOVEnabled = true;
    [Space]
    [SerializeField] protected bool _playerWithinFOVRadius;

    [Header("Ray Control")]
    [SerializeField] Vector3 rayOffset;

    protected virtual void Update()
    {
        if (_enemyFOVEnabled && !_playerWasDetected)
        {
            if (_playerWithinFOVRadius)
            {
                Vector3 direction = (_playerTransform.position + Vector3.up * 0.5f) - (_enemyTransform.position + rayOffset);

                Ray ray = new Ray(_enemyTransform.position + rayOffset, direction);

                RaycastHit hit;

                Physics.Raycast(ray, out hit);

                Debug.DrawRay(_enemyTransform.position + rayOffset, direction, Color.red);

                Debug.Log(hit.transform.name + " Detected");

                if (hit.transform.CompareTag("Player"))
                {               
                    _enemyFOVEnabled = false;

                    EnemyDetectedPlayer();
                }
            }
        }
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (_player == null)
            {
                _player = playerScript;

                _playerTransform = playerScript.transform;
            }

            _playerWithinFOVRadius = true;
        }
    }

    protected virtual void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            _playerWithinFOVRadius = false;
        }
    }
}