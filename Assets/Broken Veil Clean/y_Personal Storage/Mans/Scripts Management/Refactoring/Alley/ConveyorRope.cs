using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorRope : MonoBehaviour
{
    public Animator ropeAnimator;
    [Space]
    public List<Rigidbody> rbJoints;
    [Space]
    public PointForHand pointForHand;

    private void Start()
    {
        if (ropeAnimator == null)
        {
            ropeAnimator.GetComponent<Animator>();
        }
    }
}