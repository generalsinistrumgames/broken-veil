using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropPigCarcass : MonoBehaviour
{
    [Header("Drop Obj")]
    public float destroyPigDelay;
    [Space]
    public float destroyRopeDelay;
    [Space]
    public Animator farshAnimator;

    [Header("Create Pig")]
    public ConveyorControl conveyorControl;

    [Header("Player Death")]
    public GrindPlayerDeath playerDeathFromRopeFall;

    [Header("Play Sound Event")]
    public string choppingSoundEvent = "Play_chopping_pig";

    private void Start()
    {
        if (conveyorControl == null)
        {
            conveyorControl = GetComponentInParent<ConveyorControl>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<ConveyorPig>(out var conveyorPig))
        {
            foreach (var joint in conveyorPig.rbJoints)
            {
                joint.isKinematic = true;

                joint.velocity = Vector3.zero;
            }

            conveyorPig.transform.parent = null;

            if (!conveyorPig.pigAnimator.enabled)
            {
                conveyorPig.pigAnimator.enabled = true;
            }

            conveyorPig.pigAnimator.SetTrigger("Grind");

            StartCoroutine(GrindPig(conveyorPig.gameObject));
        }

        if (other.TryGetComponent<ConveyorRope>(out var conveyorRope))
        {
            conveyorRope.transform.parent = null;

            foreach (var rbJoint in conveyorRope.rbJoints)
            {
                rbJoint.isKinematic = false;

                rbJoint.constraints = RigidbodyConstraints.None;

                rbJoint.velocity = Vector3.zero;
            }

            if (conveyorRope.pointForHand.playerWithinRopeCollider && conveyorRope.pointForHand.playerController.hangOnHook)
            {
                StartCoroutine(GrindPlayer());
            }

            StartCoroutine(GrindRope(conveyorRope.gameObject));
        }
    }

    public IEnumerator GrindPlayer()
    {
        playerDeathFromRopeFall.SetPlayerGrinderDeath();

        yield return new WaitForSeconds(1f);

        farshAnimator.Play("Grind", -1, 0f);
    }

    public IEnumerator GrindPig(GameObject hookInternalObj)
    {
        AkSoundEngine.PostEvent(choppingSoundEvent, gameObject);

        yield return new WaitForSeconds(destroyPigDelay);

        farshAnimator.SetTrigger("Grind Is On");

        Destroy(hookInternalObj);
    }

    public IEnumerator GrindRope(GameObject hookInternalObj)
    {
        yield return new WaitForSeconds(destroyRopeDelay);

        Destroy(hookInternalObj);
    }
}