using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;

public class PlayerOnRopeChecker : MonoBehaviour
{
    PlayerController _playerController;
    [Space]
    public ConveyorControl conveyorControl;
    [Space]
    public bool checkIfPlayerOnRope = true;
    [Space]
    [SerializeField] bool _playerWithinCollider;
    [Space]
    public float releaseRopeDelayValue;

    [Header("Play Sound Event")]
    public string dropBodySoundEvent = "Play_refrigerator_body";

    [Header("Grab Dead Body Rope Hint")]
    public GameObject grabRopeHint;

    private void Update()
    {
        if (_playerWithinCollider && _playerController.hangOnHook && checkIfPlayerOnRope)
        {         
            checkIfPlayerOnRope = false;

            StartCoroutine(ReleaseRopeDelay());
        }   
    }

    public IEnumerator ReleaseRopeDelay()
    {
        grabRopeHint.SetActive(false);

        yield return new WaitForSeconds(releaseRopeDelayValue);

        conveyorControl.DropDeadBody();

        AkSoundEngine.PostEvent(dropBodySoundEvent, gameObject);

        _playerController.hangOnHook = false;

        enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<PlayerController>(out var playerController))
        {
            if (_playerController == null)
            {
                _playerController = playerController;
            }

            _playerWithinCollider = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<PlayerController>(out var playerController))
        {
            _playerWithinCollider = false;
        }
    }
}