using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushWoodPlank : MonoBehaviour
{
    MoveAndRotateTowards moveAndRotate = new MoveAndRotateTowards();

    [Header("Refs")]
    [SerializeField] CutSceneManager _cutSceneManager;
    [Space]
    [SerializeField] float _moveSpeed, _rotateSpeed;
    [Space]
    [SerializeField] PopUpHintShower pushDeskHintShower;
    [Space]
    [SerializeField] PopUpHintShower holdCtrlHint;

    [Header("Player Refs")]
    [SerializeField] Player _player;
    [Space]
    [SerializeField] Animator _playerAnimator;

    [Header("Desk Control")]
    [SerializeField] bool _playerInCollider;
    [Space]
    [SerializeField] bool _plankPushed;
    [Space]
    [SerializeField] Animation _anim;
    [Space]
    [SerializeField] BoxCollider _fenceCollider;
    [Space]
    [SerializeField] Transform _targetPos;
    [Space]
    [SerializeField] Quaternion _targetRot;

    [Header("Sound Event")]
    [SerializeField] string pushEventName;

    void Start()
    {
        _anim = GetComponent<Animation>();
    }

    void Update()
    {
        if (_playerInCollider && !_plankPushed)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && _player.PlayerIsGrounded())
            {
                _plankPushed = true;

                StartCoroutine(PushDesk());
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            if (_player == null)
            {
                _player = player;

                _playerAnimator = player.GetComponent<Animator>();
            }

            _playerInCollider = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            _playerInCollider = false;
        }
    }
   
    public IEnumerator PushDesk()
    {
        _player.DisablePlayerMovement(true);

        _player.enabled = false;

        // ignore fence collider
        _fenceCollider.enabled = false;

        bool targetPosReached = false;

        bool targetRotReached = false;

        _targetPos.position = new Vector3(_targetPos.position.x, _player.transform.position.y, _targetPos.position.z);

        while (!targetPosReached || !targetRotReached)
        {
            if (!targetPosReached)
            {
                targetPosReached = moveAndRotate.MoveToTargetPosition(_player.transform, _targetPos, _moveSpeed);
            }

            if (!targetRotReached)
            {
                targetRotReached = moveAndRotate.RotateToTargetRotation(_player.transform, _targetRot, _rotateSpeed);
            }
         
            yield return null;
        }

        _playerAnimator.GetComponent<AnimatorSwapController>().SwapAnimator(PlayerAnimatorControllers.secondaryAnimator);

        _playerAnimator.SetTrigger("Push");

        AkSoundEngine.PostEvent(pushEventName, gameObject);

        yield return new WaitForSeconds(0.5f);

        _anim.Play(_anim.clip.name);

        yield return new WaitForSeconds(1f);

        _playerAnimator.GetComponent<AnimatorSwapController>().SwapAnimator(PlayerAnimatorControllers.defaultAnimator);

        _player.EnablePlayerMovement(false);

        _player.enabled = true;

        pushDeskHintShower.gameObject.SetActive(false);

        holdCtrlHint.gameObject.SetActive(true);

        this.enabled = false;
    }
}