using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuitcaseHandleTrigger : MonoBehaviour
{
    [Header("Playable Control")]
    [SerializeField] UnityEngine.Playables.PlayableDirector _fixHandlePlayable;

    [Header("Move To Target")]
    [SerializeField] MoveAndRotateTowards moveAndRotate = new MoveAndRotateTowards();
    [Space]
    [SerializeField] float _moveSpeed, _rotateSpeed;
    [Space]
    [SerializeField] Transform _targetPos;
    [Space]
    [SerializeField] Quaternion _targetRot;

    [Header("Refs")]
    [SerializeField] GameObject _suitCase;
    [Space]
    [SerializeField] Rigidbody _suitCaseRb;
    [Space]
    [SerializeField] Player _player;
    [Space]
    [SerializeField] SuitcaseHandle _suitcaseHandle;
    [Space]
    [SerializeField] Transform _suitcaseHandleTargetTransform;
    [Space]
    [SerializeField] GameObject _suitcaseHandleParent;
    [Space]
    [SerializeField] Animator _playerAnimator;
    [Space]
    [SerializeField] bool _playerInCollider;
    [Space]
    [SerializeField] bool isHandleFixed;

    void Update()
    {
        if (_playerInCollider && !isHandleFixed)
        {
            if (_player.PlayerIsGrounded() && _suitcaseHandle.itemInPlayerHand)
            {
                StartCoroutine(FixHandleCoroutine());
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            if (_player == null)
            {
                _player = player;

                _playerAnimator = player.GetComponent<Animator>();

                PlayableDirectorUtility.DynamicAnimatorBinding(_fixHandlePlayable, _playerAnimator, "Player");
            }

            _playerInCollider = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            _playerInCollider = false;
        }
    }

    public IEnumerator FixHandleCoroutine()
    {
        isHandleFixed = true;

        _suitcaseHandle._canDropItem = false;

        _suitcaseHandle._canTakeItem = false;

        _player.DisablePlayerMovement(true);

        yield return MovePlayerToTargetPosition();

        _fixHandlePlayable.Play();
    }

    public void SetHandleTargetTransform()
    {
        _suitcaseHandle.GetComponent<Collider>().isTrigger = true;

        _suitcaseHandle._playerItemsIK._leftArm.weight = 0;

        _suitcaseHandle._playerItemsIK._rightArm.weight = 0;

        _suitcaseHandle.RemoveItem(_suitcaseHandleParent.transform);

        //_suitcaseHandle._knockItemFromHands = true;

        //_suitcaseHandle.transform.parent = _suitcaseHandleParent.transform;

        _suitcaseHandle.transform.localPosition = _suitcaseHandleTargetTransform.localPosition;

        _suitcaseHandle.transform.localRotation = _suitcaseHandleTargetTransform.localRotation;
    }

    public IEnumerator MovePlayerToTargetPosition()
    {
        bool targetPosReached = false;

        bool targetRotReached = false;

        while (!targetPosReached || !targetRotReached)
        {
            if (!targetPosReached)
            {
                targetPosReached = moveAndRotate.MoveToTargetPosition(_player.transform, _targetPos, _moveSpeed);
            }

            if (!targetRotReached)
            {
                targetRotReached = moveAndRotate.RotateToTargetRotation(_player.transform, _targetRot, _rotateSpeed);
            }

            yield return null;
        }
    }

    public void OnTimelineEnd()
    {
        //_brokenBone.transform.localPosition = _brokenTransform.localPosition;

        //_brokenBone.transform.localRotation = _brokenTransform.localRotation;
        _player.EnablePlayerMovement(false);

        _suitCaseRb.isKinematic = false;

        _suitCase.layer = LayerMask.NameToLayer("Dragged");

        Destroy(this);
    }
}