using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class OpenVentGrill : MonoBehaviour
{
    [Header("Playable Control")]
    [SerializeField] UnityEngine.Playables.PlayableDirector _ventgrillPlayable;

    [Header("Move To Target")]
    [SerializeField] MoveAndRotateTowards moveAndRotate = new MoveAndRotateTowards();
    [Space]
    [SerializeField] float _moveSpeed, _rotateSpeed;
    [Space]
    [SerializeField] Transform _targetPos;
    [Space]
    [SerializeField] Quaternion _targetRot;

    [Header("Refs")]
    [SerializeField] Player _player;
    [Space]
    [SerializeField] BrokenBone _brokenBone;
    [Space]
    [SerializeField] Transform _brokenTransform;
    [Space]
    [SerializeField] GameObject _brokenBoneAnimObject;
    [Space]
    [SerializeField] GameObject _ventClimbableCube;
    [Space]
    [SerializeField] Animator _playerAnimator;
    [Space]
    [SerializeField] Animator _ventGrillAnimator;

    [Header("Vent Grill Control")]
    [SerializeField] Rigidbody _ventGrillRb;
    [Space]
    [SerializeField] bool _playerInCollider;
    [Space]
    [SerializeField] bool _ventGrillOpening;
    [Space]
    public bool canTryToOpenVentGrill = true;
    [Space]
    [SerializeField] string _animTriggerName;

    [Header("Vent Hit Floor Sound")]
    public float hitMagnitude;
    [Space]
    public AK.Wwise.Event hitFloorEvent;

    void Update()
    {
        if (_playerInCollider && !_ventGrillOpening)
        {
            if (_player.PlayerIsGrounded() && _brokenBone.itemInPlayerHand)
            {
                StartCoroutine(OpenVentGrillCoroutine());
            }
            else if (Input.GetKeyDown(KeyCode.Mouse0) && _player.PlayerIsGrounded() && !_brokenBone.itemInPlayerHand)
            {
                if (canTryToOpenVentGrill)
                {
                    StartCoroutine(TryToOpenVentGrill());
                }        
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            if (_player == null)
            {
                _player = player;

                _playerAnimator = player.GetComponent<Animator>();

                PlayableDirectorUtility.DynamicAnimatorBinding(_ventgrillPlayable, _playerAnimator, "Player");
            }

            _playerInCollider = true;

            if (_brokenBone.itemInPlayerHand)
            {
                _brokenBone.canThrow = false;
            }
        }

        //if (other.gameObject.TryGetComponent<FloorType>(out var floorType))
        //{
        //    switch (floorType.floorType)
        //    {
        //        case FloorType.Type.ceramic:
        //            hitFloorEvent.Post(gameObject);
        //            break;
        //    }
        //}
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            _playerInCollider = false;

            if (_brokenBone.itemInPlayerHand)
            {
                _brokenBone.canThrow = true;
            }
        }
    }

    public IEnumerator OpenVentGrillCoroutine()
    {
        Debug.Log("Start Ventgrill Opennig");

        canTryToOpenVentGrill = false;

        _ventGrillOpening = true;

        _brokenBone._canDropItem = false;

        _brokenBone._canTakeItem = false;

        _player.DisablePlayerMovement(true);

        yield return MovePlayerToTargetPosition();

        _ventgrillPlayable.Play();

        _brokenBone._playerItemsIK._leftArm.weight = 0;

        _brokenBone._playerItemsIK._rightArm.weight = 0;

        _brokenBone.gameObject.SetActive(false);

        //_brokenBone.GetComponent<Renderer>().enabled = false;

        _brokenBoneAnimObject.SetActive(true);
    }

    public IEnumerator TryToOpenVentGrill()
    {
        Debug.Log("Trying Open Ventgrill");

        _ventGrillOpening = true;

        _player.DisablePlayerMovement();

        yield return MovePlayerToTargetPosition();

        _player.GetComponent<AnimatorSwapController>().SwapAnimator(PlayerAnimatorControllers.secondaryAnimator);

        _playerAnimator.SetTrigger("Open Vent Grill Hands");

        yield return new WaitForSeconds(6);

        _player.GetComponent<AnimatorSwapController>().SwapAnimator(PlayerAnimatorControllers.defaultAnimator);

        _player.EnablePlayerMovement();

        //_playerInCollider = false;

        _ventGrillOpening = false;
    }

    public IEnumerator MovePlayerToTargetPosition()
    {
        bool targetPosReached = false;

        bool targetRotReached = false;

        while (!targetPosReached || !targetRotReached)
        {
            //_targetPos.position = new Vector3(_targetPos.localPosition.x, _player.transform.localPosition.y, _targetPos.localPosition.z);

            if (!targetPosReached)
            {
                targetPosReached = moveAndRotate.MoveToTargetPosition(_player.transform, _targetPos, _moveSpeed);
            }

            if (!targetRotReached)
            {
                targetRotReached = moveAndRotate.RotateToTargetRotation(_player.transform, _targetRot, _rotateSpeed);
            }

            yield return null;
        }
    }

    public void OnTimelineEnd()
    {
        //_brokenBone.transform.localPosition = _brokenTransform.localPosition;

        //_brokenBone.transform.localRotation = _brokenTransform.localRotation;

        _brokenBone._playerItemsIK._leftArm.weight = 1;

        _brokenBone._playerItemsIK._rightArm.weight = 1;

        _brokenBoneAnimObject.SetActive(false);

        _brokenBone.gameObject.SetActive(true);

        //_brokenBone.GetComponent<Renderer>().enabled = true;

        _brokenBone._canDropItem = true;

        _player.EnablePlayerMovement(false);

        _ventClimbableCube.SetActive(true);

        //test
        _brokenBone._playerItemsIK._leftArm.weight = 0;

        _brokenBone._playerItemsIK._rightArm.weight = 0;

        Destroy(this);
    }
}