using System.Collections;
using UnityEngine;

public class ConveyorPanel : MonoBehaviour
{
    [SerializeField] ConveyorControl _conveyorControl;
    [Space]
    [SerializeField] bool _canPressBtn = true;
    [Space]
    [SerializeField] GameObject _button;
    [Space]
    [SerializeField] float _pushDelay;
    [Space]
    [SerializeField] Vector3 _buttonDefaultPos;
    [Space]
    [SerializeField] Vector3 _buttonPushedPos;
    [Space]
    [SerializeField] Color _defaultColor;
    [Space]
    [SerializeField] Color _activatedColor;

    [Header("Throw Hint")]
    public GameObject throwItemHint;

    MeshRenderer meshRenderer;

    WaitForSeconds pushWait;

    [Header("Play Sound Events")]
    public string buttonPressSoundEvent = "Play_refrigerator_button";
    [Space]
    public string conveyorStartSoundEvent = "Play_refrigerator_conveyor_start";
    [Space]
    public string conveyorFailSoundEvent = "Play_refrigerator_conveyor_fail";
 
    private void Start()
    {
        meshRenderer = transform.GetComponent<MeshRenderer>();

        pushWait = new WaitForSeconds(_pushDelay);

        _buttonDefaultPos = _button.transform.localPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Hook>(out var hookScript) && _canPressBtn)
        {
            _canPressBtn = false;

            AkSoundEngine.PostEvent(buttonPressSoundEvent, gameObject);

            StartCoroutine(PushButtonRoutine(_conveyorControl.deadBodyOnHook));
        }
    }

    IEnumerator PushButtonRoutine(bool isBodyOnHook)
    {
        meshRenderer.material.SetColor("_EmissiveColor", _activatedColor);

        _button.transform.localPosition = _buttonPushedPos;

        if (!isBodyOnHook)
        {
            Debug.Log("Starting Conveyor");

            throwItemHint.SetActive(false);

            _conveyorControl.StartConveyor();

            AkSoundEngine.PostEvent(conveyorStartSoundEvent, gameObject);

            _conveyorControl.conveyorIsWorking = true;

            Destroy(this);
        }
        else
        {
            _conveyorControl.PlayConveyorStuckReaction();

            AkSoundEngine.PostEvent(conveyorFailSoundEvent, gameObject);

            yield return pushWait;

            Debug.Log("Can't Start Conveyor, Remove The Dead Body");

            // reverse changes
            meshRenderer.material.SetColor("_EmissiveColor", _defaultColor);

            _button.transform.localPosition = _buttonDefaultPos;
          
            _canPressBtn = true;
        }
    }
}