using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulliesCutScene : MonoBehaviour
{
    [Header("Playable Control")]
    [SerializeField] UnityEngine.Playables.PlayableDirector _playableDirector;

    [Header("Player")]
    [SerializeField] Player _player;
    [Space]
    [SerializeField] Animator _playerAnimator;

    [Header("Refs")]
    [SerializeField] GameObject _dummyPlayer;
    [Space]
    [SerializeField] Transform _basementPosition;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            if (_player == null)
            {
                _player = player;

                _playerAnimator = player.GetComponent<Animator>();
            }

            if (_player.PlayerIsGrounded())
            {
                StartTimeline();
            }
        }
    }

    public void StartTimeline()
    {
        _player.DisablePlayerMovement();

        _playerAnimator.SetBool("Sneak", false);

        //_player.transform.parent = _dummyPlayer.transform;

        //_player.transform.localPosition = Vector3.zero;

        //_player.transform.localRotation = Quaternion.identity;

        //PlayableDirectorUtility.DynamicAnimatorBinding(_playableDirector, _playerAnimator, "Player");

        _player.GetComponent<PlayerRenderControl>().SetPlayerRender(false);

        _dummyPlayer.gameObject.SetActive(true);

        _playableDirector.Play();
    }

    public void TeleportPlayerInBasement()
    {
        _player.transform.position = _basementPosition.transform.position;

        _player.transform.rotation = _basementPosition.transform.rotation;
    }

    public void EnablePlayer()
    {
        _player.EnablePlayerMovement();

        _player.GetComponent<PlayerRenderControl>().SetPlayerRender(true);
    }
}