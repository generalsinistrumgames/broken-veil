using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatDropHandleTrigger : MonoBehaviour
{
    public Vector3 force;
    [Space]
    public ForceMode forceMode;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<SuitcaseHandle>(out var suitcaseHandle))
        {
            var rb = suitcaseHandle.GetComponent<Rigidbody>();

            rb.isKinematic = false;

            rb.AddForce(force, forceMode);

            Destroy(this);
        }
    }
}