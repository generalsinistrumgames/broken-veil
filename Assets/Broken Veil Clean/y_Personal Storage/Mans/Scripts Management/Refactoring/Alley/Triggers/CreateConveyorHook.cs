using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CreateConveyorHook : MonoBehaviour
{
    [Header("Create Pig")]
    public GameObject[] conveyorHooks;
    [Space]
    public ConveyorHook currentConveyorHook;
    [Space]
    public int hooksDroppedValue;
    [Space]
    public int createHookIndex;
    [Space]
    public int defaultChangeHookValue;
    [Space]
    public int minRopeHookWaitValue, maxRopeHookWaitValue;
    [Space]
    public bool swapDefaultRopeHookCreateValue;

    private void Start()
    {
        defaultChangeHookValue = minRopeHookWaitValue;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<ConveyorHook>(out var conveyorHook))
        {
            currentConveyorHook = conveyorHook;

            InstantiatePigCarcass();
        }
    }

    public void InstantiatePigCarcass()
    {
        RandomizeCreationPattern();

        var hook = Instantiate(conveyorHooks[createHookIndex], currentConveyorHook.transform);

        //hook.transform.SetParent(currentConveyorHook.transform, false);

        //currentConveyorHook.dropFromHookObject = hook;

        //currentConveyorHook.conveyorHookJoints = hook.GetComponent<ConveyorHookJoints>();
    }

    void RandomizeCreationPattern()
    {
        hooksDroppedValue++;

        createHookIndex = 0; // hook with pig

        if (hooksDroppedValue == defaultChangeHookValue)
        {
            createHookIndex = conveyorHooks.Length - 1;  // hook with rope

            swapDefaultRopeHookCreateValue = !swapDefaultRopeHookCreateValue;

            if (swapDefaultRopeHookCreateValue)
            {
                defaultChangeHookValue = maxRopeHookWaitValue;
            }
            else
            {
                defaultChangeHookValue = minRopeHookWaitValue;
            }

            hooksDroppedValue = 0; // clear dropped hooks count
        }
    }
}