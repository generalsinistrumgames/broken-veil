using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ConveyorHook : MonoBehaviour
{
    public enum ObjectOnHook
    {
        pig,
        rope
    }

    public CinemachineDollyCart cmCart;
    [Space]
    public ObjectOnHook objectOnHook;

    public float minSpeed, maxSpeed;

    private void Start()
    {
        cmCart = GetComponent<CinemachineDollyCart>();
    }

    private void Update()
    {
        if (cmCart.enabled)
        {
            cmCart.m_Speed = Random.Range(minSpeed, maxSpeed);
        }
       
        if (cmCart.m_Position >= 0.99)
        {
            cmCart.m_Position = 0;
        }
    }
}