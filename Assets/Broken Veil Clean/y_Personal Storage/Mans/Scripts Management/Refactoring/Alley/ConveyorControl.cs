using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ConveyorControl : MonoBehaviour
{
    [Header("Refs")]
    public GameObject hooksAndBodies;
    [Space]
    public GameObject stunnedHooksAndBodies;
    [Space]
    public bool conveyorImmidiatelyStarted;

    [Header("Dead Body Control")]
    public GameObject deadBody;
    [Space]
    public Animator deadBodyAnimator;
    [Space]
    public Renderer deadBodyHookRenderer;
    [Space]
    public GameObject deadBodyFallHook;
    [Space]
    public List<Collider> deadBodyAndRopeColliders;
    [Space]
    public List<Rigidbody> deadBodyAndRopeRigidbodies;
    [Space]
    public Rigidbody bodyAxisJoint;
    [Space]
    public GameObject deadBodyBoxCollider;

    [Header("Conveyor Controlling")]
    public bool conveyorIsWorking;
    [Space]
    public bool deadBodyOnHook = true;

    [Header("Conveyor Controlling")]
    public List<CinemachineDollyCart> cmStunnedCarts;

    [Header("Conveyor Stuck Reaction")]
    public string stuckAnimTrigerName;
    [Space]
    public Animator[] stuckAnimators;
    [Space]
    public float disableAnimatorsDelay;
    [Space]
    public Animator hooksCycleAnimator;
    [Space]
    public List<ConveyorPig> pigsOnConveyor;

    [Header("Conveyor Work Sounds")]
    public List<GameObject> conveyorWorkAmbients;

    public void StartConveyor()
    {
        StartCoroutine(ConveyorCoroutine());
    }

    public IEnumerator ConveyorCoroutine()
    {
        if (!deadBodyOnHook)
        {
            //hooksCycleAnimator.enabled = true;

            //hooksCycleAnimator.SetTrigger("Start Conveyor");

            foreach (var ambientObj in conveyorWorkAmbients)
            {
                ambientObj.SetActive(true);
            }

            foreach (var cart in cmStunnedCarts)
            {
                cart.enabled = true;
            }

            //foreach (var pig in pigsOnConveyor)
            //{
            //    foreach (var rb in pig.rbJoints)
            //    {
            //        rb.isKinematic = true;
            //    }

            //    pig.pigAnimator.enabled = true;

            //    pig.pigAnimator.SetTrigger("Move");
            //}
        }
        else
        {
            Debug.Log("Too Heavy To Start");

            yield break;
        }
    }

    [ContextMenu("Drop Body")]
    public void DropDeadBody()
    {
        // RECORD ANIMATION
        //HierarchyRecorder recorder = FindObjectOfType<HierarchyRecorder>();
        //recorder.record = true;

        deadBodyOnHook = false;

        deadBody.transform.parent = null;

        deadBodyHookRenderer.gameObject.transform.parent = null;

        bodyAxisJoint.isKinematic = false;

        StartCoroutine(ChangeDeadBodyCollider());
    }

    IEnumerator ChangeDeadBodyCollider()
    {
        deadBodyAnimator.enabled = true;

        deadBodyHookRenderer.enabled = false;

        deadBodyAnimator.SetTrigger("Dead Body Fall");

        deadBodyFallHook.SetActive(true);

        while (true)
        {
            if (deadBodyAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Dead Body Fall") && deadBodyAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.99f)
            {
                foreach (var collider in deadBodyAndRopeColliders)
                {
                    collider.enabled = false;
                }

                foreach (var rb in deadBodyAndRopeRigidbodies)
                {
                    rb.isKinematic = true;
                }

                deadBodyBoxCollider.SetActive(true);

                break;
            }

            yield return null;
        }
    }

    [ContextMenu("Immidiately Start Conveyor")]
    public void ImmidiatelyStartConveyor()
    {
        stunnedHooksAndBodies.gameObject.SetActive(false);

        conveyorImmidiatelyStarted = true;

        deadBodyOnHook = false;

        conveyorIsWorking = true;

        hooksAndBodies.gameObject.SetActive(true);

        foreach (var ambientObj in conveyorWorkAmbients)
        {
            ambientObj.SetActive(true);
        }
    }

    [ContextMenu("Conveyor Stuck Routine")]
    public void PlayConveyorStuckReaction()
    {
        StartCoroutine(ConveyorStuckReactionRoutine());
    }

    public IEnumerator ConveyorStuckReactionRoutine()
    {
        foreach (var pig in pigsOnConveyor)
        {
            foreach (var rb in pig.rbJoints)
            {
                rb.isKinematic = true;
            }
        }

        foreach (var animator in stuckAnimators)
        {
            animator.enabled = true;

            animator.SetTrigger(stuckAnimTrigerName);
        }

        yield return new WaitForSeconds(disableAnimatorsDelay);

        foreach (var animator in stuckAnimators)
        {
            //animator.SetTrigger(stuckAnimTrigerName);

            animator.enabled = false;
        }

        foreach (var pig in pigsOnConveyor)
        {
            foreach (var rb in pig.rbJoints)
            {
                rb.isKinematic = false;
            }
        }
    }
}