using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SecurityParametrsControl : MonoBehaviour
{
    [Header("Refs")]
    public Security security;
    [Space]
    public NavMeshAgent agent;

    [Header("Parametrs")]
    public float stoppingOffset;
    [Space]
    public float accelaration;

    //private void OnTriggerStay(Collider other)
    //{
    //    if (other.TryGetComponent<Player>(out var playerScript))
    //    {
    //        if (security.PlayerWasDetected)
    //        {
    //            if (agent.stoppingDistance != stoppingOffset)
    //            {
    //                agent.stoppingDistance = stoppingOffset;
    //            }
    //        }
    //    }
    //}

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (security.PlayerWasDetected)
            {
                if (agent.stoppingDistance != stoppingOffset)
                {
                    agent.stoppingDistance = stoppingOffset;
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (security.PlayerWasDetected)
            {
                agent.stoppingDistance = security.chasePlayerStoppingDistance;
            }
            else
            {
                agent.stoppingDistance = security.checkNoiseStopDistance;
            }
        }
    }
}