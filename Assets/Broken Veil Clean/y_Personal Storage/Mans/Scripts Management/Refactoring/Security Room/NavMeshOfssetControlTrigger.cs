﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshOfssetControlTrigger : MonoBehaviour
{
    [Header("Refs")]
    public Security security;
    [Space]
    public NavMeshAgent agent;

    [Header("On Triiger Enter Agent Offsets")]
    public Security.PlayerCatchPose playerCatchPose;
    [Space]
    public float stoppingOffset;

    void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (security.PlayerWasDetected)
            {
                //if (agent.stoppingDistance != stoppingOffset)
                //{
                    agent.stoppingDistance = stoppingOffset;

                    security.playerCatchPose = playerCatchPose;

                security.usePlayerCatchFromBackReaction = false;
                //}
            }     
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (security.PlayerWasDetected)
            {
                agent.stoppingDistance = security.chasePlayerStoppingDistance;
            }
            else
            {
                agent.stoppingDistance = security.checkNoiseStopDistance;
            }
          
            security.playerCatchPose = Security.PlayerCatchPose.low;

            security.usePlayerCatchFromBackReaction = true;
        }
    }
}