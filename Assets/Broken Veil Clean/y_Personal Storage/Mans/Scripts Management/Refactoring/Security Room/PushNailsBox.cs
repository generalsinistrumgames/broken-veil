﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class PushNailsBox : MonoBehaviour
{
    MoveAndRotateTowards _moveAndRotateTowards = new MoveAndRotateTowards();

    [Header("Refs")]
    [SerializeField] PlayableDirector _playableDirector;
    [Space]
    [SerializeField] Transform _moveToBoxOffset;
    [Space]
    [SerializeField] Player _player;
    [Space]
    [SerializeField] Transform _playerTransform;
    [Space]
    [SerializeField] bool _playerWithinCoollider;
    [Space]
    [SerializeField] bool _playableIsWorking;

    [Header("Animation Control")]
    [SerializeField] Animator _playerAnimator;
    [Space]
    [SerializeField] string _pushAnimTag;
    [Space]
    [SerializeField] string _pushAnimTriggerName;
    [Space]
    [SerializeField] float _rotationSpeed;
    [Space]
    [SerializeField] float _moveSpeed;
    [Space]
    [SerializeField] bool _animStateDelayEnds;
    [Space]
    [SerializeField] float _animNormalizedDelayValue;

    void Update()
    {
        if (_playerWithinCoollider && !_playableIsWorking)
        {
            if (_player.PlayerIsGrounded())
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    PlayTimeline();
                }
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            if (_player == null)
            {
                _player = playerScript;

                _playerAnimator = _player.GetComponent<Animator>();

                _playerTransform = _player.transform;
            }

            _playerWithinCoollider = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var playerScript))
        {
            _playerWithinCoollider = false;
        }
    }

    void PlayTimeline()
    {
        _playableIsWorking = true;

        _player.DisablePlayerMovement();

        StartCoroutine(RotateTowardsBox());
    }

    IEnumerator RotateTowardsBox()
    {
        bool targetPosReached = false;

        bool targetRotReached = false;

        while (!targetPosReached || !targetRotReached)
        {
            if (!targetPosReached)
            {
                Vector3 playerTargetPos = new Vector3(_moveToBoxOffset.position.x, _playerTransform.position.y, _moveToBoxOffset.position.z);

                targetPosReached = _moveAndRotateTowards.MoveToTargetPosition(_playerTransform, playerTargetPos, _moveSpeed);
            }

            if (!targetRotReached)
            {
                targetRotReached = _moveAndRotateTowards.RotateToTargetRotation(_playerTransform, _moveToBoxOffset.localRotation, _rotationSpeed);
            }

            yield return null;
        }

        StartCoroutine(PushingBoxCoroutine());
    }

    IEnumerator PushingBoxCoroutine()
    {
        //_playerAnimator.GetComponent<AnimatorSwapController>().SwapAnimator(PlayerAnimatorControllers.secondaryAnimator);

        foreach (var playableAssetOutput in _playableDirector.playableAsset.outputs)
        {
            if (playableAssetOutput.streamName.Equals("Player"))
            {
                _playableDirector.SetGenericBinding(playableAssetOutput.sourceObject, _playerAnimator);

            }
        }

        //_playerAnimator.SetTrigger(_pushAnimTriggerName);

        _playableDirector.Play();

        //while (!_animStateDelayEnds)
        //{
        //    CheckAnimationState(_pushAnimTag, _animNormalizedDelayValue);

        //    yield return null;
        //}

        yield return new WaitForSeconds(1f);

        //_playerAnimator.SetTrigger(_pushAnimTriggerName);

        _player.EnablePlayerMovement();
    }

    public void CheckAnimationState(string animationTag, float delayTime)
    {
        if (_playerAnimator.GetCurrentAnimatorStateInfo(0).IsTag(animationTag) && _playerAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= delayTime)
        {
            _animStateDelayEnds = true;
        }
    }
}