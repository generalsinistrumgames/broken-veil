﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TVRemote : InteractiveItem
{
    [Header("TV Control")]
    public UnityEvent tvRemoteBtnPress;
    [Space]
    public tv tvNoise;
    [Space]
    [SerializeField] bool _TvIsOn = true;
    [Space]
    public GameObject pressTvRemoteHint;
    [Space]
    public GameObject wwiseTvEmiter;

    [Header("TV Remote Sound")]
    public string TVRemoteEventPlayName;
    [Space]
    public string TVRemoteEventStopName;

    [Header("Lights Control")]
    [SerializeField] Light[] _targetLights;
    [Space]
    [SerializeField] Color _tvEnabledColor;
    [Space]
    [SerializeField] Color _tvDisabledColor;

    [Header("Press Animation")]
    [SerializeField] string _animationTag;
    [Space]
    [SerializeField] string _animationTriggerName;
    [Space]
    [SerializeField] float _animPressDelayTime;

    [Header("Foot IK")]
    [SerializeField] Transform _rightFootTarget;
    [Space]
    public float setIKSpeed;
    [Space]
    public float unsetIKSpeed;
    [Space]
    public float IKWeightValue;

    private void Update()
    {
        if (_playerWithinItemCollider && _TvIsOn)
        {
            if (_player.PlayerIsGrounded())
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    _playerWithinItemCollider = false;

                    _TvIsOn = false;

                    pressTvRemoteHint.SetActive(false);

                    StartCoroutine(PressCoroutine());
                }
            }
        }
    }

    public void TVRemoteBtnPressed()
    {
        tvRemoteBtnPress?.Invoke();

        DisableLights();
    }

    public void EnableTvRemote()
    {
        _TvIsOn = true;

        _itemTriggerCollider.enabled = true;
    }

    void PlayOnSound()
    {
        AkSoundEngine.PostEvent(TVRemoteEventPlayName, wwiseTvEmiter);
    }

    void PlayOffSound()
    {
        AkSoundEngine.PostEvent(TVRemoteEventStopName, wwiseTvEmiter);
    }

    public void EnableLights()
    {
        for (int i = 0; i < _targetLights.Length; ++i)
        {
            _targetLights[i].color = _tvEnabledColor;
        }

        tvNoise.state = tv.tw_state.on;

        if (!wwiseTvEmiter.activeSelf)
        {
            wwiseTvEmiter.SetActive(true);
        }

        PlayOnSound();
    }

    public void DisableLights()
    {
        for (int i = 0; i < _targetLights.Length; ++i)
        {
            _targetLights[i].color = _tvDisabledColor;
        }

        tvNoise.state = tv.tw_state.off;

        if (wwiseTvEmiter.activeSelf)
        {
            wwiseTvEmiter.SetActive(false);
        }

        PlayOffSound();
    }

    IEnumerator PressCoroutine()
    {     
        yield return CheckIfPlayerTowardItem();

        _animatorSwapController.SwapAnimator(PlayerAnimatorControllers.secondaryAnimator);

        _playerAnimator.SetTrigger(_animationTriggerName);

        //_playerItemsIK._rightFoot.weight = _IKWeight;

        //_playerItemsIK._rightFoot.data.target = _rightFootTarget;

        bool btnPressed = false;

        while (true)
        {
            var animatorState = _playerAnimator.GetCurrentAnimatorStateInfo(0);

            //tvRemoteCurve.Evaluate(Time.deltaTime);

            if (animatorState.IsTag(_animationTag) && animatorState.normalizedTime < _animPressDelayTime)
            {
                //_playerItemsIK._rightFoot.weight = _IKWeight для настройки точного веса в апдейти, после закоммитить и перенести значение в _IKEnableWeight

                //_playerItemsIK._rightFoot.weight = _playerAnimator.GetFloat("abc");

                IKWeightValue += setIKSpeed * Time.deltaTime;

                _playerItemsIK._rightFoot.weight = IKWeightValue;

                _playerItemsIK._rightFoot.data.target.position = _rightFootTarget.transform.position;

                _playerItemsIK._rightFoot.data.target.rotation = _rightFootTarget.transform.rotation;
            }

            if (animatorState.IsTag(_animationTag) && animatorState.normalizedTime >= _animPressDelayTime && !btnPressed)
            {
                btnPressed = true;

                TVRemoteBtnPressed();
            }

            if (animatorState.IsTag(_animationTag) && animatorState.normalizedTime >= _animPressDelayTime && animatorState.normalizedTime < 0.99f)
            {
                IKWeightValue -= unsetIKSpeed * Time.deltaTime;

                _playerItemsIK._rightFoot.weight = IKWeightValue;
            }

            if (animatorState.IsTag(_animationTag) && animatorState.normalizedTime >= 1.0f)
            {
                _IKEnabled = false;

                IKWeightValue = 0;

                _playerItemsIK._rightFoot.weight = 0;

                _animatorSwapController.SwapAnimator(PlayerAnimatorControllers.defaultAnimator);

                _player.EnablePlayerMovement();

                yield break;
            }

            yield return null;
        }
    }
}