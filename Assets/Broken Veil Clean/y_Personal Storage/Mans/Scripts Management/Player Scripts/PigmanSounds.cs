using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigmanSounds : MonoBehaviour
{
    [Header("Sound Events")]
    public string hitWeightSoundEvent = "Play_gastronome_monster_weigher_hard";

    public void PigmanHitWeightsSound()
    {
        AkSoundEngine.PostEvent(hitWeightSoundEvent, gameObject);
    }
}