using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRenderControl : MonoBehaviour
{
    public GameObject playerGeometry;
    [Space]
    public Renderer playerHat;
    [Space]
    public Renderer playerHatStar;

    public void SetPlayerRender(bool value)
    {
        playerGeometry.SetActive(value);

        playerHat.enabled = value;

        playerHatStar.enabled = value;
    }
}