using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;
public class GrindPlayerDeath : MonoBehaviour
{
    public GameEvent OnLastSavePointReload;
    [Space]
    public float reloadDelay;
    [Space]
    public bool canTrigger = true;
    [Space]
    public Player player;
    [Space]
    public PlayerController playerController;
    [Space]
    public Collider triggerCollider;
    [Space]
    public Animator _playerAnimator;
    [Space]
    public GameObject deathFromRopeFall;
    [Space]
    public GameObject deathFromFall;

    [Header("Play Sound Event")]
    public string choppingSoundEvent = "Play_chopping_pig";

    private void Start()
    {
        if (triggerCollider == null)
        {
            triggerCollider = GetComponent<Collider>();
        }  
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<PointForHand>(out var pointForHand))
        {
            if (pointForHand.playerWithinRopeCollider && pointForHand.playerController.hangOnHook)
            {
                deathFromFall.SetActive(false);

                triggerCollider.enabled = false;

                if (playerController == null)
                {
                    playerController = pointForHand.playerController;

                    player = playerController.GetComponent<Player>();

                    _playerAnimator = playerController.GetComponent<Animator>();
                }

                if (playerController.hangOnHook)
                {
                    SetPlayerGrinderDeath();
                }
            }
        }

        if (other.TryGetComponent<Player>(out var playerScript))
        {
            deathFromRopeFall.SetActive(false);

            triggerCollider.enabled = false;

            player = playerScript;

            _playerAnimator = playerScript.GetComponent<Animator>();

            player.DisablePlayerMovement();

            player.GetComponent<Collider>().isTrigger = true;

            StartCoroutine(ReloadDelayRoutine(reloadDelay, false));
        }
    }

    public void SetPlayerGrinderDeath()
    {
        playerController.hangOnHook = false;

        player.DisablePlayerMovement();

        player.GetComponent<Collider>().isTrigger = true;

        StartCoroutine(ReloadDelayRoutine(reloadDelay, true));
    }

    public IEnumerator ReloadDelayRoutine(float reloadDelay, bool falledFromRope)
    {
        if (falledFromRope)
        {
            _playerAnimator.GetComponent<AnimatorSwapController>().SwapAnimator(PlayerAnimatorControllers.secondaryAnimator);

            _playerAnimator.SetTrigger("Grinder Death");
        }

        AkSoundEngine.PostEvent(choppingSoundEvent, gameObject);

        yield return new WaitForSeconds(reloadDelay);

        GameManager.Instance.StopAllGameManagerCoroutines();

        AkSoundEngine.StopAll(gameObject);

        OnLastSavePointReload.Raise();
    }
}