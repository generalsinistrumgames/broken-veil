using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHatControl : MonoBehaviour
{
    public List<Rigidbody> rigidbodies;

    public void HatPhysicsActive(bool active)
    {
        foreach (var rb in rigidbodies)
        {
            rb.isKinematic = active;
        }
    }
}