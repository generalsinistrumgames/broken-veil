using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class PlayerInitialization : MonoBehaviour
{
    public GameObject playerPrefab;
    [Space]
    public GameObject playerObj;
    [Space]
    public bool checkIfPlayerIsActive = true;
    [Space]
    public UnityEvent OnPlayerInit;

    public void SetPlayerPositionAndRotation(Transform parentTransform)
    {
        var player = Instantiate(playerPrefab, parentTransform.position, parentTransform.rotation, parentTransform);

        checkIfPlayerIsActive = true;

        StartCoroutine(CheckPlayerActivity(player));
    }

    public void SetPlayerPositionAndRotation(Vector3 position, Quaternion rotation)
    {
        var player = Instantiate(playerPrefab, position, rotation, transform);

        checkIfPlayerIsActive = true;

        StartCoroutine(CheckPlayerActivity(player));
    }

    [ContextMenu("Execute Event")]
    public void SetPlayerPositionAndRotation(ScenesLoadDataSO scenesLoadDataSO)
    {
        var player = Instantiate(playerPrefab, scenesLoadDataSO.currentSavePoint.playerPosition, scenesLoadDataSO.currentSavePoint.playerRotation, transform);

        checkIfPlayerIsActive = true;

        StartCoroutine(CheckPlayerActivity(player));
    }

    public IEnumerator CheckPlayerActivity(GameObject player)
    {
        while (checkIfPlayerIsActive)
        {
            Debug.Log("Checking if player is active");

            if (player.activeSelf)
            {
                //yield return new WaitForSeconds(0.1f); // maybe it will be good

                playerObj = player;

                OnPlayerInit?.Invoke();

                checkIfPlayerIsActive = false;

                Debug.Log("Player is active");
            }

            yield return null;
        }
    }
}