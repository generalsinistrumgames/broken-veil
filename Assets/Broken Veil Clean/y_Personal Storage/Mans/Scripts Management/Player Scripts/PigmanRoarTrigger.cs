using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PigmanRoarTrigger : MonoBehaviour
{
    [Header("Sound Events")]
    public string roarSoundEvent = "Play_gastronome_monster_vo";

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<CinemachineDollyCart>(out var cm))
        {
            AkSoundEngine.PostEvent(roarSoundEvent, gameObject);
        }
    }
}