using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum PlayerAnimatorControllers
{
    defaultAnimator,
    secondaryAnimator,
    cutScenesAnimator   
}

public class AnimatorSwapController : MonoBehaviour
{
    public Animator playerAnimatorReference;
    [Space]
    public RuntimeAnimatorController playerCurrentAnimator;
    [Space]
    public PlayerAnimatorControllers playerAnimators;
    [Space]
    public List<RuntimeAnimatorController> runtimeAnimatorControllers;

    void Start()
    {
        playerAnimatorReference = GetComponent<Animator>();

        playerCurrentAnimator = playerAnimatorReference.runtimeAnimatorController;

        var enumArray = PlayerAnimatorControllers.GetValues(typeof(PlayerAnimatorControllers)).Cast<PlayerAnimatorControllers>().ToList();
    }

    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.B))
    //    {
    //        SwapAnimator(PlayerAnimatorControllers.defaultAnimator);
    //    }
    //    else if (Input.GetKeyDown(KeyCode.N))
    //    {
    //        SwapAnimator(PlayerAnimatorControllers.secondaryAnimator);
    //    }
    //    else if (Input.GetKeyDown(KeyCode.M))
    //    {
    //        SwapAnimator(PlayerAnimatorControllers.cutScenesAnimator);
    //    }
    //}

    public void SwapAnimator(PlayerAnimatorControllers animator)
    {
        playerAnimators = animator;

        switch (playerAnimators)
        {
            case PlayerAnimatorControllers.defaultAnimator:
                playerAnimatorReference.runtimeAnimatorController = runtimeAnimatorControllers[0];
                break;
            case PlayerAnimatorControllers.secondaryAnimator:
                playerAnimatorReference.runtimeAnimatorController = runtimeAnimatorControllers[1];
                break;
            case PlayerAnimatorControllers.cutScenesAnimator:
                playerAnimatorReference.runtimeAnimatorController = runtimeAnimatorControllers[2];
                break;
            default:
                break;
        }
    }

    public bool AnimationIsPlayingCheck(Animator anim, string stateName)
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName(stateName) &&
                anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1.0f)
            return true;
        else
            return false;
    }
}