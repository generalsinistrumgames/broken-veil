using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;

public class RopeSounds : MonoBehaviour
{
    [Header("Sound Event")]
    [SerializeField] string _playRopeSoundEvent = "Play_gastronome_rope";
    [Space]
    public bool soundPlayed;

    private void OnTriggerStay(Collider other)
    {
        if (!soundPlayed)
        {
            if (other.TryGetComponent<PlayerController>(out var playerController))
            {
                if (playerController.hangOnHook)
                {
                    AkSoundEngine.PostEvent(_playRopeSoundEvent, gameObject);

                    soundPlayed = true;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<PlayerController>(out var playerController))
        {
            soundPlayed = false;
        }
    }
}