using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSController : MonoBehaviour
{
    public bool canControlFrameRate;
    [Space]
    public int frameRateValue;

    private void Start()
    {
        Application.targetFrameRate = frameRateValue;
    }

    void Update()
    {
        if (canControlFrameRate)
        {
            Application.targetFrameRate = frameRateValue;
        }   
    }
}