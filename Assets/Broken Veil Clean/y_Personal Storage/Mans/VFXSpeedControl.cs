﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class VFXSpeedControl : MonoBehaviour
{
    public List<VisualEffect> vfxes;
    [Space]
    public List<float> vfxSpeeds;
    [Space]
    public List<float> vfxStartSpeeds;
    [Space]
    public bool canControlSpeed;
    [Space]
    public bool canSetSpeed;

    private void Start()
    {
        for (int i = 0; i < vfxes.Count; i++)
        {
            vfxes[i].playRate = vfxStartSpeeds[i];
        }
    }

    void Update()
    {
        if (canControlSpeed)
        {
            for (int i = 0; i < vfxes.Count; i++)
            {
                vfxes[i].playRate = vfxSpeeds[i];
            }
        }

        if (canSetSpeed)
        {
            SetVFXSpeed();
        }
    }

    public void SetVFXSpeed()
    {
        for (int i = 0; i < vfxes.Count; i++)
        {
            vfxes[i].playRate = vfxSpeeds[i];
        }
    }
}