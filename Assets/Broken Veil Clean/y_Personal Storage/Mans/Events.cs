using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class Events
{
    [System.Serializable] public class GameStateEvent : UnityEvent<GameManager.GameState, GameManager.GameState> { }

    [System.Serializable] public class GameOverEvent : UnityEvent<float> { }
}