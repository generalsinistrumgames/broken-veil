using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class CinemachineCameraConrtol : MonoBehaviour
{
    public Camera main;
    [Space]
    public CinemachineBrain cmBrain;
    [Space]
    public PlayableDirector _playableDirector;

    void Start()
    {
        _playableDirector = GetComponent<PlayableDirector>();

        main = Camera.main;

        cmBrain = main.GetComponent<CinemachineBrain>();

        SetCMBrainInTimeline();
    }

    public void SetCMBrainInTimeline()
    {
        foreach (var playableAssetOutput in _playableDirector.playableAsset.outputs)
        {
            if (playableAssetOutput.streamName.Equals("Cinemachine Track"))
            {
                _playableDirector.SetGenericBinding(playableAssetOutput.sourceObject, cmBrain);
            }
        }
    }
}