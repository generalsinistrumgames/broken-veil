﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentObjectUtility : MonoBehaviour
{
    [Header("Refs")]
    public GameObject targetObj;
    [Space]
    public GameObject toParentObjRoot;

    [Header("Offsets")]
    public bool useOffsets;
    [Space]
    public Vector3 toParentPositionOffset;
    [Space]
    public Quaternion toParentRotationOffset;

    [Header("Trigger Player")]
    public bool usePlayer;
    [Space]
    public bool playerIsTargetObj;

    public void ParentObjectToTarget()
    {
        targetObj.transform.SetParent(toParentObjRoot.transform);

        if (useOffsets)
        {
            targetObj.transform.SetPositionAndRotation(toParentObjRoot.transform.position + toParentPositionOffset, toParentObjRoot.transform.rotation * toParentRotationOffset);
        }
        else
        {
            targetObj.transform.SetPositionAndRotation(toParentObjRoot.transform.position, toParentObjRoot.transform.rotation);
        } 
    }

    private void OnTriggerEnter(Collider other)
    {
        if (usePlayer && !playerIsTargetObj)
        {
            if (other.TryGetComponent<Player>(out var player))
            {
                targetObj = player.gameObject;

                playerIsTargetObj = true;
            }
        }    
    }
}