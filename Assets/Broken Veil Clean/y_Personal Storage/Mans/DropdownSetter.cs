using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DropdownSetter : MonoBehaviour
{
    public TMP_Dropdown dropdown;
    [Space]
    public List<string> options = new List<string>();

    //void Start()
    //{
    //    //Add listener for when the value of the Dropdown changes, to take action
    //    dropdown.onValueChanged.AddListener(delegate
    //    {
    //        DropdownValueChanged(dropdown);
    //    });

    //    //Initialise the Text to say the first value of the Dropdown
    //    //m_Text.text = "First Value : " + dropdown.value;
    //}

    ////Ouput the new value of the Dropdown into Text
    //void DropdownValueChanged(TMP_Dropdown change)
    //{
    //    //m_Text.text = "New Value : " + change.value;
    //}


    public void FillDropdownInfo(string option, int currentValue)
    {
        dropdown.ClearOptions();

        options.Add(option);

        dropdown.AddOptions(options);

        dropdown.value = currentValue;

        dropdown.RefreshShownValue();
    }
}