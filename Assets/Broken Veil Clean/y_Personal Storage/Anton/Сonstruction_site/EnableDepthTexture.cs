﻿using UnityEngine;


    [ExecuteInEditMode]
    public class EnableDepthTexture : MonoBehaviour
    {
        void Awake()
        {
            Camera.main.depthTextureMode |= DepthTextureMode.Depth;
        }
    }