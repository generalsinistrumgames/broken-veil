using System.Collections;
using System.Collections.Generic;
using Steamworks;
using UnityEngine;

public class SteamManager : MonoBehaviour
{
    uint steamApp = 1587600;

    private void Awake()
    {
        try
        {
            SteamClient.Init(steamApp, true);
        }
        catch
        {
            Debug.LogError("SteamClient not initialized");
        }
        if (!SteamClient.IsValid)
        {
            Debug.Log("Steam client not valid");
        }
    }

    private void OnApplicationQuit()
    {
        SteamClient.Shutdown();
    }
}
