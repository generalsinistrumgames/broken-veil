﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class IKSystem : MonoBehaviour
{
    [Header("Refs")]
    [SerializeField] Animator animator;
    [Space]
    [SerializeField] Transform lookAtTarget;
    [Space]
    [SerializeField] LayerMask layerMask;

    [Header("Distances")]
    [Range(0, 1f)]
    [SerializeField] float distanceToGround;
    [Space]
    [Range(0, 1f)]
    [SerializeField] float distanceToKnee;
    [Space]
    [Range(0, 1f)]
    [SerializeField] float handDistanceToWall;
    [Space]
    [Range(0, 1f)]
    [SerializeField] float horizontalRayDistance;

    [Header("Feet Position Weights")]
    [Range(0, 10f)]
    [SerializeField] float speed;

    [Header("Feet Position Weights")]
    [Range(0, 1f)]
    [SerializeField] float leftFootPosWeight;
    [Space]
    [Range(0, 1f)]
    [SerializeField] float rightFootPosWeight;

    [Header("Feet Rotation Weights")]
    [Range(0, 1f)]
    [SerializeField] float leftFootRotWeight;
    [Space]
    [Range(0, 1f)]
    [SerializeField] float rightFootRotWeight;

    [Header("Hands Position Weights")]
    [Range(0, 1f)]
    [SerializeField] float leftHandPosWeight;
    [Space]
    [Range(0, 1f)]
    [SerializeField] float rightHandPosWeight;

    [Header("Hands Rotation Weights")]
    [Range(0, 1f)]
    [SerializeField] float leftHandRotWeight;
    [Space]
    [Range(0, 1f)]
    [SerializeField] float rightHandRotWeight;

    [Header("Look At Target Weights")]
    [Range(0, 1f)]
    [SerializeField] float lookIKWeight, bodyWeight, headWeight, clampWeight;

    [Header("Hands Refs")]
    [SerializeField] GameObject leftShoulder;
    [Space]
    [SerializeField] GameObject rightShoulder;

    [Header("Left Hand Weight Controller")]
    public bool leftHandOnWall;
    [Space]
    public float leftHandSpeed;
    [Space]
    public float leftHandMinWeightValue;
    [Space]
    public float leftHandMaxWeightValue;
    [Space]
    public float leftHandWeightValue;
    [Space]
    public float time;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void OnAnimatorIK(int layerIndex)
    {
        // Look At Target
        if (lookAtTarget != null)
        {
            animator.SetLookAtWeight(lookIKWeight, bodyWeight, headWeight, clampWeight);
            animator.SetLookAtPosition(lookAtTarget.position);
        }

        //Hands
        animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, leftHandWeightValue);
        animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, leftHandWeightValue);

        animator.SetIKPositionWeight(AvatarIKGoal.RightHand, rightHandPosWeight);
        animator.SetIKRotationWeight(AvatarIKGoal.RightHand, rightHandRotWeight);

        // Feet
        animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, animator.GetFloat("IKLeftFootWeight"));
        animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot, animator.GetFloat("IKLeftFootWeight"));

        animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, animator.GetFloat("IKRightFootWeight"));
        animator.SetIKRotationWeight(AvatarIKGoal.RightFoot, animator.GetFloat("IKRightFootWeight"));

        #region Feet
        //Left Foot
        RaycastHit feetHit;

        Ray feetRay = new Ray(animator.GetIKPosition(AvatarIKGoal.LeftFoot) + Vector3.up * distanceToKnee, Vector3.down);

        if (Physics.Raycast(feetRay, out feetHit, distanceToGround + 1f, layerMask))
        {
            Debug.Log(feetHit.transform.tag);

            if (feetHit.transform.tag.Equals("Walkable"))
            {
                Vector3 footPosition = feetHit.point;

                footPosition.y += distanceToGround;

                animator.SetIKPosition(AvatarIKGoal.LeftFoot, footPosition);
                animator.SetIKRotation(AvatarIKGoal.LeftFoot, Quaternion.FromToRotation(Vector3.up, feetHit.normal) * transform.rotation);

                Debug.DrawLine(feetRay.origin, footPosition, Color.blue);
            }
        }

        //Right Foot
        feetRay = new Ray(animator.GetIKPosition(AvatarIKGoal.RightFoot) + Vector3.up * distanceToKnee, Vector3.down);

        if (Physics.Raycast(feetRay, out feetHit, distanceToGround + 1f, layerMask))
        {
            if (feetHit.transform.tag.Equals("Walkable"))
            {
                Vector3 footPosition = feetHit.point;

                footPosition.y += distanceToGround;

                animator.SetIKPosition(AvatarIKGoal.RightFoot, footPosition);
                animator.SetIKRotation(AvatarIKGoal.RightFoot, Quaternion.FromToRotation(Vector3.up, feetHit.normal) * transform.rotation);

                Debug.DrawLine(feetRay.origin, footPosition, Color.blue);
            }
        }

        #endregion

        //Left Hand
        RaycastHit handHit;

        Ray handRay = new Ray(leftShoulder.transform.position, transform.forward * horizontalRayDistance);

        Debug.DrawRay(handRay.origin, transform.forward * horizontalRayDistance, Color.blue);

        if (Physics.Raycast(handRay, out handHit, horizontalRayDistance, layerMask))
        {
            Debug.Log(handHit.transform.tag);

            if (handHit.transform.tag.Equals("Walkable"))
            {
                if (!leftHandOnWall)
                {
                    leftHandWeightValue = Mathf.Lerp(leftHandMinWeightValue, leftHandMaxWeightValue, time += Time.deltaTime * leftHandSpeed);

                    if (leftHandWeightValue >= leftHandMaxWeightValue)
                    {
                        time -= time;

                        leftHandOnWall = true;

                        Debug.Log("Hand on Wall");
                    }

                    Debug.Log("Puting hand");
                }

                Vector3 handPosition = handHit.point;

                handPosition.z -= handDistanceToWall;

                animator.SetIKPosition(AvatarIKGoal.LeftHand, handPosition);

                animator.SetIKRotation(AvatarIKGoal.LeftHand, Quaternion.FromToRotation(Vector3.up, handHit.normal) * transform.rotation);

                Debug.DrawRay(handRay.origin, transform.forward * horizontalRayDistance, Color.green);
            }
        }

        else
        {
            if (leftHandOnWall)
            {
                leftHandWeightValue = Mathf.Lerp(leftHandMaxWeightValue, leftHandMinWeightValue, time += Time.deltaTime * leftHandSpeed);

                if (leftHandWeightValue <= leftHandMinWeightValue)
                {
                    time -= time;

                    leftHandOnWall = false;

                    Debug.Log("Hand removed");
                }

                Debug.Log("Removing hand");
            }
        }

        //Set left hand weights
        leftHandPosWeight = leftHandWeightValue;
        leftHandRotWeight = leftHandWeightValue;

        //Right Hand
        handRay = new Ray(rightShoulder.transform.position, transform.forward * horizontalRayDistance);

        Debug.DrawRay(handRay.origin, transform.forward * horizontalRayDistance, Color.blue);

        if (Physics.Raycast(handRay, out handHit, horizontalRayDistance, layerMask))
        {
            Debug.Log(handHit.transform.tag);

            if (handHit.transform.tag.Equals("Walkable"))
            {
                Vector3 handPosition = handHit.point;

                handPosition.z -= handDistanceToWall;

                animator.SetIKPosition(AvatarIKGoal.RightHand, handPosition);
                animator.SetIKRotation(AvatarIKGoal.RightHand, Quaternion.FromToRotation(Vector3.up, handHit.normal) * transform.rotation);
            }

            Debug.DrawRay(handRay.origin, transform.forward * horizontalRayDistance, Color.green);
        }
    }
}