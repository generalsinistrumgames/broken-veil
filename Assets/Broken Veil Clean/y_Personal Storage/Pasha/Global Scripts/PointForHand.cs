﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;

public class PointForHand : MonoBehaviour
{
    [Header("Refs")]
    public Rigidbody pointRb;
    [Space]
    public Transform playerTransform;
    [Space]
    public PlayerController playerController;
    [Space]
    public bool playerWithinRopeCollider;

    [Header("Impulse Force")]
    public bool useImpulseForce;
    [Space]
    public float impulseDuration;
    [Space]
    public float impulseDurationTimer;
    [Space]
    public bool timerIsEnded;
    [Space]
    public float forceValue;
    [Space]
    public Vector3 targetPos;
    [Space]
    [Range(0, 1)]
    public float treshhold = 0.1f;

    private void Start()
    {
        impulseDurationTimer = impulseDuration;
    }

    private void Update()
    {
        if (useImpulseForce && playerWithinRopeCollider)
        {
            if (playerController == null)
            {
                playerController = FindObjectOfType<PlayerController>();

                playerTransform = playerController.transform;
            }

            if (playerController.hangOnHook)
            {
                if (!timerIsEnded)
                {
                    if (impulseDurationTimer > 0)
                    {
                        impulseDurationTimer -= Time.deltaTime;

                        pointRb.AddForce(playerTransform.transform.forward * forceValue, ForceMode.Acceleration);

                        Debug.Log("Adding Impulse");
                    }
                    else
                    { 
                        impulseDurationTimer = impulseDuration;

                        timerIsEnded = true;

                        Debug.Log("Time has run out");
                    }
                }
            }
            else
            {
                playerWithinRopeCollider = false;

                timerIsEnded = false;

                impulseDurationTimer = impulseDuration;

                Debug.Log("Player not on rope now");
            }

            // for checking is rope reached target pos, Useful for cut-scenes
            //if (Vector3.Distance(pointRb.transform.localPosition, targetPos) <= treshhold)
            //{
            //    Debug.Log("Move To Target Position Reached");
            //    useImpulseForce = false;
            //    
            //}
            //else
            //{
            //    pointRb.AddForceAtPosition(playerTransform.transform.forward * force, targetPos, ForceMode.Acceleration);
            //}
        }
    }

    public IEnumerator DisableCollider()
    {
        var collider = GetComponent<Collider>();
        collider.enabled = false;
        yield return new WaitForSeconds(0.75f);
        collider.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<PlayerController>(out var playerControllerScript))
        {
            playerWithinRopeCollider = true;

            if (playerController == null)
            {
                playerController = playerControllerScript;

                playerTransform = playerControllerScript.transform;
            }   
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<PlayerController>(out PlayerController player))
        {     
            player.rays.hook = transform.parent.GetComponent<Joint>();

            playerWithinRopeCollider = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<PlayerController>(out PlayerController player))
        {
            player.rays.hook = null;

            playerWithinRopeCollider = false;
        }
    }
}
