﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPlayerForIK : MonoBehaviour
{
    [Header("Common attributes"), SerializeField] float acceleration = 17.5f; //ускорение
    [SerializeField] float speed = 2.5f; //скорость бега
    [SerializeField] float jumpForce = 250; //сила прыжка
    [SerializeField] float superJumpForce = 1.5f; //сила прыжка с разбега
    [SerializeField] float speedRotation = 10; //скорость поворота Slerp
    [SerializeField] float stepOffset = 0.1f;

    public Rigidbody rb;
    public Animator animator;
    [SerializeField] float vectorX;
    [SerializeField] float vectorZ;
    float movement;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
            GetComponent<Animator>().SetTrigger("Press TV Remote");
    }

    private void FixedUpdate()
    {
        vectorX = Input.GetAxisRaw("Horizontal");
        vectorZ = Input.GetAxisRaw("Vertical");

        Vector3 vectorMoove = new Vector3(vectorX, 0, vectorZ);

        Move(vectorMoove);
        animator.SetFloat("Movement", movement);

    }

    void Move(Vector3 velocity)
    {
        if ((velocity.x != 0 || velocity.z != 0))
        {
            if (velocity.magnitude > 1f)
            {
                velocity = velocity.normalized;
            }

            rb.AddForce(velocity * acceleration * Time.deltaTime, ForceMode.VelocityChange);

            Vector3 horizontalVelocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
            if (horizontalVelocity.magnitude >= speed)
            {
                //acceleration = 17.5f;
                rb.velocity = horizontalVelocity.normalized * speed + new Vector3(0, rb.velocity.y, 0);
            }

            var horizontalDirection = new Vector3(velocity.x, 0, velocity.z);
            var rot = Quaternion.LookRotation(horizontalDirection, Vector3.up);
            if (Mathf.Abs(Mathf.Abs(transform.rotation.eulerAngles.y) - Mathf.Abs(rot.eulerAngles.y)) > 90)
            {
                //Debug.Log("rot");
            }

            transform.rotation = Quaternion.Slerp(transform.rotation, rot, speedRotation * Time.deltaTime); //заглушка заменить. возможно...

        }
        movement = new Vector3(rb.velocity.x, 0, rb.velocity.z).magnitude;
    }
}
