using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;

public class TriggerAlwaysSprint : MonoBehaviour
{
    public bool alwaysSprint;

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            alwaysSprint = true;
            player.alwaysSprint = alwaysSprint;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            alwaysSprint = false;
            player.alwaysSprint = alwaysSprint;
        }
    }
}
