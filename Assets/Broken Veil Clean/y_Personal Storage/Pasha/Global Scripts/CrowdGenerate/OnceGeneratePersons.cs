using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnceGeneratePersons : MonoBehaviour
{
    [SerializeField] CrowdPersonsGenerator crowdPersonsData;
    [SerializeField] List<GameObject> gameObjects;

    private void Start()
    {
        crowdPersonsData = GetComponent<CrowdPersonsGenerator>();

        foreach (var item in gameObjects)
        {
            int randomPerson = Random.Range(0, 3);
            if (randomPerson == 0)
            {
                var person = Instantiate(crowdPersonsData.personsOldMan.personsOldMan, Vector3.zero, Quaternion.identity, item.transform);
                person.name = "personsOldMan";
                Transform geometry = person.transform.GetChild(0);
                person.GetComponent<AnimSync>().moveRoot = person.transform.parent.GetChild(0);

                foreach (var partNumber in crowdPersonsData.personsOldMan.variantPart[0].partsOnObject)
                {
                    int randomVariant = Random.Range(0, crowdPersonsData.personsOldMan.variantPart[0].variant.Count);
                    geometry.GetChild(partNumber).GetComponent<SkinnedMeshRenderer>().material = crowdPersonsData.personsOldMan.variantPart[0].variant[randomVariant];
                }

                foreach (var partNumber in crowdPersonsData.personsOldMan.variantPart[1].partsOnObject)
                {
                    int randomVariant = Random.Range(0, crowdPersonsData.personsOldMan.variantPart[1].variant.Count);
                    geometry.GetChild(partNumber).GetComponent<SkinnedMeshRenderer>().material = crowdPersonsData.personsOldMan.variantPart[1].variant[randomVariant];
                }

                foreach (var partNumber in crowdPersonsData.personsOldMan.variantPart[2].partsOnObject)
                {
                    int randomVariant = Random.Range(0, crowdPersonsData.personsOldMan.variantPart[2].variant.Count);
                    geometry.GetChild(partNumber).GetComponent<SkinnedMeshRenderer>().material = crowdPersonsData.personsOldMan.variantPart[2].variant[randomVariant];
                }

                foreach (var partNumber in crowdPersonsData.personsOldMan.variantPart[3].partsOnObject)
                {
                    int randomVariant = Random.Range(0, crowdPersonsData.personsOldMan.variantPart[3].variant.Count);
                    geometry.GetChild(partNumber).GetComponent<SkinnedMeshRenderer>().material = crowdPersonsData.personsOldMan.variantPart[3].variant[randomVariant];
                }
                person.transform.localPosition = Vector3.zero;
                person.transform.localRotation = Quaternion.identity;
            }
            if (randomPerson == 1)
            {
                var person = Instantiate(crowdPersonsData.personsOldWoman.personsOldMan, Vector3.zero, Quaternion.identity, item.transform);
                person.name = "personsOldWomen";
                Transform geometry = person.transform.GetChild(0);
                person.GetComponent<AnimSync>().moveRoot = person.transform.parent.GetChild(0);

                foreach (var partNumber in crowdPersonsData.personsOldWoman.variantPart[0].partsOnObject)
                {
                    int randomVariant = Random.Range(0, crowdPersonsData.personsOldWoman.variantPart[0].variant.Count);
                    geometry.GetChild(partNumber).GetComponent<SkinnedMeshRenderer>().material = crowdPersonsData.personsOldWoman.variantPart[0].variant[randomVariant];
                }

                foreach (var partNumber in crowdPersonsData.personsOldWoman.variantPart[1].partsOnObject)
                {
                    int randomVariant = Random.Range(0, crowdPersonsData.personsOldWoman.variantPart[1].variant.Count);
                    geometry.GetChild(partNumber).GetComponent<SkinnedMeshRenderer>().material = crowdPersonsData.personsOldWoman.variantPart[1].variant[randomVariant];
                }

                foreach (var partNumber in crowdPersonsData.personsOldWoman.variantPart[2].partsOnObject)
                {
                    int randomVariant = Random.Range(0, crowdPersonsData.personsOldWoman.variantPart[2].variant.Count);
                    geometry.GetChild(partNumber).GetComponent<SkinnedMeshRenderer>().material = crowdPersonsData.personsOldWoman.variantPart[2].variant[randomVariant];
                }

                person.transform.localPosition = Vector3.zero;
                person.transform.localRotation = Quaternion.identity;
                //person.AddComponent<AnimSync>().moveRoot = item.transform.GetChild(0);
            }
            if (randomPerson == 2)
            {
                var person = Instantiate(crowdPersonsData.personsYoungWoman.personsOldMan, Vector3.zero, Quaternion.identity, item.transform);
                person.name = "personsYoungWoman";
                Transform geometry = person.transform.GetChild(0);
                person.GetComponent<AnimSync>().moveRoot = person.transform.parent.GetChild(0);

                foreach (var partNumber in crowdPersonsData.personsYoungWoman.variantPart[0].partsOnObject)
                {
                    int randomVariant = Random.Range(0, crowdPersonsData.personsYoungWoman.variantPart[0].variant.Count);
                    geometry.GetChild(partNumber).GetComponent<SkinnedMeshRenderer>().material = crowdPersonsData.personsYoungWoman.variantPart[0].variant[randomVariant];
                }

                foreach (var partNumber in crowdPersonsData.personsYoungWoman.variantPart[1].partsOnObject)
                {
                    int randomVariant = Random.Range(0, crowdPersonsData.personsYoungWoman.variantPart[1].variant.Count);
                    geometry.GetChild(partNumber).GetComponent<SkinnedMeshRenderer>().material = crowdPersonsData.personsYoungWoman.variantPart[1].variant[randomVariant];
                }

                foreach (var partNumber in crowdPersonsData.personsYoungWoman.variantPart[2].partsOnObject)
                {
                    int randomVariant = Random.Range(0, crowdPersonsData.personsYoungWoman.variantPart[2].variant.Count);
                    geometry.GetChild(partNumber).GetComponent<SkinnedMeshRenderer>().material = crowdPersonsData.personsYoungWoman.variantPart[2].variant[randomVariant];
                }

                person.transform.localPosition = Vector3.zero;
                person.transform.localRotation = Quaternion.identity;

            }


        }
    }
}
