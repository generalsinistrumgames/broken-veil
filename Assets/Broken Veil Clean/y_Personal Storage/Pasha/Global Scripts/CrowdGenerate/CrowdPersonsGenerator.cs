using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CrowdPersonsGenerator : MonoBehaviour
{
    [SerializeField] public Person personsOldMan;
    [SerializeField] public Person personsOldWoman;
    [SerializeField] public Person personsYoungWoman;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    [Serializable]
    public struct Person
    {
        public GameObject personsOldMan;
        public List<Part> variantPart;
    }

    [Serializable]
    public struct Part
    {
        [SerializeField] string nameMaterialPart;
        [SerializeField] public List<int> partsOnObject;
        public List<Material> variant;
    }
}
