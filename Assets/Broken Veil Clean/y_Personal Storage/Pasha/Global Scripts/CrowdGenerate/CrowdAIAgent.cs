using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CrowdAIAgent : MonoBehaviour
{
    public CrowdPathManager crowdPathManager;
    NavMeshAgent agent;
    [SerializeField] int currentNode;
    [SerializeField] int currentSide;
    Transform moveObject;


    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        currentSide = Random.Range(0, crowdPathManager.sides.Count);
        currentNode = Random.Range(0, crowdPathManager.sides[currentSide].node.Count);
        moveObject = transform.GetChild(0);
    }

    void Update()
    {
        moveObject.localPosition = new Vector3(0, agent.velocity.magnitude, 0);
        //GetComponent<Animator>().SetFloat("Blend", agent.velocity.magnitude);
        var distance = Vector3.Distance(transform.position, crowdPathManager.sides[currentSide].node[currentNode].position);
        if (distance > agent.stoppingDistance)
        {
            agent.SetDestination(crowdPathManager.sides[currentSide].node[currentNode].position);

        }
        else
        {
            var randomSide = Random.Range(0, crowdPathManager.sides.Count);
            while (currentSide == randomSide)
            {
                randomSide = Random.Range(0, crowdPathManager.sides.Count);
            }
            currentSide = randomSide;
            Random.Range(0, crowdPathManager.sides[currentSide].node.Count);
        }
    }
}
