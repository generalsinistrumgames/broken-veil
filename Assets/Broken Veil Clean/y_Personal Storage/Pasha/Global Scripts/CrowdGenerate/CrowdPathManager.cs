using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class CrowdPathManager : MonoBehaviour
{
    [SerializeField] public List<Transform> points;
    [SerializeField] int currentPoint;
    
    [SerializeField] GameObject prefabAgents;
    List<GameObject> agents;
    [SerializeField] int countAgent;

    public GameObject root;
    public List<Side> sides;

    void Start()
    {
        agents = new List<GameObject>();
        for (int i = 0; i < countAgent; i++)
        {
            var agentObject = Instantiate(prefabAgents, points[UnityEngine.Random.RandomRange(0, points.Count)].position, Quaternion.identity, root.transform);
            agentObject.name += $"{i}";
            agentObject.AddComponent<CrowdAIAgent>().crowdPathManager = this;
            //agentObject.GetComponent<CrowdAIAgent>().crowdPathManager = this;
            agents.Add(agentObject);
        }


    }

    void Update()
    {
        
    }

    [Serializable]
    public struct Side
    {
        public List<Transform> node;
    }
}
