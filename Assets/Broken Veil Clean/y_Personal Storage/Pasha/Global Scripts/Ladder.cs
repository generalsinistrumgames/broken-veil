﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;

public class Ladder : MonoBehaviour
{
    public List<Step> steps;
    public Step nearestSt;
    public float distanceBetweenSteps;

    [Header("Sound Events")]
    public string playStairsStepsSoundEvent = "Play_chopping_stairs_steps";
    [Space]
    public string playStairsInSoundEvent = "Play_chopping_stairs_in";
    [Space]
    public string playStairsOutSoundEvent = "Play_chopping_stairs_out";

    void Start()
    {
        steps = new List<Step>(GetComponentsInChildren<Step>());
        nearestSt = new Step();
        distanceBetweenSteps = Vector2.Distance(steps[0].transform.position, steps[1].transform.position);
    }

    void Update()
    {
        //Debug.Log(steps.IndexOf(nearestSt));
    }

    public Step GetNearestStep(Collider target)
    {
        var nearestStep = steps[0];
        foreach (var item in steps)
        {
            if (Vector3.Distance(target.transform.position /*+ new Vector3(0, target.bounds.extents.y, 0)*/, item.transform.position) < Vector3.Distance(target.transform.position /*+ new Vector3(0, target.bounds.extents.y, 0)*/, nearestStep.transform.position))
                nearestStep = item;
        }
        //Debug.Log(steps[0].transform.position);

        nearestSt = nearestStep;
        return nearestStep;
    }

    private void OnDrawGizmos()
    {

        Gizmos.color = Color.green;
        if (nearestSt != null)
            Gizmos.DrawCube(nearestSt.transform.position, new Vector3(0.1f, 0.1f, 0.1f));

    }

    //    public int GetFirstIndexElementUponGrab(GameObject ropeElement)
    //    {
    //        currentRopeElement = ropeElements.IndexOf(ropeElement);
    //        return currentRopeElement;
    //    }

    //    [SerializeField] public List<GameObject> ropeElements;
    //    public int currentRopeElement;

    //    public int GetFirstIndexElementUponGrab(GameObject ropeElement)
    //    {
    //        currentRopeElement = ropeElements.IndexOf(ropeElement);
    //        return currentRopeElement;
    //    }

    public void PlayStairsSteps()
    {
        AkSoundEngine.PostEvent(playStairsStepsSoundEvent, gameObject);
    }

    public void PlayStairsIn()
    {
        AkSoundEngine.PostEvent(playStairsInSoundEvent, gameObject);
    }

    public void PlayStairsOut()
    {
        AkSoundEngine.PostEvent(playStairsOutSoundEvent, gameObject);
    }
}