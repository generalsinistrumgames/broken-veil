﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour
{
    [SerializeField] public List<GameObject> ropeElements;
    public int currentRopeElement;
    public Vector3 direction;
    [SerializeField] Color colorDirectionGizmo = Color.green;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

       
    }

    public int GetFirstIndexElementUponGrab(GameObject ropeElement)
    {
        currentRopeElement = ropeElements.IndexOf(ropeElement);
        return currentRopeElement;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = colorDirectionGizmo;
        Gizmos.DrawLine(transform.position, transform.position + direction.normalized);
    }
}
