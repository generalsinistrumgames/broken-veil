﻿using GlobalScripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]

public class Player : MonoBehaviour/* , IMoveAndRotable, IFaceCheckable*/
{
    [Header("Movement")]
    [SerializeField] float vectorX;
    [SerializeField] float vectorZ;
    [Space]
    [SerializeField] Vector3 vectorMoove;
    delegate void GetButtonUpSneak();

    [Header("References")]
    [SerializeField] public Animator animator;
    [Space]
    public PlayerController playerController;

    [Header("Facing Control")]
    private FacingChecker _facingChecker = new FacingChecker();
    [Space]
    public FaceDirection _faceDirection;

    [Header("Move And Rotate")]
    private MoveAndRotateTowards _moveAndRotateTowards = new MoveAndRotateTowards();

    [SerializeField] public List<GameObject> ears;
    [HideInInspector] public bool itemInPlayerHand;
    [SerializeField] float sprintBoost = 1.75f;
    [HideInInspector] public bool alwaysSprint;

    void Start()
    {
        foreach (var item in ears)
        {
            item.transform.parent = null;
        }

        animator = GetComponent<Animator>();

        playerController = GetComponent<PlayerController>();
    }

    private void FixedUpdate()
    {
        vectorX = Input.GetAxis("Horizontal");
        vectorZ = Input.GetAxis("Vertical");

        vectorMoove = new Vector3(vectorX, 0, vectorZ);

        playerController.Move(vectorMoove);

    }

    private void Update()
    {
        animator.SetFloat("Movement", playerController.Movement);

        //animator.SetFloat("Grab Value", playerController.HandsHeightAnimation);

        animator.SetBool("OnGround", playerController.rays.commonCollisionDown);
        animator.SetFloat("SpeedFall", playerController.speedFall);
        animator.SetBool("HangOnHook", playerController.hangOnHook);
        animator.SetBool("Drag", playerController.drag);
        animator.SetFloat("MovementDiretionX", playerController.movementDiretion.x);
        animator.SetFloat("MovementDiretionZ", playerController.movementDiretion.z);

        #region jump
        if (Input.GetButtonDown("Jump") && !itemInPlayerHand)
        {
            //if (playerController.rays.commonCollisionDown)
            //{
            if (playerController.Jump(vectorMoove))
            {

                animator.ResetTrigger("JumpUp");

                animator.SetTrigger("JumpUp");
            }
            //}

            animator.SetBool("Jump", playerController.jump);
        }
        #endregion

        //if (!playerController.jump && playerController.speedFall < -3)
        //{
        //    animator.ResetTrigger("Fall");
        //    animator.SetTrigger("Fall");
        //}


        #region sneak
        if ((Input.GetButton("Sneak") || playerController.obstacleFromTop) && playerController.climbLCMCoroutine == null)
        {
            animator.SetBool("Sneak", true);

            playerController.Sneak = true;

            playerController.maxSpeedFactor = 0.5f;
        }
        else
        {
            animator.SetBool("Sneak", false);

            playerController.Sneak = false;

            playerController.maxSpeedFactor = 1f;
        }
        #endregion

        #region boost
        if ((Input.GetButton("Boost") || alwaysSprint) && !playerController.sprintTired && !playerController.drag && !playerController.hangOnHook && !playerController.Sneak)
        {
            animator.SetBool("Boost", true);
            playerController.maxSpeedFactor = sprintBoost;
            if (playerController.Movement > 0)
                playerController.SprintTimer += Time.deltaTime;

            if (playerController.SprintTimer >= playerController.sprintMaxTime)
                playerController.sprintTired = true;
        }

        else if (!Input.GetButton("Boost") && !playerController.Sneak)
        {
            animator.SetBool("Boost", false);

            playerController.maxSpeedFactor = 1f;
        }
        #endregion
    }

    public void HangHook()
    {
        animator.SetBool("hangOnHook", true);
    }

    #region hang
    public void SetHangAnimation()
    {
        animator.SetBool("Hang", true);
    }

    public void SetHangUpFromJumpAnimation()
    {
        animator.SetTrigger("HangUpFromJump");
    }

    public void SetHangUpAnimation()
    {

        animator.SetTrigger("HangUp");

        animator.SetBool("Hang", false);
    }
    #endregion

    private void OnDisable()
    {
        animator.SetFloat("Movement", 0);
    }

    public bool MoveTowards(Transform from, Transform target, float moveSpeed = 0.1f, float offset = 0.1f, bool moveUp = false)
    {
        return _moveAndRotateTowards.MoveTowards(transform, target, moveSpeed, offset);
    }

    public bool RotateTowards(Transform from, Transform target, float rotationSpeed = 1f, bool faceRotation = false, bool rotateXAngle = false)
    {
        return _moveAndRotateTowards.RotateTowards(from, target, rotationSpeed);
    }

    public FaceDirection CheckFacing(Transform from, Transform target)
    {
        _faceDirection = _facingChecker.CheckIfFacingObject(from, target); // for debugging

        return _faceDirection;
    }

    /// <summary>
    /// Отключение управление с контроллем кинематики
    /// </summary>
    /// <param name="setIsKinematic">контроль кинематики</param>
    public void EnablePlayerMovement(bool setIsKinematic = false)
    {
        //playerController.StopAllCoroutines();

        //this.enabled = true;

        //playerController.enabled = true;

        playerController.rootMove = false;

        if (setIsKinematic)
        {
            playerController.rb.isKinematic = true;
        }
        else
        {
            playerController.rb.isKinematic = false;
        }

        //playerController.rb.velocity = Vector3.zero;
    }

    /// <summary>
    /// Отключение управление с контроллем кинематики
    /// </summary>
    /// <param name="setIsKinematic">контроль кинематики</param>
    public void DisablePlayerMovement(bool setIsKinematic = false)
    {
        //playerController.DelayMove(1000);

        playerController.rootMove = true;

        //this.enabled = false;

        //playerController.enabled = false;

        if (setIsKinematic)
        {
            playerController.rb.isKinematic = true;
        }
        else
        {
            playerController.rb.isKinematic = false;
        }

        //playerController.rb.velocity = Vector3.zero;
    }

    /// <summary>
    /// Проверка на земле ли игрок
    /// </summary>
    /// <returns>еrue - на земле, false в воздухе</returns>
    public bool PlayerIsGrounded()
    {
        return playerController.rays.commonCollisionDown;
    }
}