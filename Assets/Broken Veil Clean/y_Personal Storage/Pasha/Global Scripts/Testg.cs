﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testg : MonoBehaviour
{
    [SerializeField] Transform[] go;
    public Vector3 pivotFoots;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        pivotFoots = (go[0].position + go[1].position) / 2;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawCube((go[0].position + go[1].position) / 2, Vector3.one * 0.1f);
    }
}
