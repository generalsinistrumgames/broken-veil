﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;

//переделать под стик KeyController.GetHorizontalAxis
//удалить клавиши в project settings


public class _saves_key_values
{
    public KeyCode[][] _KeyMass;
}



public class KeyController : MonoBehaviour
{
    bool is_ini = false;

    bool _IsUpdNeeded = true;
    [SerializeField]
    public KeyCode[][] _KeyMass;

    [SerializeField]
    bool[] _LastKeys;
    [SerializeField]
    bool[] _Keys;
    [SerializeField]
    bool[] _KeysDown;
    [SerializeField]
    bool[] _KeysUp;

    public const string _path_and_name = "Keys.xml";

    public bool this[command index]
    {
        get
        {
            UpdateKeysState();
            return _Keys[(int)index];
        }
    }
    public bool[] Keys
    {
        get
        {
            UpdateKeysState();
            return _Keys;
        }
    }
    public bool[] KeysDown
    {
        get
        {
            UpdateKeysState();
            return _KeysDown;
        }
    }
    public bool[] KeysUp
    {
        get
        {
            UpdateKeysState();
            return _KeysUp;
        }
    }

    #region test
    //public float test_HorizontalAxis = 0f;
    //public bool[] test_Keys;
    //public List<command> test_pressed;
    //public List<command> test_up;
    // public List<command> test_down;
    //public bool[] test_lastkey=new bool[(int)command.Length];
    #endregion

    // Start is called before the first frame update
    public void Start()
    {
        if (is_ini)
            return;
        _LastKeys = new bool[(int)command.Length];
        _Keys = new bool[(int)command.Length];
        _KeysDown = new bool[(int)command.Length];
        _KeysUp = new bool[(int)command.Length];
        //Reset_Keys();
        load_keys();
        Debug.LogError("laser="+_KeyMass[(int)command.laser][0]);
    }

    // Update is called once per frame
    void Update()
    {
        //test
        //test
        UpdateKeysState();
        //var s = "";
        //foreach (var item in _KeyMass)
        //{
        //    s += item[0].ToString() + "   ";
        //}
        //Debug.Log(s);
    }

    public void UpdateKeysState()
    {
        if (!_IsUpdNeeded)
            return;
        _IsUpdNeeded = false;
        //LastKeys
        for (int i = 0; i < (int)command.Length; i++)
            _LastKeys[i] = _Keys[i];
        //Keys
        for (int i = 0; i < (int)command.Length; i++)
        {
            _Keys[i] = false;
            for (int j = 0; j < _KeyMass[i].Length; j++)
            {
                if (Input.GetKey(_KeyMass[i][j]))
                    _Keys[i] = true;
            }
        }
        //KeysDown
        for (int i = 0; i < (int)command.Length; i++)
            _KeysDown[i] = _Keys[i] && (!_LastKeys[i]);
        //KeysUp
        for (int i = 0; i < (int)command.Length; i++)
            _KeysUp[i] = (!_Keys[i]) && _LastKeys[i];

    }

    void LateUpdate()
    {
        _IsUpdNeeded = true;
    }

    public void Setup_Keys(KeyCode[][] _New_Keysettings)
    {
        var s = "def=";
        foreach (var item in _New_Keysettings)
        {
            s += item[0].ToString() + "   ";
        }
        Debug.Log(s);

        _KeyMass = _New_Keysettings;
        for (int i = 0; i < (int)command.Length; i++)
            _LastKeys[i] = false;
        _IsUpdNeeded = true;

        //save_keys();
    }

    public void Reset_Keys()
    {
        _KeyMass = Default_settings();
        for (int i = 0; i < (int)command.Length; i++)
            _LastKeys[i] = false;
        _IsUpdNeeded = true;
    }

    KeyCode[][] Default_settings()
    {
        KeyCode[][] result = new KeyCode[(int)command.Length][];
        result[(int)command.left] = new KeyCode[] { KeyCode.LeftArrow, KeyCode.A };
        result[(int)command.right] = new KeyCode[] { KeyCode.RightArrow, KeyCode.D };
        result[(int)command.up] = new KeyCode[] { KeyCode.UpArrow, KeyCode.W };
        result[(int)command.down] = new KeyCode[] { KeyCode.DownArrow, KeyCode.S };
        result[(int)command.jump] = new KeyCode[] { KeyCode.Space };
        result[(int)command.run] = new KeyCode[] { KeyCode.T };
        result[(int)command.laser] = new KeyCode[] { KeyCode.E };
        result[(int)command.crouch] = new KeyCode[] { KeyCode.Escape };
        result[(int)command.enter] = new KeyCode[] { KeyCode.Return, KeyCode.KeypadEnter };
        return result;
    }

    public float GetHorizontalAxis()
    {
        //переделать под стик
        if (_Keys[(int)command.left] && _Keys[(int)command.right])
            return 0f;
        if (_Keys[(int)command.right])
            return 1f;
        if (_Keys[(int)command.left])
            return -1f;
        //return 0f;
        return Input.GetAxisRaw("Horizontal");
    }
    public float GetVerticalAxis()
    {
        //переделать под стик
        if (_Keys[(int)command.down] && _Keys[(int)command.up])
            return 0f;
        if (_Keys[(int)command.down])
            return -1f;
        if (_Keys[(int)command.up])
            return 1f;
        //return 0f;
        return Input.GetAxisRaw("Vertical");
    }

    public void save_keys()
    {
       

        _saves_key_values obj = new _saves_key_values();
        obj._KeyMass = _KeyMass.Clone() as KeyCode[][];

        XmlSerializer xml = new XmlSerializer(typeof(_saves_key_values));
        using (FileStream fs=new FileStream(_path_and_name, FileMode.Create, FileAccess.Write))
        {
            xml.Serialize(fs, obj);
        }
    }

    public void load_keys()
    {
        XmlSerializer xml = new XmlSerializer(typeof(_saves_key_values));

        _saves_key_values obj = new _saves_key_values();

        try
        {
            using (FileStream fs = new FileStream(_path_and_name, FileMode.Open, FileAccess.Read))
            {
                obj = xml.Deserialize(fs) as _saves_key_values;
            }
        }
        catch
        {
            Debug.LogError("read_error");
            Reset_Keys();
            save_keys();
            return;
        }

        Setup_Keys(obj._KeyMass);
    }

    /* axis
    //переделать под стик GetHorizontalAxis
    public float GetHorizontalAxis()
    {        
        //переделать под стик
        if(IsKeyDown(command.left)&& IsKeyDown(command.right))
            return 0f;
        if (IsKeyDown(command.right))
            return 1f;
        if (IsKeyDown(command.left))
            return -1f;
        //return 0f;
        return Input.GetAxisRaw("Horizontal");
    }
    public float GetVerticalAxis()
    {
        //переделать под стик
        if (IsKeyDown(command.up) && IsKeyDown(command.down))
            return 0f;
        if (IsKeyDown(command.down))
            return -1f;
        if (IsKeyDown(command.up))
            return 1f;
        //return 0f;
        return Input.GetAxisRaw("Vertical");
    }*/
}
/* very bad code
public class KeySender
{
    public bool[] Keys=new bool[(int)command.Length];
    public List<command> pressed = new List<command>();
    public List<command> up = new List<command>();
    public List<command> down = new List<command>();
    public float HorizontalAxis = 0f;
    public float VerticalAxis = 0f;
    public bool this[command c]
    {
        get
        {
            return Keys[(int)c];
        }
        set
        {
            Keys[(int)c] = value;
        }
    }

    public void clear()
    {
        pressed.Clear();
        up.Clear();
        down.Clear();
    }
}
public class KeyCommand
{
    bool isini = false;
    KeyCode[] Keys = new KeyCode[(int)command.Length];
    KeyCode[] altkeys = new KeyCode[(int)command.Length];
    bool[] last_upd_state = new bool[(int)command.Length];

    public KeyCommand(KeyCode[] mass,KeyCode[] altmass)
    {
        Ini(mass,altmass);
    }
    public void Ini(KeyCode[] mass,KeyCode[] altmass)
    {
        isini = true;

        if (mass.Length == (int)command.Length)
            Keys = mass;
        else
            throw new System.Exception("что-то пошло не так. массив KeyCode неверно считан");

        if (altmass.Length == (int)command.Length)
            altkeys = altmass;
        else
            throw new System.Exception("что-то пошло не так. массив AltKeyCode неверно считан");

        for (int i = 0; i < (int)command.Length; i++)
            last_upd_state[i] = false;
    }
    public void after_update()
    {
        for (int i = 0; i < (int)command.Length; i++)
        {
            last_upd_state[i] = Input.GetKey(Keys[i]) || Input.GetKey(altkeys[i]);
        }
    }
    public KeyCode this[command c]
    {
        get
        {
            if (isini)
                return Keys[(int)c];
            else
                throw new System.Exception("что-то пошло не так. Keycommand не инициализирован.");
        }
    }
    public KeyCode AltKeys(command c)
    {
        return altkeys[(int)c];
    }
    public bool last_update_state(command c)
    {
        return last_upd_state[(int)c];
    }
}*/

public enum command
{
    left,
    right,
    up,
    down,
    jump,
    run,
    laser,
    crouch,
    enter,
    //
    LastElement=enter,
    Length=LastElement+1
}