﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct b_math
{
    public static bool eq(float x1, float x2, float acc)
    {
        float dx = x1 - x2;
        if (dx < 0)
            dx *= -1f;
        if (dx > acc)
            return false;
        return true;
    }
    public static bool eq(float x1, float x2)
    {
        return eq(x1, x2, 0.002f);
    }
    public static float Is_negative(float val)
    {
        if (val < 0f)
            return -1f;
        return 1f;
    }
    public static float Closest(float val, float[] points)
    {
        int result = 0;
        float dval = float.MaxValue;

        for (int i = 0; i < points.Length; i++)
        {
            float dval2 = Mathf.Abs(val - points[i]);
            if (dval2 < dval)
            {
                result = i;
                dval = dval2;
            }
        }
        return points[result];
    }

    public static float My_lerp(float current,float final,float speed_per_second)
    {
        float delta = current - final;
        delta = Mathf.Clamp01(Mathf.Abs((speed_per_second * Time.deltaTime) / delta));
        return Mathf.Lerp(current, final, delta);
    }

    public static float My_lerp(float current, float final, float speed_per_second,float percent_change_per_second)
    {
        float delta = current - final;
        if (percent_change_per_second < 0F)
            percent_change_per_second = 0f;
        delta = Mathf.Clamp01(Mathf.Abs(((speed_per_second) * Time.deltaTime) / delta + percent_change_per_second * Time.deltaTime));
        return Mathf.Lerp(current, final, delta);
    }

    public static Vector3 My_lerp(Vector3 current, Vector3 final, float speed_per_second)
    {
        float delta = (current - final).magnitude;
        if (delta < 0.000001f)
            return (current);
        delta = Mathf.Clamp01(Mathf.Abs((speed_per_second * Time.deltaTime) / delta));
        return Vector3.Lerp(current, final, delta);
    }

    public static Vector3 My_lerp(Vector3 current, Vector3 final, float speed_per_second, float percent_change_per_second)
    {
        float delta = (current - final).magnitude;
        if (percent_change_per_second < 0F)
            percent_change_per_second = 0f;
        delta = Mathf.Clamp01(Mathf.Abs(((speed_per_second) * Time.deltaTime) / delta + percent_change_per_second * Time.deltaTime));
        return Vector3.Lerp(current, final, delta);
    }

    public static Vector3 Clamp(Vector3 target,Vector3 clamp_vector1,Vector3 clamp_vector2)
    {
        return new Vector3(
            Mathf.Clamp(target.x, Mathf.Min(clamp_vector1.x, clamp_vector2.x), Mathf.Max(clamp_vector1.x, clamp_vector2.x)),
            Mathf.Clamp(target.y, Mathf.Min(clamp_vector1.y, clamp_vector2.y), Mathf.Max(clamp_vector1.y, clamp_vector2.y)),
            Mathf.Clamp(target.z, Mathf.Min(clamp_vector1.z, clamp_vector2.z), Mathf.Max(clamp_vector1.z, clamp_vector2.z))
            );
    }
    public static Vector3 Clamp(Vector3 target, Vector2 clamp_x,Vector2 clamp_y, Vector2 clamp_z)
    {
        return new Vector3(
            Mathf.Clamp(target.x, Mathf.Min(clamp_x.x, clamp_x.y), Mathf.Max(clamp_x.x, clamp_x.y)),
            Mathf.Clamp(target.y, Mathf.Min(clamp_y.x, clamp_y.y), Mathf.Max(clamp_y.x, clamp_y.y)),
            Mathf.Clamp(target.z, Mathf.Min(clamp_z.x, clamp_z.y), Mathf.Max(clamp_z.x, clamp_z.y))
            );
    }
}
