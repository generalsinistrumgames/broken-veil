﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FloatingObject : MonoBehaviour
{
    Vector3 startPos;
    [SerializeField] float amplitude = 200;
    [SerializeField] float amplitudeRotation = 50;
    [SerializeField] Vector3 offsetY;
    void Start()
    {
        startPos = transform.position;
    }

    void Update()
    {
        //var amplitudeFactor = amplitude;
        //if (transform.position.y > startPos.y)
        //    amplitudeFactor /= 50;
        transform.position += new Vector3(0, Mathf.Sin(Time.time * -1),0) / amplitude;
        transform.rotation *= Quaternion.Euler(Mathf.Cos(Time.time) / amplitude * amplitudeRotation, 0 , Mathf.Cos(Time.time) / amplitude * 50);
    }
}
