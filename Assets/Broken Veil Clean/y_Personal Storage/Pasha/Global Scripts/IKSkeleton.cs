﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;
using UnityEngine.Animations.Rigging;
using System;

public class IKSkeleton : MonoBehaviour
{
    public Transform[] sholdersMesh;
    public Transform deformationSystemPerson;

    [Header("ClimbHand")]
    public float offsetYHandClimb;

    public ChekedWall playerCollision;

    [Header("Laser Settings")]
    public bool shot;

    [SerializeField, Range(0, 1f)] float offsetPosHand;
    [SerializeField, Range(0, 1f)] float offsetPosElbowDistance;
    [SerializeField] float offsetPosXHand;
    [SerializeField] float offsetPosYHand;
    [SerializeField] float offsetPosZHand;
    [SerializeField] float speedRiseHandLaser = 2f;
    [SerializeField] float speedGiveUpHandLaser = 2f;

    [Header("Offsets Hand From Forward")]
    [SerializeField, Range(-1, 1)] float offsetFrontPositionHandWidht = 0.06f;

    [Header("Offsets Hand From Side")]
    [SerializeField, Range(-1, 1)] float offsetPositionHandX;
    [SerializeField, Range(-1, 1)] float offsetPositionHandY;
    [SerializeField, Range(-1, 1)] float offsetPositionHandZ;
    [SerializeField, Range(-180, 180)] float offsetRotationHandX;
    [SerializeField, Range(-180, 180)] float offsetRotationHandY;
    [SerializeField, Range(-180, 180)] float offsetRotationHandZ;
    [SerializeField] float minDistanceToWallFromSide;
    [SerializeField] float maxDistanceToWallFromSide;
    [SerializeField] float maxDistanceToWallFromFront;
    [SerializeField] float speedRiseHandWall = 2f;
    [SerializeField] float speedGiveUpHandWall = 2f;

    PlayerController playerController;
    [SerializeField] LayerMask layerMask;

    public const float offsetYFoot = 0.05f; //дистанция от локальных координат таргета ноги до пола 

    [Header("Legs Settings")]
    float distanceNextStep = 0.45f; //0.75f for step //!!!!!!!!!!!!!!!!!!!!!!!!!!!FIX
    float universalValueForDistanceNextStep; //!!!!!!!!!!!!!!!!!!!!!!!!!!!FIX

    public bool stop;

    Coroutine coroutineForLimbAction;

    public RigPlayer rig;
    Animator animator;
    public bool portable;
    Player player;
    public BoolVariable laserControl;
    Coroutine sneakWallCollision;
    [SerializeField] Transform head;

    void Start()
    {
        animator = GetComponent<Animator>();
        playerController = GetComponent<PlayerController>();
        universalValueForDistanceNextStep = distanceNextStep / 1.45f/*скорость бега*/;
        playerCollision = new ChekedWall();
        layerMask = playerController.maskForHandAndLeg;
        player = GetComponent<Player>();
    }

    public void CalcIKPosition()
    {
        playerCollision.Reset();

        if (!playerController.onLadder && !playerController.drag)
        {
            CheckFootCollision(Limbs.LeftFoot);
            CheckFootCollision(Limbs.RightFoot);

            CheckArm(Limbs.LeftHand);
            if (!playerController.laser)
                CheckArm(Limbs.RightHand);
        }

        if (!playerController.Stop && !playerController.onLadder && !portable && !playerController.drag)
        {
            if (!playerCollision.leftHand)
            {
                rig.leftArm.weight -= Time.deltaTime * speedGiveUpHandWall;
            }
            if (!playerCollision.rightHand)
                rig.rightArm.weight -= Time.deltaTime * speedGiveUpHandWall;
        }
    }

    public IEnumerator Laser()
    {
        RaycastHit hit = new RaycastHit();

        var arm = rig.rightArm.data;
        var headTarget = rig.head.data.sourceObjects[0];
        
        while (playerController.laser)
        {
            //Ray rayEndPos = Camera.main.ScreenPointToRay(Input.mousePosition);
            //if (Physics.Raycast(rayEndPos, out hit, layerMask))
            //{
            //    if (!shot)
            //    {
            //        arm.target.position = hit.point;
            //    }
            //    Debug.DrawRay(arm.mid.transform.position, hit.point - arm.mid.transform.position, Color.cyan);
            //}
            if (player.itemInPlayerHand)
            {
                playerController.laser = false;
                playerController.laserPrefab.SetActive(false);
                break;
            }

            Ray rayEndPos = Camera.main.ScreenPointToRay(Input.mousePosition);

            rig.laser.weight += Time.deltaTime * speedRiseHandLaser;
            if (Physics.Raycast(rayEndPos, out hit, layerMask))
            {
                arm.target.position = Vector3.Lerp(arm.target.position, hit.point, 0.25f);
                stop = true;
                Debug.DrawRay(arm.mid.transform.position, hit.point - arm.mid.transform.position, Color.cyan);

                //вращение локтя
                var directionHand = (hit.point - arm.root.position).normalized;
                var rot = transform.InverseTransformDirection(directionHand);
                rot = new Vector3(Mathf.Clamp(rot.x, 0.2f, 1), directionHand.y, Mathf.Clamp(rot.z, 0, 1));

                arm.target.localPosition = rot * offsetPosHand + Vector3.Cross(Vector3.up, rot) * offsetPosElbowDistance + new Vector3(offsetPosXHand, offsetPosYHand, offsetPosZHand);
                if (!shot)
                {
                    var hitToLocal = transform.InverseTransformPoint(hit.point);

                    if (hitToLocal.x + hitToLocal.z >= 0)
                        headTarget.transform.localPosition = Vector3.Lerp(headTarget.transform.localPosition, hitToLocal, 0.25f);
                    if (hitToLocal.x + hitToLocal.z < 0)
                    {
                        if (hitToLocal.x == hitToLocal.z) { hitToLocal = Vector3.zero;}

                        if (hitToLocal.x < hitToLocal.z)
                        {
                            hitToLocal.z = Mathf.Abs(hitToLocal.x);
                        }
                        if (hitToLocal.x > hitToLocal.z)
                        {
                            hitToLocal.x = Mathf.Abs(hitToLocal.z);
                        }

                        headTarget.transform.localPosition = Vector3.Lerp(headTarget.transform.localPosition, hitToLocal, 0.25f);
                        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(new Vector3(headTarget.transform.position.x, transform.position.y, headTarget.transform.position.z) - transform.position), 1 * Time.deltaTime);
                    }
                    
                        
                }   
            }
            yield return null;
        }
        playerController.laserCoroutine = null;
        StartCoroutine(WeightReductionLaser(rig.laser));
    }

    public IEnumerator SneakWallCollision()
    {
        while (playerCollision.head)
        {
            rig.sneakWallCollision.weight += Time.deltaTime;
            yield return null;
        }
        while (rig.sneakWallCollision.weight != 0)
        {
            rig.sneakWallCollision.weight -= Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator WeightReductionLaser(Rig rig)
    {
        while (rig.weight > 0)
        {
            rig.weight -= Time.deltaTime * speedGiveUpHandLaser;
            yield return null;
        }
    }

    public Transform GetLimb(Limbs limb)
    {
        switch ((int)limb)
        {
            case 0:
                return rig.leftArm.data.target;
            case 1:
                return rig.rightArm.data.target;
            case 2:
                return rig.leftLeg.data.target;
            case 3:
                return rig.rightLeg.data.target;
            default:
                return null;
        }

    }

    #region Ik for different animations 
    /// <summary>
    /// Корректировка НЕ ЦИКЛИЧНЫХ анимаций с помощью процедурной анимации.
    /// </summary>
    /// <param name="target"></param>
    /// <param name="delay"></param>
    /// <param name="limbStruct"></param>
    /// <param name="speed"></param>
    public void LimbAction(Vector3 target, float delay, float time, Limbs limbStruct, float speed)
    {
        if (coroutineForLimbAction != null)
            return;
        Transform limb = GetLimb(limbStruct);

        coroutineForLimbAction = StartCoroutine(LimbActionCoroutine(target, delay, time, limb, speed, limbStruct));
    }

    /// <summary>
    /// Корректировка НЕ ЦИКЛИЧНЫХ анимаций с помощью процедурной анимации.
    /// </summary>
    /// <param name="target"></param>
    /// <param name="delay"></param>
    /// <param name="limbStruct"></param>
    public void LimbAction(Vector3 target, float delay, float time, Limbs limbStruct)
    {
        if (coroutineForLimbAction != null)
            return;
        Transform limb = GetLimb(limbStruct);
        coroutineForLimbAction = StartCoroutine(LimbActionCoroutine(target, delay, time, limb, limbStruct));
    }

    IEnumerator LimbActionCoroutine(Vector3 target, float delay, float time, Transform limb, float speed, Limbs limbString)
    {
        float timer = 0;
        stop = true;

        yield return new WaitForSeconds(delay);
        //GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
        while (timer < time)
        {
            timer += Time.deltaTime;
            limb.position = Vector3.Lerp(limb.position, target, GetComponent<Animator>().GetFloat(limbString.ToString()) * speed);
            yield return new WaitForFixedUpdate();
        }
        stop = false;
        coroutineForLimbAction = null;
    }

    IEnumerator LimbActionCoroutine(Vector3 target, float delay, float time, Transform limb, Limbs limbString)
    {
        float timer = 0;
        stop = true;
        yield return new WaitForSeconds(delay);
        //float time = GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime;
        while (timer < time)
        {
            timer += Time.deltaTime;
            limb.position = Vector3.Lerp(limb.position, target, GetComponent<Animator>().GetFloat(limbString.ToString()));
            yield return new WaitForFixedUpdate();
        }
        stop = false;
        coroutineForLimbAction = null;
    }
    #endregion

    public void CheckFootCollision(Limbs leg)
    {
        var raycastOrigin = playerController.raycastOrigin.bottom;
        var hit = new RaycastHit();
        var nextHit = new RaycastHit();

        if (leg == Limbs.LeftFoot)
        {
            var foot = rig.leftLeg.data.target;
            var hip = rig.leftLeg.data.root;
            var ray = new Ray(new Vector3(hip.position.x, raycastOrigin.y, hip.position.z), Vector3.down * playerController.stepOffset * 2);

            if (Physics.Raycast(ray, out hit, playerController.stepOffset * 2, layerMask))
            {
                //foot.localPosition = new Vector3(transform.InverseTransformPoint(transform.position) - hit.point.y, 0, 0);
                ////Debug.DrawRay(ray.origin, Vector3.down * lenghtLeg * 2, Color.cyan);
                float normalizedDistance = animator.GetFloat("LeftFoot");
                var nextStepPoint = hip.position + transform.forward * playerController.Movement * universalValueForDistanceNextStep;
                //var nextStepPoint = hip.position + transform.forward * playerController.rb.velocity.magnitude * Time.fixedDeltaTime * 5;
                ray = new Ray(new Vector3(nextStepPoint.x, raycastOrigin.y, nextStepPoint.z), Vector3.down * playerController.stepOffset * 2);
                if (Physics.Raycast(ray, out nextHit, playerController.stepOffset * 4, layerMask)) ////playerController.stepOffset * 4
                {
                    Debug.DrawRay(ray.origin, ray.direction, Color.blue);
                }
                else
                {
                    nextHit = hit;
                    foot.localPosition = Vector3.zero;
                }

                var localPosNextHitY = nextHit.point.y - deformationSystemPerson.position.y;
                var localPosHitY = hit.point.y - deformationSystemPerson.position.y;

                foot.localPosition = Vector3.Lerp(foot.localPosition, new Vector3(-(localPosHitY + (localPosNextHitY - localPosHitY) * normalizedDistance), 0, 0), 0.1f);

            }
        }
        else
        {
            var foot = rig.rightLeg.data.target;
            var hip = rig.rightLeg.data.root;
            var ray = new Ray(new Vector3(hip.position.x, raycastOrigin.y, hip.position.z), Vector3.down * playerController.stepOffset * 2);

            if (Physics.Raycast(ray, out hit, playerController.stepOffset * 2, layerMask))
            {
                //Debug.DrawRay(ray.origin, Vector3.down * lenghtLeg * 2, Color.cyan);
                float normalizedDistance = animator.GetFloat("RightFoot");
                var nextStepPoint = hip.position + transform.forward * playerController.Movement * universalValueForDistanceNextStep;
                ray = new Ray(new Vector3(nextStepPoint.x, raycastOrigin.y, nextStepPoint.z), Vector3.down * playerController.stepOffset * 2);
                if (Physics.Raycast(ray, out nextHit, playerController.stepOffset * 4, layerMask)) //// playerController.stepOffset * 4
                {
                    //Debug.DrawRay(ray.origin, Vector3.down * playerController.stepOffset * 2 * 2, Color.blue);
                    //foot.localPosition = new Vector3((nextHit.point.y - hit.point.y) * normalizedDistance, 0, 0);
                }
                else
                {
                    nextHit = hit;
                    foot.localPosition = Vector3.zero;
                }

                var localPosNextHitY = nextHit.point.y - deformationSystemPerson.position.y;
                var localPosHitY = hit.point.y - deformationSystemPerson.position.y;

                foot.localPosition = Vector3.Lerp(foot.localPosition, new Vector3((localPosHitY + (localPosNextHitY - localPosHitY) * normalizedDistance), 0, 0), 0.1f);
            }
        }
    }

    void FixedUpdate()
    {
        CalcIKPosition();

        if (playerController.Sneak)
        {
            foreach (var item in Physics.OverlapSphere(head.position + new Vector3(-0.08f, 0.02f, 0), 0.14f))
            {
                if (item.tag != "Player" && item.gameObject.layer == 0 && item.gameObject.layer == 9)
                {
                    playerCollision.head = true;
                    if (sneakWallCollision == null)
                    {
                        StartCoroutine(SneakWallCollision());
                    }
                }
            }
        }

    }

    private void Update()
    {
        //debug
        //if (Input.GetMouseButtonDown(0))
        //    shot = !shot;
    }

    void CheckArm(Limbs armEnum)
    {
        if (!playerController.onLadder && !playerController.drag && !playerController.onRope && !playerController.Stop && !player.itemInPlayerHand && !playerController.hangOnHook)
        {
            Ray raySideways;
            Ray rayFront;
            var hit = new RaycastHit();
            if (armEnum == Limbs.LeftHand)
            {
                raySideways = new Ray(sholdersMesh[(int)armEnum].position, transform.right * 0.6f * -1);
                rayFront = new Ray(sholdersMesh[(int)armEnum].position, transform.forward * 0.6f);

                if (!playerController.Sneak && playerController.movementInput > 0.1f && Physics.Raycast(rayFront, out hit, maxDistanceToWallFromFront, layerMask)) //front check wall
                {
                    Debug.Log("leftCheck");
                    Debug.DrawRay(rayFront.origin, rayFront.direction, Color.red);
                    rig.leftArm.weight += Time.deltaTime * speedRiseHandWall;
                    playerCollision.leftHand = true;
                    rig.leftArm.data.target.position = hit.point - transform.forward * 0.02f - (Vector3.up * offsetPositionHandY / 2) - Vector3.up * offsetYHandClimb + transform.right * -1 * offsetFrontPositionHandWidht;
                    rig.leftArm.data.target.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal) * transform.rotation * Quaternion.Euler(90, -90, 0);
                }
                else if (playerController.movementInput > 0.1f && Physics.Raycast(raySideways, out hit, maxDistanceToWallFromSide, layerMask)) //left check wall
                {
                    Debug.DrawRay(raySideways.origin, raySideways.direction, Color.red);
                    if (hit.distance > minDistanceToWallFromSide)
                    {
                        rig.leftArm.weight += Time.deltaTime * speedRiseHandWall;
                        playerCollision.leftHand = true;
                        rig.leftArm.data.target.position = hit.point + new Vector3(offsetPositionHandX, offsetPositionHandY, offsetPositionHandZ);
                        rig.leftArm.data.target.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal) * transform.rotation * Quaternion.Euler(offsetRotationHandX, -offsetRotationHandY, offsetRotationHandZ);
                    }
                }
                else if (!playerController.Stop)
                {
                    playerCollision.leftHand = false;
                    rig.leftArm.weight -= Time.deltaTime * speedGiveUpHandWall;
                }
            }
            if (armEnum == Limbs.RightHand)
            {
                raySideways = new Ray(sholdersMesh[(int)armEnum].position, transform.right * 0.6f);
                rayFront = new Ray(sholdersMesh[(int)armEnum].position, transform.forward * 0.6f);

                if (!playerController.Sneak && playerController.movementInput > 0.1f && Physics.Raycast(rayFront, out hit, maxDistanceToWallFromFront, layerMask)) //front check wall
                {
                    Debug.DrawRay(rayFront.origin, rayFront.direction, Color.red);
                    rig.rightArm.weight += Time.deltaTime * speedRiseHandWall;
                    playerCollision.rightHand = true;
                    rig.rightArm.data.target.position = hit.point - transform.forward * 0.02f - (Vector3.up * offsetPositionHandY / 2) - Vector3.up * offsetYHandClimb + transform.right * offsetFrontPositionHandWidht;
                    rig.rightArm.data.target.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal) * transform.rotation * Quaternion.Euler(-90, 90, 0);
                }
                else if (playerController.movementInput > 0.1f && Physics.Raycast(raySideways, out hit, maxDistanceToWallFromSide, layerMask)) //right check wall
                {
                    Debug.DrawRay(raySideways.origin, raySideways.direction, Color.red);
                    if (hit.distance > minDistanceToWallFromSide)
                    {
                        rig.rightArm.weight += Time.deltaTime * speedRiseHandWall;
                        playerCollision.rightHand = true;
                        rig.rightArm.data.target.position = hit.point + new Vector3(offsetPositionHandX, offsetPositionHandY, offsetPositionHandZ);
                        rig.rightArm.data.target.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal) * transform.rotation * Quaternion.Euler(-offsetRotationHandX, offsetRotationHandY, offsetRotationHandZ);
                    }
                }
                else if (!playerController.Stop)
                {
                    playerCollision.rightHand = false;
                    rig.rightArm.weight -= Time.deltaTime * speedGiveUpHandWall;
                }
            }
        }
    }

    public enum Limbs
    {
        LeftHand,
        RightHand,
        LeftFoot,
        RightFoot,
    }

    public struct ChekedWall
    {
        public bool leftHand, rightHand;
        public bool leftFoot, rightFoot;
        public bool head;

        public void Reset()
        {
            leftHand = rightHand = leftFoot = rightFoot = head = false;
        }
    }

    [Serializable]
    public struct RigPlayer
    {
        public MultiAimConstraint chest;
        public TwoBoneIKConstraint leftArm;
        public TwoBoneIKConstraint rightArm;
        public TwoBoneIKConstraint leftLeg;
        public TwoBoneIKConstraint rightLeg;
        public MultiAimConstraint head;
        public Rig sneakWallCollision;
        public Rig laser;
        public Rig mainRig;
        public TwoBoneIKConstraint leftLegWorld;
        public TwoBoneIKConstraint rightLegWorld;
    }
}
