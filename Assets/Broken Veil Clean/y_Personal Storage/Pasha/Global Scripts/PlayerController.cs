﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;
using System.IO;
using UnityEngine.SceneManagement;

namespace GlobalScripts
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    public class PlayerController : MonoBehaviour
    {
        public Rigidbody rb;
        CapsuleCollider collider;
        [Header("Common attributes")/*, SerializeField*/]// float acceleration = 12.5f; //ускорение
        [SerializeField, Tooltip("максимальная скорость бега")] public float speedMax = 1f;
        [SerializeField, Tooltip("сила прыжка")] float jumpForce = 3.5f;
        [SerializeField, Tooltip("сила прыжка с разбега")] float superJumpForce = 1f;
        [SerializeField] float speedRotation = 7.5f; //скорость поворота Slerp
        [SerializeField, Tooltip("высота шага. В зависимости от значения параметра, изменяется основной коллайдер ГГ в режиме редактора")] public float stepOffset = 0.2f;
        [SerializeField] float physicDrag = 5; //трение замедляющее только на земле. в воздухе не работает
        [HideInInspector] public Vector3 inputVector = Vector3.zero;
        [SerializeField] float speedRestoreLegs = 0.2f;

        //[Header("Lurch")]
        [HideInInspector] /*[SerializeField]*/ float angleLurchMax = 20;
        [HideInInspector] /*[SerializeField]*/float lurchTimeInterpolation = 0.1f;
        [HideInInspector] /*[SerializeField]*/float maxAngleLurchRotZ = 22.5f;
        float localLurchAngle = 0f;
        [HideInInspector] /*[SerializeField]*/float angleLeanMax = 20;
        [HideInInspector] /*[SerializeField]*/float leanTimeInterpolation = 0.1f;
        [HideInInspector] /*[SerializeField]*/float maxAngleLeanRotX = 22.5f;
        float localLeanAngle = 0f;
        float oldVelocity = 0f;

        [Header("Collision detect"), SerializeField] float distanceHorizontalRay = 0.21f; //дистанция до взаимодествия при столкновении со стенкой
        [SerializeField, Tooltip("дистанция до взаимодествия при столкновении с потолком")] float distanceVerticalRayTop = 0.7f;
        float distanceVerticalRayBottom = 0.3f; //дистанция до взаимодествия при столкновении с землей
        float lenghtLegs;

        [Header("Animation Offsets"), SerializeField] float offYAutoClimbAnimation = 0.16f;
        //[SerializeField] float offtDirHangAnimation = 0.25f; //для изменения офсета по направлению в реалтайме
        [SerializeField, Tooltip("для изменения офсета по направлению в реалтайме")] float offtDirClimbCoroutin = 0.25f;
        [SerializeField, Tooltip("для изменения офсета по У в реалтайме")] float offYClimbCoroutin = 0.25f;
        [SerializeField] float offYHangOnGround = 0;
        [SerializeField] float offYHang = 0;
        [SerializeField] float offYHangUp = 0;
        //[SerializeField] float offsetYClimbLadderAnimation = -0.25f; //для изменения офсета по У в реалтайме

        [Header("Sprint"), SerializeField] float sprintAcceleration = 0.21f;
        [SerializeField] public float sprintMaxTime = 5;
        [SerializeField] float sprintDelay = 5;
        float sprintDelayTimer = 0;
        float sprintTimer = 0;
        [HideInInspector] public bool sprintTired;

        [Header("Hook"), SerializeField] float offsetYHook = -2.5f;


        [Header("Laser"), SerializeField] float maxEulerToRotationHandX = 30;
        [SerializeField] float maxEulerToRotationHandY = 60;
        [SerializeField] float maxEulerToRotationArm = 45;



        public LayerMask maskForHandAndLeg;
        public bool jump = true;

        const float skinWidth = 0.015f;
        public int horizontalRayCount = 3;
        public int verticalRayCount = 3;

        float horizontalRaySpacing;
        float verticalRaySpacing;

        [Header("Ladder")]

        [SerializeField] float climbingTimeLadder;
        Coroutine upwardMovementLadder;
        Coroutine downwardMovementLadder;
        bool swapGrabLadder;
        [SerializeField] float offsetPositionOnLadder;
        [SerializeField] Step currentStep;
        int currentStepIdex;
        [SerializeField] Step oldStep;
        [SerializeField] float offsetDeformationSystem;
        [SerializeField] Transform deformationSystem;
        [SerializeField] float widthOffsetGrabForHands = 0.115f;
        [SerializeField] float widthOffsetGrabForLegs = 0.115f;
        [SerializeField] float deepOffsetForLegsLadder = 0.05f;
        [SerializeField] float deepOffsetForHandsLadder = 0.05f;
        [SerializeField] float hightOffsetForLegsLadder = 0.075f;
        [SerializeField] float hightOffsetForHandsLadder = 0.05f;
        LimbsData limbs;

        [Header("Rope")]
        [SerializeField] float climbingSpeedRope;
        Coroutine upwardMovementRope;
        Coroutine downwardMovementRope;
        public bool onRope;

        [SerializeField] float offsetPositionOnRope;
        [SerializeField] bool IkRope; // for debug
        [SerializeField] int currentStepRope;
        [SerializeField] int oldStepRope;
        [SerializeField] float offsetRopeHand;
        [SerializeField] float offsetRopeFoot;

        [Header("Jump")]
        [SerializeField] float jumpDelay = 0.4f;
        [SerializeField, Range(0, 1)] float inertialDamperForSoftLand = 0.25f;
        [SerializeField, Range(0, 1)] float inertialDamperForHardLand = 0;
        [SerializeField] float fallSpeedForHardLanding = -4;
        [SerializeField] float coyoteTime = 0.25f;
        float coyoteTimer;
        bool freeFall;



        float movement;
        /// <summary>
        /// Float  Для анимации
        /// </summary>
        public float Movement { get { return movement; } }
        public bool JumpBool { get { return jump; } }


        public RaycastOrigins raycastOrigin;
        public Raycasts rays;

        [Header("Tackle")]
        [SerializeField] float tackleTime = 1;
        [SerializeField] float tackleFactorForce;
        Coroutine tackleCoroutine;

        [Header("Kradusya")]
        public bool obstacleFromTop;
        GameObject dragObject;
        bool sneak;
        float speedFactor = 1;

        public float maxSpeedFactor = 1;
        /// <summary>
        /// Модификатор скорости
        /// </summary>
        public float SpeedFactor { get => speedFactor; set => speedFactor = value; }

        bool buttonCheck;

        [HideInInspector] public float movementInput;
        [HideInInspector] public Vector3 movementDiretion;

        [Header("Drag")]
        [SerializeField] float offsetDragGrabDistance = 0.4f;
        [SerializeField] float offsetDragHandDistance = 0.7f;
        [SerializeField] float offsetDragHandWidth = 0.1f;
        [SerializeField] float speedRestoreHightLegs;
        [SerializeField] float offsetDragRotationHandX = 0;
        [SerializeField] float offsetDragRotationHandY = 0;
        [SerializeField] float offsetDragRotationHandZ = 0;
        [SerializeField] Coroutine delay;



        [Header("IK")]
        [SerializeField] bool ikEnable = true;
        [SerializeField] float hightLegsOffset = 0;//t
        [Range(0, 1)] public float timeSpeedApplicationForTest = 1;
        [SerializeField] bool stop;

        public List<AnimatorController> animators;

        [HideInInspector] public float speedFall;
        /*[HideInInspector]*/
        public bool rootMove;
        [HideInInspector] bool stunned;
        IKSkeleton iKSkeleton;
        float groundTimer;
        Player player;


        Vector3 pivotPosForTest;
        [SerializeField] public Transform targetTestIK;
        State state; //future
        [SerializeField] Transform rootForSerializePosition;

        public GameObject laserPrefab;

        public bool Stop { get => stop; private set => stop = value; }
        public float SprintTimer { get => Mathf.Clamp(sprintTimer, 0, sprintMaxTime); set => sprintTimer = value; }
        public float climbHight;
        Quaternion startRot;

        public Coroutine climbLCMCoroutine;
        Coroutine autoClimbCoroutine;
        Coroutine dragCoroutine;
        Coroutine climbLadderCoroutine;
        public Coroutine laserCoroutine;
        Coroutine hangOnHookCoroutine;
        Coroutine sneakCoroutine;

        public bool hang;
        public bool onLadder;
        public bool handsUp;
        public bool drag;
        public bool tackle;
        public bool laser;
        public bool hangOnHook;
        public bool Sneak { get => sneak; set => sneak = value; }
        bool sneakDelay;



        void Start()
        {
            //GetComponent<Animator>().runtimeAnimatorController = animators[0].controller;
            iKSkeleton = GetComponent<IKSkeleton>();
            state = new State();
            player = GetComponent<Player>();
            rb = GetComponent<Rigidbody>();
            collider = GetComponent<CapsuleCollider>();
            distanceVerticalRayBottom = stepOffset + collider.radius;
            RecalculateCollider();
            CalculateRaySpacing();
            startRot = deformationSystem.rotation;
        }

        private void FixedUpdate()
        {
            speedFactor = Mathf.Lerp(speedFactor, maxSpeedFactor, 0.1f);
            if (!drag)
                ModelRotation();
            //deformationSystem.localEulerAngles = new Vector3(deformationSystem.localEulerAngles.x, deformationSystem.localEulerAngles.y, -transform.InverseTransformDirection(rb.velocity).x * 8 * 2);
        }

        //доработать выпрямление при drag
        private void ModelRotation()
        {
            Quaternion rot = Quaternion.identity;
            var horizontalDirection = new Vector3(rb.velocity.x, 0, rb.velocity.z);
            if (horizontalDirection != Vector3.zero)
                rot = Quaternion.LookRotation(horizontalDirection, Vector3.up);
            var angle = Mathf.Clamp(Quaternion.Angle(transform.rotation, rot), 0, maxAngleLeanRotX);

            angle = transform.InverseTransformDirection(rb.velocity).x > 0 ? angle : -angle;
            var angleNormalized = angle / maxAngleLeanRotX;
            var resultAngle = -angleNormalized * angleLurchMax * horizontalDirection.magnitude;




            var newVelocity = transform.InverseTransformDirection(horizontalDirection).z;
            var leanAngle = Mathf.Clamp((newVelocity - oldVelocity) * maxAngleLeanRotX, -angleLeanMax, angleLeanMax);

            //Debug.Log(leanAngle);
            localLurchAngle = Mathf.Lerp(localLurchAngle, resultAngle, lurchTimeInterpolation);
            localLeanAngle = Mathf.Lerp(localLeanAngle, leanAngle, leanTimeInterpolation);

            GetComponent<Animator>().SetFloat("AngleMovement", angle);

            //deformationSystem.localEulerAngles = new Vector3(localLeanAngle, deformationSystem.localEulerAngles.y, localLurchAngle);

            oldVelocity = horizontalDirection.magnitude;
        }

        void Update()
        {
            if (!Input.GetButton("Boost") && !sprintTired)
                SprintTimer -= Time.deltaTime;

            if (sprintTired)
            {
                sprintDelayTimer += Time.deltaTime;
                if (sprintDelay <= sprintDelayTimer)
                {
                    SprintTimer = 0;
                    sprintDelayTimer = 0;
                    sprintTired = false;
                }
            }

            if (!player.itemInPlayerHand)
            {
                if (!Stop)
                {
                    if (climbLadderCoroutine == null && rays.ladder != null && Input.GetMouseButton(0) && !Sneak && !buttonCheck)
                    {
                        buttonCheck = true;
                        Debug.Log("ladder");
                        climbLadderCoroutine = StartCoroutine(ClimbLadder());
                    }

                    if (dragCoroutine == null && rays.commonCollisionDown && rays.hitDragObject.transform != null)
                    {
                        if (Vector3.Distance(rays.hitDragObject.point, transform.position) <= distanceHorizontalRay * 0.75f)
                        {
                            if (Input.GetMouseButton(0))
                            {
                                buttonCheck = true;
                                Debug.Log("drag");
                                dragCoroutine = StartCoroutine(Drag());
                            }
                            if (inputVector.magnitude > 0 && dragCoroutine == null)
                            {
                                if (Physics.Raycast(raycastOrigin.bottom, inputVector, distanceHorizontalRay))
                                {
                                    Debug.Log("drag");
                                    dragCoroutine = StartCoroutine(Drag());
                                }
                            }
                        }
                    }

                    if (!onRope && Input.GetMouseButton(0) && rays.rope != null && !buttonCheck)
                    {
                        buttonCheck = true;
                        Debug.Log("rope");
                        StartCoroutine(RopeClimb());
                    }

                    if (Input.GetKeyDown(KeyCode.F) && !drag && !onRope && !Stop && !onLadder && !hangOnHook)
                    {
                        if (laserCoroutine == null)
                        {
                            if (iKSkeleton.laserControl.Value)
                            {
                                laserPrefab.SetActive(true);
                                laser = true;
                                laserCoroutine = StartCoroutine(iKSkeleton.Laser());
                            }
                        }
                        else
                        {
                            laserPrefab.SetActive(false);
                            laser = false;
                        }
                    }

                    if (hangOnHookCoroutine == null && Input.GetMouseButton(0) && rays.hook && rb.useGravity && !drag && !onRope && !onLadder)
                    {
                        Debug.Log("hangOnHook");
                        GetComponent<Animator>().ResetTrigger("JumpUp");
                        GetComponent<Animator>().SetBool("OnHook", true);
                        hangOnHookCoroutine = StartCoroutine(HangOnHook(inputVector));
                    }
                }

                //if (Input.GetKeyDown(KeyCode.LeftControl) && rb.velocity.magnitude / speedMax > 1)
                if (tackleCoroutine == null && Input.GetKeyDown(KeyCode.LeftControl) && rb.velocity.magnitude > 2f)
                {
                    Debug.Log("starttackle");
                    tackleCoroutine = StartCoroutine(Tackle());
                }
            }

            //TODO: надо удалить
            if (!Input.GetMouseButton(0))
                buttonCheck = false;
            //iK.CalcIKPosition();
            speedFall = rb.velocity.y;

            if (!jump && !Stop && Sneak && !drag && !onLadder && !hangOnHook && rays.commonCollisionDown)
            {
                //GetComponent<Animator>().SetBool("Sneak", true);
                collider.center = new Vector3(0, 0.45f, 0.1f);
                collider.height = 0.7f;
            }
            else if (tackleCoroutine == null)
            {
                //GetComponent<Animator>().SetBool("Sneak", false);
                collider.center = new Vector3(0, 0.6f, 0);
                collider.height = 0.83f;
            }

            UpdateStateAnimator();
        }

        void RecalculateCollider()
        {
            collider.height -= stepOffset;
            collider.center += new Vector3(0, stepOffset / 2, 0);
        }

        void UpdateStateAnimator()
        {
            //Debug.Log($"hang {hang}, onLadder {onLadder}, handsUp {handsUp}, drag {drag}, tackle {tackle}, laser {laser}, hangOnHook {hangOnHook}, freeFall {freeFall}");
        }

        public void Move(Vector3 targetVelocity)
        {

            inputVector = targetVelocity;
            movementInput = targetVelocity.magnitude;

            if (!rays.onRaft)
                movement = new Vector3(rb.velocity.x, 0, rb.velocity.z).magnitude;
            else
                movement = targetVelocity.magnitude;

            if (rays.commonCollisionDown) // самопильный DRAG
            {
                Vector3 newVelocity = rb.velocity;
                newVelocity = newVelocity * Mathf.Clamp01(1f - physicDrag * Time.deltaTime);
                Vector3 newVelocityY = rb.velocity;
                newVelocityY = newVelocity * Mathf.Clamp01(1f - 0.0001f * Time.deltaTime);
                rb.velocity = new Vector3(newVelocity.x, newVelocityY.y, newVelocity.z);
            }

            if (!Stop) // перенести  под VerticalCollision
            {

                rays.ResetCollision();

                UpdateRaycastOrigins();

                HorizontalCollision(rb.velocity);

                VerticalCollision(rb.velocity);

                if (!player.itemInPlayerHand)
                {
                    if (!Stop && Input.GetMouseButton(0) && !rays.wallDetectedTop && rays.wallDetectedMiddle && rays.hitDragObject.transform == null && !Sneak && !hangOnHook)
                    {
                        climbLCMCoroutine = StartCoroutine(ClimbLCM());
                    }

                    if (!rays.commonCollisionDown && !Stop && !rays.wallDetectedMiddle && rays.wallDetectedBottom && !hangOnHook)
                    {
                        autoClimbCoroutine = StartCoroutine(AutoClimb());
                    }
                }

                if (!rootMove && !stunned) // Движение Move
                {
                    if ((targetVelocity.x != 0 || targetVelocity.z != 0) && !Stop)
                    {
                        if (targetVelocity.magnitude > 1f && rays.commonCollisionDown)
                        {
                            targetVelocity = targetVelocity.normalized;
                        }

                        //Debug.Log(AccelerationCounter);
                        Vector3 velocityChange = (targetVelocity * SpeedFactor - rb.velocity / 2);
                        velocityChange.x = Mathf.Clamp(velocityChange.x, -speedMax, speedMax);
                        velocityChange.z = Mathf.Clamp(velocityChange.z, -speedMax, speedMax);
                        velocityChange.y = 0;
                        rb.AddForce(velocityChange * (rays.commonCollisionDown ? 1 : 0.15f), ForceMode.VelocityChange);
                        //var velocity = targetVelocity * speed * SpeedFactor;

                        //rb.velocity = velocity * Mathf.Pow(AccelerationCounter,2);
                        ////// поможет с материалами //////
                        /*
                        //Debug.Log(Vector3.ProjectOnPlane(velocity, rays.normalHitFloor)); 

                        //velocity = Vector3.ProjectOnPlane(velocity, rays.normalHitFloor);
                        //if ((Vector3.ProjectOnPlane(velocity, rays.normalHitFloor)).y > 0)
                        //    velocity = Vector3.ProjectOnPlane(velocity, rays.normalHitFloor) * 2;
                        //Debug.Log(velocity);
                        */

                        state = State.Movement;
                        //rb.AddForce(velocity * acceleration * Time.deltaTime * (rays.commonCollisionDown ? 1 : 0.15f), ForceMode.Force);

                        //Vector3 horizontalVelocity = new Vector3(rb.velocity.x, 0, rb.velocity.z); //Ограничение скорости только по горизонтали с добавление ускорения по вертикали
                        //if (horizontalVelocity.magnitude >= speed * speedFactor && rays.commonCollisionDown)
                        //{
                        //    //rb.velocity = horizontalVelocity.normalized * speed * SpeedFactor + new Vector3(0, rb.velocity.y, 0);
                        //}

                        if (!drag && !hangOnHook)
                        {
                            var horizontalDirection = new Vector3(targetVelocity.x, 0, targetVelocity.z);
                            var rot = Quaternion.LookRotation(horizontalDirection, Vector3.up);
                            if (Mathf.Abs(Mathf.Abs(transform.rotation.eulerAngles.y) - Mathf.Abs(rot.eulerAngles.y)) > 90)
                            {
                                //Debug.Log("rot");
                            }


                            transform.rotation = Quaternion.Slerp(transform.rotation, rot, speedRotation * Time.deltaTime); //заглушка заменить. возможно...

                        }
                    }
                }
            }
        }

        void HorizontalCollision(Vector3 velocity)
        {
            RaycastHit hit = new RaycastHit();
            for (int i = 0; i < horizontalRayCount; i++)
            {
                Vector3 rayOrigin = raycastOrigin.top;
                rayOrigin -= Vector3.up * (horizontalRaySpacing * i);
                if (Physics.Raycast(new Ray(rayOrigin, transform.forward), out hit, distanceHorizontalRay))
                {
                    Debug.DrawRay(rayOrigin, transform.forward * distanceHorizontalRay, Color.green);

                    if (hit.collider.gameObject != null)
                    {
                        rays.commonCollisionForward = true;
                        rays.normalHit = hit.normal;
                        rays.obstacle = hit.collider;

                    }

                    if (i == (int)RaycastFront.Top)
                    {
                        rays.collisionPointTop = hit.point;

                        if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Climbable") || hit.transform.gameObject.layer == LayerMask.NameToLayer("Dragged"))
                        {
                            rays.wallDetectedTop = true;
                        }
                    }

                    if (i == (int)RaycastFront.Middle)
                    {
                        rays.collisionPointMiddle = hit.point;

                        if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Climbable") || hit.transform.gameObject.layer == LayerMask.NameToLayer("Dragged"))
                        {
                            rays.wallDetectedMiddle = true;
                        }
                    }

                    if (i == (int)RaycastFront.Bottom)
                    {
                        rays.collisionPointBottom = hit.point;
                        if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Climbable") || hit.transform.gameObject.layer == LayerMask.NameToLayer("Dragged"))
                        {
                            rays.wallDetectedBottom = true;
                        }
                    }

                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Dragged"))
                    {
                        rays.hitDragObject = hit;
                    }

                    if (hit.transform.TryGetComponent<Ladder>(out Ladder ladder))
                    {
                        rays.ladder = ladder;
                    }

                    if (hit.transform.TryGetComponent<Rope>(out Rope rope))
                    {

                    }
                    if (hit.transform.gameObject.layer == LayerMask.NameToLayer("Rope"))
                    {
                        rays.rope = hit.transform.GetComponentInParent<Rope>();
                        rays.ropeElement = hit.transform.gameObject;
                    }
                    if (hit.transform?.gameObject.layer == LayerMask.NameToLayer("Hook") && rays.hook is null)
                    {
                        rays.hook = hit.collider.transform.parent.GetComponent<Joint>();
                    }
                }
            }
        }

        void MoveColider(RaycastHit hit, Vector3 rayOriginBot)
        {
            //придает пацану вес толкая под собой вещи с силой равной своему весу rigidbody
            if (hit.transform.TryGetComponent<Rigidbody>(out Rigidbody rigidbodyObject))
            {
                var lengthFromCenterObject = Vector3.Distance(transform.position, rigidbodyObject.transform.position);
                rigidbodyObject.AddForceAtPosition(Vector3.down * rb.mass, transform.position, ForceMode.Force);
                RaycastHit hitMoveColider;
                rayOriginBot -= (transform.forward * verticalRaySpacing) - transform.forward * collider.radius;
                Physics.Raycast(rayOriginBot, transform.up * -2, out hitMoveColider, maskForHandAndLeg);
                //transform.position = new Vector3(transform.position.x, hitMoveColider.point.y, transform.position.z);
                //transform.position = hit.point;
            }


            //выравнивает высоту ног
            //if (Physics.Raycast(new Ray(raycastOrigin.bottom + transform.forward, transform.up * -1), out hit, distanceVerticalRayBottom + hightLegsOffset + 0.01f * 2, maskForHandAndLeg)) // возможно 0.05f
            //{

            //    var startPos = hit.point.y - stepOffset;
            //    var endPos = hit.point.y;
            //    var rangeLenght = Mathf.Sqrt(Mathf.Abs((transform.position.y - startPos) / stepOffset));
            //    if (rangeLenght <= 1)
            //        rangeLenght += Time.deltaTime * speedRestoreHightLegs;
            //    transform.position = new Vector3(transform.position.x, startPos + ((endPos - startPos) * Mathf.Clamp01(Mathf.Pow(rangeLenght, 2))), transform.position.z);
            //}


            ////////////////////////////////////////////////////
            var startPos = hit.point.y - stepOffset;
            RaycastHit nextHit = new RaycastHit();
            if (Physics.Raycast(new Ray(rayOriginBot + transform.forward * 0.3f * rb.velocity.magnitude, transform.up * -2), out nextHit, distanceVerticalRayBottom + hightLegsOffset + 0.01f + stepOffset, maskForHandAndLeg))
            {
                Debug.DrawRay(rayOriginBot + transform.forward * 0.3f * rb.velocity.magnitude, transform.up * -(distanceVerticalRayBottom), Color.green);
                //if(nextHit.point.y < transform.position.y)
                //    startPos = nextHit.point.y - stepOffset;
                if (transform.position.y > nextHit.point.y)
                    transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, nextHit.point.y, transform.position.z), speedRestoreLegs);
                else
                    transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, hit.point.y, transform.position.z), speedRestoreLegs);
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, hit.point.y, transform.position.z), speedRestoreLegs);
            }
            ////////////////////////////////////////////////////
            //float minStap = 0;
            //var hitStep = new RaycastHit();
            //if (Physics.Raycast(rayOriginBot, Vector3.down * stepOffset * 4, out hitStep))
            //{

            //}
            //var nextHitStep = new RaycastHit();
            //if (Physics.Raycast(rayOriginBot + transform.forward * rb.velocity.magnitude, Vector3.down * stepOffset * 4, out nextHitStep))
            //{
            //    Debug.DrawRay(rayOriginBot + transform.forward * rb.velocity.magnitude * 0.45f, Vector3.down * stepOffset * 4, Color.green);
            //}
            //else
            //    nextHitStep = hitStep;
            //if (hitStep.point.y < nextHitStep.point.y)
            //    minStap = hitStep.point.y;
            //else
            //    minStap = nextHitStep.point.y;
            //transform.position = new Vector3(transform.position.x, minStap, transform.position.z);
        }

        void VerticalCollision(Vector3 velocity)
        {
            //Debug.Log("check vertical");
            RaycastHit hit = new RaycastHit();

            Vector3 rayOriginBot = raycastOrigin.bottom;
            rayOriginBot -= (transform.forward * verticalRaySpacing) - transform.forward * collider.radius;
            var animator = GetComponent<Animator>();

            //приземления и отыгрыш их анимаций
            if (Physics.Raycast(new Ray(rayOriginBot, transform.up * rb.velocity.y * 2), out hit, rb.velocity.y * -2, maskForHandAndLeg))
            {

                if (Mathf.Abs(hit.distance / rb.velocity.y) <= 0.16f)
                {
                    transform.position = new Vector3(transform.position.x, hit.point.y, transform.position.z);
                    rb.useGravity = false;
                    rays.commonCollisionDown = true;
                    animator.SetFloat("InertialLand", speedFall);
                    animator.SetTrigger("Land");
                    animator.SetBool("freeFall", false);

                    if (velocity.y < fallSpeedForHardLanding) // SpeedFall less -4 to ANIMATOR
                    {
                        animator.SetTrigger("Alex_HardLand");
                        DelayMove(Mathf.Clamp(Mathf.Abs(rb.velocity.y) / 10, 0, 0.99f)); //расчет времени блокироки управления
                        MoveColider(hit, rayOriginBot);
                        rb.velocity = new Vector3(rb.velocity.x * inertialDamperForHardLand, rb.velocity.y / 2, rb.velocity.z * inertialDamperForHardLand); //тяжелое приземление
                    }
                    else
                    {
                        animator.SetTrigger("Alex_SoftLand");
                        MoveColider(hit, rayOriginBot);
                        rb.velocity = new Vector3(rb.velocity.x * inertialDamperForSoftLand, rb.velocity.y / 2, rb.velocity.z * inertialDamperForSoftLand); //гашение инерции при легком приземлении
                    }
                }
            }

            //Debug.DrawRay(rayOriginBot, transform.up * -distanceVerticalRayBottom, Color.green);
            Debug.DrawRay(rayOriginBot - new Vector3(0, distanceVerticalRayBottom, 0), transform.up * rb.velocity.y * Time.fixedDeltaTime, Color.blue);

            //расчет приземления
            //if (rb.velocity.y <= 0 && Physics.Raycast(new Ray(rayOriginBot /*- new Vector3(0, 0.1f, 0)*/, (transform.up * rb.velocity.y * Time.fixedDeltaTime)), out hit, (Mathf.Abs(rb.velocity.y) * Time.fixedDeltaTime) + distanceVerticalRayBottom + hightLegsOffset))
            //{
            //    freeFallTrigger = false;
            //    rays.commonCollisionDown = true;
            //    rays.normalHitFloor = hit.normal;
            //    rb.useGravity = false;
            //    //MoveColider(hit, rayOriginBot);
            //    rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y / 2, rb.velocity.z);
            //    //iK.CheckFootCollision(rayOriginBot);
            //}

            //if (Physics.Raycast(new Ray(rayOriginBot, transform.up * -1), out hit, distanceVerticalRayBottom + hightLegsOffset + 0.01f, maskForHandAndLeg))
            //{
            //    freeFallTrigger = false;
            //    rays.commonCollisionDown = true;
            //    rays.normalHitFloor = hit.normal;
            //    rb.useGravity = false;
            //    //MoveColider(hit, rayOriginBot);
            //    rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y / 2, rb.velocity.z);
            //    //iK.CheckFootCollision(rayOriginBot);
            //}

            if (!jump && !freeFall && Physics.Raycast(new Ray(rayOriginBot, transform.up * -1), out hit, (distanceVerticalRayBottom + hightLegsOffset) * 2 + 0.01f, maskForHandAndLeg)) // возможно 0.05f
            {
                //AnimatorTriggerReset(GetComponent<Animator>());
                freeFall = false;
                GetComponent<Animator>().SetBool("freeFall", false);
                rays.commonCollisionDown = true;
                rays.normalHitFloor = hit.normal;
                rb.useGravity = false;
                groundTimer += Time.deltaTime;
                if (groundTimer >= jumpDelay)
                {
                    jump = false;
                }

                MoveColider(hit, rayOriginBot);
                //if(ikEnable)
                //    iKSkeleton.CheckFootCollision(rayOriginBot);
                Debug.DrawRay(rayOriginBot, transform.up * -(distanceVerticalRayBottom), Color.red);

                if (hit.transform.TryGetComponent(out FloatingBox raft))
                {
                    rays.raft = raft;
                    rays.onRaft = true;
                    //Debug.Log("velocity  " + velocity + "     rb.velocity " + rb.velocity);
                    //movement = velocity.magnitude;
                }
                else
                {
                    //movement = rb.velocity.magnitude;
                    rays.onRaft = false;
                }
                coyoteTimer = 0;
            }
            else if (Physics.Raycast(new Ray(rayOriginBot, transform.up * -1), out hit, distanceVerticalRayBottom + hightLegsOffset + 0.01f, maskForHandAndLeg))
            {
                //AnimatorTriggerReset(GetComponent<Animator>());
                iKSkeleton.rig.leftLeg.weight = iKSkeleton.rig.rightLeg.weight = 1;
                //MoveColider(hit, rayOriginBot);
                rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y / 2, rb.velocity.z);
                var anim = GetComponent<Animator>();
                freeFall = false;
                anim.SetBool("freeFall", false);
                rays.commonCollisionDown = true;
                rays.normalHitFloor = hit.normal;
                rb.useGravity = false;
                groundTimer += Time.deltaTime;
                if (groundTimer >= jumpDelay)
                {
                    jump = false;
                }

                MoveColider(hit, rayOriginBot);
                //if(ikEnable)
                //    iKSkeleton.CheckFootCollision(rayOriginBot);
                Debug.DrawRay(rayOriginBot, transform.up * -(distanceVerticalRayBottom), Color.red);

                if (hit.transform.TryGetComponent(out FloatingBox raft))
                {
                    rays.raft = raft;
                    rays.onRaft = true;
                    //Debug.Log("velocity  " + velocity + "     rb.velocity " + rb.velocity);
                    //movement = velocity.magnitude;
                }
                else
                {
                    //movement = rb.velocity.magnitude;
                    rays.onRaft = false;
                }
                coyoteTimer = 0;
                //iK.CheckFootCollision(rayOriginBot);
            }
            else if (!rays.commonCollisionDown && !Stop) //onGround False
            {
                iKSkeleton.rig.leftLeg.weight = iKSkeleton.rig.rightLeg.weight = 0;

                if (!rays.commonCollisionDown && !freeFall && !hangOnHook && rb.velocity.y < 0)
                {
                    freeFall = true;
                    GetComponent<Animator>().SetBool("freeFall", true);
                }

                //Debug.Log(coyoteTimer);
                if (!jump)
                    coyoteTimer += Time.fixedDeltaTime;
                else
                {
                    rb.useGravity = true;
                    rays.commonCollisionDown = false;
                    groundTimer = 0;
                }

                if (coyoteTimer >= coyoteTime / 2)
                {
                    rb.useGravity = true;
                    if (coyoteTimer >= coyoteTime)
                    {
                        rays.commonCollisionDown = false;
                        GetComponent<Animator>().SetBool("freeFall", true);
                        groundTimer = 0;
                    }
                }
            }

            var botCollision = hit.point;

            for (int i = 0; i < verticalRayCount; i++)
            {
                Vector3 rayOriginTop = raycastOrigin.top;
                if (Physics.Raycast(new Ray(rayOriginTop, transform.up), out hit, distanceVerticalRayTop))
                {
                    Debug.DrawRay(rayOriginTop, transform.up, Color.red);

                    if (hit.transform?.gameObject.layer == LayerMask.NameToLayer("Hook") && rays.hook is null)
                    {
                        rays.hook = hit.collider.transform.parent.GetComponent<Joint>();
                    }

                    if (Math.Abs(botCollision.y - hit.point.y) <= 1.03f)
                    {
                        Sneak = true;
                    }

                    if (Sneak && hit.distance <= 0.28f)
                        obstacleFromTop = true;
                    else
                        obstacleFromTop = false;
                    //Debug.Log("Sneak "+ Sneak+ " ray " + obstacleFromTop + " hit.distance "+ hit.distance);
                }
                else
                {
                    obstacleFromTop = false;
                }
                //Debug.Log("Postray " + obstacleFromTop);
            }
        }

        void UpdateRaycastOrigins()
        {
            Bounds bounds = collider.bounds;
            bounds.Expand(skinWidth * -2);
            raycastOrigin.top = new Vector3(transform.position.x, bounds.max.y, transform.position.z);
            raycastOrigin.bottom = new Vector3(transform.position.x, bounds.min.y + collider.radius, transform.position.z);
        }

        void CalculateRaySpacing()
        {
            Bounds bounds = collider.bounds;
            bounds.Expand(skinWidth * -2);

            horizontalRayCount = Mathf.Clamp(horizontalRayCount, 3, int.MaxValue);
            verticalRayCount = Mathf.Clamp(horizontalRayCount, 3, int.MaxValue);

            horizontalRaySpacing = bounds.size.y / (horizontalRayCount - 1);
            verticalRaySpacing = bounds.size.x / (verticalRayCount - 1);
        }

        public bool Jump(Vector3 velocity)
        {
            Vector3 rayOriginBot = raycastOrigin.bottom;
            rayOriginBot -= (transform.forward * verticalRaySpacing) - transform.forward * collider.radius;

            //if (((Physics.Raycast(new Ray(rayOriginBot, transform.up * -1), out hit, distanceVerticalRayBottom + 0.1f) && groundTimer >= jumpDelay) || coyoteTime >= coyoteTimer) && !stop)
            if (((rays.commonCollisionDown && groundTimer >= jumpDelay) || coyoteTimer <= coyoteTime) && !Stop && !jump && !drag && !rootMove && !Sneak)
            {
                rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
                coyoteTimer = 0;
                groundTimer = 0;
                jump = true;
                rb.useGravity = true;
                rays.commonCollisionDown = false;
                rb.AddForce(transform.up * jumpForce /*+ rb.velocity * superJumpForce*/, ForceMode.VelocityChange);
                return true;
            }
            return false;
        }

        //TODO: переписать под говно кирилла
        KeyCode GetButtonRelativeToDirection(Vector3 direction)
        {
            if (direction.x > 0.707f && direction.z < 0.707f && direction.z > -0.707f)
                return KeyCode.D;
            if (direction.x < 0.707f && direction.z < 0.707f && direction.z > -0.707f)
                return KeyCode.A;
            if (direction.z > 0.707f && direction.x < 0.707f && direction.x > -0.707f)
                return KeyCode.W;
            if (direction.z < 0.707f && direction.x < 0.707f && direction.x > -0.707f)
                return KeyCode.S;

            return KeyCode.Space;
        }

        #region Structurs
        public struct RaycastOrigins
        {
            //public Vector3 topLeft, topLeft1, topRight, topRight1;
            //public Vector3 bottomLeft, bottomRight;
            public Vector3 top, bottom;
        }

        enum RaycastTop_Bottom
        {
            Left,
            Middle,
            Right
        }

        enum RaycastFront
        {
            Top,
            Middle,
            Bottom
        }

        public struct Raycasts
        {
            public bool wallDetectedTop, wallDetectedMiddle, wallDetectedBottom;
            public Vector3 collisionPointTop, collisionPointMiddle, collisionPointBottom;
            public Collider obstacle;
            public Vector3 normalHit;
            public bool commonCollisionForward, commonCollisionRight, commonCollisionLeft, commonCollisionUp, commonCollisionDown;
            public Joint hook;
            public Ladder ladder;
            public GameObject stepSlope;
            public Rope rope;
            public bool onRaft;
            public FloatingBox raft;
            public GameObject ropeElement;
            public Vector3 normalHitFloor;
            public RaycastHit hitDragObject;

            public void ResetCollision()
            {
                commonCollisionForward = commonCollisionRight = commonCollisionLeft = commonCollisionUp = commonCollisionDown = false;
                wallDetectedTop = wallDetectedMiddle = wallDetectedBottom = false;
                collisionPointTop = collisionPointMiddle = collisionPointBottom = Vector3.zero;
                hook = null;
                ladder = null;
                obstacle = null;
                normalHit = Vector3.zero;
                raft = null;
                rope = null;
                ropeElement = null;
                stepSlope = null;
                normalHitFloor = Vector3.zero;
                hitDragObject = new RaycastHit();
            }
        }
        #endregion

        #region Coroutines
        //TODO одновременное нажатие ctrl+ space баг
        IEnumerator SneakCoroutine()
        {
            float timer = 0;
            while (timer <= 0.25f)
            {
                timer += Time.deltaTime;
                yield return null;
            }
            sneakDelay = false;
        }

        IEnumerator AutoClimb()
        {
            //GetComponent<Animator>().ResetTrigger("FreeFallTrigger");
            rb.velocity = Vector3.zero;
            rb.useGravity = false;
            collider.enabled = false;
            transform.rotation = Quaternion.LookRotation(rays.normalHit * -1, Vector3.up);
            Stop = true;
            var aim = StartCoroutine(AutoClimbAim());
            yield return aim;
            player.SetHangUpFromJumpAnimation();
            Coroutine anim = StartCoroutine(HangUpAnimationDelay("AutoHangUpAnimation.json", (collider.height + collider.radius - 0.1f) / (rays.obstacle.bounds.max.y - transform.position.y), Vector3.zero, Vector3.zero));
            yield return anim;
            StopClimbAnimation(ref autoClimbCoroutine, "HangUpFromJump");
        }

        IEnumerator AutoClimbAim()
        {
            float delay = 0.125f;
            float timer = 0f;
            while (true)
            {
                timer += Time.deltaTime;
                transform.position = Vector3.Lerp(transform.position, new Vector3(rays.collisionPointBottom.x, rays.obstacle.bounds.max.y - collider.height / 2 - collider.center.y + offYAutoClimbAnimation, rays.collisionPointBottom.z) - transform.forward * collider.radius, timer / delay);
                if (timer >= delay)
                    break;
                yield return null;
            }
        }

        /// <summary>
        /// Расчет Времени приземления при равноускоренном движении
        /// </summary>
        IEnumerator CalcTG(float time)
        {
            Debug.Log(time);
            Debug.Log(time + " время до приземления");
            yield return new WaitForSeconds(time);
            Debug.Log("приземление");
            rb.velocity = Vector3.zero;
        }

        /// <summary>
        /// Добавлен в ивент анимации
        /// </summary>
        public void StopClimbAnimation(ref Coroutine action, string triggerName)
        {
            var anim = GetComponent<Animator>();
            Stop = false;
            collider.enabled = true;
            rb.useGravity = true;
            Sneak = true;
            freeFall = false;
            action = null;

            anim.SetBool("freeFall", freeFall);
            anim.ResetTrigger(triggerName);
        }

        IEnumerator AimClimbLCM(Vector3 targetPos, float timeToConnect)
        {
            float timer = 0;
            while (true)
            {
                transform.position = Vector3.Lerp(transform.position, targetPos, timer / timeToConnect);
                transform.rotation = Quaternion.LookRotation(rays.normalHit * -1, Vector3.up);
                timer += Time.deltaTime;
                if (timer >= 0.125f)
                    break;
                yield return null;
            }
        }

        IEnumerator ClimbLCM()
        {
            Stop = true;
            rb.velocity = Vector3.zero;
            rb.useGravity = false;
            collider.enabled = false;

            state = State.Climb;
            var leftHand = iKSkeleton.GetLimb(IKSkeleton.Limbs.LeftHand);
            var rightHand = iKSkeleton.GetLimb(IKSkeleton.Limbs.RightHand);
            Coroutine delay = null;

            if (!rays.commonCollisionDown)
            {
                hang = true;
                GetComponent<Animator>().SetBool("Hang", hang);
                player.SetHangAnimation();
                var newPos = new Vector3(rays.collisionPointMiddle.x, rays.obstacle.bounds.max.y - collider.height / 2 - collider.center.y + offYHang, rays.collisionPointMiddle.z) - transform.forward * collider.radius / 2;
                yield return StartCoroutine(AimClimbLCM(newPos, 0.125f));
                yield return new WaitForSeconds(0.2f);// animation transition time
                while (hang) // висит ждет кнопку для подъема
                {
                    rb.velocity = Vector3.zero;
                    transform.position = Vector3.Lerp(transform.position, new Vector3(rays.collisionPointMiddle.x, rays.obstacle.bounds.max.y - collider.height / 2 - collider.center.y + offYHang, rays.collisionPointMiddle.z) - transform.forward * collider.radius / 2, 0.2f);

                    var leftHandPos = new Vector3(rays.collisionPointMiddle.x, rays.obstacle.bounds.max.y - 0.05f, rays.collisionPointMiddle.z) - transform.forward * 0.025f - transform.right * 0.1f;
                    var rightHandPos = new Vector3(rays.collisionPointMiddle.x, rays.obstacle.bounds.max.y - 0.05f, rays.collisionPointMiddle.z) - transform.forward * 0.025f - transform.right * -0.1f;

                    leftHand.position = leftHandPos;
                    rightHand.position = rightHandPos;
                    leftHand.rotation = Quaternion.FromToRotation(Vector3.up, rays.normalHit) * transform.rotation * Quaternion.Euler(90, -90, 0);
                    rightHand.rotation = Quaternion.FromToRotation(Vector3.up, rays.normalHit) * transform.rotation * Quaternion.Euler(-90, 90, 0);

                    iKSkeleton.rig.leftArm.weight = iKSkeleton.rig.rightArm.weight += Time.deltaTime * 2;

                    Debug.Log("Press " + GetButtonRelativeToDirection(transform.position) + " to pay");

                    if (Input.GetKeyDown(KeyCode.Space) || Input.GetKey(GetButtonRelativeToDirection(transform.forward)))
                    {
                        //var offsetY = (rays.obstacle.bounds.max.y - collider.height / 2 - collider.center.y /* offset hands animation */) - transform.position.y;
                        player.SetHangUpAnimation();
                        hang = false;
                        GetComponent<Animator>().SetBool("Hang", hang);
                        delay = StartCoroutine(HangUpAnimationDelay("hangUpAnimation.json", (collider.height + collider.radius - 0.13f) / (rays.obstacle.bounds.max.y - transform.position.y), leftHandPos, rightHandPos));//time 1.17f
                        break;
                    }

                    if (!Input.GetMouseButton(0))
                    {
                        hang = false;
                        GetComponent<Animator>().SetBool("Hang", hang);
                        break;
                    }
                    yield return null;
                }
            }
            else
            {
                handsUp = true;
                var newPos = new Vector3(rays.collisionPointMiddle.x, transform.position.y, rays.collisionPointMiddle.z) - transform.forward * collider.radius;
                yield return StartCoroutine(AimClimbLCM(newPos, 0.125f));

                while (handsUp) //стоит на земле тянет руки для подъема
                {
                    rb.velocity = Vector3.zero;
                    //HandsHeightAnimation += Time.deltaTime * 2;
                    var leftHandPos = new Vector3(rays.collisionPointMiddle.x, rays.obstacle.bounds.max.y - 0.05f, rays.collisionPointMiddle.z) - transform.forward * 0.025f - transform.right * 0.1f;
                    var rightHandPos = new Vector3(rays.collisionPointMiddle.x, rays.obstacle.bounds.max.y - 0.05f, rays.collisionPointMiddle.z) - transform.forward * 0.025f - transform.right * -0.1f;
                    transform.position = Vector3.Lerp(transform.position, new Vector3(rays.collisionPointMiddle.x, transform.position.y, rays.collisionPointMiddle.z) - transform.forward * collider.radius, 0.2f);
                    leftHand.position = leftHandPos;
                    rightHand.position = rightHandPos;
                    leftHand.rotation = Quaternion.FromToRotation(Vector3.up, rays.normalHit) * transform.rotation * Quaternion.Euler(90, -90, 0);
                    rightHand.rotation = Quaternion.FromToRotation(Vector3.up, rays.normalHit) * transform.rotation * Quaternion.Euler(-90, 90, 0);

                    iKSkeleton.rig.leftArm.weight = iKSkeleton.rig.rightArm.weight += Time.deltaTime * 2;

                    Debug.Log("Press " + GetButtonRelativeToDirection(transform.forward) + " to pay");

                    if (Input.GetKey(KeyCode.Space) || Input.GetKey(GetButtonRelativeToDirection(transform.forward)))
                    {
                        handsUp = false;
                        player.SetHangUpAnimation();
                        var endPos = new Vector3(rays.collisionPointMiddle.x, rays.obstacle.bounds.max.y, rays.collisionPointMiddle.z) + transform.forward * 0.2f;
                        //Debug.Log($"y {rays.obstacle.bounds.max.y - transform.position.y} itog {(stepOffset + collider.height + collider.radius) / (rays.obstacle.bounds.max.y - transform.position.y)} ");
                        delay = StartCoroutine(HangUpAnimationDelay("hangUpAnimation.json", (collider.height + collider.radius - 0.1f) / (rays.obstacle.bounds.max.y - transform.position.y), leftHandPos, rightHandPos));//time 1.17f
                        break;
                    }
                    if (!Input.GetMouseButton(0))
                    {
                        handsUp = false;
                        break;
                    }
                    yield return null;
                }
            }
            yield return delay;
            StopClimbAnimation(ref climbLCMCoroutine, "HangUp");
        }

        IEnumerator Tackle()
        {
            tackle = true;
            float timer = 0;
            Stop = true;
            var hightCollider = collider.height;
            var colliderOffset = collider.center;
            collider.height = 0;
            collider.center = new Vector3(0, 0.3f, 0);

            GetComponent<Animator>().SetBool("TackleBool", true);
            iKSkeleton.rig.mainRig.weight = 0;

            while (timer < tackleTime)
            {
                if (!Physics.Raycast(transform.position - Vector3.up * collider.height / 2, Vector3.down, 0.2f, maskForHandAndLeg)) //TODO: кинуть рейкаст под ноги, для проверки на земле или нет
                {
                    GetComponent<Animator>().SetBool("TackleBool", false);
                    tackleCoroutine = null;
                    break;
                }
                timer += Time.deltaTime;
                rb.AddForce(transform.forward * tackleFactorForce / timer * timer, ForceMode.Acceleration);
                yield return null;
            }
            GetComponent<Animator>().SetBool("TackleBool", false);
            Stop = false;
            collider.height = hightCollider;
            collider.center = colliderOffset;
            tackleCoroutine = null;
            iKSkeleton.rig.mainRig.weight = 1;
            tackle = false;
            yield return null;
        }

        IEnumerator AimDrag(Vector3 newPos)
        {
            float timer = 0;
            while (timer <= 0.125f)
            {
                timer += Time.deltaTime;
                transform.position = Vector3.Lerp(transform.position, newPos, timer / 0.125f);
            }
            yield return null;
        }

        IEnumerator Drag()
        {
            drag = true;

            DragObject dragObject = rays.hitDragObject.transform.GetComponent<DragObject>();
            Rigidbody rbDragObject = dragObject.GetComponent<Rigidbody>();
            float heightHand = dragObject.HandGripHeight;
            RaycastHit hit;

            if (Physics.Raycast(transform.position + Vector3.up * stepOffset, transform.forward, out hit)) ;
            Debug.DrawRay(hit.point, Vector3.up, Color.red, 5);
            var pivot = rays.hitDragObject.transform.GetChild(0);
            pivot.position = hit.point;
            transform.rotation = Quaternion.LookRotation(rays.hitDragObject.normal * -1, Vector3.up);
            var newPos = new Vector3(pivot.position.x, transform.position.y, pivot.position.z) + transform.forward * -1 * distanceHorizontalRay * offsetDragGrabDistance;

            yield return StartCoroutine(AimDrag(newPos));

            ConfigurableJoint joint = gameObject.AddComponent<ConfigurableJoint>();
            joint.xMotion = ConfigurableJointMotion.Locked;
            joint.zMotion = ConfigurableJointMotion.Locked;

            var leftHand = iKSkeleton.GetLimb(IKSkeleton.Limbs.LeftHand);
            var rightHand = iKSkeleton.GetLimb(IKSkeleton.Limbs.RightHand);

            leftHand.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal) * transform.rotation * Quaternion.Euler(90, -90, 0) * Quaternion.Euler(offsetDragRotationHandX, -offsetDragRotationHandY, offsetDragRotationHandZ);
            rightHand.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal) * transform.rotation * Quaternion.Euler(-90, 90, 0) * Quaternion.Euler(offsetDragRotationHandX, -offsetDragRotationHandY, offsetDragRotationHandZ);

            joint.connectedBody = rbDragObject;

            dragObject.dragHint.SetActive(false);

            while (drag)
            {
                if (!Input.GetMouseButton(0))
                {
                    if (Physics.Raycast(raycastOrigin.bottom, inputVector, out hit, distanceHorizontalRay))
                    {
                        Debug.DrawRay(raycastOrigin.bottom, inputVector, Color.cyan, 1);
                        if (hit.transform.gameObject.layer != LayerMask.NameToLayer("Dragged"))
                        {
                            if (inputVector.magnitude == 0)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                Debug.DrawRay(transform.position, transform.forward, Color.red);
                Debug.DrawRay(dragObject.transform.position, transform.forward * -1, Color.green);
                iKSkeleton.rig.leftArm.weight = iKSkeleton.rig.rightArm.weight += Time.deltaTime * 5;
                leftHand.position = transform.position + transform.forward * offsetDragHandDistance - transform.right * offsetDragHandWidth + Vector3.up * dragObject.HandGripHeight;
                rightHand.position = transform.position + transform.forward * offsetDragHandDistance - transform.right * -offsetDragHandWidth + Vector3.up * dragObject.HandGripHeight;

                Debug.DrawRay(pivot.transform.position + /*(dragObject.transform.rotation * dragObjectPos) + */ (transform.forward), Vector3.up, Color.red);

                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(new Vector3(pivot.position.x, transform.position.y, pivot.position.z) - transform.position, Vector3.up), 0.1f);

                movementDiretion = transform.InverseTransformDirection(rb.velocity);
                yield return null;
            }
            //iKSkeleton.rig.leftArm.weight = iKSkeleton.rig.rightArm.weight = 0;

            Destroy(joint);
            pivot.localPosition = Vector3.zero;
            dragCoroutine = null;
            yield return null;
            drag = false;
        }

        #region Ladder

        IEnumerator ClimbLadder()
        {
            List<Step> steps = new List<Step>(rays.ladder.steps);
            currentStepIdex = steps.IndexOf(rays.ladder.GetNearestStep(collider));
            if (currentStepIdex >= 0 && currentStepIdex <= steps.Count - 1 - 4) //TODO : офсет захвата ступене для рук и ног под лестницу в вентиляции (гастроном)
            {
                onLadder = true;
                GetComponent<Animator>().SetBool("freeFall", false);
                limbs = new LimbsData();
                GetComponent<Animator>().ResetTrigger("SwapGrabLadder");
                GetComponent<Animator>().SetBool("onLadder", onLadder);
                rb.velocity = Vector3.zero;
                rb.useGravity = false;
                collider.enabled = false;
                transform.rotation = Quaternion.LookRotation(rays.ladder.transform.right, Vector3.up);
                Stop = true;

                swapGrabLadder = false;



                //if (rays.ladder.steps.IndexOf(rays.ladder.nearestSt) == 0)
                //{
                //    //Debug.Log("Играем анимацию раскачки и ждем, Т.к. схватились за самую первую ступеньку");
                //    yield return new WaitForSeconds(0.5f);
                //}
                //if (rays.ladder.steps.IndexOf(rays.ladder.nearestSt) == 1)
                //{
                //    //Debug.Log("Играем анимацию полу раскачки и ждем, Т.к. схватились за вторую ступеньку");
                //    yield return new WaitForSeconds(0.5f);
                //}


                var ladderObject = rays.ladder;
                var directionToLadder = ladderObject.transform.position - ladderObject.transform.right * offsetPositionOnLadder;
                var newPosOnLadder = new Vector3(directionToLadder.x, ladderObject.nearestSt.transform.position.y, directionToLadder.z);

                yield return StartCoroutine(ClimbLadderDelay(newPosOnLadder));

                ladderObject.PlayStairsIn();

                limbs.leftHand.target = iKSkeleton.GetLimb(IKSkeleton.Limbs.LeftHand);
                limbs.rightLeg.target = iKSkeleton.rig.rightLegWorld.data.target;
                limbs.leftLeg.target = iKSkeleton.rig.leftLegWorld.data.target;
                limbs.rightHand.target = iKSkeleton.GetLimb(IKSkeleton.Limbs.RightHand);

                limbs.leftHand.stepIndex = steps.IndexOf(rays.ladder.nearestSt) + 4;
                limbs.rightHand.stepIndex = steps.IndexOf(rays.ladder.nearestSt) + 3;
                limbs.leftLeg.stepIndex = steps.IndexOf(rays.ladder.nearestSt);
                limbs.rightLeg.stepIndex = steps.IndexOf(rays.ladder.nearestSt) + 1;

                limbs.leftHand.oldStepIndex = limbs.leftHand.stepIndex;
                limbs.rightHand.oldStepIndex = limbs.rightHand.stepIndex;
                limbs.leftLeg.oldStepIndex = limbs.leftLeg.stepIndex;
                limbs.rightLeg.oldStepIndex = limbs.rightLeg.stepIndex;

                limbs.rightLeg.target.rotation = Quaternion.Euler(0, 90, 90);
                limbs.leftLeg.target.rotation = Quaternion.Euler(0, 90, -90);
                limbs.leftHand.target.rotation = Quaternion.Euler(90, 0, 90);
                limbs.rightHand.target.rotation = Quaternion.Euler(-90, 90, 0);

                limbs.leftHand.pos = steps[limbs.leftHand.stepIndex].transform.position - transform.right * widthOffsetGrabForLegs + Vector3.up * hightOffsetForHandsLadder - transform.forward * deepOffsetForHandsLadder;
                limbs.rightLeg.pos = steps[limbs.rightLeg.stepIndex].transform.position - transform.right * -widthOffsetGrabForHands + Vector3.up * hightOffsetForLegsLadder - transform.forward * deepOffsetForLegsLadder;
                limbs.leftLeg.pos = steps[limbs.leftLeg.stepIndex].transform.position - transform.right * widthOffsetGrabForLegs + Vector3.up * hightOffsetForLegsLadder - transform.forward * deepOffsetForLegsLadder;
                limbs.rightHand.pos = steps[limbs.rightHand.stepIndex].transform.position - transform.right * -widthOffsetGrabForHands + Vector3.up * hightOffsetForHandsLadder - transform.forward * deepOffsetForHandsLadder;

                limbs.leftHand.oldPos = steps[limbs.leftHand.oldStepIndex].transform.position - transform.right * widthOffsetGrabForLegs + Vector3.up * hightOffsetForHandsLadder - transform.forward * deepOffsetForHandsLadder;
                limbs.rightLeg.oldPos = steps[limbs.rightLeg.oldStepIndex].transform.position - transform.right * -widthOffsetGrabForHands + Vector3.up * hightOffsetForLegsLadder - transform.forward * deepOffsetForLegsLadder;
                limbs.leftLeg.oldPos = steps[limbs.leftLeg.oldStepIndex].transform.position - transform.right * widthOffsetGrabForLegs + Vector3.up * hightOffsetForLegsLadder - transform.forward * deepOffsetForLegsLadder;
                limbs.rightHand.oldPos = steps[limbs.rightHand.oldStepIndex].transform.position - transform.right * -widthOffsetGrabForHands + Vector3.up * hightOffsetForHandsLadder - transform.forward * deepOffsetForHandsLadder;

                limbs.leftHand.target.position = limbs.leftHand.pos;
                limbs.rightLeg.target.position = limbs.rightLeg.pos;
                limbs.leftLeg.target.position = limbs.leftLeg.pos;
                limbs.rightHand.target.position = limbs.rightHand.pos;


                while (onLadder)
                {
                    GetComponent<Animator>().SetBool("onLadder", true);

                    if (rays.ladder.steps.IndexOf(rays.ladder.nearestSt) > 1 && rays.ladder.steps.IndexOf(rays.ladder.nearestSt) < rays.ladder.steps.Count - 1)
                    {
                        //Debug.Log("Играем анимацию когда ноги на ступеньках");
                    }

                    if (rays.ladder.steps.IndexOf(rays.ladder.nearestSt) < 2)
                    {
                        //Debug.Log("Играем анимацию когда ноги висят и мы держимся только руками");
                    }

                    if (!Input.GetMouseButton(0))
                    {
                        onLadder = false;
                        GetComponent<Animator>().SetBool("onLadder", onLadder);
                        iKSkeleton.rig.rightLegWorld.weight = 0;
                        iKSkeleton.rig.leftLegWorld.weight = 0;

                    }

                    if (Input.GetKey(KeyCode.W))
                    {
                        //if (currentStepIdex + 5 < ladderObject.steps.Count - 1 && !rays.ladder.nearestSt.last) // нормальное условие
                        //{
                        if (upwardMovementLadder == null)
                        {
                            yield return upwardMovementLadder = StartCoroutine(MoveUpLadder(climbingTimeLadder, steps, ladderObject, directionToLadder));
                            if (!onLadder)
                                break;
                        }
                        if (downwardMovementLadder != null)
                        {
                            StopCoroutine(downwardMovementLadder);
                            downwardMovementLadder = null;
                        }
                        //}
                    }

                    if (Input.GetKey(KeyCode.S))
                    {
                        if (downwardMovementLadder == null)
                        {
                            yield return downwardMovementLadder = StartCoroutine(MoveDownLadder(climbingTimeLadder, steps, ladderObject, directionToLadder));
                            if (!onLadder)
                                break;
                        }
                        if (upwardMovementLadder != null)
                        {
                            StopCoroutine(upwardMovementLadder);
                            upwardMovementLadder = null;
                        }
                    }

                    if (ikEnable)
                    {
                        iKSkeleton.rig.leftArm.weight = GetComponent<Animator>().GetFloat("LeftHand");
                        iKSkeleton.rig.rightLegWorld.weight = GetComponent<Animator>().GetFloat("RightFoot");
                        iKSkeleton.rig.leftLegWorld.weight = GetComponent<Animator>().GetFloat("LeftFoot");
                        iKSkeleton.rig.rightArm.weight = GetComponent<Animator>().GetFloat("RightHand");
                    }
                    yield return new WaitForFixedUpdate();

                }
                onLadder = false;
                iKSkeleton.rig.rightLegWorld.weight = 0;
                iKSkeleton.rig.leftLegWorld.weight = 0;
                GetComponent<Animator>().SetBool("onLadder", onLadder);
                StopClimbAnimation(ref climbLadderCoroutine, "HangUp");
            }
            climbLadderCoroutine = null;
        }

        IEnumerator ClimbLadderDelay(Vector3 pos)
        {
            float timer = 0;
            while (timer <= 0.125f)
            {
                timer += Time.fixedDeltaTime;
                transform.position = Vector3.Lerp(transform.position, pos, 0.3f);
                yield return null;
            }
        }

        #region MoveLadder
        IEnumerator MoveUpLadder(float distance, List<Step> steps, Ladder ladderObject, Vector3 directionToLadder)
        {
            Debug.Log($"step{1}");
            if (ladderObject.steps.Count - 1 < currentStepIdex + 5)
            {
                Debug.Log($"step{2}");

                GetComponent<Animator>().SetBool("onLadder", onLadder);
                player.SetHangUpAnimation();

                iKSkeleton.rig.rightLegWorld.weight = 0;
                iKSkeleton.rig.leftLegWorld.weight = 0;

                var lastStepPos = steps[steps.Count - 1].transform.position;

                var leftHandPos = new Vector3(lastStepPos.x, rays.obstacle.bounds.max.y - 0.05f, lastStepPos.z) - transform.forward * 0.025f - transform.right * 0.1f;
                var rightHandPos = new Vector3(lastStepPos.x, rays.obstacle.bounds.max.y - 0.05f, lastStepPos.z) - transform.forward * 0.025f - transform.right * -0.1f;

                yield return StartCoroutine(HangUpAnimationDelay("hangUpAnimation.json", (collider.height + collider.radius - stepOffset) / (lastStepPos.y - transform.position.y), leftHandPos, rightHandPos));
                onLadder = false;
            }
            else
            {

                if (!swapGrabLadder)
                {

                    limbs.leftLeg.oldStepIndex = limbs.leftLeg.stepIndex;
                    limbs.rightHand.oldStepIndex = limbs.rightHand.stepIndex;
                    limbs.rightHand.stepIndex += 2;
                    limbs.leftLeg.stepIndex += 2;

                    limbs.leftLeg.pos = steps[limbs.leftLeg.stepIndex].transform.position - transform.right * widthOffsetGrabForLegs + Vector3.up * hightOffsetForLegsLadder - transform.forward * deepOffsetForLegsLadder;
                    limbs.rightHand.pos = steps[limbs.rightHand.stepIndex].transform.position - transform.right * -widthOffsetGrabForHands + Vector3.up * hightOffsetForHandsLadder - transform.forward * deepOffsetForHandsLadder;

                    limbs.leftLeg.oldPos = steps[limbs.leftLeg.oldStepIndex].transform.position - transform.right * widthOffsetGrabForLegs + Vector3.up * hightOffsetForLegsLadder - transform.forward * deepOffsetForLegsLadder;
                    limbs.rightHand.oldPos = steps[limbs.rightHand.oldStepIndex].transform.position - transform.right * -widthOffsetGrabForHands + Vector3.up * hightOffsetForHandsLadder - transform.forward * deepOffsetForHandsLadder;
                }
                else
                {
                    limbs.leftHand.oldStepIndex = limbs.leftHand.stepIndex; //todo ladder
                    limbs.rightLeg.oldStepIndex = limbs.rightLeg.stepIndex;
                    limbs.leftHand.stepIndex += 2;
                    limbs.rightLeg.stepIndex += 2;

                    limbs.leftHand.pos = steps[limbs.leftHand.stepIndex].transform.position - transform.right * widthOffsetGrabForLegs + Vector3.up * hightOffsetForHandsLadder - transform.forward * deepOffsetForHandsLadder;
                    limbs.rightLeg.pos = steps[limbs.rightLeg.stepIndex].transform.position - transform.right * -widthOffsetGrabForHands + Vector3.up * hightOffsetForLegsLadder - transform.forward * deepOffsetForLegsLadder;

                    limbs.leftHand.oldPos = steps[limbs.leftHand.oldStepIndex].transform.position - transform.right * widthOffsetGrabForLegs + Vector3.up * hightOffsetForHandsLadder - transform.forward * deepOffsetForHandsLadder;
                    limbs.rightLeg.oldPos = steps[limbs.rightLeg.oldStepIndex].transform.position - transform.right * -widthOffsetGrabForHands + Vector3.up * hightOffsetForLegsLadder - transform.forward * deepOffsetForLegsLadder;
                }

                var posOnLadder = new Vector3(directionToLadder.x, ladderObject.steps[currentStepIdex + 1].transform.position.y, directionToLadder.z);

                float counter = 0;
                swapGrabLadder = !swapGrabLadder;

                currentStepIdex++;
                rays.ladder.nearestSt = rays.ladder.steps[currentStepIdex];
                currentStep = rays.ladder.nearestSt;
                var speed = distance / climbingTimeLadder;
                float direction = 0;


                ladderObject.PlayStairsSteps();

                while (counter < climbingTimeLadder)
                {
                    yield return new WaitForFixedUpdate();

                    if (counter < climbingTimeLadder / 2)
                        direction += Time.deltaTime * 5;
                    else
                        direction -= Time.deltaTime * 5;

                    transform.position = Vector3.Lerp(transform.position, posOnLadder, counter / climbingTimeLadder);

                    if (!swapGrabLadder)
                    {
                        limbs.leftHand.target.position = Vector3.Lerp(limbs.leftHand.oldPos, limbs.leftHand.pos, counter / climbingTimeLadder);
                        limbs.rightLeg.target.position = Vector3.Lerp(limbs.rightLeg.oldPos, limbs.rightLeg.pos, counter / climbingTimeLadder);
                        limbs.rightHand.target.position = limbs.rightHand.pos;
                        limbs.leftLeg.target.position = limbs.leftLeg.pos;
                        GetComponent<Animator>().SetFloat("LadderDirectionRight", Mathf.Clamp01(direction));
                    }
                    else
                    {
                        limbs.rightHand.target.position = Vector3.Lerp(limbs.rightHand.oldPos, limbs.rightHand.pos, counter / climbingTimeLadder);
                        limbs.leftLeg.target.position = Vector3.Lerp(limbs.leftLeg.oldPos, limbs.leftLeg.pos, counter / climbingTimeLadder);
                        limbs.leftHand.target.position = limbs.leftHand.pos;
                        limbs.rightLeg.target.position = limbs.rightLeg.pos;
                        GetComponent<Animator>().SetFloat("LadderDirectionLeft", Mathf.Clamp01(direction));
                    }

                    counter += Time.fixedDeltaTime;
                }
                GetComponent<Animator>().SetTrigger("SwapGrabLadder");
                transform.position = new Vector3(transform.position.x, currentStep.transform.position.y, transform.position.z);
            }

            upwardMovementLadder = null;
        }

        IEnumerator MoveDownLadder(float distance, List<Step> steps, Ladder ladderObject, Vector3 directionToLadder)
        {
            if (rays.ladder.steps.IndexOf(rays.ladder.GetNearestStep(collider)) == 0)
            {
                onLadder = false;
                transform.rotation = Quaternion.LookRotation(rays.ladder.transform.right * -1, Vector3.up);
                iKSkeleton.rig.rightLegWorld.weight = 0;
                iKSkeleton.rig.leftLegWorld.weight = 0;
                GetComponent<Animator>().SetBool("onLadder", onLadder);
                //TODO: практически бесполезное ожидание
                yield return new WaitForSeconds(0.5f);
            }
            else
            {
                if (swapGrabLadder)
                {
                    limbs.leftLeg.oldStepIndex = limbs.leftLeg.stepIndex;
                    limbs.rightHand.oldStepIndex = limbs.rightHand.stepIndex;
                    limbs.rightHand.stepIndex -= 2;
                    limbs.leftLeg.stepIndex -= 2;

                    limbs.leftLeg.pos = steps[limbs.leftLeg.stepIndex].transform.position - transform.right * widthOffsetGrabForLegs + Vector3.up * hightOffsetForLegsLadder - transform.forward * deepOffsetForLegsLadder;
                    limbs.rightHand.pos = steps[limbs.rightHand.stepIndex].transform.position - transform.right * -widthOffsetGrabForHands + Vector3.up * hightOffsetForHandsLadder - transform.forward * deepOffsetForHandsLadder;

                    limbs.leftLeg.oldPos = steps[limbs.leftLeg.oldStepIndex].transform.position - transform.right * widthOffsetGrabForLegs + Vector3.up * hightOffsetForLegsLadder - transform.forward * deepOffsetForLegsLadder;
                    limbs.rightHand.oldPos = steps[limbs.rightHand.oldStepIndex].transform.position - transform.right * -widthOffsetGrabForHands + Vector3.up * hightOffsetForHandsLadder - transform.forward * deepOffsetForHandsLadder;
                }
                else
                {
                    limbs.leftHand.oldStepIndex = limbs.leftHand.stepIndex;
                    limbs.rightLeg.oldStepIndex = limbs.rightLeg.stepIndex;
                    limbs.leftHand.stepIndex -= 2;
                    limbs.rightLeg.stepIndex -= 2;

                    limbs.leftHand.pos = steps[limbs.leftHand.stepIndex].transform.position - transform.right * widthOffsetGrabForLegs + Vector3.up * hightOffsetForHandsLadder - transform.forward * deepOffsetForHandsLadder;
                    limbs.rightLeg.pos = steps[limbs.rightLeg.stepIndex].transform.position - transform.right * -widthOffsetGrabForHands + Vector3.up * hightOffsetForLegsLadder - transform.forward * deepOffsetForLegsLadder;

                    limbs.leftHand.oldPos = steps[limbs.leftHand.oldStepIndex].transform.position - transform.right * widthOffsetGrabForLegs + Vector3.up * hightOffsetForHandsLadder - transform.forward * deepOffsetForHandsLadder;
                    limbs.rightLeg.oldPos = steps[limbs.rightLeg.oldStepIndex].transform.position - transform.right * -widthOffsetGrabForHands + Vector3.up * hightOffsetForLegsLadder - transform.forward * deepOffsetForLegsLadder;
                }

                var posOnLadder = new Vector3(directionToLadder.x, ladderObject.steps[currentStepIdex - 1].transform.position.y, directionToLadder.z);

                float counter = 0;
                swapGrabLadder = !swapGrabLadder;

                currentStepIdex--;
                rays.ladder.nearestSt = rays.ladder.steps[currentStepIdex];
                currentStep = rays.ladder.nearestSt;
                var speed = distance / climbingTimeLadder;
                float direction = 0;

                ladderObject.PlayStairsSteps();

                while (counter < climbingTimeLadder)
                {
                    yield return new WaitForFixedUpdate();

                    if (counter < climbingTimeLadder / 2)
                        direction += Time.deltaTime * 5;
                    else
                        direction -= Time.deltaTime * 5;

                    transform.position = Vector3.Lerp(transform.position, posOnLadder, counter / climbingTimeLadder);

                    if (swapGrabLadder)
                    {
                        limbs.leftHand.target.position = Vector3.Lerp(limbs.leftHand.oldPos, limbs.leftHand.pos, counter / climbingTimeLadder);
                        limbs.rightLeg.target.position = Vector3.Lerp(limbs.rightLeg.oldPos, limbs.rightLeg.pos, counter / climbingTimeLadder);
                        limbs.rightHand.target.position = limbs.rightHand.pos;
                        limbs.leftLeg.target.position = limbs.leftLeg.pos;
                        GetComponent<Animator>().SetFloat("LadderDirectionRight", Mathf.Clamp01(direction));
                    }
                    else
                    {
                        limbs.rightHand.target.position = Vector3.Lerp(limbs.rightHand.oldPos, limbs.rightHand.pos, counter / climbingTimeLadder);
                        limbs.leftLeg.target.position = Vector3.Lerp(limbs.leftLeg.oldPos, limbs.leftLeg.pos, counter / climbingTimeLadder);
                        limbs.leftHand.target.position = limbs.leftHand.pos;
                        limbs.rightLeg.target.position = limbs.rightLeg.pos;
                        GetComponent<Animator>().SetFloat("LadderDirectionLeft", Mathf.Clamp01(direction));
                    }

                    counter += Time.fixedDeltaTime;

                }
            }
            GetComponent<Animator>().SetTrigger("SwapGrabLadder");
            transform.position = new Vector3(transform.position.x, currentStep.transform.position.y, transform.position.z);
            downwardMovementLadder = null;
            //if (Input.GetKey(KeyCode.S))
            //    downwardMovementLadder = StartCoroutine(MoveDownLadder(climbingSpeedLadder));
        }
        #endregion
        #endregion

        IEnumerator HangHook(Transform rope, Quaternion rot)
        {
            float timer = 0;
            while (true)
            {
                transform.position = Vector3.Lerp(transform.position, rope.position - new Vector3(0, offsetYHook, 0), timer / 0.125f);
                transform.rotation = Quaternion.Lerp(transform.rotation, rot, timer / 0.125f);
                timer += Time.deltaTime;
                if (timer >= 0.125f)
                    break;
                yield return null;
            }
        }

        IEnumerator HangOnHook(Vector3 velocity)
        {

            player.HangHook();
            //stop = true;
            hangOnHook = true;
            PointForHand pointForHand = rays.hook.GetComponentInChildren<PointForHand>();
            var rbHook = rays.hook.GetComponent<Rigidbody>();

            yield return StartCoroutine(HangHook(rays.hook.GetComponentInChildren<PointForHand>().transform, Quaternion.LookRotation(Vector3.Dot(transform.forward, Vector3.right) >= 0 ? Vector3.right : -Vector3.right, Vector3.up)));

            var addedJoint = gameObject.AddComponent<FixedJoint>();
            addedJoint.connectedBody = rbHook;
            addedJoint.autoConfigureConnectedAnchor = false;
            //addedJoint.connectedAnchor = new Vector3(0, offsetYHook, 0);
            Transform[] hands = new Transform[2] { iKSkeleton.GetLimb(IKSkeleton.Limbs.LeftHand), iKSkeleton.GetLimb(IKSkeleton.Limbs.LeftHand) };
            collider.enabled = false;
            Animator animator = GetComponent<Animator>();

            //var objectPointForHand = pointForHand.gameObject;

            rb.constraints = RigidbodyConstraints.None;

            yield return new WaitForSeconds(0.3f);

            while (hangOnHook)
            {
                if (!Input.GetMouseButton(0) || !pointForHand.gameObject.activeSelf)
                {
                    break;
                }

                for (int i = 0; i < hands.Length; i++)
                {
                    hands[i].position = pointForHand.transform.position;
                }
                var hookRotation = pointForHand.transform.parent.rotation;
                //transform.transform.rotation = Quaternion.Euler(hookRotation.eulerAngles.z * -1, transform.rotation.eulerAngles.y, hookRotation.eulerAngles.x * -1);

                animator.SetFloat("HookMomentumForward", transform.InverseTransformDirection(rb.velocity).z / 2);
                animator.SetFloat("HookMomentumSide", transform.InverseTransformDirection(rb.velocity).x / 2);

                if (Input.GetButtonDown("Jump") && inputVector.magnitude >= 1)
                {
                    Destroy(addedJoint);
                    animator.SetTrigger("JumpUp");
                    hangOnHook = false;
                    rb.constraints = RigidbodyConstraints.FreezeRotation;
                    pointForHand.StartCoroutine(pointForHand.DisableCollider());

                    rb.velocity = Vector3.zero;
                    //if (velocity.x != 0 || velocity.z != 0)
                    //{
                    //transform.position += transform.up * 0.075f;
                    rb.AddForce((Vector3.up * jumpForce * superJumpForce * 8 + inputVector * jumpForce * 15), ForceMode.Impulse);
                    //}
                    break;
                }
                yield return null;
            }
            if (pointForHand != null)
                pointForHand.StartCoroutine(pointForHand.DisableCollider());
            Destroy(addedJoint);
            hangOnHook = false;
            animator.SetBool("hangOnHook", hangOnHook);
            transform.rotation = Quaternion.identity;
            rb.constraints = RigidbodyConstraints.FreezeRotation;

            //TODO: ожидать перед тем как сбросить
            yield return new WaitForSeconds(0.3f); //trasition time
            StopClimbAnimation(ref hangOnHookCoroutine, "JumpUp");
            yield return null;
        }

        IEnumerator RopeClimb()
        {
            rb.useGravity = false;
            rb.velocity = Vector3.zero;
            collider.enabled = false;
            rays.rope.GetFirstIndexElementUponGrab(rays.ropeElement);
            Stop = true;
            onRope = true;
            int oldIndexStep = rays.rope.currentRopeElement;

            //transform.rotation = Quaternion.LookRotation(rays.rope.transform.position - transform.position, Vector3.up);
            float timer = 0;
            GetComponent<Animator>().SetTrigger("OnRopeEnter");
            GetComponent<Animator>().SetBool("OnRope", true);
            while (onRope)
            {
                Debug.Log(rays.rope.ropeElements[rays.rope.currentRopeElement].name);
                timer += Time.deltaTime;
                var oldStepPos = rays.rope.ropeElements[oldIndexStep].transform.position;
                var nextStepPos = rays.rope.ropeElements[rays.rope.currentRopeElement].transform.position;
                transform.position = Vector3.Lerp(oldStepPos /*- rays.rope.ropeElements[oldIndexStep].transform.right * collider.radius*/, nextStepPos /*- rays.rope.ropeElements[rays.rope.currentRopeElement].transform.right * collider.radius*/, timer * climbingSpeedRope);
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(
                    Vector3.Lerp(oldStepPos, nextStepPos, timer * climbingSpeedRope) - transform.position, Vector3.up), timer);
                //if (Input.GetMouseButtonUp(0))
                //{
                //    onRope = false;
                //    transform.rotation = Quaternion.Euler(0, transform.position.y, 0);
                //    GetComponent<Animator>().SetBool("OnRope", false);
                //    StopClimbAnimation();
                //}

                if (!IkRope)
                {
                    var leftHand = iKSkeleton.GetLimb(IKSkeleton.Limbs.LeftHand);
                    leftHand.position = Vector3.Lerp(leftHand.position, rays.rope.ropeElements[oldIndexStep - 2 - 1].transform.position + Vector3.up * offsetRopeHand + Vector3.up * -0.035f + transform.right * -0.035f, 0.1f);
                    var rightFoot = iKSkeleton.GetLimb(IKSkeleton.Limbs.RightFoot);
                    rightFoot.position = Vector3.Lerp(rightFoot.position, rays.rope.ropeElements[oldIndexStep - 1].transform.position + Vector3.up * offsetRopeFoot + transform.right * 0.035f, 0.1f);
                    var leftFoot = iKSkeleton.GetLimb(IKSkeleton.Limbs.LeftFoot);
                    leftFoot.position = Vector3.Lerp(leftFoot.position, rays.rope.ropeElements[oldIndexStep - 1].transform.position + Vector3.up * offsetRopeFoot + transform.right * -0.035f, 0.1f);
                    var rightHand = iKSkeleton.GetLimb(IKSkeleton.Limbs.RightHand);
                    rightHand.position = Vector3.Slerp(rightHand.position, rays.rope.ropeElements[oldIndexStep - 2 - 1].transform.position + Vector3.up * offsetRopeHand + Vector3.up * 0.035f + transform.right * 0.035f, 0.1f);
                }

                if (Input.GetKey(KeyCode.W))
                {

                    if (rays.rope.currentRopeElement > 0 && timer >= 1)
                    {
                        //if (upwardMovementRope == null)
                        //    upwardMovementRope = StartCoroutine(MoveUpRope(climbingSpeedRope));
                        //if (downwardMovementRope != null)
                        //{
                        //    StopCoroutine(downwardMovementRope);
                        //    downwardMovementRope = null;
                        //}

                        GetComponent<Animator>().SetFloat("MoveRope", 1);
                        oldIndexStep = rays.rope.currentRopeElement;
                        --rays.rope.currentRopeElement;
                        timer = 0;
                    }

                }
                else
                {
                    GetComponent<Animator>().SetFloat("MoveRope", 0);
                }



                //if (Input.GetKeyDown(KeyCode.D))
                //    rays.rope.ropeElements[currentStepRope].GetComponent<Rigidbody>().AddForce(new Vector3(1000, 0,0));

                //if (Input.GetKeyDown(KeyCode.A))
                //    rays.rope.ropeElements[currentStepRope].GetComponent<Rigidbody>().AddForce(new Vector3(-1000, 0, 0));

                if (Input.GetKey(KeyCode.S))
                {
                    if (timer >= 1)
                    {
                        if (rays.rope.ropeElements.Count - 1 == rays.rope.currentRopeElement)
                        {
                            onRope = false;
                            var rot = Quaternion.LookRotation(rays.rope.transform.position - transform.position * -1, Vector3.up);
                            transform.rotation = Quaternion.Euler(0, rot.y, 0);
                            break;
                        }

                        if (rays.rope.ropeElements.Count - 1 > rays.rope.currentRopeElement)
                        {
                            oldIndexStep = rays.rope.currentRopeElement;
                            ++rays.rope.currentRopeElement;
                            timer = 0;
                        }
                    }

                }

                if (!rootMove && Input.GetButton("Jump"))
                {
                    onRope = false;
                    rb.velocity = Vector3.zero;
                    rb.AddForce((Vector3.up * jumpForce * superJumpForce + transform.forward * jumpForce * 2.5f));

                    break;
                }
                yield return null;
            }
            yield return null;
        }

        /// <summary>
        /// Отключение управление с поддержанием физики в рабочем состоянии
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public void DelayMove(float time)
        {
            if (delay == null)
            {
                delay = StartCoroutine(Delay(time));
            }
        }

        public IEnumerator Delay(float time)
        {
            stunned = true;
            Stop = true;
            yield return new WaitForSeconds(time);
            Stop = false;
            stunned = false;
            delay = null;
        }

        IEnumerator HangUpAnimationDelay(string nameAnim, float endPos, Vector3 leftHandPos, Vector3 rightHandPos)
        {
            GetComponent<Animator>().SetFloat("Movement", 0);
            obstacleFromTop = true;
            var leftHand = iKSkeleton.GetLimb(IKSkeleton.Limbs.LeftHand);
            var rightHand = iKSkeleton.GetLimb(IKSkeleton.Limbs.RightHand);

            string json = File.ReadAllText(Application.streamingAssetsPath + "/" + nameAnim);// File.ReadAllText(Application.dataPath + $"/AnimationData/{nameAnim}");
            KeyFrame[] keyFrame = JsonHelper.FromJson<KeyFrame>(json);

            Vector3 oldPos = new Vector3(keyFrame[0].x, keyFrame[0].y, keyFrame[0].z);

            GetComponent<PlayerSoundsControl>().PlayClimbSound();

            foreach (var key in keyFrame)
            {
                yield return new WaitForFixedUpdate();
                leftHand.position = leftHandPos;
                rightHand.position = rightHandPos;
                iKSkeleton.rig.leftArm.weight = GetComponent<Animator>().GetFloat("LeftHand");
                iKSkeleton.rig.rightArm.weight = GetComponent<Animator>().GetFloat("RightHand");
                iKSkeleton.rig.leftLeg.weight = 0;
                iKSkeleton.rig.rightLeg.weight = 0;
                transform.position += oldPos;
                oldPos = transform.rotation * new Vector3(key.x, key.y / endPos, key.z);
            }

            iKSkeleton.rig.leftLeg.weight = 0;
            iKSkeleton.rig.rightLeg.weight = 0;

            yield return new WaitForFixedUpdate();

            yield return new WaitForSeconds(0.3f);
        }
        #endregion

        enum State
        {
            Movement,
            Stay,
            OnRope,
            OnHook,
            Drag,
            OnLadder,
            Climb,
            Jump,
            Fall
        }

        void UpdateState(State state)
        {
            switch (state)
            {
                case State.Movement:
                    Console.WriteLine("Case 1");
                    break;
                case State.Stay:
                    Console.WriteLine("Case 2");
                    break;
                case State.OnRope:
                    Console.WriteLine("Case 2");
                    break;
                case State.OnHook:
                    Console.WriteLine("Case 2");
                    break;
                case State.Drag:
                    Console.WriteLine("Case 2");
                    break;
                case State.OnLadder:
                    Console.WriteLine("Case 2");
                    break;
                case State.Climb:
                    Console.WriteLine("Case 2");
                    break;
                case State.Jump:
                    Console.WriteLine("Case 2");
                    break;
                case State.Fall:
                    Console.WriteLine("Case 2");
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            //Gizmos.DrawSphere(rays.hitDragObject.point, 0.1f);
            Gizmos.DrawSphere(pivotPosForTest, 0.1f);
        }
    }

    [Serializable]
    class KeyFrame
    {
        public float time;
        public float x;
        public float y;
        public float z;
    }

    [Serializable]
    public struct AnimatorController
    {
        public string name;
        public RuntimeAnimatorController controller;
    }

    public static class JsonHelper
    {
        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.Items;
        }

        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper);
        }

        public static string ToJson<T>(T[] array, bool prettyPrint)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper, prettyPrint);
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }

    struct LimbsData
    {
        public Limb leftLeg;
        public Limb rightLeg;
        public Limb leftHand;
        public Limb rightHand;
    }

    struct Limb
    {
        public Transform target;
        public Vector3 pos;
        public Vector3 oldPos;
        public int stepIndex;
        public int oldStepIndex;
    }
}