﻿using UnityEngine;


public class FastIKLook : MonoBehaviour
{
    /// <summary>
    /// Look at target
    /// </summary>
    public Transform target;

    [SerializeField] Transform[] bones;
    [SerializeField, Range(-1, 1)] float[] weight;
    /// <summary>
    /// Initial Rotation
    /// </summary>
    [SerializeField, Range(-180, 180)] float offsetRotationHeadX;
    [SerializeField, Range(-180, 180)] float offsetRotationHeadY;
    [SerializeField, Range(-180, 180)] float offsetRotationHeadZ;

    [SerializeField] float maxEulerHorizontal;
    [SerializeField] float maxEulerVertical;
    Transform player;

    public bool active;

    private void Awake()
    {
        player = GetComponentInParent<Player>().transform;
    }

    public void Init()
    {

    }

    void Update()
    {
        if (target == null)
            return;

        //if(!active)
        //    return;

        for (int i = 0; i < bones.Length; i++)
        {
            Quaternion rotBones = Quaternion.LookRotation(target.position - bones[i].position, Vector3.up);
            Quaternion rot = Quaternion.LookRotation(bones[i].InverseTransformDirection(target.position - bones[i].position), Vector3.up);
            Quaternion rotRoot = Quaternion.LookRotation(player.InverseTransformDirection(target.position - player.position), Vector3.up);
            Quaternion finalRot = rotBones * Quaternion.Euler(offsetRotationHeadX, offsetRotationHeadY, offsetRotationHeadZ); //180,89,105
            var govno =  finalRot.eulerAngles - player.rotation.eulerAngles /*+ new Vector3(0,90,0)*/;
            //Debug.Log(rot.eulerAngles);
            //finalRot *= Quaternion.Euler(0, 0, -15);
            //Debug.Log(bones[i].name + "   eulerAngles bone rot=  " + finalRot.eulerAngles);
            //Debug.Log(bones[i].name + "  bone rot=  "+ new Vector3(0,0,(finalRot.eulerAngles.z - 285)/*/ (bones.Length + 1 - i)*/ ).z);
            //finalRot = Quaternion.Euler(finalRot.eulerAngles.x, finalRot.eulerAngles.y, 285 + (finalRot.eulerAngles.z - 285) / (bones.Length + 1 - i));

            var trueRotation = Quaternion.Euler(finalRot.eulerAngles.x, finalRot.eulerAngles.y, 285 + (finalRot.eulerAngles.z - 285) * weight[i]);


            //Debug.Log(bones[i].name + "   eulerAngles bone rot Z=  " + bones[i].eulerAngles);

                govno.y = govno.y > 200 ? govno.y - 360 : govno.y;
            //Debug.Log(govno);

            if (Mathf.Abs(govno.y) < maxEulerHorizontal)
            {
                bones[i].eulerAngles = new Vector3(bones[i].eulerAngles.x, finalRot.eulerAngles.y, bones[i].eulerAngles.z);
                //bones[i].localEulerAngles = /*Vector3.Lerp(bones[i].localEulerAngles, */new Vector3(bones[i].localEulerAngles.x, finalRot.y, bones[i].localEulerAngles.z)/*, 0.5f)*/;

                //Debug.Log(bones[i].localEulerAngles.y);
                //bones[i].eulerAngles = Vector3.Lerp(bones[i].eulerAngles, new Vector3(bones[i].eulerAngles.x, finalRot.eulerAngles.y, bones[i].eulerAngles.z), 0.5f);
            }
            bones[i].eulerAngles = new Vector3(bones[i].eulerAngles.x, bones[i].eulerAngles.y, finalRot.eulerAngles.z);
            //Debug.Log(bones[i].eulerAngles);
            //bones[i].eulerAngles = new Vector3(trueRotation.eulerAngles.x, 
            //    Mathf.Clamp(trueRotation.eulerAngles.y, 269 - maxEulerHorizontal, 269 + maxEulerHorizontal), 
            //    Mathf.Clamp(trueRotation.eulerAngles.z, 285 - maxEulerVertical, 285 + maxEulerVertical));
            //Debug.Log(bones[i].name + "   eulerAngles bone rot Z=  " + bones[i].eulerAngles + "   ////  " + (285 + (finalRot.eulerAngles.z - 285) * weight[i])); // 285 - это 0 градусов оси Z
            //Debug.Log("Rot Root   " + ((rotRoot.eulerAngles.y > 300 ? new Vector3(rotRoot.eulerAngles.x, rotRoot.eulerAngles.y - 360, rotRoot.eulerAngles.z) : rotRoot.eulerAngles)));
            //trueRotation.eulerAngles.z < 180 ?
            //Quaternion.Euler(finalRot.eulerAngles.x, finalRot.eulerAngles.y, 285 + (finalRot.eulerAngles.z - 285) * weight[i] + 340)
            //:
            //Quaternion.Euler(finalRot.eulerAngles.x, finalRot.eulerAngles.y, 285 + (finalRot.eulerAngles.z - 285) * weight[i]);
        }
    }
    
}
