using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ChangeCenterOfMass : MonoBehaviour
{
    [SerializeField] Transform pivot;
    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = Vector3.Scale(pivot.localPosition, transform.localScale);
    }
}
