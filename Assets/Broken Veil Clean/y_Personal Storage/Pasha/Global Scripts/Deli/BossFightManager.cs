using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Events;
using GlobalScripts;



public class BossFightManager : MonoBehaviour
{
    [SerializeField] FindPlayer findPlayer;
    [SerializeField] CinemachineDollyCart boss;
    public GameObject boy;
    [SerializeField] GameObject wrist;
    [SerializeField] float deltaDistance;
    Coroutine gameover;
    [SerializeField] public List<Stage> stages;
    [SerializeField] CinemachineSmoothPath path;
    [SerializeField] DeliWeight weight;
    [SerializeField] RuntimeAnimatorController animatorDeath;
    
    [SerializeField] int currentStage;
    [SerializeField] float currentTimeStage;
    [SerializeField] float resultTimeStage;

    [Header("Game Over Trigger")]
    [SerializeField] float _gameOverDelay;
    [Space, SerializeField] FloatVariable _gameOverSO;
    [Space, SerializeField] UnityEvent OnGameOver;
    Coroutine stagesCorut;
    [SerializeField] float distanceForGrab;
    [SerializeField] AnimData[] animData;
    public AK.Wwise.Event catchPlayerSound;


    void Start()
    {
        findPlayer.StartSearch();
        boss.GetComponent<Collider>().enabled = true;
    }

    public void StartFightStage(int numberStage)
    {
        if(stagesCorut == null)
            stagesCorut = StartCoroutine(Stages(numberStage));
        findPlayer.player.GetComponent<PlayerController>().sprintMaxTime = 100;
    }

    void Update()
    {
        if(findPlayer.player != null)
            boy = findPlayer.player;

        if (boy == null)
            return;

        deltaDistance = Vector3.Distance(new Vector3(boss.transform.position.x, boy.transform.position.y, boss.transform.position.z) , boy.transform.position);
        Debug.DrawRay(boss.transform.position, (boy.transform.position - boss.transform.position + Vector3.up * 0.5f).normalized * distanceForGrab, Color.green);

        if (deltaDistance <= distanceForGrab && gameover == null && !weight.deliTriggerWeight.stay)
        {
            
            StopAllCoroutines();
            gameover = StartCoroutine(Gameover());
        }
    }

    IEnumerator Stages(int numberStage)
    {
        for (int i = numberStage; i < stages.Count; i++)
        {
            boss.m_Position = path.m_Waypoints[i].position.x;
            yield return PlayStage(stages[i], i);
        }
    }

    IEnumerator PlayStage(Stage stage, int number)
    {
        currentStage = number;
        //yield return new WaitForSeconds(stage.offsetTime);
        var distance = Vector3.Distance(path.m_Waypoints[number].position, path.m_Waypoints[number + 1].position);
        //boss.m_Speed = 
        var time = stage.timeForFinishBoy;
        var resulTimeForFinishBoy = time;
        if(number != 0)
            resulTimeForFinishBoy -= stages[number - 1].timeForFinishBoy;
        
        float timeToFinish = resulTimeForFinishBoy - stage.offsetTimeForFinishBoss;
        resultTimeStage = timeToFinish;
        boss.m_Speed = distance / timeToFinish;
        var bossAnimator = boss.transform.GetChild(0).GetComponent<Animator>();
        bossAnimator.SetFloat("Speed", boss.m_Speed);
        float timer = 0;
        bool animTrigger = false;

        var animoffset = AnimOffset(stage.trigger);

        while (timer <= timeToFinish)
        {
            timer += Time.deltaTime;
            currentTimeStage = timer;
            //path.m_Waypoints 
            //boss.m_Speed = stage.speedBoss;

            if (timer >= timeToFinish - (stage.offsetTimeAnimation + animoffset) && !animTrigger)
            {
                bossAnimator.SetTrigger(stage.trigger.ToString());
                animTrigger = true;
            }
            yield return null;
        }

        kostil_traska.impulse(stage.timeAndStrenghtShake);

        if (stage.coliders.Length != 0)
        {
            foreach (var item in stage.coliders)
            {
                item.enabled = false;
            }
        }

        if (stage.particles.Length != 0)
        {
            foreach (var item in stage.particles)
            {
                item.Play();
            }
        }

        if (stage.animators.Length != 0)
        {
            foreach (var item in stage.animators)
            {
                item.enabled = true;
            }
            //bossAnimator.SetTrigger(stage.trigger.ToString()); //0,37
        }

        if (stage.obstacleName == "���� ����")
        {
            yield return new WaitForSeconds(0.5f);
            distanceForGrab += 1.5f;
            weight.BiteAndForce();
        }

        boss.m_Speed = 0;
        bossAnimator.SetFloat("Speed", 0);
        yield return new WaitForSeconds(stage.stopTimeBoss);
    }

    IEnumerator Gameover()
    {
        var player = boy.GetComponent<Player>();
        boss.m_Speed = 0;
        var animator = boss.GetComponentInChildren<Animator>();
        animator.SetTrigger("Catch");
        yield return new WaitForSeconds(0.014f* 16);
        if (Vector3.Distance(stages[currentStage].obstacleObject.transform.position,boss.transform.position) < 1f)
        {
            if (stages[currentStage].particles.Length != 0)
            {
                foreach (var item in stages[currentStage].particles)
                {
                    item.Play();
                }
            }

            if (stages[currentStage].animators.Length != 0)
            {
                foreach (var item in stages[currentStage].animators)
                {
                    item.enabled = true;
                }
            }
        }

        catchPlayerSound.Post(gameObject);

        boy.transform.parent = wrist.transform;
        boy.transform.localPosition = new Vector3(-0.105f, -0.097f, -0.048f);
        boy.transform.localRotation = Quaternion.Euler(3.975f, 265.642f, 2.628f);
        boy.GetComponent<Rigidbody>().velocity = Vector3.zero;
        boy.GetComponent<Animator>().runtimeAnimatorController = animatorDeath;
        Destroy(boy.GetComponent<Joint>());
        foreach (var item in player.ears)
        {
            var joint = item.GetComponent<ConfigurableJoint>();
            joint.xMotion = ConfigurableJointMotion.Locked;
            joint.yMotion = ConfigurableJointMotion.Locked;
            joint.zMotion = ConfigurableJointMotion.Locked;
            joint.angularXMotion = ConfigurableJointMotion.Locked;
            joint.angularYMotion = ConfigurableJointMotion.Locked;
            joint.angularZMotion = ConfigurableJointMotion.Locked;
        }
        player.enabled = false;
        boy.GetComponent<PlayerController>().enabled = false;
        boy.GetComponent<Collider>().enabled = false;
        boy.GetComponent<Rigidbody>().isKinematic = true;
        

        _gameOverSO.Value = _gameOverDelay;

        OnGameOver?.Invoke();
        yield return new WaitForSeconds(_gameOverDelay);
    }

    float AnimOffset(AnimTriggers triggers)
    {
        float timeOffset = 0;
        foreach (var item in animData)
        {
            if (item.triggers == triggers)
            {
                timeOffset = item.timeOffset + item.timeTransiton;
                break;
            }
        } 
        return timeOffset;
    }

    [System.Serializable]
    public struct Stage
    {
        public string obstacleName;
        public GameObject obstacleObject;
        public Animator[] animators;
        public AnimTriggers trigger;
        public float offsetTimeAnimation;
        public float offsetTimeForFinishBoss;
        public float timeForFinishBoy;
        public float stopTimeBoss;
        public ParticleSystem[] particles;
        public Collider[] coliders;
        public float timeAndStrenghtShake;
        //public float offsetTime;
    }

    public enum typeDestroyObject
    {
        Animation,
        Physics
    }

    [System.Serializable]
    public enum AnimTriggers
    {
        HookTrigger,
        HitTrigger,
        PushV1,
        PushV2
    }

    [System.Serializable]
    public struct AnimData
    {
        public AnimTriggers triggers;
        public float timeTransiton;
        public float timeOffset;
    }


    private void OnGizmosDraw()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawCube(Vector3.zero, Vector3.one * 20);
        //Gizmos.DrawFrustum(transform.position, 2, 2, 0, 0.5f);
    }
}
