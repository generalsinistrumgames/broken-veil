using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;

public class DeliWeight : MonoBehaviour
{
    [SerializeField] Transform leftMax;
    [SerializeField] Transform rightMax;
    [SerializeField] Transform leftMin;
    [SerializeField] Transform rightMin;

    [SerializeField] Transform leftBowl;
    [SerializeField] Transform rightBowl;
    [SerializeField] Transform arrow;
    Rigidbody leftBowlRB;
    Rigidbody rightBowlRB;
    [SerializeField] bool bite;
    public bool bitePlayer;
    [SerializeField] float forceBite = 50f;
    [SerializeField] float forceForBoy = 7f;
    [SerializeField] GameObject player;
    [SerializeField] FindPlayer findPlayer;
    public DeliTriggerWeight deliTriggerWeight;
    [SerializeField] float timeBubble = 1.3f;
    public Coroutine bubbleDelay;

    private void Start()
    {
        deliTriggerWeight = GetComponentInChildren<DeliTriggerWeight>();
        leftBowlRB = leftBowl.GetComponent<Rigidbody>();
        rightBowlRB = rightBowl.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (findPlayer.player != null)
            player = findPlayer.player;

        if (bite)
            BiteAndForce();
    }

    private void FixedUpdate()
    {

        if (leftBowl.position.y > leftMax.position.y || leftBowl.position.y < leftMin.position.y)
            leftBowlRB.velocity = Vector3.zero;

        if (rightBowl.position.y > rightMax.position.y || rightBowl.position.y < rightMin.position.y)
            rightBowlRB.velocity = Vector3.zero;

        if (leftBowlRB.velocity.y < 0)
        {
            rightBowlRB.velocity = -leftBowlRB.velocity;
            //Debug.Log("rightUp");
        }
        if (rightBowlRB.velocity.y < 0)
        {
            leftBowlRB.velocity = -rightBowlRB.velocity;
            //Debug.Log("leftUp");
        }

        leftBowl.position = new Vector3(leftBowl.position.x, Mathf.Clamp(leftBowl.position.y, leftMin.position.y, leftMax.position.y), leftBowl.position.z);
        rightBowl.position = new Vector3(rightBowl.position.x, Mathf.Clamp(rightBowl.position.y, rightMin.position.y, rightMax.position.y), rightBowl.position.z);

        var normalizerWeight = (leftBowl.position.y - leftMin.position.y) / (leftMax.position.y - leftMin.position.y);
        arrow.localEulerAngles = new Vector3(arrow.localEulerAngles.x, Mathf.Clamp(-normalizerWeight * 50 + 25, -25, 25), arrow.localEulerAngles.z);

    }

    public void BiteAndForce()
    {
        var rbPlayer = player.GetComponent<Rigidbody>();
        rbPlayer.velocity = Vector3.zero;
        //if (deliTriggerWeight.stay)
        //{

        rightBowl.GetComponent<BoxCollider>().enabled = false;
        deliTriggerWeight.GetComponent<BoxCollider>().enabled = false;
        player.GetComponent<PlayerController>().enabled = false;
        rbPlayer.AddForce(Vector3.up * forceForBoy, ForceMode.Impulse);
        bitePlayer = true;
        bubbleDelay = StartCoroutine(BubbleDelay());

        //}
        leftBowlRB.AddForce(Vector3.up * -forceBite, ForceMode.Impulse);
        bite = false;

    }

    IEnumerator BubbleDelay()
    {
        var timeKostil = 0.8f;
        yield return new WaitForSeconds(timeKostil);
        player.GetComponent<PlayerController>().enabled = false;
        yield return new WaitForSeconds(timeBubble - timeKostil);
        deliTriggerWeight.stay = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(leftMax.position, 0.05f);
        Gizmos.DrawWireSphere(rightMax.position, 0.05f);
        Gizmos.DrawWireSphere(leftMin.position, 0.05f);
        Gizmos.DrawWireSphere(rightMin.position, 0.05f);
    }
}
