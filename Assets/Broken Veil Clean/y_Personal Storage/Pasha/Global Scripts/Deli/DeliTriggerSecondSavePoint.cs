using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliTriggerSecondSavePoint : MonoBehaviour
{
    [SerializeField] BossFightManager fightManager;
    [SerializeField] GameObject boss;
    [SerializeField] int stageStart;

    public void StartFight()
    {
        boss.SetActive(true);
        fightManager.enabled = true;
        fightManager.StartFightStage(stageStart);
    }
}
