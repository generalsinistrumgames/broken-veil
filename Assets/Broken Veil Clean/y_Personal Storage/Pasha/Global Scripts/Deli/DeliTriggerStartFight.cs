using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliTriggerStartFight : MonoBehaviour
{
    [SerializeField] BossFightManager fightManager;
    [SerializeField] GameObject boss;
    [SerializeField] bool start;
    [SerializeField] int stage;

    private void Update()
    {
        if(start)
        {
            boss.SetActive(true);
            fightManager.enabled = true;
            fightManager.StartFightStage(stage);
            start = !start;
        }
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent<Player>(out Player player))
        {
            boss.SetActive(true);
            fightManager.enabled = true;
            fightManager.StartFightStage(stage);
        }
    }
}
