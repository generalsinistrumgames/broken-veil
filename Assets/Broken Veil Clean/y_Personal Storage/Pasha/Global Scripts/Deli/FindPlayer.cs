using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindPlayer : MonoBehaviour
{
    public GameObject player;

    public void StartSearch()
    {
        StartCoroutine(StartSearchPlayer());
    }

    IEnumerator StartSearchPlayer()
    {
        while(player == null)
        {
            player = FindObjectOfType<Player>().gameObject;
            yield return null;
        }
    }
}
