using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;

public class DeliTriggerWeight : MonoBehaviour
{
    public bool stay;
    [SerializeField] DeliWeight weight;

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent(out PlayerController player))
        {
            stay = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out PlayerController player))
        {
            if(!weight.bitePlayer)
                stay = false;
        }
    }
}
