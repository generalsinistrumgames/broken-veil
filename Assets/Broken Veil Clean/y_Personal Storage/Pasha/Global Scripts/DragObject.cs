﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
//[RequireComponent(typeof(BoxCollider))]
public class DragObject : MonoBehaviour
{
    [SerializeField] float handGripHeight;
    [SerializeField] Color color = Color.green;
    [Range(0, 1)] public float normalizedWeight = 0.5f;
    float height;
    /// <summary>
    /// Высота Захвата рук
    /// </summary>
    [HideInInspector] public float HandGripHeight { get => height; set => handGripHeight = value; }

    public Rigidbody boxRigidbody;

    [Header("Sound Events")]
    public string dragPlaySoundEventName = "Play_chopping_box";
    [Space]
    public string dragStopSoundEventName = "Stop_chopping_box";
    [Space]
    public string playerBoxHitEventName = "Play_chopping_box_hit";
    [Space]
    public bool soundIsPlaying;
    [Space]
    public float dragOffsetSoundPlay = 0.1f;
    [Space]
    public float dragHitSoundPlay = 1f;

    [Header("Drag Object Hint")]
    public GameObject dragHint;

    void Start()
    {
        boxRigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (boxRigidbody.velocity.magnitude > dragOffsetSoundPlay && !soundIsPlaying)
        {
            AkSoundEngine.PostEvent(dragPlaySoundEventName, gameObject);

            soundIsPlaying = true;
        }
        else if (boxRigidbody.velocity.magnitude <= dragOffsetSoundPlay && soundIsPlaying)
        {
            AkSoundEngine.PostEvent(dragStopSoundEventName, gameObject);

            soundIsPlaying = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<Player>(out var player))
        {
            if (collision.relativeVelocity.magnitude > dragHitSoundPlay)
            {
                PlayPlayerBoxHitSound();
            }
        }
    }
    public void PlayPlayerBoxHitSound()
    {
        AkSoundEngine.PostEvent(playerBoxHitEventName, gameObject);
    }

    //private void OnDrawGizmos()
    //{
    //    //Vector3 bounds = GetComponent<BoxCollider>().bounds.extents;

    //    //var width = bounds.x;
    //    //height = Mathf.Clamp(handGripHeight, -bounds.y, bounds.y);
    //    //var depth = bounds.z;
    //    //Gizmos.color = color;
    //    //Gizmos.DrawLine(new Vector3(width, height, depth) + transform.position, new Vector3(-width, height, depth) + transform.position);
    //    //Gizmos.DrawLine(new Vector3(-width, height, -depth) + transform.position, new Vector3(width, height, -depth) + transform.position);
    //    //Gizmos.DrawLine(new Vector3(-width, height, -depth) + transform.position, new Vector3(-width, height, depth) + transform.position);
    //    //Gizmos.DrawLine(new Vector3(width, height, -depth) + transform.position, new Vector3(width, height, depth).normalized + transform.position);
    //}
}