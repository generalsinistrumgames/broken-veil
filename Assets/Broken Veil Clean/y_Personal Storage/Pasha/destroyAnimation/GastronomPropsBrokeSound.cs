using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GastronomPropsBrokeSound : MonoBehaviour
{
    [Header("Sound Events")]
    public List<AK.Wwise.Event> playEvents;

    public void PlaySound()
    {
        foreach (var soundEvent in playEvents)
        {
            soundEvent.Post(gameObject);
        }
    }
}