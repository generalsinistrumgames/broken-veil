using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ImpulsForce : MonoBehaviour
{
    [SerializeField] Vector3 force;
    [SerializeField] Vector3 relativeForce;
    [SerializeField] Vector3 torque;
    [SerializeField] Vector3 relativeTorque;

    Rigidbody rb;
    public bool check;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (check)
        {
            rb.isKinematic = false;
            rb.AddForce(force, ForceMode.VelocityChange);
            rb.AddRelativeForce(relativeForce, ForceMode.VelocityChange);
            rb.AddTorque(torque, ForceMode.VelocityChange);
            rb.AddRelativeTorque(relativeTorque, ForceMode.VelocityChange);
            check = !check;
        }
        
    }
}
