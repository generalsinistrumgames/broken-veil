using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliWeightSounds : MonoBehaviour
{
    [Header("Sound Events")]
    public string weightFallSoundEvent = "Play_gastronome_monster_weigher";
    [Space]
    public float hitMagnitude = 1f;
    public void WeightFallSoundEvent()
    {
        AkSoundEngine.PostEvent(weightFallSoundEvent, gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > hitMagnitude)
        {
            if (collision.gameObject.TryGetComponent<FloorType>(out var floorType))
            {
                WeightFallSoundEvent();
            }
        } 
    }
}