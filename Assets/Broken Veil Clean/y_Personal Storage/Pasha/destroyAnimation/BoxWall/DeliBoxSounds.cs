using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliBoxSounds : MonoBehaviour
{
    [Header("Sound Events")]
    public string boxDestroySoundEvent = "Play_gastronome_monster_boxes";
    [Space]
    public string vegFallSoundEvent = "Play_gastronome_steps_veg_fall";

    public void PlayDestroyBoxSound()
    {
        AkSoundEngine.PostEvent(boxDestroySoundEvent, gameObject);
    }

    public void PlayVegFallSound()
    {
        AkSoundEngine.PostEvent(vegFallSoundEvent, gameObject);
    }
}