using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliObjFallSoundPlayer : MonoBehaviour
{
    [Header("Sound Event")]
    public string fallSoundEvent;
    [Space]
    public float hitMagnitude = 1f;
    public void FallSoundEvent()
    {
        AkSoundEngine.PostEvent(fallSoundEvent, gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > hitMagnitude)
        {
            if (collision.gameObject.TryGetComponent<FloorType>(out var floorType))
            {
                FallSoundEvent();
            }
        }
    }
}