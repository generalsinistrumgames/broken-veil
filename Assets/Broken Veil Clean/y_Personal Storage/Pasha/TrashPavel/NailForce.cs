﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NailForce : MonoBehaviour
{
    public float xMin;
    public float xMax;
    public float zMin;
    public float zMax;
    public float yMin;
    public float yMax;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 randomForceVector = new Vector3(Random.Range(xMin, xMax), Random.Range(yMin, yMax), Random.Range(zMin, zMax));
        GetComponent<Rigidbody>().AddForce(randomForceVector,ForceMode.Impulse);
        Quaternion randomRotation = Quaternion.Euler(Random.Range(-360, 360), Random.Range(-360, 360), Random.Range(-360, 360));
        transform.rotation = randomRotation;
        //Vector3 randomTorqueForceVector = new Vector3(Random.Range(xMin, xMax), Random.Range(yMin, yMax), Random.Range(zMin, zMax));
        //GetComponent<Rigidbody>().AddTorque(randomTorqueForceVector, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
