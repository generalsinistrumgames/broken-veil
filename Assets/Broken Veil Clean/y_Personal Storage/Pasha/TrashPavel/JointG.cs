﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointG : MonoBehaviour
{
    [SerializeField] Rigidbody rb;
    public float force;
    // Start is called before the first frame update
    void Start()
    {
        rb.AddForce(transform.forward * force, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
