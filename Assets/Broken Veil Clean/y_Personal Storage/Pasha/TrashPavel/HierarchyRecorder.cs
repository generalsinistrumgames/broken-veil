﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEditor;
//using UnityEditor.Animations;

//public class HierarchyRecorder : MonoBehaviour
//{
//    // The clip the recording is going to be saved to.
//    public AnimationClip clip;

//    // Checkbox to start/stop the recording.
//    public bool record = false;

//    // The main feature: the actual recorder.
//    private GameObjectRecorder m_Recorder;
//    Coroutine recordCorut;
//    [SerializeField, Tooltip("Ожидание n кадров перед записью ключа анимации.")] int frameRate;


//    void Start()
//    {
//        // Create the GameObjectRecorder.
//        m_Recorder = new GameObjectRecorder(gameObject);

//        // Set it up to record the transforms recursively.
//        m_Recorder.BindComponentsOfType<Transform>(gameObject, true);
//    }

//    // The recording needs to be done in LateUpdate in order
//    // to be done once everything has been updated
//    // (animations, physics, scripts, etc.).
//    void LateUpdate()
//    {
//        if (clip == null)
//            return;

//        if (record)
//        {
//            // As long as "record" is on: take a snapshot.
//            if (recordCorut == null)
//                recordCorut = StartCoroutine(Record());
//        }
//        else if (m_Recorder.isRecording)
//        {
//            // "record" is off, but we were recording:
//            // save to clip and clear recording.
//            m_Recorder.SaveToClip(clip);
//            m_Recorder.ResetRecording();
//        }
//    }

//    IEnumerator Record()
//    {
//        int counter = 0;
//        while (record)
//        {
//            if (counter == frameRate)
//            {
//                m_Recorder.TakeSnapshot(Time.deltaTime);
//                counter = 0;
//            }

//            counter++;
//            yield return new WaitForEndOfFrame();
//        }
//        recordCorut = null;
//    }
//}