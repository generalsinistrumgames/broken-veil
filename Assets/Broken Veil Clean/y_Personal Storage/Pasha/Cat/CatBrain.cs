using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations.Rigging;
using GlobalScripts;
using FIMSpace.FSpine;
//using UnityStandardAssets.Characters.ThirdPerson;

namespace Cat
{
    public class CatBrain : MonoBehaviour
    {
        public laser_work laser;
        Vector3 target;
        List<CurrentState> states;
        [SerializeField] float minDistanceForRun;
        NavMeshAgent cat;
        float timeRotation = 0.15f;
        Coroutine rotation;
        Animator animator;
        [SerializeField, Range(0, 180)] float angleRot;
        [SerializeField] public float walkSpeed;
        [SerializeField] public float runSpeed;
        bool run;
        [SerializeField] Transform headTargetIK;
        [SerializeField] Rig rig;
        [SerializeField] float multiplierSpeedAimIK = 2f;
        [SerializeField] int countTimeTryFindTargetMin;
        [SerializeField] int countTimeTryFindTargetMax;
        [SerializeField] float durationIterationFindTarget;
        [SerializeField] float speedAimTarget = 0.1f;
        Coroutine find;
        bool jump;
        bool jumpEnd;
        bool targetAvailable;
        float normalizedDot;
        float angle;
        bool idle;
        Coroutine idleCor;
        public float offsetRay = 0.2f;
        AgentLinkMover linkMover;
        FSpineAnimator spineAnimator;

        private void Start()
        {
            linkMover = GetComponent<AgentLinkMover>();
            cat = GetComponent<NavMeshAgent>();
            cat.updateRotation = false;
            animator = GetComponent<Animator>();
            spineAnimator = GetComponent<FSpineAnimator>();
            StartCoroutine(Track(target));
            StartCoroutine(SpeedController());
            StartCoroutine(Movement());
            StartCoroutine(Scan());
            StartCoroutine(Rotation());
            StartCoroutine(Idle());
        }

        private void Update()
        {
            if (laser == null)
            {
                laser_work[] lw = GameObject.FindObjectsOfType<laser_work>();
                if (lw.Length < 1)
                    return;
                else
                    laser = lw[0];
            }

            target = laser.hitRay.point;

            normalizedDot = Vector3.Dot(transform.forward, (new Vector3(target.x, transform.position.y, target.z) - transform.position).normalized);
            if (normalizedDot >= Mathf.Abs(angleRot / 180 - 1) && laser.gameObject.activeSelf)
            {
                targetAvailable = true;
                //rotation = StartCoroutine(Rotation());
            }
            else
                targetAvailable = false;    

            animator.SetFloat("Speed", cat.velocity.magnitude);

            if (linkMover.jumpTrigger)
            {
                if (cat.currentOffMeshLinkData.startPos.y < cat.currentOffMeshLinkData.endPos.y)
                    animator.SetTrigger("JumpUp");
                else
                    animator.SetTrigger("JumpDown");
                linkMover.jumpTrigger = false;
            }

            if (cat.isOnOffMeshLink)
                spineAnimator.enabled = false;
            else
                spineAnimator.enabled = true;
        }

        IEnumerator Movement()
        {
            while (true)
            {
                if (normalizedDot < 0.866f)//0.95f
                {

                    cat.velocity *= 0.75f;
                    angle = angle / 2;
                }
                else
                {
                    headTargetIK.position = laser.end_point;
                    if (!cat.currentOffMeshLinkData.activated)
                    {
                        SetTarget();
                    }
                }
                yield return null;
            }
            

            //find = StartCoroutine(FindTarget());
        }

        IEnumerator Rotation()
        {
            bool triggerFind = true;
            while (true)
            {
                if (targetAvailable)
                {
                    idle = false;
                    find = null;
                    if (!cat.currentOffMeshLinkData.activated)
                    {
                        rig.weight += Time.deltaTime * multiplierSpeedAimIK;
                        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(new Vector3(target.x, transform.position.y, target.z) - transform.position), 6 * Time.deltaTime);
                    }
                    triggerFind = false;
                }
                else if (find == null && !triggerFind)
                {
                    triggerFind = true;
                    find = StartCoroutine(FindTarget());
                }
                yield return null;
            }

        }

        IEnumerator Scan()
        {
            while (true)
            {
                Quaternion rot = Quaternion.identity;
                var horizontalDirection = new Vector3(cat.velocity.x, 0, cat.velocity.z);
                if (horizontalDirection != Vector3.zero)
                    rot = Quaternion.LookRotation(horizontalDirection, Vector3.up);

                var angle = Mathf.Clamp(Quaternion.Angle(transform.rotation, rot), 0, 180);
                angle = transform.InverseTransformDirection(cat.velocity).x;
                angle = angle > 0 ? angle : -angle;

                animator.SetFloat("AngleMovement", angle);
                yield return null;
            }
        }

        IEnumerator FindTarget()
        {
            //headTargetIK.position = potentialPos;
            int countTarget = Random.Range(countTimeTryFindTargetMin, countTimeTryFindTargetMax);

            for (int i = 0; i < countTarget; i++)
            {
                Vector3 potentialPos = transform.position + transform.forward * Random.Range(0.2f, 2f) + transform.right * Random.Range(-2f, 2f);
                yield return StartCoroutine(AimTarget(potentialPos, speedAimTarget));
                yield return new WaitForSeconds(durationIterationFindTarget);
            }
            idleCor = StartCoroutine(Idle());
            find = null;
        }

        IEnumerator Idle()
        {
            idle = true;
            while (idle)
            {
                rig.weight = Mathf.Clamp01(rig.weight - Time.deltaTime * multiplierSpeedAimIK);
                animator.SetBool("Idle", idle);
                yield return null;
            }
            animator.SetBool("Idle", idle);
            idleCor = null;
        }

        IEnumerator AimTarget(Vector3 potentialPos, float aimTime)
        {
            //headTargetIK.position = potentialPos;
            Vector3 startPos = headTargetIK.position;
            float timer = 0;
            while (timer < aimTime)
            {
                headTargetIK.position = Vector3.Lerp(startPos, potentialPos, timer / aimTime);
                timer += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
        }

        void SetTarget()
        {
            var distance = (transform.position - target).magnitude;

            if (distance > minDistanceForRun)
                run = true;
            else run = false;

            cat.SetDestination(target);
        }

        IEnumerator Track(Vector3 target)
        {
            while (true)
            {
                if (laser == null || !laser.gameObject.activeSelf)
                {
                    //Weights = Mathf.Lerp(a, b, c);
                    yield return null;
                }
                yield return null;
            }
        }

        IEnumerator SpeedController()
        {
            while (true)
            {
                if (run)
                    cat.speed = Mathf.Clamp(cat.speed + Time.deltaTime * cat.acceleration, 0, runSpeed);
                else
                    cat.speed = Mathf.Clamp(cat.speed - Time.deltaTime * cat.acceleration, walkSpeed, runSpeed);
                yield return null;
            }
        }
    }

    public struct CurrentState
    {
        public Coroutine state;
    }
}
