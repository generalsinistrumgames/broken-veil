using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using Cat;
using FIMSpace.FSpine;


public enum OffMeshLinkMoveMethod
{
    Teleport,
    NormalSpeed,
    Parabola,
    Curve,
    ConfigurableCurve
}

[RequireComponent(typeof(NavMeshAgent))]
public class AgentLinkMover : MonoBehaviour
{
    public OffMeshLinkMoveMethod m_Method = OffMeshLinkMoveMethod.Parabola;
    public AnimationCurve m_Curve = new AnimationCurve();

    [SerializeField] float delayForJumpUp = 0.8f;
    [SerializeField] float curveDurationForJumpUp = 0.53f;
    [SerializeField] float delayAfterJumpUp = 0.8f;

    [SerializeField] float delayForJumpDown = 0.8f;
    [SerializeField] float curveDurationForJumpDown = 0.5f;
    [SerializeField] float delayAfterJumpDown = 0.2f;
    
    public bool jumpTrigger;

    IEnumerator Start()
    {
        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        agent.autoTraverseOffMeshLink = false;
        while (true)
        {
            if (agent.isOnOffMeshLink)
            {
                if (m_Method == OffMeshLinkMoveMethod.NormalSpeed)
                    yield return StartCoroutine(NormalSpeed(agent));
                else if (m_Method == OffMeshLinkMoveMethod.Parabola)
                    yield return StartCoroutine(Parabola(agent, 2.0f, 0.5f));
                else if (m_Method == OffMeshLinkMoveMethod.Curve)
                    yield return StartCoroutine(Curve(agent, 0.5f));
                else if (m_Method == OffMeshLinkMoveMethod.ConfigurableCurve)
                    yield return StartCoroutine(ConfigurableCurve(agent));
                agent.CompleteOffMeshLink();
            }
            yield return null;
        }
    }

    IEnumerator NormalSpeed(NavMeshAgent agent)
    {
        OffMeshLinkData data = agent.currentOffMeshLinkData;
        Vector3 endPos = data.endPos + Vector3.up * agent.baseOffset;
        while (agent.transform.position != endPos)
        {
            agent.transform.position = Vector3.MoveTowards(agent.transform.position, endPos, agent.speed * Time.deltaTime);
            yield return null;
        }
    }

    IEnumerator Parabola(NavMeshAgent agent, float height, float duration)
    {
        OffMeshLinkData data = agent.currentOffMeshLinkData;
        Vector3 startPos = agent.transform.position;
        Vector3 endPos = data.endPos + Vector3.up * agent.baseOffset;
        float normalizedTime = 0.0f;
        while (normalizedTime < 1.0f)
        {
            float yOffset = height * 4.0f * (normalizedTime - normalizedTime * normalizedTime);
            agent.transform.position = Vector3.Lerp(startPos, endPos, normalizedTime) + yOffset * Vector3.up;
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }
    }

    IEnumerator Curve(NavMeshAgent agent, float duration)
    {
        OffMeshLinkData data = agent.currentOffMeshLinkData;
        Vector3 startPos = agent.transform.position;
        Vector3 endPos = data.endPos + Vector3.up * agent.baseOffset;
        float normalizedTime = 0.0f;
        while (normalizedTime < 1.0f)
        {
            float yOffset = m_Curve.Evaluate(normalizedTime);
            agent.transform.position = Vector3.Lerp(startPos, endPos, normalizedTime) + yOffset * Vector3.up;
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }
    }

    IEnumerator ConfigurableCurve(NavMeshAgent agent)
    {
        var spineAnimator = GetComponent<FSpineAnimator>();
        spineAnimator.SpineAnimatorAmount = 0;
        yield return Aim(agent);
        jumpTrigger = true;
        

        
        OffMeshLinkData data = agent.currentOffMeshLinkData;
        Vector3 startPos = agent.transform.position;
        Vector3 endPos = data.endPos + Vector3.up * agent.baseOffset;
        float normalizedTime = 0.0f;

        float delayBefore;
        float duration;
        float delayAfter;

        if (data.endPos.y < data.startPos.y)
        {
            delayBefore = delayForJumpDown;
            duration = curveDurationForJumpDown;
            delayAfter = delayAfterJumpDown;
        }
        else
        {
            delayBefore = delayForJumpUp;
            duration = curveDurationForJumpUp;
            delayAfter = delayAfterJumpUp;
        }

        yield return new WaitForSeconds(delayBefore);

        while (normalizedTime < 1.0f)
        {
            float yOffset = m_Curve.Evaluate(normalizedTime);
            //Debug.Log(m_Curve.Evaluate(normalizedTime));
            agent.transform.position = Vector3.Lerp(startPos, endPos, normalizedTime) + yOffset * Mathf.Abs(endPos.y - startPos.y) * Vector3.up;
            normalizedTime += Time.deltaTime / duration;
            yield return null;
        }
        spineAnimator.SpineAnimatorAmount = 1;
        yield return new WaitForSeconds(delayAfter);
    }

    IEnumerator Aim(NavMeshAgent agent)
    {
        var maxSpeed = GetComponent<CatBrain>().runSpeed;
        Animator cat = GetComponent<Animator>();
        OffMeshLinkData data = agent.currentOffMeshLinkData;
        Vector3 startPos = agent.transform.position;
        Vector3 endPos = data.startPos;
        float normalizedTime = 0.0f;
        while (normalizedTime < 1.0f)
        {
            cat.SetFloat("Speed", maxSpeed * Mathf.Abs(normalizedTime - 1));
            agent.transform.position = Vector3.Lerp(startPos, endPos, normalizedTime);
            agent.transform.rotation = Quaternion.Lerp(agent.transform.rotation, Quaternion.LookRotation(new Vector3(data.endPos.x, agent.transform.position.y, data.endPos.z)  - agent.transform.position), normalizedTime);
            normalizedTime += Time.deltaTime / 0.3f;
            yield return null;
        }
    }
}
