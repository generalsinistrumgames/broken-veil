using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GlobalScripts;

public class PlayerArrivedAlleyCutScene : MonoBehaviour
{
    [Header("Playable Control")]
    [SerializeField] UnityEngine.Playables.PlayableDirector _playableDirector;

    [Header("Player")]
    [SerializeField] Player _player;
    [Space]
    [SerializeField] float _playerUpOffset;
    [Space]
    [SerializeField] GameObject _playerObj;
    [Space]
    [SerializeField] GameObject _dummyPlayerObj;

    public void SetPlayerInPlayable()
    {
        //_playerObj = GameObject.FindGameObjectWithTag("Player");

        _player = FindObjectOfType<Player>();

        if (_player == null)
        {
            Debug.LogError(_playableDirector.name + " Player Not Found Do Something!!!");

            return;
        }
        else
        {
            _playerObj = _player.gameObject;

            _player.GetComponent<PlayerController>().rootMove = true;

            _player.GetComponent<PlayerRenderControl>().SetPlayerRender(false);
        
            //_player.enabled = false;

            _playableDirector.Play();

            Debug.Log(_playableDirector.name + " Playing");
        }
    }

    public void EnablePlayerAfterPlayableEnds()
    {
        _dummyPlayerObj.SetActive(false);

        _player.GetComponent<PlayerController>().rootMove = false;

        //_player.enabled = true;

        _player.transform.position = _player.transform.position + Vector3.up * _playerUpOffset;

        _player.GetComponent<PlayerRenderControl>().SetPlayerRender(true);

        Debug.Log(_playableDirector.name + " Stopped, Player Can Move");
    }
}