﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLeavesOrphanageCutScene : MonoBehaviour
{
    [Header("Colliders")]
    [SerializeField] CapsuleCollider[] _tentCapsuleColliders;
    [Space]
    [SerializeField] string _bonesObjTag;
    [Space]
    [SerializeField] Cloth _tent;

    [Header("Move And Rotate Control")]
    MoveAndRotateTowards _moveAndRotate = new MoveAndRotateTowards();
    [Space]
    [SerializeField] Transform _targetTransform;
    [Space]
    [SerializeField] Quaternion _targetRot;
    [Space]
    [SerializeField] float _moveSpeed;
    [Space]
    [SerializeField] float _rotateSpeed;

    [Header("Playable Control")]
    [SerializeField] KeyCode _activateKeyCode;
    [Space]
    [SerializeField] bool _cutSceneActivated;
    [Space]
    [SerializeField] UnityEngine.Playables.PlayableDirector _playableDirector;

    [Header("Player")]
    [SerializeField] Player _player;
    [Space]
    [SerializeField] Animator _playerAnimator;
    [Space]
    [SerializeField] bool _playerWithinCollider;

    [Header("Dummy Player")]
    [SerializeField] GameObject _dummyPlayer;
    [Space]
    [SerializeField] Animator _dummyPlayerAnimator;

    private void Update()
    {
        if (_playerWithinCollider && !_cutSceneActivated)
        {
            if (!_player.itemInPlayerHand)
            {
                if (Input.GetKeyDown(_activateKeyCode))
                {
                    _cutSceneActivated = true;

                    StartCoroutine(MoveAndRotateRoutine());
                }
            }
        }
    }

    public IEnumerator MoveAndRotateRoutine()
    {
        _player.GetComponent<Rigidbody>().isKinematic = true;

        _player.enabled = false;

        _player.playerController.enabled = false;

        // disable foot ik
        _player.GetComponent<UnityEngine.Animations.Rigging.RigBuilder>().layers[0].rig.weight = 0;

        //DisablePlayerGravity(); // doesnt work

        GetBonesCollider(_player.transform, _bonesObjTag);

        bool targetPosReached = false;

        bool targetRotReached = false;

        Vector3 targetPos = new Vector3(_player.transform.position.x, _targetTransform.position.y, _targetTransform.position.z);

        while (!targetPosReached || !targetRotReached)
        {
            if (!targetPosReached)
            {              
                targetPosReached = _moveAndRotate.MoveToTargetPosition(_player.transform, targetPos, _moveSpeed);
            }

            if (!targetRotReached)
            {
                targetRotReached = _moveAndRotate.RotateToTargetRotation(_player.transform, _targetRot, _rotateSpeed);
            }

            yield return null;

            Debug.Log("Rotate routine");
        }

        _playableDirector.Play();
    }

    public void GetBonesCollider(Transform parent, string tag)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            Transform child = parent.GetChild(i);

            if (child.CompareTag(tag))
            {
                var playerBonesCollider = child.GetComponent<CapsuleCollider>();

                _player.GetComponent<CapsuleCollider>().enabled = false;

                _tentCapsuleColliders[_tentCapsuleColliders.Length - 1] = playerBonesCollider;

                _tent.capsuleColliders = _tentCapsuleColliders;

                playerBonesCollider.enabled = true;
            }

            if (child.childCount > 0)
            {
                GetBonesCollider(child, tag);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            _playerWithinCollider = true;

            if (_player == null)
            {
                _player = player;

                _playerAnimator = player.GetComponent<Animator>();

                PlayableDirectorUtility.DynamicAnimatorBinding(_playableDirector, _playerAnimator, "Player");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<Player>(out var player))
        {
            _playerWithinCollider = false;
        }
    }

    public void SetDummyPlayerInPlayable()
    {
        _player.GetComponent<PlayerRenderControl>().SetPlayerRender(false);

        //PlayableDirectorUtility.DynamicAnimatorBinding(_playableDirector, _dummyPlayerAnimator, "Player");

        //_dummyPlayer.gameObject.SetActive(true);
    }

    public void FreezeCamera()
    {
        CW2_Main cameraControl = FindObjectOfType<CW2_Main>();

        cameraControl.work = false;
    }
}