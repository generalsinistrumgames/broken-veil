using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pedofile : MonoBehaviour
{
    [Header("Pedofile")]
    [SerializeField] Animator _pedofileAnimator;

    [Header("Knock Repeat")]
    public bool _isDetected;
    [Space]
    [SerializeField] float _knockRepeatTimer;
    [Space]
    [SerializeField] float _knockRepeatDefaultRate;

    [Header("Knock Anim")]
    [SerializeField] string _knockAnimTriggerName;
    [Space]
    [SerializeField] int _knockAnimHashValue;

    [Header("Sound Event")]
    [SerializeField] string _knockSoundEvent;

    private void Start()
    {
        _knockAnimHashValue = Animator.StringToHash(_knockAnimTriggerName);

        _pedofileAnimator = GetComponent<Animator>();

        _knockRepeatTimer = _knockRepeatDefaultRate;
    }

    void Update()
    {
        if (!_isDetected)
        {
            if (_knockRepeatTimer > 0)
            {
                _knockRepeatTimer -= Time.deltaTime;
            }
            else
            {
                PlayKnockAnim();

                _knockRepeatTimer = _knockRepeatDefaultRate;
            }
        }
    }

    void PlayKnockAnim()
    {
        _pedofileAnimator.SetTrigger(_knockAnimHashValue);
    }

    void PlayKnockSound()
    {
        if (_knockSoundEvent != null)
        {
            AkSoundEngine.PostEvent(_knockSoundEvent, gameObject);
        }
    }

    public void ScreamerActivated()
    {
        _isDetected = true;

        gameObject.SetActive(true);
    }
}