using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PedofileScreamer : MonoBehaviour
{
    [Header("Pedofile")]
    [SerializeField] GameObject _pedofile;
    [Space]
    [SerializeField] Animator _pedofileAnimator;
    [Space]
    [SerializeField] float disappearTime;

    [Header("Screamer Control")]
    [SerializeField] BoolVariable _screamerActivated;

    [Header("Light Control")]
    [SerializeField] Animation _lightAnim;
    [Space]
    [SerializeField] GameObject _blinkLight;
    [Space]
    [SerializeField] GameObject _brightLight;

    [Header("Sound Event")]
    [SerializeField] AK.Wwise.Event _bulbSoundEvent;

    private string saveFileName = "/pedofileScreamer.json";

    public string localSavesDirectory = "/local_saves";

    private void Start()
    {     
        if (!SaveAndLoadManager.Instance.IsSaveFileExists(SaveAndLoadManager.Instance.localSavesDirectory))
        {       
            _screamerActivated.Value = false;
        }

        SaveAndLoadManager.Instance.Load(saveFileName, _screamerActivated, localSavesDirectory);

        gameObject.SetActive(_screamerActivated.Value ? false : true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!_screamerActivated.Value)
        {
            if (other.TryGetComponent<Player>(out var playerScript))
            {
                _screamerActivated.Value = true;

                SaveAndLoadManager.Instance.Save(saveFileName, _screamerActivated, localSavesDirectory);

                _lightAnim.Play();
            }
        }
    }

    //public IEnumerator PedofileRoutine()
    //{
    //    //_brightLight.SetActive(true);

    //    yield return new WaitForSeconds(disappearTime);

    //    _lightAnim.Play();
    //}

    public void PedofileDisappears()
    {
        _bulbSoundEvent.Post(gameObject);

        _pedofile.SetActive(false);

        this.enabled = false;

        //gameObject.SetActive(false);
    }

    //private void OnDisable()
    //{
    //    _screamerActivated.Value = false;
    //}
}