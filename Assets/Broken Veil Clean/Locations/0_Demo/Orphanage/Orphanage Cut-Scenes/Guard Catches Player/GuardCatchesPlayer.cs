using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

public class GuardCatchesPlayer : MonoBehaviour
{
    [Header("Playable")]
    [SerializeField] PlayableDirector playableDirector;
    [Space]
    [SerializeField] GameObject _cutSceneRoot;

    [Header("Game Over Trigger")]
    [SerializeField] float _gameOverDelay;
    [Space]
    [SerializeField] FloatVariable _gameOverSO;
    [Space]
    [SerializeField] UnityEvent OnGameOver;

    public void PlayPlayableDirector()
    {
        _cutSceneRoot.SetActive(true);

        playableDirector.Play();
    }

    public void TriggerGameOver()
    {
        _gameOverSO.Value = _gameOverDelay;

        OnGameOver?.Invoke();
    }
}