﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class SecurityCartControl : MonoBehaviour
{
    [SerializeField] CinemachineDollyCart securityCart;

    private void Start()
    {
        securityCart = GetComponent<CinemachineDollyCart>();
    }

    public void EnableCart()
    {
        securityCart.enabled = true;
    }

    public void DisableCart()
    {
        securityCart.enabled = false;
    }
}