using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimSync : MonoBehaviour
{
    [SerializeField] public Transform moveRoot;
    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        moveRoot = transform.parent.GetChild(0);
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetFloat("Blend", moveRoot.localPosition.y);
    }
}
