Event	ID	Name			Wwise Object Path	Notes
	299121437	Play_corridor_heartbeat			\Default Work Unit\Orphahage SFX\New\loop\Play_corridor_heartbeat	
	349318818	Play_player_step_wood			\Default Work Unit\Orphahage SFX\New\sfx\Play_player_step_wood	
	373925961	Play_playground_scoop_dig			\Default Work Unit\Orphahage SFX\New\sfx\Play_playground_scoop_dig	
	409383287	Stop_game_room_tv_on_without_remote			\Default Work Unit\Orphahage SFX\New\loop\Stop_game_room_tv_on_without_remote	
	454444414	Play_car_door_open			\Default Work Unit\Orphahage SFX\New\sfx\Play_car_door_open	
	545598759	Play_uaz_away			\Default Work Unit\Orphahage SFX\New\sfx\Play_uaz_away	
	610560337	Play_game_room_tv_on			\Default Work Unit\Orphahage SFX\New\loop\Play_game_room_tv_on	
	799860863	Play_playground_swing_idle			\Default Work Unit\Orphahage SFX\New\loop\Play_playground_swing_idle	
	846599083	Stop_game_room_noise			\Default Work Unit\Orphahage SFX\New\loop\Stop_game_room_noise	
	924628379	Play_checkpoint_security_chair			\Default Work Unit\Orphahage SFX\New\sfx\Play_checkpoint_security_chair	
	965041074	Play_shadows_monster			\Default Work Unit\Orphahage SFX\New\loop\Play_shadows_monster	
	1052983729	Play_playground_ball_hit			\Default Work Unit\Orphahage SFX\New\sfx\Play_playground_ball_hit	
	1152693463	Play_checkpoint_stop_vo			\Default Work Unit\Orphahage SFX\New\voices\Play_checkpoint_stop_vo	
	1164273195	Play_checkpoint_security_standup_vo			\Default Work Unit\Orphahage SFX\New\voices\Play_checkpoint_security_standup_vo	
	1167353446	Stop_amb_main_menu			\Default Work Unit\Orphahage SFX\New\loop\Stop_amb_main_menu	
	1174105686	Play_amb_gaz452_pullaway			\Default Work Unit\Orphahage SFX\Old\Play_amb_gaz452_pullaway	
	1243961392	Play_pantry_nails_drop			\Default Work Unit\Orphahage SFX\New\sfx\Play_pantry_nails_drop	
	1250516584	Stop_shadows_monster			\Default Work Unit\Orphahage SFX\New\loop\Stop_shadows_monster	
	1365536687	Play_sfx_door_wood_close			\Default Work Unit\Orphahage SFX\Old\Play_sfx_door_wood_close	
	1382448533	Play_game_room_tv_work			\Default Work Unit\Orphahage SFX\New\loop\Play_game_room_tv_work	
	1413142245	Play_game_room_clock			\Default Work Unit\Orphahage SFX\New\loop\Play_game_room_clock	
	1452344984	Play_foley_security_steps			\Default Work Unit\Orphahage SFX\New\sfx\Play_foley_security_steps	
	1492649371	Stop_playground_scoop_dig			\Default Work Unit\Orphahage SFX\New\sfx\Stop_playground_scoop_dig	
	1542278216	Play_sfx_thunder			\Default Work Unit\Orphahage SFX\New\sfx\Play_sfx_thunder	
	1564781196	Play_amb_footstep_woman			\Default Work Unit\Orphahage SFX\Old\Play_amb_footstep_woman	
	1569104630	Play_yard_truck_work			\Default Work Unit\Orphahage SFX\New\loop\Play_yard_truck_work	
	1623107714	Stop_sfx_wind			\Default Work Unit\Orphahage SFX\New\loop\Stop_sfx_wind	
	1656208626	Stop_sfx_rain			\Default Work Unit\Orphahage SFX\New\loop\Stop_sfx_rain	
	1728716780	Play_pantry_box			\Default Work Unit\Orphahage SFX\New\sfx\Play_pantry_box	
	1738890490	Play_car_door_close			\Default Work Unit\Orphahage SFX\New\sfx\Play_car_door_close	
	1750416541	Play_sfx_door_metal2_close			\Default Work Unit\Orphahage SFX\Old\Play_sfx_door_metal2_close	
	1763375706	Play_game_room_yule			\Default Work Unit\Orphahage SFX\New\sfx\Play_game_room_yule	
	1817441765	Play_game_room_nanny_standup_vo			\Default Work Unit\Orphahage SFX\New\voices\Play_game_room_nanny_standup_vo	
	1827226138	Play_sfx_monster_pigmale			\Default Work Unit\Orphahage SFX\New\sfx\Play_sfx_monster_pigmale	
	1834137327	Play_game_room_toy			\Default Work Unit\Orphahage SFX\New\sfx\Play_game_room_toy	
	1840366487	Play_sfx_tv_switch			\Default Work Unit\Orphahage SFX\Old\Play_sfx_tv_switch	
	1918662866	Stop_yard_fireflies			\Default Work Unit\Orphahage SFX\New\loop\Stop_yard_fireflies	
	1947009975	Play_sfx_door_metal2_open			\Default Work Unit\Orphahage SFX\Old\Play_sfx_door_metal2_open	
	1964631308	Play_sfx_door_metal3_close			\Default Work Unit\Orphahage SFX\Old\Play_sfx_door_metal3_close	
	2080667080	Play_amb_main_menu			\Default Work Unit\Orphahage SFX\New\loop\Play_amb_main_menu	
	2134005970	Paly_playground_scoop_drop			\Default Work Unit\Orphahage SFX\New\sfx\Paly_playground_scoop_drop	
	2201060256	Stop_yard_truck_work			\Default Work Unit\Orphahage SFX\New\loop\Stop_yard_truck_work	
	2203697449	Play_game_room_tv_on_without_remote			\Default Work Unit\Orphahage SFX\New\loop\Play_game_room_tv_on_without_remote	
	2208779140	Play_sfx_glass_hit			\Default Work Unit\Orphahage SFX\New\sfx\Play_sfx_glass_hit	
	2225847978	Play_bedroom_knock_glass			\Default Work Unit\Orphahage SFX\New\sfx\Play_bedroom_knock_glass	
	2229926534	Play_foley_sec_chair_down			\Default Work Unit\Orphahage SFX\Old\Play_foley_sec_chair_down	
	2239885562	Play_sfx_door_metal1_open			\Default Work Unit\Orphahage SFX\Old\Play_sfx_door_metal1_open	
	2246830232	Play_sfx_door_metal3_open			\Default Work Unit\Orphahage SFX\Old\Play_sfx_door_metal3_open	
	2250502253	Play_game_room_nanny_standup_sfx			\Default Work Unit\Orphahage SFX\New\sfx\Play_game_room_nanny_standup_sfx	
	2310188845	Play_foley_sec_chair_move			\Default Work Unit\Orphahage SFX\Old\Play_foley_sec_chair_move	
	2363710316	Play_yard_fireflies			\Default Work Unit\Orphahage SFX\New\loop\Play_yard_fireflies	
	2443797598	Play_sfx_door_metal1_close			\Default Work Unit\Orphahage SFX\Old\Play_sfx_door_metal1_close	
	2544215095	Play_playground_scales_move			\Default Work Unit\Orphahage SFX\New\sfx\Play_playground_scales_move	
	2551213633	Stop_yard_bush			\Default Work Unit\Orphahage SFX\New\sfx\Stop_yard_bush	
	2613909726	Play_game_room_nanny_sitdown			\Default Work Unit\Orphahage SFX\New\sfx\Play_game_room_nanny_sitdown	
	2616006433	Stop_sfx_curtains			\Default Work Unit\Orphahage SFX\New\loop\Stop_sfx_curtains	
	2735855485	Play_game_room_tv_off			\Default Work Unit\Orphahage SFX\New\loop\Play_game_room_tv_off	
	2893112652	Play_game_room_horse			\Default Work Unit\Orphahage SFX\New\sfx\Play_game_room_horse	
	2959133501	Play_game_room_wood_train			\Default Work Unit\Orphahage SFX\New\sfx\Play_game_room_wood_train	
	2965629745	Stop_playground_scales_move			\Default Work Unit\Orphahage SFX\New\sfx\Stop_playground_scales_move	
	2967300694	Play_sfx_rats			\Default Work Unit\Orphahage SFX\New\sfx\Play_sfx_rats	
	2972306722	Play_player_steps_wet_concrete			\Default Work Unit\Orphahage SFX\New\sfx\Play_player_steps_wet_concrete	
	3005305911	Play_game_room_nanny_steps_vo			\Default Work Unit\Orphahage SFX\New\voices\Play_game_room_nanny_steps_vo	
	3094117967	Stop_game_room_tv_work			\Default Work Unit\Orphahage SFX\New\loop\Stop_game_room_tv_work	
	3108062259	Play_female_steps_heel			\Default Work Unit\Orphahage SFX\New\sfx\Play_female_steps_heel	
	3174799661	Stop_checkpoint_security_idle_smokes			\Default Work Unit\Orphahage SFX\New\voices\Stop_checkpoint_security_idle_smokes	
	3192791034	Play_pantry_doll			\Default Work Unit\Orphahage SFX\New\sfx\Play_pantry_doll	
	3215255436	Play_amb_gaz3307_pullaway			\Default Work Unit\Orphahage SFX\Old\Play_amb_gaz3307_pullaway	
	3244725865	Play_sfx_door_wood_open			\Default Work Unit\Orphahage SFX\Old\Play_sfx_door_wood_open	
	3281634778	Play_game_room_duck			\Default Work Unit\Orphahage SFX\New\sfx\Play_game_room_duck	
	3286075488	Play_sfx_rain			\Default Work Unit\Orphahage SFX\New\loop\Play_sfx_rain	
	3289425441	Play_sfx_water_box			\Default Work Unit\Orphahage SFX\New\sfx\Play_sfx_water_box	
	3330427648	Play_sfx_wind			\Default Work Unit\Orphahage SFX\New\loop\Play_sfx_wind	
	3356813298	Play_game_room_nanny_standup_grumble_vo			\Default Work Unit\Orphahage SFX\New\voices\Play_game_room_nanny_standup_grumble_vo	
	3362331119	Stop_corridor_heartbeat			\Default Work Unit\Orphahage SFX\New\loop\Stop_corridor_heartbeat	
	3461046120	Stop_sfx_water_stream			\Default Work Unit\Orphahage SFX\New\loop\Stop_sfx_water_stream	
	3477598913	Stop_playground_swing_idle			\Default Work Unit\Orphahage SFX\New\loop\Stop_playground_swing_idle	
	3524390135	Play_game_room_nanny_steps			\Default Work Unit\Orphahage SFX\New\sfx\Play_game_room_nanny_steps	
	3564287711	Play_yard_bush			\Default Work Unit\Orphahage SFX\New\sfx\Play_yard_bush	
	3617582813	Play_checkpoint_security_idle_cough			\Default Work Unit\Orphahage SFX\New\voices\Play_checkpoint_security_idle_cough	
	3620745040	Play_game_room_nanny_stop_vo			\Default Work Unit\Orphahage SFX\New\voices\Play_game_room_nanny_stop_vo	
	3662092463	Play_sfx_curtains			\Default Work Unit\Orphahage SFX\New\loop\Play_sfx_curtains	
	3679631501	Play_game_room_noise			\Default Work Unit\Orphahage SFX\New\loop\Play_game_room_noise	
	3685972909	Play_foley_sec_chair_up			\Default Work Unit\Orphahage SFX\Old\Play_foley_sec_chair_up	
	3756995976	Play_pantry_nails_land			\Default Work Unit\Orphahage SFX\New\sfx\Play_pantry_nails_land	
	3765196655	Stop_game_room_clock			\Default Work Unit\Orphahage SFX\New\loop\Stop_game_room_clock	
	3805075634	Stop_sfx_cart			\Default Work Unit\Orphahage SFX\New\sfx\Stop_sfx_cart	
	3906424190	Play_checkpoint_door_open			\Default Work Unit\Orphahage SFX\New\sfx\Play_checkpoint_door_open	
	3941961359	Play_checkpoint_security_idle_smokes			\Default Work Unit\Orphahage SFX\New\voices\Play_checkpoint_security_idle_smokes	
	3982920712	Play_sfx_cart			\Default Work Unit\Orphahage SFX\New\sfx\Play_sfx_cart	
	3985813554	Play_sfx_water_stream			\Default Work Unit\Orphahage SFX\New\loop\Play_sfx_water_stream	
	4057751920	Play_game_room_nanny_standup_grumble_sfx			\Default Work Unit\Orphahage SFX\New\sfx\Play_game_room_nanny_standup_grumble_sfx	
	4065046125	Play_playground_scoop_get			\Default Work Unit\Orphahage SFX\New\sfx\Play_playground_scoop_get	
	4076029807	Play_game_room_pyramid			\Default Work Unit\Orphahage SFX\New\sfx\Play_game_room_pyramid	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	1018703	amb_gaz3307_pullaway	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\amb_gaz3307_pullaway_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\amb_gaz3307_pullaway		982710
	2775650	checkpoint_security_standup_vo	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_standup_vo_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\voices\checkpoint_security_standup_vo		120998
	13482345	game room_duck_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_duck_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_duck\game room_duck_3		41159
	38735030	checkpoint_security_idle_cough_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_idle_cough_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\voices\checkpoint_security_idle_cough\checkpoint_security_idle_cough_2		139102
	55019716	game room_pyramid_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_pyramid_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_pyramid\game room_pyramid_3		54135
	75742964	game room_nanny_standup_grumble_vo	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_standup_grumble_vo_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\voices\game room_nanny_standup_grumble_vo		280368
	78356570	playground_scales_move_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\playground_scales_move_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\playground_scales_move\playground_scales_move_3		130339
	97646181	female_steps_heel_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\female_steps_heel_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\female_steps_heel\female_steps_heel_2		18569
	100061160	sfx_door_wood_open	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_door_wood_open_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_door_wood_open		43670
	106064090	playground_scoop_dig_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\playground_scoop_dig_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\playground_scoop_dig\playground_scoop_dig_2		51205
	106648765	yard_bush_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\yard_bush_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\yard_bush\yard_bush_2		98151
	118469504	sfx_thunder	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_thunder_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_thunder		322006
	133348287	checkpoint_security_steps_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_steps_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_security_steps\foley_security_step_l\checkpoint_security_steps_1		22510
	139125083	pantry_box_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\pantry_box_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\pantry_box\pantry_box_3		43298
	140450372	game room_noise	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_noise_073B4D0C.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\game room_noise		315722
	143840843	checkpoint_security_steps_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_steps_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_security_steps\foley_security_step_l\checkpoint_security_steps_3		22314
	154023388	sfx_monster_pigmale	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_monster_pigmale_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_monster_pigmale		193157
	160099011	game room_nanny_standup	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_standup_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_nanny_standup		102486
	161979354	sfx_door_metal2_close	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_door_metal2_close_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_door_metal2_close		52440
	163263510	game room_nanny_sitdown	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_sitdown_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_nanny_sitdown		95267
	169963699	game room_clock	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_clock_073B4D0C.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\game room_clock		63866
	169963754	game room_nanny_steps_vo_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_steps_vo_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\voices\game room_nanny_steps_vo\game room_nanny_steps_vo_2		178231
	170422156	sfx_door_metal1_close	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_door_metal1_close_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_door_metal1_close		56475
	184653489	sfx_door_wood_close	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_door_wood_close_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_door_wood_close		40698
	187698798	game room_nanny_steps_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_steps_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_granny_step\foley_granny_step_l\game room_nanny_steps_1		29675
	232861830	yard_truck_work	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\yard_truck_work_073B4D0C.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\yard_truck_work		265120
	240917175	female_steps_heel_7	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\female_steps_heel_7_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\female_steps_heel\female_steps_heel_7		17417
	242544303	playground_ball_hit_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\playground_ball_hit_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\playground_ball_hits\playground_ball_hit_3		19880
	252183036	checkpoint_security_idle_smoke_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_idle_smoke_2_073B4D0C.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\voices\checkpoint_security_idle_smokes\checkpoint_security_idle_smoke_2		228377
	252697092	female_steps_heel_4	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\female_steps_heel_4_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\female_steps_heel\female_steps_heel_4		14768
	255094810	female_steps_heel_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\female_steps_heel_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\female_steps_heel\female_steps_heel_1		20504
	263449106	playground_scoop_drop_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\playground_scoop_drop_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\playground_scoop_drop\playground_scoop_drop_1		43835
	265624713	checkpoint_security_steps_8	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_steps_8_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_security_steps\foley_security_step_r\checkpoint_security_steps_8		17975
	268740135	car_door_open	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\car_door_open_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\car_door_open		69310
	276281006	playground_ball_hit_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\playground_ball_hit_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\playground_ball_hits\playground_ball_hit_1		25644
	296204560	checkpoint_door_open	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_door_open_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\checkpoint_door_open		92763
	299719440	game room_nanny_steps_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_steps_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_granny_step\foley_granny_step_l\game room_nanny_steps_3		27491
	319764795	playground_scoop_get	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\playground_scoop_get_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\playground_scoop_get		56887
	333333522	foley_sec_chair_move	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\foley_sec_chair_move_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_sec_chair_move		46696
	343885137	game room_horse_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_horse_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_horse\game room_horse_2		58161
	346850578	yard_fireflies	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\yard_fireflies_073B4D0C.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\yard_fireflies		589793
	348483012	game room_yule_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_yule_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_yule\game room_yule_1		76884
	352087635	game room_nanny_steps_7	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_steps_7_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_granny_step\foley_granny_step_r\game room_nanny_steps_7		25489
	353643990	checkpoint_security_chair	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_chair_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\checkpoint_security_chair		65244
	355986786	checkpoint_security_steps_7	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_steps_7_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_security_steps\foley_security_step_r\checkpoint_security_steps_7		19720
	360094241	game room_toy_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_toy_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_toy\game room_toy_1		125283
	367451031	playground_ball_hit_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\playground_ball_hit_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\playground_ball_hits\playground_ball_hit_2		21407
	369050511	checkpoint_security_idle_cough_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_idle_cough_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\voices\checkpoint_security_idle_cough\checkpoint_security_idle_cough_3		149318
	376617091	playground_scales_move_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\playground_scales_move_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\playground_scales_move\playground_scales_move_1		83047
	381580421	sfx_door_metal1_open	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_door_metal1_open_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_door_metal1_open		35333
	390414235	checkpoint_security_steps_9	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_steps_9_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_security_steps\foley_security_step_r\checkpoint_security_steps_9		18834
	391011247	game room_pyramid_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_pyramid_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_pyramid\game room_pyramid_1		66510
	391863409	pantry_box_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\pantry_box_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\pantry_box\pantry_box_1		64431
	397763655	playground_scoop_drop_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\playground_scoop_drop_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\playground_scoop_drop\playground_scoop_drop_2		49582
	408324973	game room_toy_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_toy_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_toy\game room_toy_2		145897
	416011119	pantry_nails_land	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\pantry_nails_land_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\pantry_nails_land		93795
	458882545	game room_nanny_stop_vo	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_stop_vo_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\voices\game room_nanny_stop_vo		221851
	470066569	game room_tv_on	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_tv_on_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_tv_on		49593
	470133305	game room_tv_work	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_tv_work_073B4D0C.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\game room_tv_work		1493588
	480233562	checkpoint_security_steps_4	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_steps_4_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_security_steps\foley_security_step_l\checkpoint_security_steps_4		21946
	488512983	game room_wood_train_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_wood_train_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_wood_train\game room_wood_train_1		51595
	489249404	game room_duck_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_duck_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_duck\game room_duck_1		64386
	494590235	pantry_nails_drop	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\pantry_nails_drop_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\pantry_nails_drop		46641
	500857998	game room_pyramid_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_pyramid_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_pyramid\game room_pyramid_2		61482
	501103804	sfx_door_metal3_close	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_door_metal3_close_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_door_metal3_close		40961
	520555265	sfx_rats	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_rats_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_rats		178694
	537871227	game room_tv_off	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_tv_off_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_tv_off		61547
	538881836	checkpoint_security_idle_cough_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_idle_cough_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\voices\checkpoint_security_idle_cough\checkpoint_security_idle_cough_1		151834
	552143333	uaz_away	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\uaz_away_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\uaz_away		245931
	554775505	female_steps_heel_9	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\female_steps_heel_9_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\female_steps_heel\female_steps_heel_9		14552
	583645844	game room_nanny_standup_grumble_sfx	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_standup_grumble_sfx_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_nanny_standup_grumble_sfx		105866
	599443280	game room_nanny_steps_vo_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_steps_vo_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\voices\game room_nanny_steps_vo\game room_nanny_steps_vo_1		127870
	618127666	female_steps_heel_6	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\female_steps_heel_6_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\female_steps_heel\female_steps_heel_6		15019
	622911303	checkpoint_security_steps_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_steps_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_security_steps\foley_security_step_l\checkpoint_security_steps_2		16877
	626421923	game room_duck_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_duck_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_duck\game room_duck_2		45123
	630449626	pantry_doll_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\pantry_doll_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\pantry_doll\pantry_doll_3		33230
	631771347	game room_nanny_standup_vo	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_standup_vo_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\voices\game room_nanny_standup_vo		137492
	633414918	game room_wood_train_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_wood_train_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_wood_train\game room_wood_train_2		43787
	639090945	sfx_door_metal3_open	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_door_metal3_open_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_door_metal3_open		29645
	640858616	sfx_curtains	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_curtains_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\sfx_curtains		754626
	641184275	shadows_monster	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\shadows_monster_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\shadows_monster		1284122
	654069494	sfx_wind	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_wind_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\sfx_wind		1186552
	677377382	sfx_glass_hit	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_glass_hit_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_glass_hit		144278
	679934756	game room_yule_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_yule_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_yule\game room_yule_2		65367
	704529709	sfx_rain	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_rain_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\sfx_rain		1879936
	717709566	yard_bush_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\yard_bush_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\yard_bush\yard_bush_3		126193
	722194443	game room_horse_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_horse_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_horse\game room_horse_1		61489
	740306857	foley_sec_chair_down	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\foley_sec_chair_down_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_sec_chair_down		66571
	756259588	game room_nanny_steps_4	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_steps_4_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_granny_step\foley_granny_step_l\game room_nanny_steps_4		28395
	760041840	female_steps_heel_8	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\female_steps_heel_8_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\female_steps_heel\female_steps_heel_8		16916
	762114291	sfx_cart	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_cart_073B4D0C.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\sfx_cart		346632
	767963422	yard_bush_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\yard_bush_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\yard_bush\yard_bush_1		92711
	769325683	female_steps_heel_5	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\female_steps_heel_5_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\female_steps_heel\female_steps_heel_5		17144
	778183943	checkpoint_security_idle_smoke_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_idle_smoke_1_073B4D0C.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\voices\checkpoint_security_idle_smokes\checkpoint_security_idle_smoke_1		235534
	781288021	sfx_tv_switch	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_tv_switch_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_tv_switch		19457
	796687358	game room_nanny_steps_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_steps_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_granny_step\foley_granny_step_l\game room_nanny_steps_2		28961
	805676605	game room_nanny_steps_9	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_steps_9_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_granny_step\foley_granny_step_r\game room_nanny_steps_9		36526
	815011499	amb_main_menu	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\Главная тема на уровень_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\2D\loop\amb_main_menu		9023992
	825458439	corridor_heartbeat	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\corridor_heartbeat_073B4D0C.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\2D\loop\corridor_heartbeat		158379
	826307402	car_door_close	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\car_door_close_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\car_door_close		88920
	826968015	pantry_box_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\pantry_box_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\pantry_box\pantry_box_2		49523
	862272612	game room_yule_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_yule_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_yule\game room_yule_3		71191
	865007362	game room_nanny_steps_5	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_steps_5_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_granny_step\foley_granny_step_l\game room_nanny_steps_5		26151
	875584619	female_steps_heel_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\female_steps_heel_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\female_steps_heel\female_steps_heel_3		16847
	888747309	foley_sec_chair_up	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\foley_sec_chair_up_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_sec_chair_up		28411
	893497185	game room_wood_train_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_wood_train_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\game room_wood_train\game room_wood_train_3		60799
	901138298	pantry_doll_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\pantry_doll_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\pantry_doll\pantry_doll_2		30412
	902769034	checkpoint_security_steps_6	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_steps_6_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_security_steps\foley_security_step_r\checkpoint_security_steps_6		18130
	915992877	checkpoint_stop_vo	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_stop_vo_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\voices\checkpoint_stop_vo		228583
	927789526	sfx_door_metal2_open	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_door_metal2_open_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_door_metal2_open		44139
	945953103	pantry_doll_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\pantry_doll_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\pantry_doll\pantry_doll_1		35090
	951551331	amb_footstep_woman	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\amb_footstep_woman_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\2D\oneshot\amb_footstep_woman		977616
	958269102	game room_nanny_steps_6	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_steps_6_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_granny_step\foley_granny_step_r\game room_nanny_steps_6		23759
	967839666	checkpoint_security_steps_5	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\checkpoint_security_steps_5_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_security_steps\foley_security_step_l\checkpoint_security_steps_5		21678
	969410497	playground_scoop_dig_1	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\playground_scoop_dig_1_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\playground_scoop_dig\playground_scoop_dig_1		53911
	983549517	sfx_water_stream	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_water_stream_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\sfx_water_stream		978865
	996598328	amb_gaz452_pullaway	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\amb_gaz452_pullaway_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\amb_gaz452_pullaway		1101331
	998222649	playground_scoop_dig_3	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\playground_scoop_dig_3_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\loop\playground_scoop_dig\playground_scoop_dig_3		59974
	1016148759	sfx_water_box	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\sfx_water_box_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\sfx_water_box		176413
	1021057941	playground_scales_move_2	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\playground_scales_move_2_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\playground_scales_move\playground_scales_move_2		102424
	1035480114	game room_nanny_steps_8	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\game room_nanny_steps_8_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\foley_granny_step\foley_granny_step_r\game room_nanny_steps_8		35505
	1069876130	bedroom_knock_glass	C:\Users\indie\Broken Veil\Wwise\Wwise Project\.cache\Windows\SFX\bedroom_knock_glass_755F56E2.wem		\Actor-Mixer Hierarchy\Default Work Unit\main\AMB\3D\oneshot\bedroom_knock_glass		148358

